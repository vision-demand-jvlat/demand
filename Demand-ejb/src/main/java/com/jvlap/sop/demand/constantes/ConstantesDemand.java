/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.constantes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Roberto Osorio
 */
public class ConstantesDemand {

	private ConstantesDemand() {
		throw new IllegalStateException("Utility class");
	}

	// Cantidad de meses de calculos en forecast
	public static final int CANTIDAD_MESE = 12; // Debe ser 18

	// Cantidad de meses que se deben mostrar en pantalla para los combos
	public static final int CANTIDAD_MESE_COMBO = 18; // Debe ser 18

	// Codigos de ESTADO_FORECAST
	public static final String EST_FORECAST_INI = "INIC"; // Iniciado, Cuando lo crea el KAM o lo modifica sin cerrar
	public static final String EST_FORECAST_CERR = "CERR"; // Cerrado, Cuando el Kam lo envio al Gerente, no lo puede
															// editar el KAM
	public static final String EST_FORECAST_CNFG = "CNFG"; // Confirmado por el gerente, Cuando el gerente confirma, no
															// se puede editar
	public static final String EST_FORECAST_CNFD = "CNFD"; // Confirmado por el Director
	public static final String EST_FORECAST_DEVL = "DEVL"; // Devuelto por el Director para el Gerente
	public static final String EST_FORECAST_ELSE = "ELSE"; // Devuelto por el Director para el Gerente
	public static final String EST_FORECAST_DEVL_KAM = "DEVL_KAM"; // Devuelto de forecast compania a forecast retailer

	// Cargos a notificar

	public static final String CARGO_USUARIO_KAM = "KAM"; // Cargo KAM
	public static final String CARGO_USUARIO_BI = "BI"; // Cargo KAM
	public static final String CARGO_USUARIO_GERENTE_PAIS = "GERENTE PAIS"; // Cargo
	public static final String CARGO_USUARIO_GERENCIA_COMERCIAL_HQ = "GERENCIA COMERCIAL HQ"; // Cargo
	public static final String CARGO_USUARIO_SUPPLY = "SUPPLY"; // Cargo

	// Codigos para tabla ORIGEN para Rolling Forecast
	public static final String ID_ORIGEN_FORE = "FORE";
	public static final String ID_ORIGEN_BUDG = "BUDG";

	// Etpas del proceso
	public static final String ETAPA_SEMANAS_REQUERIDAS = "SEMANAS REQUERIDAS";
	public static final String ETAPA_FORECAST_RETAILER = "FORECAST RETAILER";
	public static final String ETAPA_ELEVACION_SELLIN = "ELEVACION SELLIN";
	public static final String ETAPA_FORECAST_COMPANIA = "FORECAST COMPAÑÍA";
	public static final String ETAPA_FORECAST_GENERAL = "FORECAST GENERAL";
	public static final String ETAPA_PSI_COMPANIA = "PLAN DESPACHO";

	// Rol Forecasr Retailer

	public static final String ROL_FORECAST_RETAILER = "FORECAST RETAILER";
	public static final String ROL_FORECAST_COMPANIA = "FORECAST COMPANIA";

	// Lanzamientos Descontinuados
	public static final String FLAG_ESTADO_CURRENT = "C";
	public static final String FLAG_ESTADO_LANZAMIENTO = "L";
	public static final String FLAG_ESTADO_DESCONTINUADO = "D";

	public static final int VALIDA_NEGATIVO = 1;
	public static final int VALIDA_RETAILER_PRODUCTO = 2;
	public static final int VALIDA_UPC_PRODUCTO = 3;

	public static final List<String> COLUMNAS = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H",
			"I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC",
			"AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU",
			"AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM",
			"BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BX", "BY", "BZ"));
}
