
package com.jvlap.sop.demand.logica;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.jvlap.sop.demand.logica.vos.FilasArchivoForecastVO;
import com.jvlat.sop.entidadessop.entidades.Forecast;
import com.jvlat.sop.entidadessop.entidades.ForecastProducto;
import com.jvlat.sop.entidadessop.entidades.MesSop;

@SuppressWarnings("rawtypes")
@Stateless
public class CargaMasivaEJB extends AbstractFacade {

	@PersistenceContext(unitName = "EntidadesSOPPU")
	private EntityManager em;

	@EJB
	ModuloGeneralEJB moduloGeneralEJB;
	static Logger log = LogManager.getLogger(CargaMasivaEJB.class.getName());

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	private int contador = 0;
	private static final int BATCH_SIZE = 1000;
	String mesArchivo = null;
	String anioArchivo = null;
	String compania = "";
	Map<String, Forecast> forecastMap = new LinkedHashMap<>();
	List<String> retailersMarcas = new ArrayList<>();
	List<String> sTextoExtraido = new ArrayList<>();

	/**
	 * Carga archivo csv y empieza proceso de validaciones
	 *
	 * @param buffer
	 * @param listaErrores
	 * @param compania
	 * @param username
	 * 
	 */
	@TransactionTimeout(value = 60, unit = TimeUnit.MINUTES)
	public void cargarArchivo(InputStream buffer, List<String> listaErrores, String compania, String username) {

		/*
		 * String encabezado[] = { "Año", "Mes", "Retailer", "Descripcion Retailer",
		 * "Marca", "Budget Category", "Codigo corto", "SKU(Modelo)", "UPC",
		 * "Descripcion", "Forecast Kam - Units", "", "", "", "", "", "", "" };
		 */
		int longitudRequerida[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		List<FilasArchivoForecastVO> listFilasArchivo = new ArrayList<>();
		FilasArchivoForecastVO filasArchivoVO;
		int parameter = moduloGeneralEJB.consultarParametroMeses();
		forecastMap.clear();
		retailersMarcas.clear();

		try {
			String linea;
			BufferedReader input = new BufferedReader(new InputStreamReader(buffer));

			while ((linea = input.readLine()) != null) {
				filasArchivoVO = new FilasArchivoForecastVO();
				filasArchivoVO.setLinea(linea);
				listFilasArchivo.add(filasArchivoVO);
			}
			input.close();

			if (listFilasArchivo.isEmpty()) {
				log.error("[cargarArchivo] No se encontro informacion para procesar en el archivo");
				listaErrores.add("No se encontró información para procesar en el archivo ");
				return;
			}

			validaFilas(listFilasArchivo, listaErrores, (longitudRequerida.length + parameter), compania, username);

		} catch (Exception e) {
			log.error("[cargarArchivo] Error al cargar archivo ", e);
		}
	}

	/**
	 * Metodo general que llama a las validaciones de las filas
	 *
	 * @param listFilasArchivo
	 * @param listaErrores
	 * @param encabezado
	 * @param columnas
	 * @param compania
	 * @param userName
	 * 
	 */
	private void validaFilas(List<FilasArchivoForecastVO> listFilasArchivo, List<String> listaErrores, int columnas,
			String compania, String userName) {

		String textoLinea;
		List<String> mesesKamExcel = new ArrayList<>();
		List<String> productos = new ArrayList<>();
		List<String> retailers = new ArrayList<>();
		Map<String, Long> mapaMeses = new LinkedHashMap<>();
		this.compania = compania;

		try {
			for (int i = 1; i < listFilasArchivo.size(); i++) {
				textoLinea = listFilasArchivo.get(i).getLinea();
				String[] arrayColumnas = textoLinea.split(";");
				if (arrayColumnas.length == columnas) {
					if (i == 1) {
						try {
							mesesKamExcel = construirListMeses(arrayColumnas);
						} catch (Exception ex) {
							listaErrores.add(ex.getMessage());
						}
					}

					if (i > 2) {

						System.out.println("validaFilas->" + arrayColumnas[6].trim());
						if (!productos.contains(arrayColumnas[6].trim())) {
							productos.add(arrayColumnas[6].trim());
						}

						if (!retailers.contains(arrayColumnas[2].trim())) {
							retailers.add(arrayColumnas[2].trim());
						}

						if (!retailersMarcas.contains(arrayColumnas[2].trim() + "_" + arrayColumnas[4].trim())) {
							retailersMarcas.add(arrayColumnas[2].trim() + "_" + arrayColumnas[4].trim());
						}

						System.out.println("obtenerMesesCabecera->" + arrayColumnas);

						if (i == 3) {
							System.out.println("obtenerMesesCabecera->" + arrayColumnas);
							mapaMeses = obtenerMesesCabecera(arrayColumnas, listaErrores, mesesKamExcel, mapaMeses);
						}

						listFilasArchivo.get(i).setAge(arrayColumnas[0].trim());
						listFilasArchivo.get(i).setMes(arrayColumnas[1].trim());
						listFilasArchivo.get(i).setRetailer(arrayColumnas[2].trim());
						listFilasArchivo.get(i).setMarca(arrayColumnas[4].trim());
						listFilasArchivo.get(i).setCategoria(arrayColumnas[5].trim());
						listFilasArchivo.get(i).setCodigo(arrayColumnas[6].trim());
						listFilasArchivo.get(i).setSku(arrayColumnas[7].trim());
						listFilasArchivo.get(i).setUpc(arrayColumnas[8].trim());
						listFilasArchivo.get(i).setDescripcion(arrayColumnas[9].trim());
						int limit = 10;
						for (Map.Entry<String, Long> entry : mapaMeses.entrySet()) {
							if (limit < arrayColumnas.length) {
								try {
									listFilasArchivo.get(i).agregarMeses(entry.getValue(),
											new BigDecimal(arrayColumnas[limit].trim()));
									limit++;
								} catch (NumberFormatException e) {
									listaErrores.add("No se puede registrar el valor de la linea " + (i + 1)
											+ " el valor no es un tipo de dato valido " + arrayColumnas[limit].trim());
									break;
								}
							}
						}
					}
				}
			}

			List<String> oProdExits = consultaProducto(productos);
			if (oProdExits.isEmpty()) {
				listaErrores.add(
						"La compañia seleccionada no esta relacionada a ningun registro del archivo " + this.compania);
				listaErrores.add("El archivo no tiene información o el delimitador no es el correcto. ");
			} else {
				List<String> oRetExits = consultaRetailer(retailers);
				if (oRetExits.isEmpty()) {
					listaErrores.add("La compañia seleccionada no esta relacionada a ningun registro del archivo "
							+ this.compania);
				} else {
					Long parametroMes = mapaMeses.get(mesArchivo + "_" + anioArchivo);
					validaEstadoForecast(listaErrores, parametroMes);
					Map<String, BigDecimal> oPreciosRetailer = consultaPrecioRetailer(oRetExits, oProdExits,
							parametroMes);

					for (int i = 3; i < listFilasArchivo.size(); i++) {
						textoLinea = listFilasArchivo.get(i).getLinea();
						String[] split = textoLinea.split(";");
						int validacionNegativa = 0;
						if (split.length == columnas) {
							if (oRetExits.contains(listFilasArchivo.get(i).getRetailer())) {
								if (forecastMap.containsKey(listFilasArchivo.get(i).getRetailer() + "_"
										+ listFilasArchivo.get(i).getMarca())) {
									if (oProdExits.contains(listFilasArchivo.get(i).getCodigo())) {
										listFilasArchivo.get(i).setPrecioRetailer(
												oPreciosRetailer.containsKey(listFilasArchivo.get(i).getCodigo().trim()
														+ "_" + listFilasArchivo.get(i).getRetailer().trim())
																? oPreciosRetailer.get(
																		listFilasArchivo.get(i).getCodigo().trim()
																				+ "_"
																				+ listFilasArchivo.get(i).getRetailer()
																						.trim())
																: BigDecimal.ZERO);
										for (Map.Entry<Long, BigDecimal> entry : listFilasArchivo.get(i)
												.getValoresMeses().entrySet()) {
											if (!validaTipoDatoNegativo(entry.getValue().toString(), listaErrores, i))
												validacionNegativa++;
										}
										if (validacionNegativa <= 0) {
											mapearDatosEntidad(listFilasArchivo.get(i), mapaMeses);
										}
									} else {
										listaErrores.add("No se puede registrar el valor de la linea " + (i + 1)
												+ " producto no valido " + listFilasArchivo.get(i).getCodigo());
									}
								} else {
									listaErrores.add("Linea " + (i + 1) + " No existe Forecast para la marca: "
											+ listFilasArchivo.get(i).getMarca() + " Retailer: "
											+ listFilasArchivo.get(i).getRetailer());
								}
							} else {
								listaErrores.add("No se puede registrar el valor de la linea " + (i + 1)
										+ " Retailer no valido " + listFilasArchivo.get(i).getRetailer());
							}
						} else {
							listaErrores.add("Linea " + (i + 1)
									+ " no contiene todas las columnas requeridas: La especificación requiere una cantidad igual a: "
									+ columnas);
						}
					}
					if (!forecastMap.isEmpty())
						actualizarForecast(userName);
				}
			}
		} catch (Exception ex) {
			listaErrores.add("Hubo un error procesando el archivo, por favor verificar el formato del archivo.");
			log.error("[validaFilas] Error procesando el archivo ", ex);
		}
	}

	/**
	 * Obtiene los identificadores del mes de la tabla MesSop dependiendo de la
	 * cambecerda del archivo.
	 *
	 * @param arrayColumnas
	 * @param listaErrores
	 * @param mesesExcel
	 * @param mapaMeses
	 * 
	 */
	private Map<String, Long> obtenerMesesCabecera(String[] arrayColumnas, List<String> listaErrores,
			List<String> mesesExcel, Map<String, Long> mapaMeses) {

		anioArchivo = arrayColumnas[0].trim();
		mesArchivo = arrayColumnas[1].trim().toUpperCase();
		String oMesFinal = "DICIEMBRE";

		if (mesArchivo != null && anioArchivo != null) {
			String oMes = "";
			String oAnio = anioArchivo;
			try {
				MesSop mesSop;
				for (String mesObj : mesesExcel) {
					if (!mapaMeses.containsKey(mesObj + "_" + oAnio)) {
						oMes = mesObj;
						Query query = em.createQuery("SELECT m FROM MesSop m WHERE m.nombre = :mes AND m.anio = :anio");
						query.setParameter("mes", mesObj);
						query.setParameter("anio", Integer.parseInt(oAnio));
						mesSop = (MesSop) query.getSingleResult();
						mapaMeses.put(mesObj + "_" + oAnio, mesSop.getId());
						oAnio = oMesFinal.equals(mesObj) ? "" + (Integer.parseInt(oAnio) + 1) : oAnio;
					}
				}
			} catch (NoResultException e) {
				listaErrores.add("No se encontro información del mes: " + oMes + " para el año: " + oAnio + " "
						+ e.getMessage());
			}
		}
		return mapaMeses;
	}

	/**
	 * Valida que el estado del forecast sea abierto o devuelto
	 *
	 * @param mapaMeses
	 * @param listaErrores
	 * 
	 */
	private void validaEstadoForecast(List<String> listaErrores, Long parametroMes) {

		if (!retailersMarcas.isEmpty()) {
			for (String retailMarca : retailersMarcas) {

				String[] retailerMarcaArray = retailMarca.split("_");

				try {
					Query query = em.createQuery("SELECT fr " + "FROM Forecast fr where fr.retailerFk.id = :retailer \n"
							+ "AND fr.mesFk.id= :mes and (fr.estadoFk.codigo ='INIC' OR fr.estadoFk.codigo ='DEVL_KAM') AND fr.companiaFk.id = :compania AND fr.marcaFk.nombre = :marcaNombre",
							Forecast.class);
					query.setParameter("retailer", retailerMarcaArray[0]);
					query.setParameter("mes", parametroMes);
					query.setParameter("compania", compania);
					query.setParameter("marcaNombre", retailerMarcaArray[1]);

					forecastMap.put(retailMarca, (Forecast) query.getSingleResult());

				} catch (NoResultException e) {
					listaErrores
							.add("No se puede cargar el archivo estado  Forecast es distinto a INICIADO o DEVUELTO");
					String msg = "No se encontro informacion del retailer " + retailerMarcaArray[0] + " para "
							+ parametroMes + "-" + anioArchivo + " Marca " + retailerMarcaArray[1];
					log.error(msg, e);

				} catch (Exception e) {
					listaErrores.add("Error al validar estado forecast Retailer: " + retailerMarcaArray[0] + " Mes "
							+ parametroMes + "  Compania " + compania + " Marca " + retailerMarcaArray[1]);
					String msg = "Error al validar estado forecast Retailer: " + retailerMarcaArray[0] + " Mes "
							+ parametroMes + "  Compania " + compania + " Marca " + retailerMarcaArray[1];
					log.error(msg, e);

				}
			}
		}
	}

	/**
	 * Valida el tipo de dato, si es negativo o no.
	 * 
	 * @param value
	 * @param listaErrores
	 * @param linea
	 * @return
	 */
	public boolean validaTipoDatoNegativo(String value, List<String> listaErrores, int linea) {
		try {
			if (value.contains(".") || value.contains(",")) {
				Double number = Double.parseDouble(value);
				if (number.doubleValue() < 0.0) {
					listaErrores.add("No se puede registrar el valor de la linea " + (linea + 1)
							+ " el valor es negativo " + value);
					return false;
				}
			} else {
				Integer number = Integer.parseInt(value);
				if (number < 0) {
					listaErrores.add("No se puede registrar el valor de la linea " + (linea + 1)
							+ " el valor es negativo " + value);
					return false;
				}
			}
		} catch (NumberFormatException nfe) {
			listaErrores.add(
					"No se puede registrar el valor de la linea " + (linea + 1) + " Valor tipo no valido " + value);
			log.error("[validaTipoDatoNegativo] Error al convertidato ", nfe);
			return false;
		}
		return true;
	}

	/**
	 * Consultar todos los retailer del archivo si estan relacionados con la
	 * compañía seleccionada.
	 * 
	 * @param idRetailer
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> consultaRetailer(List<String> idRetailer) {

		List<String> oRetExits = new ArrayList<>();
		if (!idRetailer.isEmpty()) {
			Query query = em.createNativeQuery(
					"SELECT r.id, r.compania_fk FROM Retailer r WHERE r.id IN :idRetailer and r.compania_fk= :compania");
			query.setParameter("idRetailer", idRetailer);
			query.setParameter("compania", compania);
			List<Object[]> resultRetailer = query.getResultList();

			for (Object[] obj : resultRetailer) {
				if (!oRetExits.contains(obj[0].toString().trim()))
					oRetExits.add(obj[0].toString().trim());
			}
		}

		return oRetExits;
	}

	/**
	 * Consulta si todos los productos del archivo estan relacionados con la
	 * compañia seleccionada.
	 * 
	 * @param idProducto
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> consultaProducto(List<String> idProducto) {

		List<String> oProdExits = new ArrayList<>();
		if (!idProducto.isEmpty()) {
			Query query2 = em.createNativeQuery(
					"SELECT p.id, p.producto_fk FROM Producto_X_Compania p WHERE p.producto_fk IN :idProducto AND p.compania_fk= :compania");
			query2.setParameter("idProducto", idProducto);
			query2.setParameter("compania", compania);
			List<Object[]> resultProduct = query2.getResultList();

			for (Object[] obj : resultProduct) {
				if (!oProdExits.contains(obj[1].toString().trim()))
					oProdExits.add(obj[1].toString().trim());
			}
		}

		return oProdExits;
	}

	/**
	 * Consulta todos los precios retailers de todos los productos y retailer, y los
	 * convierte a dolar.
	 * 
	 * @param retailer
	 * @param productoFk
	 * @param mes
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, BigDecimal> consultaPrecioRetailer(List<String> retailer, List<String> productoFk, Long mes) {
		Map<String, BigDecimal> oPreciosRetailers = new LinkedHashMap<>();
		try {
			Query query = em.createNativeQuery("SELECT p.id producto_fk,\n" + "isnull((select top(1) \n"
					+ "case when com.moneda_fk = 'USD' THEN\n" + "isnull(precio_retailer,0) ELSE\n"
					+ "CAST((ISNULL(prm.precio_retailer,0) / ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = com.id AND tc.mes_fk BETWEEN :mesAnterior AND :mes ORDER BY mes_fk DESC),1)) as decimal(18,2))\n"
					+ "END pre\n" + "from PRODUCTO_X_RETAILER_X_MES prm inner join RETAILER ret\n"
					+ "ON ret.id = prm.retailer_fk INNER JOIN COMPANIA com ON com.id = ret.compania_fk\n"
					+ "where prm.producto_fk= p.id\n"
					+ "and  mes_fk between :mesAnterior and  :mes and prm.retailer_fk= r.id \n"
					+ "order by mes_fk desc),0) as precio,\n" + "r.id retailer_fk\n" + "FROM PRODUCTO p, RETAILER r\n"
					+ "WHERE p.id IN :idProductos \n" + "AND r.id IN :idRetailers ");
			query.setParameter("idRetailers", retailer);
			query.setParameter("idProductos", productoFk);
			query.setParameter("mesAnterior", mes - 12);
			query.setParameter("mes", mes);

			List<Object[]> resultados = query.getResultList();
			for (Object[] obj : resultados) {
				if (!oPreciosRetailers.containsKey(obj[0].toString().trim() + "_" + obj[2].toString().trim()))
					oPreciosRetailers.put(obj[0].toString().trim() + "_" + obj[2].toString().trim(),
							new BigDecimal(obj[1].toString()));
			}
		} catch (NoResultException ex) {
			return oPreciosRetailers;
		}
		return oPreciosRetailers;
	}

	/**
	 * Mapea los datos del valor kam, precio retailer y total, en cada forecast
	 * dependiendo del producto.
	 * 
	 * @param filasArchivo
	 * @param mapaMeses
	 */
	private void mapearDatosEntidad(FilasArchivoForecastVO filasArchivo, Map<String, Long> mapaMeses) {
		if (filasArchivo.getRetailer() != null && filasArchivo.getCodigo() != null
				&& !filasArchivo.getRetailer().equals("") && mapaMeses != null) {
			try {
				Forecast forecast = forecastMap
						.get(filasArchivo.getRetailer().trim() + "_" + filasArchivo.getMarca().trim());
				List<ForecastProducto> foreProductoList = forecast.getForecastProductoList();

				for (ForecastProducto forecastProducto : foreProductoList) {
					for (Map.Entry<Long, BigDecimal> entry : filasArchivo.getValoresMeses().entrySet()) {
						Object key = entry.getKey();

						if (forecastProducto.getMesSop().getId() == Long.parseLong(key.toString())
								&& forecastProducto.getProductoFk().getId().trim()
										.equals(filasArchivo.getCodigo().trim())
								&& forecast.getMarcaFk().getNombre().trim().equals(filasArchivo.getMarca().trim())) {
							String valor = entry.getValue().toString().contains(".")
									? entry.getValue().toString().replace(".", "")
									: entry.getValue().toString();
							forecastProducto.setForecastKam(Long.parseLong(valor));
							BigDecimal result = new BigDecimal(Long.parseLong(valor))
									.multiply(filasArchivo.getPrecioRetailer());
							forecastProducto.setTotalforcastkam(result);
							forecastProducto.setPrecioRetailer(filasArchivo.getPrecioRetailer());
							forecastProducto.setForecastGerente(forecastProducto.getForecastKam());
							forecastProducto.setTotalForecastGerente(forecastProducto.getTotalforcastkam());
							forecastProducto.setEditarKam(true);
							forecastProducto.setEditarGerente(null);
							break;
						}
					}
				}
			} catch (Exception e) {
				log.error("[MapearDatosEntidad]   Mapeando Datos en la Entidad:::: ", e);
			}
		}
	}

	/**
	 * Actualización de Forecast ya con los valores seteados, por productos de cada
	 * forecast.
	 * 
	 * @param username
	 */
	@Transactional
	private void actualizarForecast(String username) {
		try {
			for (Map.Entry<String, Forecast> entry : forecastMap.entrySet()) {
				Long iniEm = System.currentTimeMillis();
				Forecast forecast = entry.getValue();
				forecast.setFechaModifica(new Date());
				forecast.setUsuarioModifica(username);
				em.merge(forecast);
				Long finEm = System.currentTimeMillis();
				log.info("[actualizarForecast]  Tiempo de ejecucion merge {}", (finEm - iniEm));
				contador++;
				if (contador % BATCH_SIZE == 0) {
					em.flush();
					em.clear();
				}
			}
		} catch (Exception ex) {
			log.error("[actualizarForecast] Hubo un error al enviar el mesesForecast", ex);
		}
	}

	/**
	 * 
	 * @param inArrayColumn
	 * @return
	 * @throws Exception
	 */
	private List<String> construirListMeses(String[] inArrayColumn) throws Exception {
		List<String> mesesValidos = new ArrayList<>();
		mesesValidos.add("ENERO");
		mesesValidos.add("FEBRERO");
		mesesValidos.add("MARZO");
		mesesValidos.add("ABRIL");
		mesesValidos.add("MAYO");
		mesesValidos.add("JUNIO");
		mesesValidos.add("JULIO");
		mesesValidos.add("AGOSTO");
		mesesValidos.add("SEPTIEMBRE");
		mesesValidos.add("OCTUBRE");
		mesesValidos.add("NOVIEMBRE");
		mesesValidos.add("DICIEMBRE");

		List<String> mesesKamExcel = new ArrayList<>();
		for (int j = 10; j < inArrayColumn.length; j++) {
			String mes = inArrayColumn[j].trim().toUpperCase();
			if (mesesValidos.contains(mes)) {
				if (!mesesKamExcel.contains(mes)) {
					mesesKamExcel.add(mes);
				}
			} else {
				String msg = "Mes invalido: '" + mes + "'";
				log.error("[construirListMeses] {}", msg);
				throw new Exception(msg);
			}
		}
		return mesesKamExcel;
	}
}
