/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlap.sop.demand.constantes.ConstantesDemand;
//import com.jvlap.sop.demand.logica.ForecastEJB.log;
import com.jvlap.sop.demand.logica.utilitarios.FuncionesUtilitariasDemand;
import com.jvlap.sop.demand.logica.vos.ForecastXMesVO;
import com.jvlap.sop.demand.logica.vos.ItemForecastRetailerVO;
import com.jvlap.sop.demand.logica.vos.ProductosForecastMesVO;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.EstadoForecast;
import com.jvlat.sop.entidadessop.entidades.Forecast;
import com.jvlat.sop.entidadessop.entidades.ForecastProducto;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Producto;
import com.jvlat.sop.entidadessop.entidades.ProductoXRetailerXMes;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.entidadessop.entidades.Usuario;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author Seidor
 */
@SuppressWarnings("rawtypes")
@Singleton
public class ElevacionSellInEJB extends AbstractFacade {

	@PersistenceContext(unitName = "EntidadesSOPPU")
	private EntityManager em;

	@EJB
	ForecastEJB forecastEJB;
	@EJB
	ModuloGeneralEJB moduloGeneralEJB;

	static Logger log = LogManager.getLogger(ElevacionSellInEJB.class.getName());

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	/**
	 * Consulta el forecast que se creo para el retailer 'otros'
	 *
	 * @param compania
	 * @param mes
	 * @param marcas
	 * @param mesesForecastParametro
	 * @return
	 */
	public List<ItemForecastRetailerVO> consultarForecastRetailerOtros(MesSop mes, Compania compania, String marcas,
			int mesesForecastParametro, List<String> listMarcas, Retailer retailer, boolean isCerrado, String idGrupo)
			throws Exception {

		List<ItemForecastRetailerVO> forecastOtros = null;

		try {
			if (retailer != null) {
				if (!isCerrado) {
					this.ejecutaProcedimientoBDElevacion(mes.getAnio(), mes.getMes(), compania.getId(),
							retailer.getId(), idGrupo, mes, mesesForecastParametro);
				}
				List<String> oIDRetailer = new ArrayList<>();
				oIDRetailer.add(retailer.getId());
				forecastOtros = forecastEJB.consultarForecastRetailer(oIDRetailer, mes, marcas, isCerrado, compania,
						mesesForecastParametro, true, false, null, true);

			}
		} catch (Exception e) {
			log.error("Error en consultarForecast RetailerOtros", e);
			throw (e);
		}

		return forecastOtros;
	}

	/**
	 * Almacena los productos modificados del forecast 'otros'
	 *
	 * 
	 * @param estado
	 * @param forecast
	 * @param nombreMeses
	 * @param usuario
	 * @param productoCategoria
	 * @throws com.jvlap.sop.commonsop.excepciones.ExcepcionSOP
	 */
	public void guardarForecastRetailerOtros(String estado, ForecastXMesVO forecast, List<String> nombreMeses,
			UsuarioVO usuario, Map<String, ForecastXMesVO> productoCategoria) throws ExcepcionSOP {

		try {
			forecastEJB.guardarForecastMeses(forecast, estado, usuario, nombreMeses, productoCategoria);
		} catch (Exception e) {
			throw new ExcepcionSOP("Error guardando Forecast Retailer otros, consulte con el administrador ", e);
		}

	}

	/**
	 * Consulta retailer 'otros' por compañia seleccionada
	 *
	 * @param companiaSel
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Retailer consultarRetailerOtrosPorCompania(Compania companiaSel) {
		Retailer retailer = new Retailer();

		try {
			Query query;
			query = em.createQuery(
					"SELECT r FROM Retailer r WHERE r.nombreComercial like :nombre AND r.companiaFk = :compania");
			query.setParameter("nombre", "%otros%");
			query.setParameter("compania", companiaSel);

			List<Retailer> ret = query.getResultList();
			if (ret != null && !ret.isEmpty()) {
				retailer = ret.get(0);
			}

		} catch (Exception e) {
			log.error("Error en la consulta de retailer otros", e);
			return null;
		}

		return retailer;
	}

	/**
	 * Metodo que valida si la compania seleccionada es un CLUSTERS o un BORDERS
	 * 00072-->CLU = CLUSTERS y 00074-->BRD = BORDERS
	 * 
	 * @since 18-11-2016 Jose Avila
	 * @param companiaId
	 * @return sql
	 */
	public String validarCompaniaXRetailers(String companiaId) {

		String sql = " AND r.compania_fk = '";

		if (companiaId.equals("00074")) {
			companiaId = "00070";
			sql = sql + companiaId + "' AND r.canal_distribucion ='BRD'";
		} else if (companiaId.equals("00072")) {
			companiaId = "00070";
			sql = sql + companiaId + "' AND r.canal_distribucion !='BRD' ";
		} else {
			sql = sql + companiaId + "'";
		}

		return sql;
	}

	/**
	 * Ejecuta en el procedimiento almacenado en base de datos con los calculos de
	 * Elevacion SellIn
	 *
	 * @return
	 */
	@Timeout
	@AccessTimeout(value = 60, unit = TimeUnit.MINUTES)
	private boolean ejecutaProcedimientoBDElevacion(int anio, int mes, String compania, String retailerOtros,
			String marca, MesSop mesId, int parametroMes) throws Exception {
		String msg = "[ejecutaProcedimientoBDElevacion]  ENTRO AL METODO AL PROCEDIMIENTO ::::: " + new Date()
				+ " COMPANIA :::::: " + compania;
		log.info(msg);

		StoredProcedureQuery storedProcQuery = em.createStoredProcedureQuery("Elevacion_SellIn_Mes");

		storedProcQuery.registerStoredProcedureParameter("compania", String.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("marca", String.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("retailer_otros", String.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("meSop", Integer.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("cantidad", Integer.class, ParameterMode.IN);

		storedProcQuery.setParameter("compania", compania);
		storedProcQuery.setParameter("marca", marca);
		storedProcQuery.setParameter("anio", anio);
		storedProcQuery.setParameter("mes", mes);
		storedProcQuery.setParameter("retailer_otros", retailerOtros);
		storedProcQuery.setParameter("meSop", mesId.getId().intValue());
		storedProcQuery.setParameter("cantidad", parametroMes);

		storedProcQuery.execute();
		msg = "[ejecutaProcedimientoBDElevacion] -------------------------FINALIZO EL PROCEDIMIENTO ----------------- "
				+ new Date() + " COMPANIA ------------------- " + compania;

		log.info(msg);

		return true;
	}

	/**
	 * Guarda Forecast y Forecast producto
	 *
	 * @param cerrar
	 * @param productosForecast
	 * @param retailerSeleccionado
	 * @param marcaSeleccionada
	 * @param mesSeleccionado
	 * @param usuario
	 * @return
	 * @throws ExcepcionSOP
	 */
	@SuppressWarnings("unused")
	private Forecast guardarElevacionSellin(boolean cerrar, List<ItemForecastRetailerVO> productosForecast,
			Retailer retailerSeleccionado, MarcaCat2 marcaSeleccionada, MesSop mesSeleccionado, Usuario usuario)
			throws ExcepcionSOP {

		boolean procesoCompleto = false;
		Forecast forecast = null;
		try {

			forecast = moduloGeneralEJB.consultarForecast(retailerSeleccionado, /* marcaSeleccionada */null,
					mesSeleccionado, null);
			EstadoForecast estadoForecast = null;

			if (forecast != null) {
				estadoForecast = forecast.getEstadoFk();
				if (estadoForecast == null) {
					throw new ExcepcionSOP("El Forecast " + forecast.getId() + " no tiene un estado asociado ", null);
				}

				if (!estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_ELSE)) {
					throw new ExcepcionSOP("No es posible guardar este Forecast porque está cerrado ", null);
				}

				forecast.setFechaModifica(new Date());
				forecast.setUsuarioModifica(usuario.getUserName());

			} else {
				// Forecast nuevo
				forecast = new Forecast();
				estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_ELSE);
				if (estadoForecast == null) {
					String msg = "No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_ELSE;
					throw new ExcepcionSOP(msg, null);
				}
				forecast.setCompaniaFk(retailerSeleccionado.getCompaniaFk());
				forecast.setRetailerFk(retailerSeleccionado);
				forecast.setMarcaFk(marcaSeleccionada);
				forecast.setUsuarioCrea(usuario.getUserName());
				forecast.setFechaCrea(new Date());
				forecast.setMesFk(mesSeleccionado);
			}
			if (cerrar) {
				// Cerrar el forecast

				int mesActualInt = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "MM");
				int anioActual = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "YYYY");

				if ((mesSeleccionado.getAnio() > anioActual)
						|| ((mesSeleccionado.getAnio() == anioActual) && (mesSeleccionado.getMes() > mesActualInt))) {
					throw new ExcepcionSOP("No es posible cerrar el Forecast de un mes superior al actual ", null);
				}

				estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_CERR);
				if (estadoForecast == null) {
					throw new ExcepcionSOP("No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_CERR, null);
				}
			} else {
				estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_ELSE);
				if (estadoForecast == null) {
					throw new ExcepcionSOP("No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_ELSE, null);
				}
			}

			forecast.setEstadoFk(estadoForecast);
			forecast = em.merge(forecast);
			Producto prod = null;

			for (ItemForecastRetailerVO item : productosForecast) {
				if (item.isEsProducto()) {
					ForecastProducto forecastProducto = null;
					if (item.getIdForecastProducto() != null) {
						// Ya existe el forecastProducto
						forecastProducto = em.find(ForecastProducto.class, item.getIdForecastProducto());
						if (forecastProducto == null) {
							throw new ExcepcionSOP("No existe forecastProducto con id : " + item.getIdProducto());
						}

					} else {
						// No existe el forecastProducto
						forecastProducto = new ForecastProducto();

					}
					forecastProducto.setForecastKam(item.getForecast());
					forecastProducto.setSellinSugerido(item.getSellInPropuesto());
					forecastProducto.setForecastFk(forecast);
					prod = em.find(Producto.class, item.getIdProducto());
					forecastProducto.setProductoFk(prod);
					forecastProducto.setTotalforcastkam(item.getTotalForecast());
					forecastProducto.setMargenKam(item.getMargen());
					forecastProducto.setTotalsellinSugerido(item.getTotalPropuesto());
					forecastProducto.setMesSop(em.find(MesSop.class, item.getIdMes()));
					forecastProducto.setPrecioRetailer(item.getPrecioRetailer());
					forecastProducto.setCogs(item.getCogs());
					em.merge(forecastProducto);

				}
			}
			System.out.println("ES ESTADO 2" + estadoForecast);
			int cont = 0;
			// actualiza estado producto por retailer por mes
			ProductoXRetailerXMes productoXRetailerXMes;
			for (ItemForecastRetailerVO item : productosForecast) {
				if (item.getIdProductoPorRetailerPorMes().equals(BigDecimal.ZERO)) {
					continue;
				}
				// aqui long en vez de big
				productoXRetailerXMes = em.find(ProductoXRetailerXMes.class,
						item.getIdProductoPorRetailerPorMes().longValue());
				String msg = "542 valor producto por retailer por mes" + item.getIdProductoPorRetailerPorMes();
				log.info(msg);
				if (productoXRetailerXMes != null) {
					productoXRetailerXMes.setEtapa_fk(estadoForecast);
					em.merge(productoXRetailerXMes);
				}
			}
			procesoCompleto = true;

		} catch (ExcepcionSOP e) {
			throw e;
		} catch (Exception e) {
			throw new ExcepcionSOP("Error guardando Elevacion Sellin, consulte con el administrador ", e);
		}

		// Notifica
		if (cerrar && procesoCompleto) {
			moduloGeneralEJB.notifica(retailerSeleccionado.getCompaniaFk(), marcaSeleccionada.getId(), null,
					mesSeleccionado, "Elevación SellIn - ",
					"Estimado (a) <br /><br /> Proceso Elevación SellIn ha finalizado con fecha: ",
					"Favor iniciar el proceso Forecast Compañia. ", ConstantesDemand.ETAPA_ELEVACION_SELLIN, false);
			moduloGeneralEJB.notifica(retailerSeleccionado.getCompaniaFk(), marcaSeleccionada.getId(), null,
					mesSeleccionado, "Elevación SellIn - ",
					"Estimado (a) <br /><br /> Proceso Elevación SellIn ha finalizado con fecha: ",
					"Favor iniciar el proceso Forecast Compañia. ", ConstantesDemand.ETAPA_FORECAST_COMPANIA, true);
		}

		return forecast;
	}

	public void notificarCreacionElevationSellin(MesSop mesSeleccionado, String grupoMarcaSeleccionadaIde,
			Retailer retailer) {

		GrupoMarca gMarcas = moduloGeneralEJB.consultarGrupoMarcaID(grupoMarcaSeleccionadaIde);

		moduloGeneralEJB.notifica(retailer.getCompaniaFk(), gMarcas.getNombre(), null, mesSeleccionado,
				"Elevación SellIn - ", "Estimado (a) <br /><br /> Proceso Elevación SellIn ha finalizado con fecha: ",
				"Favor iniciar el proceso Forecast Compañia. ", ConstantesDemand.ETAPA_ELEVACION_SELLIN, false);
		moduloGeneralEJB.notifica(retailer.getCompaniaFk(), gMarcas.getNombre(), null, mesSeleccionado,
				"Elevación SellIn - ", "Estimado (a) <br /><br /> Proceso Elevación SellIn ha finalizado con fecha: ",
				"Favor iniciar el proceso Forecast Compañia. ", ConstantesDemand.ETAPA_FORECAST_COMPANIA, true);
	}

	public synchronized ForecastXMesVO consultarElevacion(List<ItemForecastRetailerVO> listProdForecast,
			ForecastXMesVO forecast, Map<String, ForecastXMesVO> productoCategoria, MesSop mes, Compania compania,
			String marcas, int mesesForecastParametro, List<String> listMarcas, Retailer retailer, boolean isCerrado,
			String idGrupo, List<String> nombreMeses) throws Exception {

		List<ItemForecastRetailerVO> productosForecast = consultarForecastRetailerOtros(mes, compania, marcas,
				mesesForecastParametro, listMarcas, retailer, isCerrado, idGrupo);

		if (productosForecast.isEmpty()) {
			FacesMessage msg = new FacesMessage(
					"No se ha cerrado el proceso del forecast para esta compañía, por favor verifique que se haya realizado el proceso de Forecast Retailer");
			msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			forecast = moduloGeneralEJB.armarListaPantallaNMeses(productosForecast, nombreMeses);
			if (forecast.getProductosForecast() != null) {
				for (ProductosForecastMesVO oCategoria : forecast.getProductosForecast()) {
					if (!oCategoria.isEsTotalMarca()) {
						List<String> oIDRetailer = new ArrayList<>();
						oIDRetailer.add(retailer.getId());
						List<ItemForecastRetailerVO> productos = forecastEJB.consultarForecastRetailer(oIDRetailer, mes,
								marcas, isCerrado, compania, mesesForecastParametro, false, true,
								oCategoria.getIdCategoria(), true);
						ForecastXMesVO prod = moduloGeneralEJB.armarListaPantallaNMeses(productos, nombreMeses);
						forecastEJB.actualizarCategoriaForecast(oCategoria, prod);
						productoCategoria.put(oCategoria.getIdProducto(), prod);
					}
				}
			}
		}
		return forecast;
	}

}
