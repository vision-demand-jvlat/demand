/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlap.sop.demand.constantes.ConstantesDemand;
import com.jvlap.sop.demand.logica.utilitarios.FuncionesUtilitariasDemand;
import com.jvlap.sop.demand.logica.vos.ForecastVO;
import com.jvlap.sop.demand.logica.vos.ForecastXMesVO;
import com.jvlap.sop.demand.logica.vos.InformacionForecastXProductoVO;
import com.jvlap.sop.demand.logica.vos.ItemForecastRetailerVO;
import com.jvlap.sop.demand.logica.vos.MarcaGrupoForecast;
import com.jvlap.sop.demand.logica.vos.ProductosForecastMesVO;
import com.jvlap.sop.demand.logica.vos.RetailerGrupoForecast;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.EstadoForecast;
import com.jvlat.sop.entidadessop.entidades.Forecast;
import com.jvlat.sop.entidadessop.entidades.ForecastProducto;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Producto;
import com.jvlat.sop.entidadessop.entidades.ProductoXRetailerXMes;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.entidadessop.entidades.Usuario;
import com.jvlat.sop.login.vos.UsuarioVO;
import com.jvlat.sop.reportes.logica.ReportesEJB;

/**
 *
 * @author iviasus
 */
@SuppressWarnings("rawtypes")
@Stateless
public class ForecastEJB extends AbstractFacade {

	private static final String SELECT_FORECAST_ID = "SELECT f FROM Forecast f WHERE f.id = :id";

	@PersistenceContext(unitName = "EntidadesSOPPU")
	private EntityManager em;
	private List<ItemForecastRetailerVO> listaFC = new ArrayList<>();
	BigDecimal totalPresupuestoCat = BigDecimal.ZERO;
	BigDecimal totalForecastKamCat = BigDecimal.ZERO;
	BigDecimal totalForecastGerenCat = BigDecimal.ZERO;
	BigDecimal totalMargenGerente = BigDecimal.ZERO;
	BigDecimal totalForecastGerente = BigDecimal.ZERO;
	BigDecimal sellInPropuesto = BigDecimal.ZERO;
	BigDecimal ferecastKam = BigDecimal.ZERO;

	@EJB
	ModuloGeneralEJB moduloGeneralEJB;

	@EJB
	ReportesEJB reportesEJB;

	static Logger log = LogManager.getLogger(ForecastEJB.class.getName());

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	/**
	 * Consulta principal para tabla de caso de uso Calcular Forecast Retailer
	 *
	 * @param retailer
	 * @param mes
	 * @param marca
	 * @param evaluaEstado
	 * @param compania
	 * @param parametroMes
	 * @param isTotalizar
	 * @param esProducto
	 * @param categoria
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ItemForecastRetailerVO> consultarForecastRetailer(List<String> retailer, MesSop mes, String marca,
			boolean evaluaEstado, Compania compania, int parametroMes, boolean isTotalizar, boolean esProducto,
			String categoria, boolean isElevacion) {

		Map<String, Retailer> mapRetailer = new HashMap<>();
		String queryStr;
		boolean control = true;

		if (evaluaEstado) {
			queryStr = consultarForecastRetailerCerrado(mes, marca, esProducto, categoria, isElevacion);
			control = false;
		} else {
			if (esProducto) {
				queryStr = "SELECT pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria,\n"
						+ " pr.id prod, pr.descripcion categoria_producto,pr.descripcion categoria_producto2,\n"
						+ " ROUND(ISNULL(ssp.sellin,0),0) sellin_sug, CASE WHEN comp.moneda_fk = 'USD'\n"
						+ "   THEN ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)\n"
						+ "   ELSE CAST((ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)/ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = ?6 AND tc.mes_fk BETWEEN sr.mes_fk-12 AND sr.mes_fk ORDER BY mes_fk DESC),1)) AS decimal(18,2))\n"
						+ " END AS precio_retailer,  CASE WHEN comp.moneda_fk = 'USD'\n"
						+ "   THEN ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)*ISNULL(ROUND(ssp.sellin,0),0)\n"
						+ "   ELSE CAST((ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)/ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = ?6 AND tc.mes_fk BETWEEN sr.mes_fk-12 AND sr.mes_fk ORDER BY mes_fk DESC),1)) AS decimal(18,2)) *ISNULL(ROUND(ssp.sellin,0),0)\n"
						+ " END AS total_propuesto, \n"
						+ " ISNULL((SELECT TOP (1) cogs FROM VALORES_X_COMPANIA_X_MES vcm WHERE vcm.mes_fk BETWEEN ?5 AND sr.mes_fk AND vcm.producto_compania_fk = pc.id ORDER BY mes_fk DESC),0) cogs,\n"
						+ " CASE WHEN comp.moneda_fk = 'USD'\n"
						+ "   THEN (ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)*ISNULL(ROUND(ssp.sellin,0),0)) - (ISNULL((SELECT TOP (1) cogs FROM VALORES_X_COMPANIA_X_MES vcm WHERE vcm.mes_fk BETWEEN ?5 AND sr.mes_fk AND vcm.producto_compania_fk = pc.id ORDER BY mes_fk DESC),0)*ISNULL(ROUND(ssp.sellin,0),0))\n"
						+ "   ELSE (CAST((ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)/ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = ?6 AND tc.mes_fk BETWEEN sr.mes_fk-12 AND sr.mes_fk ORDER BY mes_fk DESC),1)) AS decimal(18,2)) *ISNULL(ROUND(ssp.sellin,0),0)) - (ISNULL((SELECT TOP (1) cogs FROM VALORES_X_COMPANIA_X_MES vcm WHERE vcm.mes_fk BETWEEN ?5 AND sr.mes_fk AND vcm.producto_compania_fk = pc.id ORDER BY mes_fk DESC),0)*ISNULL(ROUND(ssp.sellin,0),0))\n"
						+ " END AS margen_sugerido,  fcp.forecast_fk forecast_id, \n"
						+ " fcp.id forecast_prod_id, fcp.forecast_kam, CASE WHEN comp.moneda_fk = 'USD'\n"
						+ "   THEN ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)*fcp.forecast_kam\n"
						+ "   ELSE CAST((ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)/ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = ?6 AND tc.mes_fk BETWEEN sr.mes_fk-12 AND sr.mes_fk ORDER BY mes_fk DESC),1)) AS decimal(18,2))*fcp.forecast_kam\n"
						+ " END AS total_forecast_kam,  CASE WHEN comp.moneda_fk = 'USD'\n"
						+ "   THEN (ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)*fcp.forecast_kam)-(ISNULL((SELECT TOP (1) cogs FROM VALORES_X_COMPANIA_X_MES vcm WHERE vcm.mes_fk BETWEEN ?5 AND sr.mes_fk AND vcm.producto_compania_fk = pc.id ORDER BY mes_fk DESC),0)*fcp.forecast_kam)\n"
						+ "   ELSE (CAST((ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)/ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = ?6 AND tc.mes_fk BETWEEN sr.mes_fk-12 AND sr.mes_fk ORDER BY mes_fk DESC),1)) AS decimal(18,2))*fcp.forecast_kam)-(ISNULL((SELECT TOP (1) cogs FROM VALORES_X_COMPANIA_X_MES vcm WHERE vcm.mes_fk BETWEEN ?5 AND sr.mes_fk AND vcm.producto_compania_fk = pc.id ORDER BY mes_fk DESC),0)*fcp.forecast_kam)\n"
						+ " END AS  margen_kam,  fcp.forecast_gerente,\n"
						+ " CASE (SELECT TOP(1) prm.mes_fk FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC)  \n"
						+ "   WHEN sr.mes_fk THEN (SELECT TOP(1) prm.id FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC) \n"
						+ "   ELSE '0'   END id_prod_ret_mes, sr.mes_fk, mes.nombre, \n"
						+ " sr.marca_fk,  sr.retailer_fk,  pc.compania_fk, "
						+ "ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0) precio_sin_tasa, "
						+ "ISNULL((SELECT SUM(cantidad_disponible) FROM INVENTARIO where producto_fk = pr.id and bodega_fk IN (SELECT id FROM BODEGA WHERE compania_fk = ?6 AND  RIGHT(id, 2) != 02)),0) inventario_wh , \n"
						+ "ISNULL(fma.forecast_gerente ,0)gerente_mes_anterior," + "pr.marca_cat2_fk marca_categ, \n"
						+ "ISNULL((SELECT SUM(cantidad_transito) FROM INVENTARIO where producto_fk = pr.id and bodega_fk IN (SELECT id FROM BODEGA WHERE compania_fk = ?6 AND  RIGHT(id, 2) != 02)),0) transito,"
						+ "pr.UPC,  \n" + "pr.modelo, \n" + "pc.estado_des, \n" + "fcp.estadoProducto,"
						+ "pr.flag_estado,  \n" + "fcp.editar_kam, \n"
						+ " (select nombre from GRUPO_CAT4 where id= pr.grupo_cat4_fk) grupo, \n"
						+ " (select nombre from PLATAFORMA_CAT5 where id= pr.plataforma_cat5_fk) plataforma \n" + "\n"
						+ " FROM dbo.sell_in_sugerido_producto ssp \n"
						+ "       INNER JOIN dbo.SEMANAS_REQUERIDAS sr ON ssp.semanas_requeridas_fk = sr.id\n"
						+ "       INNER JOIN dbo.producto pr ON pr.id = ssp.producto_fk \n"
						+ "       INNER JOIN dbo.PRODUCTO_X_COMPANIA pc ON pc.producto_fk = pr.id AND pc.compania_fk = ?6\n"
						+ "       INNER JOIN MES_SOP mes ON mes.id = sr.mes_fk \n"
						+ "       INNER JOIN COMPANIA comp ON comp.id = pc.compania_fk        LEFT JOIN ( \n"
						+ "           SELECT                frp.*, fc.retailer_fk            FROM \n"
						+ "               FORECAST fc \n"
						+ "               INNER JOIN FORECAST_PRODUCTO frp ON fc.id = frp.forecast_fk\n"
						+ "           WHERE               fc.mes_fk = ?3 \n" + "               and fc.marca_fk IN "
						+ marca + " \n" + "               and fc.retailer_fk IN ?1 \n" + "\n"
						+ "       )fcp ON fcp.producto_fk = pr.id AND fcp.mes_sop = sr.mes_fk AND sr.retailer_fk = fcp.retailer_fk "
						+ "LEFT JOIN (    \n"
						+ " SELECT fp.forecast_gerente, fp.producto_fk, fp.mes_sop, fc.retailer_fk from FORECAST_PRODUCTO fp\n"
						+ " inner join FORECAST fc ON fc.id = fp.forecast_fk \n" + " where fc.mes_fk = ?3 -1 and\n"
						+ " fc.retailer_fk IN ?1  and \n" + " fc.marca_fk IN  " + marca + "      \n"
						+ ")fma ON fma.producto_fk = pr.id AND fma.mes_sop = sr.mes_fk AND sr.retailer_fk = fma.retailer_fk "
						+ " WHERE        sr.marca_fk IN " + marca + " AND \n"
						+ "       sr.retailer_fk IN ?1 AND        sr.mes_fk>=?3 AND sr.mes_fk<?4  \n"
						+ " and pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk = '" + categoria + "' \n "
						+ " ORDER BY pr.modelo ASC , id_categoria, sr.retailer_fk, mes_fk";
			} else {
				queryStr = "SELECT tb.id_categoria id_categoria,\n" + " '0' prod,\n"
						+ " tb.categoria_producto categoria_producto, tb.nombreCategoria nombreCategoria, SUM(tb.sellin_sug) sellin_sug,\n"
						+ " '0' precio_retailer,\n" + " SUM(tb.total_propuesto) total_propuesto,\n" + " '0' cogs,\n"
						+ " SUM(tb.margen_sugerido) margen_sugerido,\n" + " '0' forecast_id,\n"
						+ " '0' forecast_prod_id,\n" + " SUM(tb.forecast_kam) forecast_kam,\n"
						+ " SUM(tb.total_forecast_kam) total_forecast_kam,\n" + " SUM(tb.margen_kam) margen_kam,\n"
						+ " SUM(tb.forecast_gerente) forecast_gerente,\n" + " '0' id_prod_ret_mes,\n" + " tb.mes_fk,\n"
						+ " tb.nombre,\n" + " '0' marca_fk,\n" + " tb.retailer retailer_fk,\n" + " '0' compania_fk,\n"
						+ " '0' precio_sin_tasa, " + "SUM(tb.total_inventario_wh) inventario_wh ,\n"
						+ "SUM(tb.gerente_mes_anterior ) gerente_mes_anterior,    " + "tb.marca_categ marca_categ, \n"
						+ "SUM(tb.total_transito) transito \n" + "\n" + " FROM  (\n"
						+ "SELECT pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria,\n"
						+ " '0' prod,\n"
						+ " mc.nombre+' '+gr.nombre+' '+pl.nombre categoria_producto,pl.nombre nombreCategoria,   \n"
						+ " ROUND(ISNULL(ssp.sellin,0),0) sellin_sug,\n" + " '0' precio_retailer,\n"
						+ " CASE WHEN comp.moneda_fk = 'USD'\n"
						+ "   THEN ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)*ISNULL(ROUND(ssp.sellin,0),0)\n"
						+ "   ELSE CAST((ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)/ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = ?6 AND tc.mes_fk BETWEEN sr.mes_fk-12 AND sr.mes_fk ORDER BY mes_fk DESC),1)) AS decimal(18,2)) *ISNULL(ROUND(ssp.sellin,0),0)\n"
						+ " END AS total_propuesto, \n" + " '0' cogs,\n" + " CASE WHEN comp.moneda_fk = 'USD'\n"
						+ "   THEN (ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)*ISNULL(ROUND(ssp.sellin,0),0)) - (ISNULL((SELECT TOP (1) cogs FROM VALORES_X_COMPANIA_X_MES vcm WHERE vcm.mes_fk BETWEEN ?5 AND sr.mes_fk AND vcm.producto_compania_fk = pc.id ORDER BY mes_fk DESC),0)*ISNULL(ROUND(ssp.sellin,0),0))\n"
						+ "   ELSE (CAST((ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)/ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = ?6 AND tc.mes_fk BETWEEN sr.mes_fk-12 AND sr.mes_fk ORDER BY mes_fk DESC),1)) AS decimal(18,2)) *ISNULL(ROUND(ssp.sellin,0),0)) - (ISNULL((SELECT TOP (1) cogs FROM VALORES_X_COMPANIA_X_MES vcm WHERE vcm.mes_fk BETWEEN ?5 AND sr.mes_fk AND vcm.producto_compania_fk = pc.id ORDER BY mes_fk DESC),0)*ISNULL(ROUND(ssp.sellin,0),0))\n"
						+ " END AS margen_sugerido, \n" + " '0' forecast_id, \n" + " '0' forecast_prod_id,\n"
						+ " fcp.forecast_kam,\n" + " CASE WHEN comp.moneda_fk = 'USD'\n"
						+ "   THEN ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)*fcp.forecast_kam\n"
						+ "   ELSE CAST((ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)/ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = ?6 AND tc.mes_fk BETWEEN sr.mes_fk-12 AND sr.mes_fk ORDER BY mes_fk DESC),1)) AS decimal(18,2))*fcp.forecast_kam\n"
						+ " END AS total_forecast_kam, \n" + " CASE WHEN comp.moneda_fk = 'USD'\n"
						+ "   THEN (ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)*fcp.forecast_kam)-(ISNULL((SELECT TOP (1) cogs FROM VALORES_X_COMPANIA_X_MES vcm WHERE vcm.mes_fk BETWEEN ?5 AND sr.mes_fk AND vcm.producto_compania_fk = pc.id ORDER BY mes_fk DESC),0)*fcp.forecast_kam)\n"
						+ "   ELSE (CAST((ISNULL((SELECT TOP(1) precio_retailer FROM dbo.PRODUCTO_X_RETAILER_X_MES prm WHERE prm.producto_fk = pr.id AND prm.retailer_fk = sr.retailer_fk AND mes_fk BETWEEN ?5 AND ?3 ORDER BY mes_fk DESC),0)/ISNULL((SELECT TOP(1) valor FROM dbo.TASA_CAMBIO tc WHERE tc.compania_fk = ?6 AND tc.mes_fk BETWEEN sr.mes_fk-12 AND sr.mes_fk ORDER BY mes_fk DESC),1)) AS decimal(18,2))*fcp.forecast_kam)-(ISNULL((SELECT TOP (1) cogs FROM VALORES_X_COMPANIA_X_MES vcm WHERE vcm.mes_fk BETWEEN ?5 AND sr.mes_fk AND vcm.producto_compania_fk = pc.id ORDER BY mes_fk DESC),0)*fcp.forecast_kam)\n"
						+ " END AS  margen_kam, \n" + " fcp.forecast_gerente,\n" + " '0' id_prod_ret_mes,\n"
						+ " sr.mes_fk,\n" + " mes.nombre,\n" + " '0' marca, \n" + " sr.retailer_fk retailer, \n"
						+ " '0' compania, \n" + " '0' precio_sin_tasa,"
						+ "ISNULL((SELECT SUM(cantidad_disponible) FROM INVENTARIO where producto_fk = pr.id and bodega_fk IN (SELECT id FROM BODEGA WHERE compania_fk = ?6 AND  RIGHT(id, 2) != 02)),0) total_inventario_wh , "
						+ "ISNULL(fma.forecast_gerente,0)  gerente_mes_anterior, " + "pr.marca_cat2_fk marca_categ, \n"
						+ "ISNULL((SELECT SUM(cantidad_transito) FROM INVENTARIO where producto_fk = pr.id and bodega_fk IN (SELECT id FROM BODEGA WHERE compania_fk = ?6 AND  RIGHT(id, 2) != 02)),0) total_transito \n"
						+ "\n" + " FROM  dbo.sell_in_sugerido_producto ssp \n"
						+ "       INNER JOIN dbo.SEMANAS_REQUERIDAS sr ON ssp.semanas_requeridas_fk = sr.id\n"
						+ "       INNER JOIN dbo.producto pr ON pr.id = ssp.producto_fk \n"
						+ "       INNER JOIN dbo.PRODUCTO_X_COMPANIA pc ON pc.producto_fk = pr.id AND pc.compania_fk = ?6\n"
						+ "       INNER JOIN MARCA_CAT2 mc ON mc.id = pr.marca_cat2_fk\n"
						+ "       INNER JOIN GRUPO_CAT4 gr ON gr.id = pr.grupo_cat4_fk\n"
						+ "       INNER JOIN PLATAFORMA_CAT5 pl ON pl.id = pr.plataforma_cat5_fk\n"
						+ "       INNER JOIN MES_SOP mes ON mes.id = sr.mes_fk\n"
						+ "       INNER JOIN COMPANIA comp ON comp.id = pc.compania_fk \n" + "       LEFT JOIN ( \n"
						+ "           SELECT \n" + "               frp.*, fc.retailer_fk  \n" + "           FROM \n"
						+ "               FORECAST fc \n"
						+ "               INNER JOIN FORECAST_PRODUCTO frp ON fc.id = frp.forecast_fk\n"
						+ "           WHERE\n" + "               fc.mes_fk = ?3 \n"
						+ "               and fc.marca_fk IN " + marca + " \n"
						+ "               and fc.retailer_fk IN ?1 \n" + "\n"
						+ "       )fcp ON fcp.producto_fk = pr.id AND fcp.mes_sop = sr.mes_fk AND fcp.retailer_fk = sr.retailer_fk	"
						+ "" + "       LEFT JOIN (   \n" + "       \n"
						+ "           SELECT fp.forecast_gerente, fp.producto_fk, fp.mes_sop, fc.retailer_fk from FORECAST_PRODUCTO fp\n"
						+ "           inner join FORECAST fc ON fc.id = fp.forecast_fk \n"
						+ "           where fc.mes_fk = ?3 -1 and\n" + "           fc.retailer_fk IN ?1  and \n"
						+ "           fc.marca_fk IN  " + marca + "   \n"
						+ "       )fma ON fma.producto_fk = pr.id AND fma.mes_sop = sr.mes_fk AND fma.retailer_fk = sr.retailer_fk "
						+ "\n" + " WHERE \n" + "       sr.marca_fk IN " + marca + " AND\n"
						+ "       sr.retailer_fk IN ?1 AND \n" + "       sr.mes_fk>=?3 AND sr.mes_fk<?4\n" + " ) tb \n"
						+ " GROUP BY tb.id_categoria, tb.categoria_producto,tb.nombreCategoria, tb.mes_fk, tb.nombre, tb.marca_categ, tb.retailer	\n"
						+ " ORDER BY id_categoria, tb.retailer, mes_fk, prod";
			}
		}
		Query query = em.createNativeQuery(queryStr);
		query.setParameter(1, retailer);
		query.setParameter(3, mes.getId());
		Calendar fecha = Calendar.getInstance();
		int anioActual = fecha.get(Calendar.YEAR);
		int mesActual = fecha.get(Calendar.MONTH) + 1;
		if (mesActual == mes.getMes() && anioActual == mes.getAnio()) {
			query.setParameter(4, mes.getId() + parametroMes); // 6 Parametro de numero de meses, este parametro es
			// enviado por el controlador
		}
		if (control) {
			query.setParameter(4, mes.getId() + parametroMes); // 6 Parametro de numero de meses, este parametro es
			// enviado por el controlador
			query.setParameter(5, mes.getId() - 12);
			query.setParameter(6, compania.getId());
		} else {
			if (esProducto)
				query.setParameter(6, compania.getId());
		}

		List<Object[]> resultados = query.getResultList();
		List<String> proDescontinuados = new ArrayList<>();
		List<String> marcasQuery = new ArrayList<>();
		List<String> idsRetailers = new ArrayList<>();
		String idProd;
		BigDecimal sellInPropuestoBig;
		List<ItemForecastRetailerVO> listaResultado = new ArrayList<>();

		if (!resultados.isEmpty()) {
			boolean semanasMesEscogido = false;

			for (Object[] obj : resultados) {
				try {
					if (obj.length >= 9) {
						ItemForecastRetailerVO item = new ItemForecastRetailerVO();

						item.setIdCategoria((String) obj[0]);

						idProd = (String) obj[1];
						item.setIdProducto(idProd);
						item.setCategoriaProducto((String) obj[2]);

						item.setCategoriaNombre((String) obj[3]);

						sellInPropuestoBig = (BigDecimal) obj[4];
						item.setSellInPropuesto(sellInPropuestoBig != null ? sellInPropuestoBig.longValue() : 0L);
						item.setPrecioRetailer(new BigDecimal(obj[5].toString()));
						item.setTotalPropuesto((BigDecimal) obj[6]);
						item.setCogs(new BigDecimal(obj[7].toString()));

						item.setIdForecast(obj[9] != null ? (new BigDecimal(obj[9].toString())).longValue() : null);
						item.setIdForecastProducto(
								obj[10] != null ? (new BigDecimal(obj[10].toString())).longValue() : null);
						item.setTotalForecastKam(obj[12] != null ? ((BigDecimal) obj[12]) : BigDecimal.ZERO);
						item.setForecastGerente(obj[14] != null ? ((BigDecimal) obj[14]).longValue() : null);
						item.setIdProductoPorRetailerPorMes(new BigDecimal(obj[15].toString()));
						item.setIdMes(obj[16] != null ? ((BigDecimal) obj[16]).longValue() : 0L);
						item.setNombre((String) obj[17]);
						item.setIdMarca(idProd.equals("0") ? (String) obj[24] : (String) obj[18]);
						item.setIdRetailer((String) obj[19]);

						if (mapRetailer.get(item.getIdRetailer()) == null) {
							Retailer r = reportesEJB.buscarRetailerXId(item.getIdRetailer());
							mapRetailer.put(item.getIdRetailer(), r);
						}

						if (mapRetailer.get(item.getIdRetailer()) != null) {
							String nombreComercial = "";
							nombreComercial = mapRetailer.get(item.getIdRetailer()).getNombreComercial();
							item.setDescripcionRetailer(nombreComercial);

						}

						item.setIdCompania((String) obj[20]);
						item.setPrecioSinTasa(new BigDecimal(obj[21].toString()));
						item.setInventarioWH(obj[22] != null ? (new BigDecimal(obj[22].toString())) : BigDecimal.ZERO);
						item.setTransito(obj[25] != null ? (new BigDecimal(obj[25].toString())) : BigDecimal.ZERO);
						item.setTotalForecastGerenteAnterior(
								obj[23] != null ? (new BigDecimal(obj[23].toString())) : BigDecimal.ZERO);
						item.setMarcaCategoria((String) obj[24]);

						if (!marcasQuery.contains(item.getIdMarca()))
							marcasQuery.add(item.getIdMarca());

						if (!idsRetailers.contains(item.getIdRetailer()))
							idsRetailers.add(item.getIdRetailer());

						if (esProducto) {
							item.setUpc((String) obj[26]);
							item.setModelo((String) obj[27]);
							item.setEditarKam(obj[31] != null ? new Boolean(obj[31].toString()) : false);
							item.setNombreGrupo((String) obj[32]);
							item.setNombrePlataforma((String) obj[33]);
						} else {
							item.setEditarKam(false);
						}

						if (evaluaEstado) {
							item.setForecastKam(new BigDecimal(obj[11].toString()).longValue());
						} else {
							if (item.getEditarKam()) {
								item.setForecastKam(new BigDecimal(obj[11].toString()).longValue());
							} else {
								if (obj[11] != null && !obj[11].toString().equals("0")) {
									item.setForecastKam(new BigDecimal(obj[11].toString()).longValue());
								} else {
									item.setForecastKam(new BigDecimal(obj[23].toString()).longValue());
								}
							}
						}

						if (item.getIdMes().equals(mes.getId())) {
							item.setEsReal(true);
							semanasMesEscogido = true;
						} else {
							item.setEsReal(false);
						}

						if (item.getForecastKam() != null) {
							item.setMargen(obj[13] != null ? (BigDecimal) obj[13] : BigDecimal.ZERO);
						} else {
							item.setTotalForecastKam(item.getTotalPropuesto());
							item.setMargen(obj[8] != null ? (BigDecimal) obj[8] : BigDecimal.ZERO);
						}

						if (!idProd.equals("0") && esProducto) {
							item.setEsProducto(true);
							if (item.isEsReal() && !proDescontinuados.contains(item.getIdProducto())) {
								if (obj[29] != null) {
									item.setEstadoProducto(obj[29].toString());

								} else if (isElevacion) {
									String esta = moduloGeneralEJB.buscarEstadoProdElevacionInic(item.getIdProducto(),
											marca, compania.getId(), mes.getId(), item.getIdRetailer());
									item.setEstadoProducto(esta);
								} else {
									if (obj[28] != null && obj[28].equals(ConstantesDemand.FLAG_ESTADO_DESCONTINUADO)) {
										// Si el producto es descontinuado verifica si tiene inventario
										BigDecimal inventario = moduloGeneralEJB
												.obtenerInvetariosProducto(item.getIdProducto(), compania.getId());
										item.setEstadoProducto("D");
										if (inventario.compareTo(BigDecimal.ZERO) <= 0) {
											proDescontinuados.add(item.getIdProducto());
											continue;
										}
									} else {
										item.setEstadoProducto(obj[28] != null ? obj[28].toString()
												: obj[30] != null ? obj[30].toString() : "C");
									}
								}
							} else {
								if (proDescontinuados.contains(item.getIdProducto())) {
									continue;
								}
							}
						} else {
							// Es una categoria
							item.setEsProducto(false);
							item.setEsTotalMarca(false);
							item.setCategoriaProducto(item.getCategoriaProducto() + " /TC");
						}
						listaResultado.add(item);
					}
				} catch (Exception e) {
					log.error("[consultarForecastRetailer] Error realizando la extracion de la informacion : "
							+ Arrays.toString(obj));
				}
			}

			if (isTotalizar) {
				List<ItemForecastRetailerVO> lista = semanasMesEscogido ? listaResultado
						: new ArrayList<ItemForecastRetailerVO>();
				return totalizarMarcaNivelCompania(lista, mes, parametroMes, marcasQuery, idsRetailers, isElevacion);
			}
		}
		return listaResultado;
	}

	/**
	 * 
	 * @param codCategoria
	 * @param idMes
	 * @param listaResultado
	 * @return
	 */
	public int devolverIndiceList(String codCategoria, Long idMes, List<ItemForecastRetailerVO> listaResultado) {
		int contador = 0;
		for (ItemForecastRetailerVO lista : listaResultado) {
			if (codCategoria != null) {
				if (lista.getIdCategoria().equals(codCategoria) && lista.getIdMes().equals(idMes)) {
					return contador;
				}
			}
			contador++;
		}
		return -1;
	}

	/**
	 * Consulta el forecast Compañía cuando selecciona un Retailer
	 *
	 * @param retailer
	 * @param mes
	 * @param marcas
	 * @param mesesForecastParametro
	 * @return
	 */
	public List<ItemForecastRetailerVO> consultarForecastCompaniaNivelRetailer(List<String> retailer, MesSop mes,
			String marcas, int mesesForecastParametro, boolean esProducto, String categoria, boolean isTotalizar,
			boolean forecastCerrado) {

		Calendar fecha = Calendar.getInstance();
		String esActual;
		String controlEstados = "";
		boolean control = false;

		if (fecha.get(Calendar.MONTH) == mes.getMes() && fecha.get(Calendar.YEAR) == mes.getAnio()) {
			esActual = "fcp.mes_sop between ?2 AND ?3 and fc.mes_fk = ?2 \n ";
			control = true;
		} else {
			esActual = "fc.mes_fk = ?2 \n";
		}

		if (!forecastCerrado) {
			controlEstados = " (fc.estado_fk= 'ELSE' OR fc.estado_fk= 'CERR' OR fc.estado_fk= 'DEVL') AND ";
		} else {
			controlEstados = " (fc.estado_fk= 'CNFD' OR fc.estado_fk= 'CNFG') AND ";
		}

		String queryStr;

		if (esProducto) {
			queryStr = "SELECT  pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria,\n"
					+ "pr.id prod,\n" + "pr.descripcion categoria_producto,\n"
					+ "ISNULL(fcp.sellin_sugerido,0) sellin_sug,\n" + "ISNULL(fcp.precio_retailer,0) precio_retailer,\n"
					+ "ISNULL(fcp.total_sellin_sugerido,0) total_propuesto,\n" + "ISNULL(fcp.cogs,0) cogs,\n"
					+ "'0' margen_sugerido,\n" + "fcp.forecast_fk forecast_id,\n" + "fcp.id forecast_prod_id,\n"
					+ "ISNULL(fcp.forecast_kam,0) forecast_kam,\n"
					+ "ISNULL(fcp.total_forecast_kam,0) total_forecast_kam,\n"
					+ "ISNULL(fcp.margen_kam,0) margen_kam,\n" + "fcp.forecast_gerente,\n"
					+ "ISNULL((fcp.precio_retailer*fcp.forecast_gerente),0) total_forecast_gerente,\n"
					+ "(ISNULL(fcp.precio_retailer*fcp.forecast_gerente,0) - (ISNULL(fcp.cogs,0)*fcp.forecast_gerente)) margen_gerente,\n"
					+ "'0' id_prod_ret_mes,\n" + "fcp.mes_sop,\n" + "mes.nombre,\n" + "pr.marca_cat2_fk, \n"
					+ "'0' marca_marca_cat, \n" + "pr.UPC, \n" + "pr.modelo, \n" + "fcp.estadoProducto, \n"
					+ "fcp.editar_gerente, \n" + "fcp.editar_kam, \n" + "fc.retailer_fk \n" + "FROM FORECAST fc\n"
					+ "INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id\n"
					+ "INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk\n"
					+ "INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop\n" + "WHERE\n" + "fc.retailer_fk IN ?1 AND\n"
					+ controlEstados + "fc.marca_fk IN " + marcas + " AND\n" + esActual
					+ " AND pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk = '" + categoria + "' \n "
					+ "ORDER BY pr.modelo ASC, retailer_fk, fcp.mes_sop";

		} else {
			queryStr = "SELECT  pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria,\n" + "'0' prod,\n"
					+ "mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre categoria_producto,\n"
					+ "SUM(ISNULL(fcp.sellin_sugerido,0)) sellin_sug,\n" + "'0' precio_retailer,\n"
					+ "SUM(ISNULL(fcp.total_sellin_sugerido,0)) total_propuesto,\n" + "'0' cogs,\n" + "'0' margen,\n"
					+ "'0' forecast_id,\n" + "'0' forecast_prod_id,\n"
					+ "SUM(ISNULL(fcp.forecast_kam,0)) forecast_kam,\n"
					+ "SUM(ISNULL(fcp.total_forecast_kam,0)) total_forecast_kam,\n"
					+ "SUM(ISNULL(fcp.margen_kam,0)) margen_kam,\n" + "SUM(fcp.forecast_gerente),\n"
					+ "SUM(ISNULL(fcp.precio_retailer*fcp.forecast_gerente,0)) total_forecast_gerente,\n"
					+ "SUM((ISNULL(fcp.precio_retailer*fcp.forecast_gerente,0) - (ISNULL(fcp.cogs,0)*fcp.forecast_gerente)) )  margen_gerente,\n"
					+ "'0' id_prod_ret_mes,\n" + "fcp.mes_sop,\n" + "mes.nombre,\n" + "'0' marca_cat2_fk, \n"
					+ "pr.marca_cat2_fk marca_marca_cat, \n" + "fc.retailer_fk \n" + "FROM FORECAST fc\n"
					+ "INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id\n"
					+ "INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk\n"
					+ "INNER JOIN MARCA_CAT2 mr ON mr.id = pr.marca_cat2_fk\n"
					+ "INNER JOIN GRUPO_CAT4 gr ON gr.id = pr.grupo_cat4_fk \n"
					+ "INNER JOIN PLATAFORMA_CAT5 pl ON pl.id = pr.plataforma_cat5_fk \n"
					+ "INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop\n" + "WHERE\n" + "fc.retailer_fk IN ?1 AND \n"
					+ controlEstados + "fc.marca_fk IN " + marcas + " AND \n" + esActual
					+ "GROUP BY pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk, mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre, fcp.mes_sop, mes.nombre, pr.marca_cat2_fk, fc.retailer_fk\n"
					+ "ORDER BY id_categoria, fc.retailer_fk, mes_sop  ";
		}

		Query query = em.createNativeQuery(queryStr);
		query.setParameter(1, retailer);
		query.setParameter(2, mes.getId());
		if (control) {
			query.setParameter(3, mes.getId() + mesesForecastParametro); // crear nuevo parametro para los n-meses
		}

		@SuppressWarnings("unchecked")
		List<Object[]> resultados = query.getResultList();
		List<String> marcaQuery = new ArrayList<>();
		List<String> idsRetailers = new ArrayList<>();
		listaFC = new ArrayList<>();

		if (!resultados.isEmpty()) {
			String idProd;
			BigDecimal sellInPropuestoBig;

			for (Object[] obj : resultados) {

				if (obj.length >= 11) {
					ItemForecastRetailerVO item = new ItemForecastRetailerVO();

					item.setIdCategoria((String) obj[0]);

					idProd = (String) obj[1];
					item.setIdProducto(idProd);
					item.setCategoriaProducto((String) obj[2]);

					if (idProd != null && !idProd.equals("0")) {
						item.setEsProducto(true);
					} else {
						// Es una categoria
						item.setEsProducto(false);
						item.setEsTotalMarca(false);
						item.setCategoriaProducto(item.getCategoriaProducto() + " /TC");
						item.setMarcaCategoria((String) obj[20]);
					}

					sellInPropuestoBig = (BigDecimal) obj[3];
					item.setSellInPropuesto(sellInPropuestoBig != null ? sellInPropuestoBig.longValue() : 0L);
					item.setPrecioRetailer(new BigDecimal(obj[4].toString()));
					item.setTotalPropuesto((BigDecimal) obj[5]);
					item.setCogs(new BigDecimal(obj[6].toString()));

					// Informacion de forecast puede ser null
					item.setIdForecast(obj[8] != null ? (new BigDecimal(obj[8].toString())).longValue() : null);
					item.setIdForecastProducto(obj[9] != null ? (new BigDecimal(obj[9].toString())).longValue() : null);
					item.setForecastKam(obj[10] != null ? ((BigDecimal) obj[10]).longValue() : null);
					item.setTotalForecastKam(obj[11] != null ? ((BigDecimal) obj[11]) : null);

					item.setForecastGerente(obj[13] != null ? ((BigDecimal) obj[13]).longValue() : null);
					item.setTotalForecastGerente(obj[14] != null ? ((BigDecimal) obj[14]) : null);
					item.setMargenGerente(obj[15] != null ? ((BigDecimal) obj[15]) : null);

					item.setIdProductoPorRetailerPorMes(new BigDecimal(obj[16].toString()));

					item.setIdMes(obj[14] != null ? ((BigDecimal) obj[17]).longValue() : null);
					item.setNombre((String) obj[18]);
					item.setIdMarca(idProd.equals("0") ? (String) obj[20] : (String) obj[19]);

					if (!marcaQuery.contains(item.getIdMarca()))
						marcaQuery.add(item.getIdMarca());

					if (esProducto == true) {

						item.setUpc((String) obj[21]);
						item.setModelo((String) obj[22]);
						item.setEstadoProducto(obj[23] != null ? obj[23].toString() : null);
						item.setEditarGerente(obj[24] != null ? new Boolean(obj[24].toString()) : false);
						item.setEditarKam(obj[25] != null ? new Boolean(obj[25].toString()) : false);
						item.setIdRetailer(obj[26].toString());
					} else {
						item.setEditarGerente(false);
						item.setEditarKam(false);
						item.setIdRetailer(obj[21].toString());
					}

					if (!idsRetailers.contains(item.getIdRetailer()))
						idsRetailers.add(item.getIdRetailer());

					if (item.getIdMes().equals(mes.getId())) {
						item.setEsReal(true);
					} else {
						item.setEsReal(false);
					}

					if (item.getForecastKam() == null) {
						item.setForecastKam(0L);
						item.setTotalForecastKam(BigDecimal.ZERO);
						item.setForecastGerente(0L);
						item.setTotalForecastGerente(BigDecimal.ZERO);
						item.setMargenGerente(BigDecimal.ZERO);
					} else {
						if (item.getEditarGerente()) {
							item.setForecastGerente(item.getForecastGerente());
							item.setTotalForecastGerente(item.getTotalForecastGerente());

						} else {
							item.setForecastGerente(item.getForecastKam());
							item.setTotalForecastGerente(item.getTotalForecastKam());
							item.setMargenGerente((BigDecimal) obj[12]);
						}
					}
					listaFC.add(item);
				}

			}
			if (isTotalizar) {
				return totalizarMarcaNivelCompania(listaFC, mes, mesesForecastParametro, marcaQuery, idsRetailers,
						false);
			}
		}
		return listaFC;
	}

	/**
	 * 
	 * @param compania
	 * @param marcas
	 * @param mesSeleccionado
	 * @param usuario
	 * @param listMarcas
	 * @param grupoSeleccionadoIde
	 * @param selectedRetailer
	 * @return
	 */
	public boolean devolverForecastCompania(Compania compania, List<String> marcas, MesSop mesSeleccionado,
			UsuarioVO usuario, List<String> listMarcas, String grupoSeleccionadoIde, List<String> selectedRetailer) {

		try {
			EstadoForecast estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_DEVL_KAM);
			if (estadoForecast == null) {
				log.info("[devolverForecastCompania] No existe el estado forecast: "
						+ ConstantesDemand.EST_FORECAST_DEVL_KAM);
				return false;
			}

			log.info("[devolverForecastCompania] Trae forecast:::  Retail " + selectedRetailer + " marca " + marcas
					+ " mes" + mesSeleccionado);
			List<Forecast> forecasts = moduloGeneralEJB.consultarForecastXRetailersYMarcas(selectedRetailer, marcas,
					mesSeleccionado);

			for (Forecast forecast : forecasts) {
				forecast.setEstadoFk(estadoForecast);
				forecast.setForecastCompania("false");
				forecast.setFechaModifica(new Date());
				forecast.setUsuarioModifica(usuario.getUserName());

				log.info("[devolverForecastCompania] Va a realizar merge");
				em.merge(forecast);
			}
		} catch (Exception ex) {
			log.error("[devolverForecastCompania] Error al devolver forecast compania ejb ", ex);
			return false;
		}
		return true;
	}

	/**
	 * Guarda Forecast y Forecast producto
	 *
	 * @param cerrar
	 * @param productosForecast
	 * @param retailerSeleccionado
	 * @param marcaSeleccionada
	 * @param mesSeleccionado
	 * @param usuario
	 * @return
	 * @throws ExcepcionSOP
	 */
	@SuppressWarnings("unused")
	public Forecast guardarForecastRetailer(String companiaId, boolean cerrar,
			List<ItemForecastRetailerVO> productosForecast, Retailer retailerSeleccionado, MarcaCat2 marcaSeleccionada,
			MesSop mesSeleccionado, Usuario usuario) throws ExcepcionSOP {

		boolean procesoCompleto = false;
		Forecast forecast = null;
		try {

			forecast = moduloGeneralEJB.consultarForecast(retailerSeleccionado, /* marcaSeleccionada */ null,
					mesSeleccionado, null);
			EstadoForecast estadoForecast = null;

			if (forecast != null) {
				estadoForecast = forecast.getEstadoFk();
				if (estadoForecast == null) {
					throw new ExcepcionSOP("El Forecast " + forecast.getId() + " no tiene un estado asociado ", null);
				}

				if (!estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_INI)) {
					throw new ExcepcionSOP("No es posible guardar este Forecast porque está cerrado ", null);
				}

				forecast.setFechaModifica(new Date());
				forecast.setUsuarioModifica(usuario.getUserName());

			} else {
				// Forecast nuevo
				forecast = new Forecast();
				estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_INI);
				if (estadoForecast == null) {
					throw new ExcepcionSOP("No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_INI, null);
				}
				forecast.setCompaniaFk(retailerSeleccionado.getCompaniaFk());
				forecast.setRetailerFk(retailerSeleccionado);
				forecast.setMarcaFk(marcaSeleccionada);
				forecast.setUsuarioCrea(usuario.getUserName());
				forecast.setFechaCrea(new Date());
				forecast.setMesFk(mesSeleccionado);
			}

			if (cerrar) {
				// Cerrar el forecast

				int mesActualInt = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "MM");
				int anioActual = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "YYYY");

				if ((mesSeleccionado.getAnio() > anioActual)
						|| ((mesSeleccionado.getAnio() == anioActual) && (mesSeleccionado.getMes() > mesActualInt))) {
					throw new ExcepcionSOP("No es posible cerrar el Forecast de un mes superior al actual ", null);
				}

				// estadoForecast = em.find(EstadoForecast.class,
				estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_ELSE);
				if (estadoForecast == null) {
					throw new ExcepcionSOP("No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_ELSE, null);
				}
			} else {
				estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_INI);
				if (estadoForecast == null) {
					throw new ExcepcionSOP("No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_INI, null);
				}
			}

			forecast.setEstadoFk(estadoForecast);
			forecast = em.merge(forecast);
			Producto prod = null;

			for (ItemForecastRetailerVO item : productosForecast) {
				if (item.isEsProducto()) {
					ForecastProducto forecastProducto = null;
					if (item.getIdForecastProducto() != null) {
						// Ya existe el forecastProducto
						forecastProducto = em.find(ForecastProducto.class, item.getIdForecastProducto());
						if (forecastProducto == null) {
							throw new ExcepcionSOP("No existe forecastProducto con id : " + item.getIdProducto());
						}

					} else {
						// No existe el forecastProducto
						forecastProducto = new ForecastProducto();

					}
					forecastProducto.setForecastKam(item.getForecast());
					forecastProducto.setForecastFk(forecast);
					prod = em.find(Producto.class, item.getIdProducto());
					forecastProducto.setProductoFk(prod);
					forecastProducto.setTotalforcastkam(item.getTotalForecast());
					forecastProducto.setMargenKam(item.getMargen());
					forecastProducto.setSellinSugerido(item.getSellInPropuesto());
					forecastProducto.setTotalsellinSugerido(item.getTotalPropuesto());
					forecastProducto.setMesSop(em.find(MesSop.class, item.getIdMes()));
					forecastProducto.setPrecioRetailer(item.getPrecioRetailer());
					forecastProducto.setCogs(item.getCogs());
					em.merge(forecastProducto);
				}
			}

			int cont = 0;
			// actualiza estado producto por retailer por mes
			ProductoXRetailerXMes productoXRetailerXMes;
			for (ItemForecastRetailerVO item : productosForecast) {
				// aqui long en vez de big
				productoXRetailerXMes = em.find(ProductoXRetailerXMes.class,
						item.getIdProductoPorRetailerPorMes().longValue());
				if (productoXRetailerXMes != null) {
					productoXRetailerXMes.setEtapa_fk(estadoForecast);
					em.merge(productoXRetailerXMes);
				}
			}

			if (cerrar) {

				// Actualiza los datos asociándolos al retailer otros
				// 1-Buscar el retailer otros
				Retailer retailerOtros = consultarRetailerOtrosForecastRetailer(new Compania(companiaId));

				// 2-busca producto por retailer x mes con esos parámetros en un ciclo de
				// productosForecast
				ProductoXRetailerXMes productoXRetailerXMesOtros = new ProductoXRetailerXMes();
				ProductoXRetailerXMes productoXRetailerXMesTM = new ProductoXRetailerXMes();
				for (ItemForecastRetailerVO item : productosForecast) {
					if (!item.isEsProducto()) {
						continue;
					}
					try {
						// Si existe acualiza
						Producto objProducto = new Producto();
						objProducto.setId(item.getIdProducto());
						Producto productoFk = moduloGeneralEJB.buscaProductoPorCodigo(objProducto);

						productoXRetailerXMesTM = buscaProductoPorRetailerPorMesID(
								item.getIdProductoPorRetailerPorMes());

						if (retailerOtros != null) {
							productoXRetailerXMesOtros = buscarProductoXRetailerXMesOtros(productoFk, retailerOtros,
									em.find(MesSop.class, item.getIdMes()));
						}

						if (productoXRetailerXMesOtros == null) {
							productoXRetailerXMesOtros = new ProductoXRetailerXMes();
							productoXRetailerXMesOtros.setMesFk(em.find(MesSop.class, item.getIdMes()));
							productoXRetailerXMesOtros.setProductoFk(productoFk);
							productoXRetailerXMesOtros.setRetailerFk(retailerOtros);
							productoXRetailerXMesOtros
									.setEtapa_fk(em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_INI));
							if (productoXRetailerXMesTM == null) {
								productoXRetailerXMesOtros.setPrecioRetailer(BigDecimal.ZERO);
								productoXRetailerXMesOtros.setSrpSinIva(BigDecimal.ZERO);
							} else {
								productoXRetailerXMesOtros
										.setPrecioRetailer(productoXRetailerXMesTM.getPrecioRetailer());
								productoXRetailerXMesOtros.setSrpSinIva(productoXRetailerXMesTM.getSrpSinIva());
							}
						}
						estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_ELSE);
						productoXRetailerXMesOtros.setEtapa_fk(estadoForecast);
						em.merge(productoXRetailerXMesOtros);
						em.flush();

					} catch (Exception e) {
						log.error("Error creando productoXRetailerXMes ", e);
					}

				}
			}
			procesoCompleto = true;
			// Notifica
		} catch (ExcepcionSOP e) {
			throw e;
		} catch (Exception e) {
			throw new ExcepcionSOP("Error guardando Forecast Retailer, consulte con el administrador ", e);
		}

		if (cerrar && procesoCompleto) {
			Compania compaTM = moduloGeneralEJB.buscaCompaniaPorId(companiaId);
			moduloGeneralEJB.notifica(compaTM, marcaSeleccionada.getId(), Arrays.asList(retailerSeleccionado.getId()),
					mesSeleccionado, "Forecast Retailer - ",
					"Estimado (a) <br /><br /> Proceso Forecast Retailer ha finalizado con fecha: ", "",
					ConstantesDemand.ETAPA_FORECAST_RETAILER, false);
			moduloGeneralEJB.notifica(compaTM, marcaSeleccionada.getId(), Arrays.asList(retailerSeleccionado.getId()),
					mesSeleccionado, "Forecast Retailer - ",
					"Estimado (a) <br /><br /> Proceso Forecast Retailer ha finalizado con fecha: ", "",
					ConstantesDemand.ETAPA_ELEVACION_SELLIN, true);

		}

		return forecast;
	}

	/**
	 * 
	 * @param productoFk
	 * @param retailerOtros
	 * @param mesSop
	 * @return
	 */
	public ProductoXRetailerXMes buscarProductoXRetailerXMesOtros(Producto productoFk, Retailer retailerOtros,
			MesSop mesSop) {
		try {
			Query queryOtros = em.createQuery(
					"SELECT p FROM ProductoXRetailerXMes p WHERE p.productoFk = :productoFk AND p.retailerFk = :retailerFk AND p.mesFk = :mesFk");
			queryOtros.setParameter("productoFk", productoFk);
			queryOtros.setParameter("retailerFk", retailerOtros);
			queryOtros.setParameter("mesFk", mesSop);
			queryOtros.setHint("javax.persistence.cache.storeMode", "REFRESH");
			return (ProductoXRetailerXMes) queryOtros.getSingleResult();
		} catch (NoResultException e) {
			log.error("[buscarProductoXRetailerXMesOtros] Error en la consulta de retailer otros");
			return null;
		}

	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	private ProductoXRetailerXMes buscaProductoPorRetailerPorMesID(BigDecimal id) {
		try {
			if (!id.equals(BigDecimal.ZERO)) {
				Query query = em.createQuery("SELECT p FROM ProductoXRetailerXMes p WHERE p.id= :id");
				query.setParameter("id", id.longValue());
				return (ProductoXRetailerXMes) query.getSingleResult();
			}
		} catch (NoResultException e) {
			log.error("Error en la consulta de retailer otros", e);
			return null;
		}
		return null;
	}

	/**
	 * Consulta el Forecast Compañia cuando no selecciona Retailer (Todos los
	 * Retailer de la Compañía)
	 *
	 * @param compania
	 * @param mes
	 * @param marca
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ItemForecastRetailerVO> consultarForecastCompañia(Compania compania, MesSop mes, String marca,
			int mesesForecastParametro, boolean forecastCerrado, boolean esProducto, String categoria,
			boolean isTotalizar) {

		Compania companiaActual = new Compania();
		companiaActual.setId(compania.getId());

		String esActual = "";
		Calendar fecha = Calendar.getInstance();
		boolean control = false;
		boolean control2 = false;
		if ((fecha.get(Calendar.MONTH) + 1) == mes.getMes() && fecha.get(Calendar.YEAR) == mes.getAnio()) {
			esActual = "fcp.mes_sop between ?3 AND ?6 and \n" + "fc.mes_fk = ?3";
			control = true;
		} else {
			esActual = "fc.mes_fk = ?3 ";
		}

		String queryStr;

		if (forecastCerrado) {
			if (esProducto) {
				queryStr = "SELECT  pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria, \n"
						+ "pr.id prod, \n" + "pr.descripcion categoria_producto, \n"
						+ "SUM(ISNULL(fcp.sellin_sugerido,0)) sellin_sug, \n"
						+ "SUM(ISNULL(fcp.precio_retailer,0)) precio_retailer, \n"
						+ "SUM(ISNULL(fcp.total_sellin_sugerido,0)) total_propuesto, \n"
						+ "SUM(ISNULL(fcp.cogs,0)) cogs, \n" + "'0' margen_sugerido, \n"
						+ "SUM(ISNULL(fcp.forecast_kam,0)) forecast_kam, \n"
						+ "SUM(ISNULL(fcp.total_forecast_kam,0)) total_forecast_kam, \n"
						+ "SUM(ISNULL(fcp.margen_kam,0)) margen_kam, \n"
						+ "SUM(fcp.forecast_gerente) forecast_gerente, \n"
						+ "SUM(ISNULL((fcp.precio_retailer*fcp.forecast_gerente),0)) total_forecast_gerente, \n"
						+ "SUM((ISNULL(fcp.precio_retailer*fcp.forecast_gerente,0) - (ISNULL(fcp.cogs,0)*fcp.forecast_gerente))) margen_gerente, \n"
						+ "fcp.mes_sop, \n" + "mes.nombre, \n" + "pr.marca_cat2_fk, \n" + "'0' marca_marca_cat, \n"
						+ "pr.UPC, \n" + "pr.modelo, \n"
						+ "ISNULL((SELECT pc.estado_des FROM PRODUCTO_X_COMPANIA pc WHERE pc.producto_fk = pr.id AND pc.compania_fk = ?1),NULL) estadoDesc \n"
						+ "\n" + "FROM FORECAST fc \n"
						+ "     INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id \n"
						+ "     INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk \n"
						+ "     INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop \n" + "WHERE \n"
						+ "     fc.compania_fk = ?1 AND     fc.marca_fk IN " + marca + " AND \n" + esActual
						+ " AND      (fc.estado_fk = 'CNFD' or fc.estado_fk = 'CNFG')\n"
						+ " and pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk = '" + categoria + "' \n "
						+ "\n"
						+ "GROUP BY pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk,pr.id,pr.descripcion,fcp.mes_sop,mes.nombre,pr.marca_cat2_fk, pr.UPC, pr.modelo \n"
						+ "ORDER BY pr.modelo ASC, fcp.mes_sop";

			} else {
				// categoria
				queryStr = "SELECT  e.id_categoria, \n" + "e.prod, \n" + "e.categoria_producto, \n"
						+ "SUM(ISNULL(e.sellin_sug,0)) sellin_sug, \n" + "e.precio_retailer, \n"
						+ "SUM(ISNULL(e.total_propuesto,0)) total_propuesto, \n" + "e.cogs, \n" + "e.margen, \n"
						+ "SUM(ISNULL(e.forecast_kam,0)) forecast_kam, \n"
						+ "SUM(ISNULL(e.total_forecast_kam,0)) total_forecast_kam, \n"
						+ "SUM(ISNULL(e.margen_kam,0)) margen_kam, \n" + "SUM(e.forecast_gerente) forecast_gerente, \n"
						+ "SUM(ISNULL(e.total_forecast_gerente,0)) total_forecast_gerente, \n"
						+ "SUM((ISNULL(e.margen_gerente,0)))  margen_gerente, \n" + "e.mes_sop, \n" + "e.nombre, \n"
						+ "e.marca_cat2_fk, \n" + "e.marca_marca_cat \n" + "\n" + "FROM (\n"
						+ "     SELECT  pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria, \n"
						+ "     '0' prod, \n"
						+ "     mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre categoria_producto, \n"
						+ "     SUM(ISNULL(fcp.sellin_sugerido,0)) sellin_sug,      '0' precio_retailer, \n"
						+ "     SUM(ISNULL(fcp.total_sellin_sugerido,0)) total_propuesto,      '0' cogs, \n"
						+ "     '0' margen,      SUM(ISNULL(fcp.forecast_kam,0)) forecast_kam, \n"
						+ "     SUM(ISNULL(fcp.total_forecast_kam,0)) total_forecast_kam, \n"
						+ "     SUM(ISNULL(fcp.margen_kam,0)) margen_kam, \n"
						+ "     SUM(fcp.forecast_gerente) forecast_gerente, \n"
						+ "     SUM(ISNULL(fcp.precio_retailer*fcp.forecast_gerente,0)) total_forecast_gerente, \n"
						+ "     SUM((ISNULL(fcp.precio_retailer*fcp.forecast_gerente,0) - (ISNULL(fcp.cogs,0)*fcp.forecast_gerente)) )  margen_gerente, \n"
						+ "     fcp.mes_sop,      mes.nombre,      '0' marca_cat2_fk, \n"
						+ "      pr.marca_cat2_fk marca_marca_cat      FROM FORECAST fc \n"
						+ "         INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id \n"
						+ "         INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk \n"
						+ "         INNER JOIN MARCA_CAT2 mr ON mr.id = pr.marca_cat2_fk \n"
						+ "         INNER JOIN GRUPO_CAT4 gr ON gr.id = pr.grupo_cat4_fk \n"
						+ "         INNER JOIN PLATAFORMA_CAT5 pl ON pl.id = pr.plataforma_cat5_fk \n"
						+ "         INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop      WHERE \n"
						+ "         fc.compania_fk = ?1 AND         fc.marca_fk IN " + marca + " AND \n" + esActual
						+ " AND          (fc.estado_fk = 'CNFD' or fc.estado_fk = 'CNFG')\n" + "\n"
						+ "     GROUP BY pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk, mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre, fcp.mes_sop, mes.nombre, pr.marca_cat2_fk )e \n"
						+ "GROUP BY e.id_categoria, e.categoria_producto, e.mes_sop, e.nombre,\n"
						+ "e.prod,e.cogs,e.margen,e.precio_retailer,e.marca_cat2_fk, e.marca_marca_cat \n" + "\n"
						+ "ORDER BY id_categoria, mes_sop";

			}
		} else {
			control2 = true;
			if (esProducto) {
				queryStr = "SELECT    \n" + "d.id_categoria,    \n" + "d.prod,   \n" + "d.categoria_producto,   \n"
						+ "SUM(ISNULL(ROUND(d.sellin_sug,0),0)) sellin_sug,  \n"
						+ "ISNULL(SUM(distinct d.precio_retailer),0) precio_retailer,   \n"
						+ "ISNULL(SUM(d.total_propuesto),0) total_propuesto,   \n"
						+ "ISNULL(sum(distinct d.cogs),0) cogs,  \n" + "'0' margen_sugerido,   \n"
						+ "ISNULL(SUM(d.forecast_kam),0) forecast_kam,   \n"
						+ "ISNULL(SUM(d.total_forecast_kam),0) total_forecast_kam,   \n"
						+ "ISNULL(SUM(distinct d.margen_kam),0) margen_kam,   \n"
						+ "ISNULL(SUM(d.forecast_gerente),0) forecast_gerente,   \n"
						+ "ISNULL(SUM(d.total_forecast_gerente),0) total_forecast_gerente,   \n"
						+ "ISNULL(SUM(d.margen_gerente),0) margen_gerente,   \n" + "d.mes_sop,\n" + "d.nombre,\n"
						+ "d.marca_cat2_fk, \n" + "d.marca_marca_cat, \n" + "d.UPC upc, \n" + "d.modelo modelo, \n"
						+ "d.estadoDesc estadoDesc \n" + "\n" + "FROM (          select distinct   \n"
						+ "        pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria,   \n"
						+ "        pr.id prod,           pr.descripcion categoria_producto,   \n"
						+ "        ROUND(ISNULL(fcp.sellin_sugerido,0),0) sellin_sug,    \n"
						+ "        fcp.precio_retailer,   \n"
						+ "        fcp.total_sellin_sugerido total_propuesto,            fcp.cogs cogs,   \n"
						+ "        '0' margen_sugerido,           fcp.forecast_kam ,   \n"
						+ "        fcp.total_forecast_kam total_forecast_kam,          fcp.margen_kam,    \n"
						+ "        CASE WHEN fcp.editar_gerente = null \n"
						+ "        THEN CASE WHEN fcp.forecast_gerente = 0 \n"
						+ "        THEN fcp.forecast_kam ELSE fcp.forecast_gerente END \n"
						+ "        ELSE  fcp.forecast_gerente END AS forecast_gerente, \n" + "\n "
						+ "        CASE WHEN fcp.editar_gerente = null \n"
						+ "        THEN CASE WHEN fcp.forecast_gerente = 0 \n"
						+ "        THEN fcp.total_forecast_kam ELSE (fcp.precio_retailer*fcp.forecast_gerente) END \n"
						+ "        ELSE (fcp.precio_retailer*fcp.forecast_gerente) END AS total_forecast_gerente, \n"
						+ "\n"
						+ "        (fcp.precio_retailer*fcp.forecast_gerente-(fcp.cogs*fcp.forecast_gerente)) margen_gerente,    \n"
						+ "        '0' id_prod_ret_mes,          fc.retailer_fk,        fcp.mes_sop,\n"
						+ "        mes.nombre,        pr.marca_cat2_fk,         '0' marca_marca_cat, \n"
						+ "        pr.UPC upc,        pr.modelo modelo, \n"
						+ "        ISNULL((SELECT pc.estado_des FROM PRODUCTO_X_COMPANIA pc WHERE pc.producto_fk = pr.id AND pc.compania_fk = ?1),NULL) estadoDesc \n"
						+ "     from FORECAST fc\n"
						+ "     INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id \n"
						+ " INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk \n"
						+ " INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop      where   \n"
						+ "     fc.compania_fk = ?1 and        fc.marca_fk IN " + marca + " and   \n" + esActual
						+ " and      (fc.estado_fk = ?4 OR fc.estado_fk = ?7) AND \n"
						+ "     pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk = '" + categoria + "' \n"
						+ "     UNION       select distinct   \n"
						+ "     ISNULL(pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk,0) id_categoria,   \n"
						+ "     ISNULL( pr.id,0) prod,        ISNULL(pr.descripcion,0) categoria_producto,   \n"
						+ "     ROUND(ISNULL(fcp.sellin_sugerido,0),0) sellin_sug,    \n"
						+ "     ISNULL(fcp.precio_retailer,0) precio_retailer,   \n"
						+ "     ISNULL(fcp.total_sellin_sugerido,0) total_propuesto,    \n"
						+ "     ISNULL(fcp.cogs,0) cogs,        '0' margen_sugerido,   \n"
						+ "     ISNULL(fcp.forecast_kam,0) forecast_kam,   \n"
						+ "     ISNULL(fcp.total_forecast_kam,0) total_forecast_kam,  \n"
						+ "     ISNULL(fcp.margen_kam,0) margen_kam,    \n" + "\n"
						+ "        CASE WHEN fcp.editar_gerente = null \n"
						+ "        THEN CASE WHEN fcp.forecast_gerente = 0 \n"
						+ "        THEN fcp.forecast_kam ELSE fcp.forecast_gerente END \n"
						+ "        ELSE  fcp.forecast_gerente END AS forecast_gerente, \n" + "\n "
						+ "        CASE WHEN fcp.editar_gerente = null \n"
						+ "        THEN CASE WHEN fcp.forecast_gerente = 0 \n"
						+ "        THEN fcp.total_forecast_kam ELSE (fcp.precio_retailer*fcp.forecast_gerente) END \n"
						+ "        ELSE (fcp.precio_retailer*fcp.forecast_gerente) END AS total_forecast_gerente, \n"
						+ "\n"
						+ "     (fcp.precio_retailer*fcp.forecast_gerente-(fcp.cogs*fcp.forecast_gerente)) margen_gerente,    \n"
						+ "    '0' id_prod_ret_mes,      fc.retailer_fk,     fcp.mes_sop,\n"
						+ "     mes.nombre,       pr.marca_cat2_fk,      '0' marca_marca_cat, \n"
						+ "     pr.UPC upc,       pr.modelo modelo, \n"
						+ "     ISNULL((SELECT pc.estado_des FROM PRODUCTO_X_COMPANIA pc WHERE pc.producto_fk = pr.id AND pc.compania_fk = ?1),NULL) estadoDesc  \n"
						+ "           from FORECAST fc\n"
						+ "         INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id \n"
						+ " INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk \n"
						+ " INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop      where  \n"
						+ "     fc.retailer_fk = ?5 and        fc.marca_fk IN " + marca + " and  \n" + esActual
						+ " and      fc.estado_fk = 'CERR' and  \n"
						+ "     pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk = '" + categoria + "' \n "
						+ "     )d    \n"
						+ "    GROUP BY d.id_categoria,d.prod,d.categoria_producto,d.mes_sop,d.nombre,d.marca_cat2_fk,d.marca_marca_cat, d.upc, d.modelo, d.estadoDesc \n"
						+ "    ORDER BY d.modelo ASC \n";

			} else {
				// categoria
				queryStr = "   SELECT e.id_categoria,   \n" // 0
						+ "   e.prod,   \n" // 1
						+ "    e.categoria_producto,    \n" // 2
						+ "   SUM(ISNULL(e.sellin_sug,0)) sellin_sugerido,   \n" // 3
						+ "   e.precio_retailer,    \n" // 4
						+ "   ISNULL(SUM(e.total_propuesto),0) total_propuesto,   \n" // 5
						+ "   e.cogs,   \n" // 6
						+ "   '0' margen,   \n" // 7
						+ "   ISNULL(SUM(e.forecast_kam),0) forecast_kam,   \n" // 8
						+ "   ISNULL(SUM(e.total_forecast_kam),0) total_forecast_kam,   \n" // 9
						+ "   ISNULL(SUM(e.margen_kam),0) margen_kam,    \n" // 10
						+ "   ISNULL(SUM(e.forecast_gerente),0) forecast_gerente,   \n" // 11
						+ "   ISNULL(SUM(e.total_forecast_gerente),0) total_forecast_gerente, \n" // 12
						+ "   ISNULL(SUM(e.margen_gerente),0)  margen_gerente,\n" // 13
						+ "   e.mes_sop,\n" // 14
						+ "   e.nombre,   \n" // 15
						+ "   e.marca_cat2_fk, \n" // 16
						+ "   e.marca_marca_cat \n" // 17
						+ " FROM(     select     \n"
						+ "  pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria,   \n"
						+ "  '0' prod,   \n"
						+ "  mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre categoria_producto,   \n"
						+ "  ISNULL(sum(distinct ROUND(fcp.sellin_sugerido,0)),0) sellin_sug,    \n"
						+ "  '0' precio_retailer,    \n"
						+ "  sum(distinct fcp.total_sellin_sugerido) total_propuesto,     '0' cogs,    \n"
						+ "  '0' margen,      sum(distinct fcp.forecast_kam) forecast_kam,   \n"
						+ "  sum(distinct fcp.total_forecast_kam) total_forecast_kam,   \n"
						+ "  sum(distinct fcp.margen_kam) margen_kam,   \n"
						+ "  sum (distinct fcp.forecast_gerente) forecast_gerente,   \n"
						+ "  sum(distinct (fcp.precio_retailer*fcp.forecast_gerente)) total_forecast_gerente,   \n"
						+ "  sum(distinct (fcp.precio_retailer*fcp.forecast_gerente-(fcp.cogs*fcp.forecast_gerente))) margen_gerente,   \n"
						+ "  '0' id_prod_ret_mes,  fcp.mes_sop,  mes.nombre, \n"
						+ "  '0' marca_cat2_fk,   pr.marca_cat2_fk marca_marca_cat \n" + "\n"
						+ "  from FORECAST fc   \n" + " INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id\n"
						+ " INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk\n"
						+ "     INNER JOIN MARCA_CAT2 mr ON mr.id = pr.marca_cat2_fk\n"
						+ "     INNER JOIN GRUPO_CAT4 gr ON gr.id = pr.grupo_cat4_fk\n"
						+ "     INNER JOIN PLATAFORMA_CAT5 pl ON pl.id = pr.plataforma_cat5_fk\n"
						+ " INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop         where   \n"
						+ "  fc.compania_fk = ?1 and     fc.marca_fk IN " + marca + " and   \n " + esActual
						+ " and   (fc.estado_fk = ?4 OR fc.estado_fk = ?7) \n"
						+ "   GROUP BY pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk,mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre,fc.retailer_fk, fc.estado_fk,fcp.mes_sop,mes.nombre,pr.marca_cat2_fk     \n"
						+ "                                       UNION        select \n"
						+ "      pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria,   \n"
						+ "  '0' prod,   \n"
						+ "  mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre categoria_producto,   \n"
						+ "  ISNULL(sum(distinct ROUND(fcp.sellin_sugerido,0)),0) sellin_sug,    \n"
						+ "  '0' precio_retailer,    \n"
						+ "  sum(distinct fcp.total_sellin_sugerido) total_propuesto,     '0' cogs,    \n"
						+ "  '0' margen,      sum(distinct fcp.forecast_kam) forecast_kam,   \n"
						+ "  sum(distinct fcp.total_forecast_kam) total_forecast_kam,   \n"
						+ "  sum(distinct fcp.margen_kam) margen_kam,   \n"
						+ "  sum (distinct fcp.forecast_gerente) forecast_gerente,   \n"
						+ "  sum(distinct (fcp.precio_retailer*fcp.forecast_gerente)) total_forecast_gerente,   \n"
						+ "   sum(distinct (fcp.precio_retailer*fcp.forecast_gerente-(fcp.cogs*fcp.forecast_gerente))) margen_gerente,   \n"
						+ "  '0' id_prod_ret_mes,  fcp.mes_sop,  mes.nombre, \n"
						+ "  '0' marca_cat2_fk,   pr.marca_cat2_fk marca_marca_cat         \n"
						+ "              from FORECAST fc\n"
						+ "      INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id\n"
						+ "  INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk\n"
						+ "      INNER JOIN MARCA_CAT2 mr ON mr.id = pr.marca_cat2_fk\n"
						+ "      INNER JOIN GRUPO_CAT4 gr ON gr.id = pr.grupo_cat4_fk\n"
						+ "      INNER JOIN PLATAFORMA_CAT5 pl ON pl.id = pr.plataforma_cat5_fk\n"
						+ "  INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop\n" + "       \n"
						+ "              where             fc.retailer_fk = ?5 and   \n" + "      fc.marca_fk IN "
						+ marca + " and  \n" + esActual + " and \n" + "      fc.estado_fk = 'CERR'        \n"
						+ "      GROUP BY pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk,mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre,fc.retailer_fk, fc.estado_fk,fcp.mes_sop,mes.nombre,pr.marca_cat2_fk     \n"
						+ "  )e     \n" + "\n"
						+ "  --inner join  dbo.I_VIEW_CATEGORIA as cat on cat.id = e.id_categoria   \n"
						+ "  GROUP BY e.id_categoria,e.prod,e.categoria_producto,e.precio_retailer,e.cogs,e.mes_sop,e.nombre,e.marca_cat2_fk,e.marca_marca_cat   \n"
						+ "    order by id_categoria,mes_sop\n";
			}
		}

		Query query = em.createNativeQuery(queryStr);
		query.setParameter(1, compania.getId());
		query.setParameter(3, mes.getId());
		Retailer retailer = consultarRetaOtrosComp(companiaActual);
		if (control2) {
			query.setParameter(4, ConstantesDemand.EST_FORECAST_ELSE);
			query.setParameter(5, retailer.getId());
			query.setParameter(7, ConstantesDemand.EST_FORECAST_DEVL);
		}

		if (control) {
			query.setParameter(6, mes.getId() + (mesesForecastParametro - 1)); // crear nuevo parametro para los n-meses
		}

		List<Object[]> resultados = query.getResultList();
		String idProd;
		BigDecimal sellInPropuestoBig;
		List<ItemForecastRetailerVO> listaResultado = new ArrayList<>();
		List<String> marcaQuery = new ArrayList<>();

		if (!resultados.isEmpty()) {
			int primerElemento = 0;

			for (Object[] obj : resultados) {

				if (obj.length >= 11) {
					ItemForecastRetailerVO item = new ItemForecastRetailerVO();

					item.setIdCategoria((String) obj[0]);

					idProd = (String) obj[1];
					item.setIdProducto(idProd);
					item.setCategoriaProducto((String) obj[2]);

					if (idProd != null && !idProd.equals("0")) {
						item.setEsProducto(true);

					} else {
						// Es una categoria
						item.setEsProducto(false);
						item.setEsTotalMarca(false);
						item.setCategoriaProducto(item.getCategoriaProducto() + " /TC");
						item.setMarcaCategoria((String) obj[17]);
					}

					sellInPropuestoBig = (BigDecimal) obj[3];
					item.setSellInPropuesto(sellInPropuestoBig != null ? sellInPropuestoBig.longValue() : 0L);
					item.setPrecioRetailer(new BigDecimal(obj[4].toString()));
					item.setTotalPropuesto((BigDecimal) obj[5]);
					item.setCogs(new BigDecimal(obj[6].toString()));

					// Informacion de forecast puede ser null
					item.setForecastKam(obj[8] != null ? ((BigDecimal) obj[8]).longValue() : null);
					item.setTotalForecastKam(obj[9] != null ? ((BigDecimal) obj[9]) : null);

					item.setForecastGerente(obj[11] != null ? ((BigDecimal) obj[11]).longValue() : null);
					item.setTotalForecastGerente(obj[12] != null ? ((BigDecimal) obj[12]) : null);
					item.setMargenGerente(obj[13] != null ? ((BigDecimal) obj[13]) : null);

					item.setIdMes(obj[14] != null ? ((BigDecimal) obj[14]).longValue() : null);
					item.setNombre((String) obj[15]);
					item.setIdMarca(idProd.equals("0") ? (String) obj[17] : (String) obj[16]);

					if (!marcaQuery.contains(item.getIdMarca()))
						marcaQuery.add(item.getIdMarca());

					if (esProducto) {
						item.setUpc((String) obj[18]);
						item.setModelo((String) obj[19]);
					}

					if (item.getIdMes().equals(mes.getId())) {
						item.setEsReal(true);

						if (esProducto) {
							String estadoProd = buscarEstadoProdPorCompania(item.getIdProducto(), marca,
									compania.getId(), mes.getId());
							item.setEstadoProducto(estadoProd);
						}
					} else {
						item.setEsReal(false);
					}

					if (item.getForecastKam() == null) {
						item.setForecastKam(0L);
						item.setTotalForecastKam(BigDecimal.ZERO);
						item.setForecastGerente(0L);
						item.setTotalForecastGerente(BigDecimal.ZERO);
						item.setMargenGerente(BigDecimal.ZERO);

					}

					if (primerElemento == 1) {
						item.setIdCompania(compania.getId());
						primerElemento = -1;
					}

					if (primerElemento == 0) {
						primerElemento = 1;
					}

					listaResultado.add(item);
				}

			}
			if (isTotalizar) {
				return totalizarMarcaNivelCompania(listaResultado, mes, mesesForecastParametro, marcaQuery);
			}
		}
		return listaResultado;
	}

	/**
	 * Confirma un forecast unicamente si esta en estado Cerrado
	 *
	 * @param confirmarForecast    true cambia el estado del forecast a confirmado
	 * @param productosForecast
	 * @param retailerSeleccionado
	 * @param marcaSeleccionada
	 * @param mesSeleccionado
	 * @param usuario
	 * @param companiaId
	 * @return
	 * @throws ExcepcionSOP
	 */
	public Forecast guardarForecastCompaniaNivelRetailer(boolean confirmarForecast,
			List<ItemForecastRetailerVO> productosForecast, Retailer retailerSeleccionado, MarcaCat2 marcaSeleccionada,
			MesSop mesSeleccionado, UsuarioVO usuario, String companiaId) throws ExcepcionSOP {

		Forecast forecast = null;
		Compania compania = moduloGeneralEJB.buscaCompaniaPorId(companiaId);
		@SuppressWarnings("unused")
		Retailer retailerOtros = consultarRetailerOtrosPorCompania(compania);
		try {
			if (productosForecast.size() > 0) {
				forecast = moduloGeneralEJB.consultarForecast(retailerSeleccionado, /* marcaSeleccionada */ null,
						mesSeleccionado, null);
				EstadoForecast estadoForecast = null;

				if (forecast == null) {
					throw new ExcepcionSOP(
							"No es posible actualizar este Forecast, debe ser creado previamente por el Kam", null);
				}

				estadoForecast = forecast.getEstadoFk();
				if (estadoForecast == null) {
					throw new ExcepcionSOP("El Forecast " + forecast.getId() + " no tiene un estado asociado ", null);
				}

				if (estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_CNFG)) {
					throw new ExcepcionSOP(
							"No es posible guardar este Forecast porque ya fue Confirmado por el Gerente", null);
				}

				if (estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_CERR)
						&& estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_DEVL)) {
					log.info("Operación invalida para el estado: " + estadoForecast.getNombre()
							+ " del Forecast del Retailer: " + forecast.getRetailerFk().getId());
					return null;
				}

				forecast.setFechaModifica(new Date());
				forecast.setUsuarioModifica(usuario.getUserName());
				forecast.setForecastCompania("Procesado");

				if (confirmarForecast) {
					// Confirmar el forecast

					int mesActualInt = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "MM");
					int anioActual = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "YYYY");

					if ((mesSeleccionado.getAnio() > anioActual) || ((mesSeleccionado.getAnio() == anioActual)
							&& (mesSeleccionado.getMes() > mesActualInt))) {
						throw new ExcepcionSOP("No es posible Confirmar el Forecast de un mes superior al actual ",
								null);
					}

					if (!estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_ELSE)
							&& !estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_DEVL)
							&& !estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_CERR)) {
						throw new ExcepcionSOP(
								"No es posible Confirmar este Forecast por un Gerente porque aún no está cerrado por el Kam ",
								null);

					}

					estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_CNFG);
					if (estadoForecast == null) {
						throw new ExcepcionSOP("No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_CNFG,
								null);
					}

					forecast.setEstadoFk(estadoForecast);
				}

				forecast = em.merge(forecast);

				ForecastProducto forecastProducto = null;
				for (ItemForecastRetailerVO item : productosForecast) {
					if (item.isEsProducto()) {
						forecastProducto = null;
						if (item.getIdForecastProducto() == null) {
							throw new ExcepcionSOP(
									"Error, el Forecast Producto debe tener identificador para poder actualizar", null);

						}

						// Ya existe el forecastProducto
						forecastProducto = em.find(ForecastProducto.class, item.getIdForecastProducto());
						if (forecastProducto == null) {
							throw new ExcepcionSOP("No existe forecastProducto con id : " + item.getIdProducto());
						}

						forecastProducto.setForecastGerente(item.getForecastGerente());
						em.merge(forecastProducto);

					} // END esProducto
				} // END For productos

				// actualiza estado producto por retailer por mes
				ProductoXRetailerXMes productoXRetailerXMes;

				for (ItemForecastRetailerVO item : productosForecast) {
					productoXRetailerXMes = em.find(ProductoXRetailerXMes.class,
							item.getIdProductoPorRetailerPorMes().longValue());
					if (productoXRetailerXMes == null) {
						continue;
					}
					productoXRetailerXMes.setEtapa_fk(estadoForecast);

					em.merge(productoXRetailerXMes);
				}

			} // END if produtcos.size > 0
			else {
				forecast = moduloGeneralEJB.consultarForecast(retailerSeleccionado, /* marcaSeleccionada */ null,
						mesSeleccionado, null);
				EstadoForecast estadoForecast = null;
				if (forecast != null) {
					estadoForecast = forecast.getEstadoFk();
					if (estadoForecast == null) {
						throw new ExcepcionSOP("El Forecast " + forecast.getId() + " no tiene un estado asociado ",
								null);
					}

					if (estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_CNFG)) {
						throw new ExcepcionSOP(
								"No es posible guardar este Forecast porque ya fue Confirmado por el Gerente", null);
					}

					if (estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_CERR)
							&& estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_DEVL)) {
						log.info("Operación invalida para el estado: " + estadoForecast.getNombre()
								+ " del Forecast del Retailer: " + forecast.getRetailerFk().getId());
						return null;
					}

					forecast.setFechaModifica(new Date());
					forecast.setUsuarioModifica(usuario.getUserName());
					forecast.setForecastCompania("Procesado");

					if (confirmarForecast) {
						// Confirmar el forecast

						int mesActualInt = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "MM");
						int anioActual = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "YYYY");

						if ((mesSeleccionado.getAnio() > anioActual) || ((mesSeleccionado.getAnio() == anioActual)
								&& (mesSeleccionado.getMes() > mesActualInt))) {
							throw new ExcepcionSOP("No es posible Confirmar el Forecast de un mes superior al actual ",
									null);
						}
						if (!estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_ELSE)
								&& !estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_DEVL)
								&& !estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_CERR)) {
							throw new ExcepcionSOP(
									"No es posible Confirmar este Forecast por un Gerente porque aún no está cerrado por el Kam ",
									null);

						}

						estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_CNFG);
						if (estadoForecast == null) {
							throw new ExcepcionSOP(
									"No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_CNFG, null);
						}

						forecast.setEstadoFk(estadoForecast);
					}

					forecast = em.merge(forecast);
				}
			}

		} catch (ExcepcionSOP e) {
			throw e;
		} catch (Exception e) {
			throw new ExcepcionSOP("Error guardando Forecast Retailer, consulte con el administrador ", e);
		}
		return forecast;
	}

	/**
	 * 
	 * @param companiaId
	 * @param confirmarForecast
	 * @param compania
	 * @param marcas
	 * @param mesSeleccionado
	 * @param usuario
	 * @param listMarcas
	 * @param mesesForecastParametro
	 * @throws ExcepcionSOP
	 */
	public void guardarForecastCompania(String companiaId, boolean confirmarForecast, Compania compania, String marcas,
			MesSop mesSeleccionado, UsuarioVO usuario, List<MarcaCat2> listMarcas, int mesesForecastParametro)
			throws ExcepcionSOP {

		List<ItemForecastRetailerVO> productosForecast = null;
		for (Retailer retailer : compania.getRetailerList()) {
			List<String> oIDRetailers = new ArrayList<>();
			oIDRetailers.add(retailer.getId());
			productosForecast = consultarForecastCompaniaNivelRetailer(oIDRetailers, mesSeleccionado, marcas,
					mesesForecastParametro, false, null, false, false);
			for (MarcaCat2 marca : listMarcas) {
				guardarForecastCompaniaNivelRetailer(confirmarForecast, productosForecast, retailer, marca,
						mesSeleccionado, usuario, companiaId);
			}

		}
		Compania compaTM = moduloGeneralEJB.buscaCompaniaPorId(companiaId);
		for (MarcaCat2 marca : listMarcas) {
			moduloGeneralEJB.notifica(compaTM, marca.getId(), null, mesSeleccionado, "Forecast Compañía - ",
					"Estimado (a) <br /><br /> Proceso Forecast Compañia ha finalizado con fecha: ",
					"Favor iniciar el proceso Forecast General. ", ConstantesDemand.ETAPA_FORECAST_COMPANIA, true);
			moduloGeneralEJB.notifica(compaTM, marca.getId(), null, mesSeleccionado, "Forecast Compañía - ",
					"Estimado (a) <br /><br /> Proceso Forecast Compañia ha finalizado con fecha: ",
					"Favor iniciar el proceso Forecast General. ", ConstantesDemand.ETAPA_FORECAST_GENERAL, false);
		}
	}

	/**
	 * Consulta tabla para Forecasst General por Compañía, si la compañia es null
	 * consulta para todas las compañias
	 *
	 * @param companiaSeleccionada si es null consulta para todas las compañias
	 * @param mesSeleccionado
	 * @param marcaSeleccionada
	 * @return
	 * @throws ExcepcionSOP
	 */
	@SuppressWarnings("unchecked")
	public List<ItemForecastRetailerVO> consultarForecastGeneralCompañia(Compania companiaSeleccionada,
			MesSop mesSeleccionado, String marcas) throws ExcepcionSOP {
		String queryStr = null;
		String esActual;
		Calendar fecha = Calendar.getInstance();
		boolean control = false;
		if (fecha.get(Calendar.MONTH + 1) == mesSeleccionado.getMes()
				&& fecha.get(Calendar.YEAR) == mesSeleccionado.getAnio()) {
			esActual = " fcp.mes_sop between ?2 AND ?7 and  fc.mes_fk = ?2 AND \n";
			control = true;
		} else {
			esActual = " fc.mes_fk = ?2 AND \n";
		}

		if (companiaSeleccionada == null) {
			queryStr = "SELECT\n" + "  cat.id id_categoria,     cat.categoria, \n" + "    fcp.mes_sop mes_actual, \n"
					+ "    ISNULL(sum(distinct fcp.precio_retailer),0) precio_retailer_actual, \n"
					+ "    ISNULL(sum(distinct fcp.cogs),0) cogs_actual, \n"
					+ "    sum(fcp.forecast_gerente) forecast_gerente_actual, \n"
					+ "    ISNULL(sum(fcp.total_forecast_gerente),0) total_forecast_gerente_actual, \n"
					+ "    ISNULL(sum(fcp.margen_gerente),0) margen_gerente_actual,\n"
					+ "  fcp.mes_sop-1 mes_anterior,\n" + "  CASE WHEN ms.id = ?2\n"
					+ "  THEN (SELECT ISNULL(precio.precio_retailer,0) FROM (\n"
					+ "   select distinct  catego.id id_categoria, catego.categoria, mess.id mes, ISNULL(sum(distinct fcp.precio_retailer),0) precio_retailer\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fc.mes_fk AND mess.id = fcp.mes_sop INNER JOIN COMPANIA c ON c.id=fc.compania_fk \n"
					+ "   where fc.marca_fk IN " + marcas
					+ " and  fc.mes_fk  = ?4 AND (fc.estado_fk=?5 or fc.estado_fk=?6) AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) precio)\n"
					+ "  ELSE (SELECT ISNULL(precio.precio_retailer,0) FROM(\n"
					+ "   select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum(distinct fcp.precio_retailer),0) precio_retailer\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk\n"
					+ "   INNER JOIN MES_SOP mess ON mess.id = fcp.mes_sop INNER JOIN COMPANIA c ON c.id=fc.compania_fk \n"
					+ "   where fc.marca_fk IN " + marcas + " and " + esActual
					+ " fcp.mes_sop = (ms.id-1) AND (fc.estado_fk=?5 or fc.estado_fk=?6) AND catego.id = cat.id\n"
					+ "   GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) precio)\n"
					+ "  END precio_retailer_anterior,\n" + "  CASE WHEN ms.id = ?2\n"
					+ "  THEN (SELECT ISNULL(cog.cogs,0) FROM ( select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum(distinct fcp.cogs),0) cogs \n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fc.mes_fk AND mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk where fc.marca_fk IN " + marcas + " and  \n"
					+ "   fc.mes_fk  = ?4 AND (fc.estado_fk=?5 or fc.estado_fk=?6) AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id) cog)\n"
					+ "  ELSE (SELECT ISNULL(cog.cogs,0) FROM (select distinct catego.id id_categoria, catego.categoria, mess.id mes, ISNULL(sum(distinct fcp.cogs),0) cogs \n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk  INNER JOIN MES_SOP mess ON mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk  where fc.marca_fk IN " + marcas + " and "
					+ esActual + " fcp.mes_sop = (ms.id-1) AND\n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6) AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) cog)\n"
					+ "  END cogs_anterior,\n" + "  CASE WHEN ms.id = ?2\n"
					+ "  THEN (SELECT ISNULL(forc.forecast_gerente,0) FROM (select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum( ROUND(fcp.forecast_gerente,0)),0) forecast_gerente \n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fc.mes_fk AND mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk where fc.marca_fk IN " + marcas
					+ " and fc.mes_fk  = ?4 AND \n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6)  AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre ) forc)\n"
					+ "  ELSE (SELECT ISNULL(forc.forecast_gerente,0) FROM (select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum( ROUND(fcp.forecast_gerente,0)),0) forecast_gerente\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk where fc.marca_fk IN " + marcas + " and "
					+ esActual + " fcp.mes_sop = (ms.id-1) AND  \n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6) AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) forc)\n"
					+ "  END forecast_gerente_anterior,\n" + "  CASE WHEN ms.id = ?2\n"
					+ "  THEN (SELECT ISNULL(tot.total_forecast_gerente,0) FROM(select distinct catego.id id_categoria,catego.categoria, mess.id mes,ISNULL(sum(fcp.total_forecast_gerente),0)  total_forecast_gerente\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fc.mes_fk AND mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk where fc.marca_fk IN " + marcas
					+ " and   fc.mes_fk  = ?4 AND \n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6) AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) tot)\n"
					+ "  ELSE (SELECT ISNULL(tot.total_forecast_gerente,0) FROM (select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum(fcp.total_forecast_gerente),0) total_forecast_gerente\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk  where fc.marca_fk IN " + marcas + " and "
					+ esActual + " fcp.mes_sop = (ms.id-1) AND\n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6) AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre)tot)\n"
					+ "  END total_forecast_gerente_anterior,\n" + "  ms.nombre,   pr.marca_cat2_fk \n" + "   FROM \n"
					+ "   FORECAST fc\n" + "   INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id \n"
					+ "   INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk                       \n"
					+ "   INNER JOIN I_VIEW_CATEGORIA cat ON cat.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk\n"
					+ "   INNER JOIN MES_SOP ms ON ms.id=fcp.mes_sop\n"
					+ "     INNER JOIN COMPANIA c ON c.id=fc.compania_fk                        WHERE \n"
					+ "       fc.marca_fk IN " + marcas + "  AND \n" + esActual + "\n"
					+ "       (fc.estado_fk=?5 or fc.estado_fk=?6)\n" + "\n"
					+ "      GROUP BY cat.id,cat.categoria,fcp.mes_sop,ms.nombre,ms.id,      pr.marca_cat2_fk\n"
					+ "   ORDER BY cat.id,ms.id";

		} else {

			queryStr = "SELECT \n" + "   cat.id id_categoria,    cat.categoria, \n" + "   fcp.mes_sop mes_actual, \n"
					+ "   ISNULL(sum(distinct fcp.precio_retailer),0) precio_retailer_actual, \n"
					+ "   ISNULL(sum(distinct fcp.cogs),0) cogs_actual, \n"
					+ "   sum(fcp.forecast_gerente) forecast_gerente_actual, \n"
					+ "   ISNULL(sum(fcp.total_forecast_gerente),0) total_forecast_gerente_actual, \n"
					+ "   ISNULL(sum(fcp.margen_gerente),0) margen_gerente_actual,  \n"
					+ "   fcp.mes_sop-1 mes_anterior,\n" + "   CASE WHEN ms.id = ?2\n"
					+ "  THEN (SELECT ISNULL(precio.precio_retailer,0) FROM (\n"
					+ "   select distinct  catego.id id_categoria, catego.categoria, mess.id mes, ISNULL(sum(distinct fcp.precio_retailer),0) precio_retailer\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fc.mes_fk AND mess.id = fcp.mes_sop INNER JOIN COMPANIA c ON c.id=fc.compania_fk \n"
					+ "   where fc.marca_fk IN " + marcas
					+ " and  fc.mes_fk  = ?4 AND fc.compania_fk = ?3 AND (fc.estado_fk=?5 or fc.estado_fk=?6) AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) precio)\n"
					+ "  ELSE (SELECT ISNULL(precio.precio_retailer,0) FROM(\n"
					+ "   select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum(distinct fcp.precio_retailer),0) precio_retailer\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk\n"
					+ "   INNER JOIN MES_SOP mess ON mess.id = fcp.mes_sop INNER JOIN COMPANIA c ON c.id=fc.compania_fk \n"
					+ "   where fc.marca_fk IN " + marcas + " and " + esActual
					+ " fcp.mes_sop = (ms.id-1) AND fc.compania_fk = ?3 AND (fc.estado_fk=?5 or fc.estado_fk=?6) AND catego.id = cat.id\n"
					+ "   GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) precio)\n"
					+ "  END precio_retailer_anterior,\n" + "  CASE WHEN ms.id = ?2\n"
					+ "  THEN (SELECT ISNULL(cog.cogs,0) FROM ( select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum(distinct fcp.cogs),0) cogs \n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fc.mes_fk AND mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk where fc.marca_fk IN " + marcas + " and  \n"
					+ "   fc.mes_fk  = ?4 AND (fc.estado_fk=?5 or fc.estado_fk=?6) AND fc.compania_fk = ?3 AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id) cog)\n"
					+ "  ELSE (SELECT ISNULL(cog.cogs,0) FROM (select distinct catego.id id_categoria, catego.categoria, mess.id mes, ISNULL(sum(distinct fcp.cogs),0) cogs \n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk  INNER JOIN MES_SOP mess ON mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk  where fc.marca_fk IN " + marcas + " and "
					+ esActual + " fcp.mes_sop = (ms.id-1) AND\n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6) AND fc.compania_fk = ?3 AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) cog)\n"
					+ "  END cogs_anterior,\n" + "  CASE WHEN ms.id = ?2\n"
					+ "  THEN (SELECT ISNULL(forc.forecast_gerente,0) FROM (select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum( ROUND(fcp.forecast_gerente,0)),0) forecast_gerente \n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fc.mes_fk AND mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk where fc.marca_fk IN " + marcas
					+ " and fc.mes_fk  = ?4 AND \n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6)  AND fc.compania_fk = ?3 AND  catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre ) forc)\n"
					+ "  ELSE (SELECT ISNULL(forc.forecast_gerente,0) FROM (select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum( ROUND(fcp.forecast_gerente,0)),0) forecast_gerente\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk where fc.marca_fk IN " + marcas + " and "
					+ esActual + " fcp.mes_sop = (ms.id-1) AND  \n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6) AND fc.compania_fk = ?3 AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) forc)\n"
					+ "  END forecast_gerente_anterior,\n" + "  CASE WHEN ms.id = ?2\n"
					+ "  THEN (SELECT ISNULL(tot.total_forecast_gerente,0) FROM(select distinct catego.id id_categoria,catego.categoria, mess.id mes,ISNULL(sum(fcp.total_forecast_gerente),0)  total_forecast_gerente\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fc.mes_fk AND mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk where fc.marca_fk IN " + marcas
					+ " and   fc.mes_fk  = ?4 AND \n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6) AND fc.compania_fk = ?3 AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre) tot)\n"
					+ "  ELSE (SELECT ISNULL(tot.total_forecast_gerente,0) FROM (select distinct catego.id id_categoria,catego.categoria,mess.id mes,ISNULL(sum(fcp.total_forecast_gerente),0) total_forecast_gerente\n"
					+ "   from FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk INNER JOIN I_VIEW_CATEGORIA catego ON catego.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk INNER JOIN MES_SOP mess ON mess.id = fcp.mes_sop\n"
					+ "   INNER JOIN COMPANIA c ON c.id=fc.compania_fk  where fc.marca_fk IN " + marcas + " and "
					+ esActual + " fcp.mes_sop = (ms.id-1) AND\n"
					+ "   (fc.estado_fk=?5 or fc.estado_fk=?6) AND fc.compania_fk = ?3 AND catego.id = cat.id GROUP BY catego.id,catego.categoria, mess.id, mess.nombre)tot)\n"
					+ "  END total_forecast_gerente_anterior,     ms.nombre,     pr.marca_cat2_fk \n"
					+ "     FROM        FORECAST fc\n"
					+ "       INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id \n"
					+ "       INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk\n"
					+ "       INNER JOIN I_VIEW_CATEGORIA cat ON cat.id = pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk\n"
					+ "       INNER JOIN MES_SOP ms ON ms.id=fcp.mes_sop\n"
					+ "       INNER JOIN COMPANIA c ON c.id=fc.compania_fk          WHERE \n" + "       fc.marca_fk IN "
					+ marcas + " AND \n" + esActual + "\n"
					+ "       fc.compania_fk = ?3 AND       (fc.estado_fk=?5 or fc.estado_fk=?6)\n" + "\n"
					+ "     GROUP BY cat.id,cat.categoria,fcp.mes_sop,ms.nombre,ms.id,       pr.marca_cat2_fk \n"
					+ "     ORDER BY cat.id, ms.id";

		}

		Query query = em.createNativeQuery(queryStr);
		query.setParameter(2, mesSeleccionado.getId());

		if (companiaSeleccionada != null) {
			query.setParameter(3, companiaSeleccionada.getId());
		}

		MesSop mesAnterior = null;
		if (mesSeleccionado.getMes().equals(1)) {
			// Es enero, consulta diciembre del año anterior
			mesAnterior = moduloGeneralEJB.consultarMesSOP(12, mesSeleccionado.getAnio() - 1);
		} else {
			mesAnterior = moduloGeneralEJB.consultarMesSOP(mesSeleccionado.getMes() - 1, mesSeleccionado.getAnio());
		}

		if (mesAnterior == null) {
			throw new ExcepcionSOP("No existe en el sistema un mes anterior a " + mesSeleccionado.getNombre() + " de "
					+ mesSeleccionado.getAnio());
		}

		query.setParameter(4, mesAnterior.getId());
		query.setParameter(5, ConstantesDemand.EST_FORECAST_CNFG);
		query.setParameter(6, ConstantesDemand.EST_FORECAST_CNFD);

		if (control) {
			query.setParameter(7, mesSeleccionado.getId() + 6L);// Parametro de meses
		}
		List<Object[]> resultados = query.getResultList();
		List<String> marcaQuery = new ArrayList<>();

		List<ItemForecastRetailerVO> listaResultado = new ArrayList<>();

		if (!resultados.isEmpty()) {
			for (Object[] obj : resultados) {

				ItemForecastRetailerVO item = new ItemForecastRetailerVO();

				item.setIdCategoria((String) obj[0]);
				item.setCategoriaProducto((String) obj[1]);
				item.setIdMes(obj[2] != null ? ((BigDecimal) obj[2]).longValue() : 0L);
				item.setPrecioRetailer(obj[3] != null ? ((BigDecimal) obj[3]) : BigDecimal.ZERO);
				item.setCogs(obj[4] != null ? ((BigDecimal) obj[4]) : BigDecimal.ZERO);
				item.setForecastGerente(obj[5] != null ? ((BigDecimal) obj[5]).longValue() : 0L);
				item.setTotalForecastGerente(obj[6] != null ? ((BigDecimal) obj[6]) : BigDecimal.ZERO);
				item.setMargenGerente(obj[7] != null ? ((BigDecimal) obj[7]) : BigDecimal.ZERO);
				item.setMargen(obj[7] != null ? ((BigDecimal) obj[7]) : BigDecimal.ZERO);

				item.setMesAnterior(obj[8] != null ? ((BigDecimal) obj[8]).longValue() : 0L);
				item.setPrecioRetailerAnterior(obj[9] != null ? (BigDecimal) obj[9] : BigDecimal.ZERO);
				item.setCogsAnterior(obj[10] != null ? (BigDecimal) obj[10] : BigDecimal.ZERO);
				item.setForecastGerenteAnterior(obj[11] != null ? ((BigDecimal) obj[11]).longValue() : 0L);
				item.setTotalForecastGerenteAnterior(obj[12] != null ? (BigDecimal) obj[12] : BigDecimal.ZERO);
				item.setNombre((String) obj[13]);
				item.setIdMarca((String) obj[14]);
				item.setMarcaCategoria((String) obj[14]);
				item.setEsTotalMarca(false);

				if (!marcaQuery.contains(item.getIdMarca()))
					marcaQuery.add(item.getIdMarca());

				item.setEsReal(item.getIdMes().equals(mesSeleccionado.getId()));

				listaResultado.add(item);
			}

			return totalizarMarcaNivelCompania(listaResultado, mesSeleccionado, 7, marcaQuery, null, true);
		} else {
			return listaResultado;
		}
	}

	/**
	 * Confirma todos los forecast una compañía o de todas las compañioas de SOP por
	 * el Director
	 *
	 * @param compania
	 * @param marcas
	 * @param mesSeleccionado
	 * @param usuario
	 * @param estadoForecast
	 * @param listMarcass
	 * @param mesesForecastParametro
	 * @throws ExcepcionSOP
	 */
	public void guardarForecastGeneral(Compania compania, String marcas, MesSop mesSeleccionado, UsuarioVO usuario,
			String estadoForecast, List<String> listMarcass, int mesesForecastParametro, String idGrupoMarcas)
			throws ExcepcionSOP {

		List<Forecast> forecast;
		log.info("forecast string al grabar:{} ", estadoForecast);
		forecast = moduloGeneralEJB.consultarForecast(marcas, mesSeleccionado, null);
		for (Forecast objForecast : forecast) {
			confirmarForecastDirector(objForecast, null, usuario);
		}
	}

	/**
	 * Pasa a estado Devuelto los forecast asociados a la Compañía para que el
	 * Gerente puedas editarlos
	 *
	 * @param compania
	 * @param marcas
	 * @param mesSeleccionado
	 * @param usuario
	 * @param listMarcas
	 * @param mesesForecastParametro
	 * @param grupoSeleccionadoIde
	 * @param oForecastMes
	 * @throws ExcepcionSOP
	 */
	public void devolverForecastGeneral(Compania compania, String marcas, MesSop mesSeleccionado, UsuarioVO usuario,
			List<String> listMarcas, int mesesForecastParametro, String grupoSeleccionadoIde,
			ForecastXMesVO oForecastMes) throws ExcepcionSOP {

		List<Forecast> forecast = moduloGeneralEJB.consultarForecast(marcas, mesSeleccionado, compania);

		for (Forecast oForecast : forecast) {
			devolverForecast(oForecast, null, usuario);
		}

		List<Integer> idGrupo = new ArrayList<>();

		for (ProductosForecastMesVO item : oForecastMes.getProductosForecast()) {
			if (item.isEsTotalMarca() && !item.getIdProducto().equals("00")) {
				String[] division = item.getIdProducto().split("-");
				GrupoMarca gMarcas = moduloGeneralEJB.getGrupoByMarca(division[0]);
				if (idGrupo.contains(gMarcas.getId())) {
					continue;
				}
				log.info("ENVIA EMAIL DEVUELTO AAA ::: " + gMarcas.getNombre());
				idGrupo.add(gMarcas.getId());
			}
		}
	}

	/**
	 * 
	 * @param forecast
	 * @param productosForecast
	 * @param usuario
	 * @return
	 * @throws ExcepcionSOP
	 */
	private Forecast confirmarForecastDirector(Forecast forecast, List<ItemForecastRetailerVO> productosForecast,
			UsuarioVO usuario) throws ExcepcionSOP {
		EstadoForecast estadoForecast;

		if (forecast == null) {
			throw new ExcepcionSOP("No es posible actualizar este Forecast, debe ser creado previamente por el Kam",
					null);
		}

		estadoForecast = forecast.getEstadoFk();
		if (estadoForecast == null) {
			throw new ExcepcionSOP("El Forecast " + forecast.getId() + " no tiene un estado asociado ", null);
		}

		if (!estadoForecast.getCodigo().equals(ConstantesDemand.EST_FORECAST_CNFG)) {
			throw new ExcepcionSOP("El forecast " + forecast.getId()
					+ " debe estar en estado Confirmado por el Gerente para poder ser Confirmado por el Director",
					null);
		}

		int mesActualInt = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "MM");
		int anioActual = FuncionesUtilitariasDemand.obtenerDiaMesAnio(new Date(), "YYYY");
		MesSop mes = forecast.getMesFk();
		if ((mes.getAnio() > anioActual) || ((mes.getAnio() == anioActual) && (mes.getMes() > mesActualInt))) {
			throw new ExcepcionSOP("No es posible Confirmar el Forecast de un mes superior al actual ", null);

		}

		estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_CNFD);
		if (estadoForecast == null) {
			throw new ExcepcionSOP("No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_CNFD, null);
		}

		forecast.setEstadoFk(estadoForecast);
		forecast.setFechaModifica(new Date());
		forecast.setUsuarioModifica(usuario.getUserName());

		forecast = em.merge(forecast);

		return forecast;
	}

	/**
	 * 
	 * @param forecast
	 * @param productosForecast
	 * @param usuario
	 * @return
	 * @throws ExcepcionSOP
	 */
	private Forecast devolverForecast(Forecast forecast, List<ItemForecastRetailerVO> productosForecast,
			UsuarioVO usuario) throws ExcepcionSOP {
		EstadoForecast estadoForecast;

		if (forecast == null) {
			throw new ExcepcionSOP("No es posible devolver este Forecast, debe ser creado previamente por el Kam",
					null);
		}

		estadoForecast = forecast.getEstadoFk();
		if (estadoForecast == null) {
			throw new ExcepcionSOP("El Forecast " + forecast.getId() + " no tiene un estado asociado ", null);

		}

		estadoForecast = em.find(EstadoForecast.class, ConstantesDemand.EST_FORECAST_DEVL);
		if (estadoForecast == null) {
			throw new ExcepcionSOP("No existe el estado forecast: " + ConstantesDemand.EST_FORECAST_DEVL, null);
		}

		forecast.setEstadoFk(estadoForecast);
		forecast.setForecastCompania("true");

		forecast.setFechaModifica(new Date());
		forecast.setUsuarioModifica(usuario.getUserName());

		forecast = em.merge(forecast);
		return forecast;
	}

	/**
	 * Consulta retailer 'otros' por compañia seleccionada
	 *
	 * @param companiaSel
	 * @return
	 */
	private Retailer consultarRetailerOtrosPorCompania(Compania companiaSel) {
		Retailer retailer = null;

		try {
			Query query = em.createQuery(
					"SELECT r FROM Retailer r WHERE r.nombreComercial like :nombre AND r.companiaFk = :compania");
			query.setParameter("nombre", "%otros%");
			query.setParameter("compania", companiaSel);
			retailer = (Retailer) query.getSingleResult();
		} catch (Exception e) {
			log.error("Error en la consulta de retailer otros", e);
		}
		return retailer;
	}

	/**
	 * Consulta retailer 'otros' por compañia seleccionada
	 *
	 * @param companiaSel
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Retailer consultarRetaOtrosComp(Compania companiaSel) {
		List<Retailer> retailer;

		try {
			Query query = em.createQuery(
					"SELECT r FROM Retailer r WHERE r.nombreComercial like :nombre AND r.companiaFk.id = :compania ");
			query.setParameter("nombre", "%otros%");
			query.setParameter("compania", companiaSel.getId());
			retailer = query.getResultList();
			if (!retailer.isEmpty()) {
				return retailer.get(0);
			}
		} catch (Exception e) {
			log.error("Error en la consulta de retailer otros", e);
		}
		return null;
	}

	/**
	 * Metodo que valida si la compania seleccionada es un CLUSTERS o un BORDERS
	 * 00072-->CLU = CLUSTERS y 00074-->BRD = BORDERS
	 *
	 * @since 21-10-2016 Jose Avila
	 * @param inCompaniaId
	 * @return sql
	 */
	public String validarCompaniaXRetailers(String inCompaniaId) {

		String sql = "";

		if (inCompaniaId.equals("00074")) {
			inCompaniaId = "00070";
			sql = " AND ret.compania_fk = '" + inCompaniaId + "' AND ret.canal_distribucion ='BRD' ";
		} else if (inCompaniaId.equals("00072")) {
			inCompaniaId = "00070";
			sql = " AND ret.compania_fk = '" + inCompaniaId + "' AND ret.canal_distribucion !='BRD' ";
		}

		return sql;
	}

	/**
	 * Consulta retailer 'otros' por compañia seleccionada
	 *
	 * @param companiaSel
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Retailer consultarRetailerOtrosForecastRetailer(Compania oCompany) {
		List<Retailer> listaRetailer;

		try {
			Query query = em.createQuery(
					"SELECT r FROM Retailer r WHERE r.nombreComercial like :nombre AND r.companiaFk = :compania ");
			query.setParameter("nombre", "%otros%");
			query.setParameter("compania", oCompany);
			listaRetailer = query.getResultList();
			if (!listaRetailer.isEmpty()) {
				return listaRetailer.get(0);
			}
		} catch (Exception e) {
			log.error("Error en la consulta de retailer otros", e);
		}
		return null;
	}

	/**
	 * 
	 * @param mes
	 * @param marca
	 * @param bodegaCompania
	 * @param esProducto
	 * @param categoria
	 * @param isElevacion
	 * @return
	 */
	private String consultarForecastRetailerCerrado(MesSop mes, String marca, boolean esProducto, String categoria,
			boolean isElevacion) {

		Calendar fecha = Calendar.getInstance();
		int anioActual = fecha.get(Calendar.YEAR);
		int mesActual = fecha.get(Calendar.MONTH) + 1;
		String mesesFuturos = "";
		String estadosCerradoRetailer = "";
		if (mesActual == mes.getMes() && anioActual == mes.getAnio()) {
			mesesFuturos = "fcp.mes_sop >= ?3 AND fcp.mes_sop < ?4 AND \n" + "fc.mes_fk = ?3";
		} else {
			mesesFuturos = "fc.mes_fk = ?3";
		}

		if (!isElevacion) {
			estadosCerradoRetailer = " AND fc.estado_fk IN ('ELSE','CNFG', 'CNFD', 'CERR', 'DEVL') ";
		}

		String qlString;

		if (esProducto) {
			qlString = " SELECT pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria,\n"
					+ " pr.id prod, pr.descripcion categoria_producto, pr.descripcion categoria_nombre,\n"
					+ " ROUND(ISNULL(fcp.sellin_sugerido,0),0) sellin_sug,\n"
					+ " ISNULL(fcp.precio_retailer,0) precio_retailer,\n"
					+ " ISNULL(fcp.total_sellin_sugerido,0) total_propuesto, ISNULL(fcp.cogs,0) cogs,\n"
					+ " '0' margen_sugerido, fcp.forecast_fk forecast_id, fcp.id forecast_prod_id,\n"
					+ " fcp.forecast_kam forecast_kam, ISNULL(fcp.total_forecast_kam,0) total_forecast_kam,\n"
					+ " ISNULL(fcp.margen_kam,0) margen_kam, fcp.forecast_gerente, '0' id_prod_ret_mes,\n"
					+ " fcp.mes_sop, mes.nombre, fc.marca_fk,  fc.retailer_fk, \n"
					+ " fc.compania_fk,  '0' precio_sin_tasa," + "fcp.inventario inventario_wh, \n"
					+ "ISNULL(fma.forecast_gerente ,0)gerente_mes_anterior, \n" + "pr.marca_cat2_fk marca_categ, \n"
					+ "fcp.transito, \n" + "pr.UPC, \n" + "pr.modelo, \n"
					+ "ISNULL((SELECT TOP(1) pc.estado_des FROM PRODUCTO_X_COMPANIA pc WHERE pc.producto_fk = pr.id AND compania_fk = ?6),NULL) estadoDesc, \n"
					+ "fcp.estadoProducto, " + "pr.flag_estado, \n" + "fcp.editar_kam, \n"
					+ " (select nombre from GRUPO_CAT4 where id= pr.grupo_cat4_fk) grupo,  \n"
					+ " (select nombre from PLATAFORMA_CAT5 where id= pr.plataforma_cat5_fk) plataforma \n"
					+ " FROM FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id\n"
					+ " INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk\n"
					+ " INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop\n" + "LEFT JOIN (   \n"
					+ "SELECT fp.forecast_gerente, fp.producto_fk, fp.mes_sop from FORECAST_PRODUCTO fp\n"
					+ " inner join FORECAST fc ON fc.id = fp.forecast_fk  where fc.mes_fk = ?3 -1 and\n"
					+ " fc.retailer_fk IN ?1 and  fc.marca_fk IN  " + marca + "     \n"
					+ ")fma ON fma.producto_fk = pr.id AND fma.mes_sop = fcp.mes_sop" + " WHERE \n"
					+ " fc.retailer_fk IN ?1 AND fc.marca_fk IN " + marca + " AND\n" + mesesFuturos + " "
					+ estadosCerradoRetailer + " and pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk = '"
					+ categoria + "' \n " + " ORDER BY pr.modelo ASC, id_categoria, fcp.mes_sop asc  \n";

		} else {
			qlString = "SELECT pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk id_categoria, '0' prod,\n"
					+ " mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre categoria_producto,pl.nombre categoria_nombre,\n"
					+ " SUM(ROUND(ISNULL(fcp.sellin_sugerido,0),0)) sellin_sug, '0' precio_retailer,\n"
					+ " SUM(ISNULL(fcp.total_sellin_sugerido,0)) total_propuesto, '0' cogs,\n"
					+ " '0' margen_sugerido, '0' forecast_id, '0' forecast_prod_id,\n"
					+ " SUM(ISNULL(fcp.forecast_kam,0)) forecast_kam,\n"
					+ " SUM(ISNULL(fcp.total_forecast_kam,0)) total_forecast_kam,\n"
					+ " SUM(ISNULL(fcp.margen_kam,0)) margen_kam, SUM(fcp.forecast_gerente) forecast_gerente,\n"
					+ " '0' id_prod_ret_mes, fcp.mes_sop, mes.nombre, '0' marca_fk," + " fc.retailer_fk retailer_fk,"
					+ " '0' compania_fk," + " '0' precio_sin_tasa," + "SUM(fcp.inventario) inventario_wh, "
					+ "SUM(fma.forecast_gerente ) gerente_mes_anterior, " + "pr.marca_cat2_fk marca_categ, \n"
					+ "SUM(fcp.transito) transito \n" + "\n"
					+ " FROM FORECAST fc INNER JOIN FORECAST_PRODUCTO fcp ON fcp.forecast_fk = fc.id\n"
					+ " INNER JOIN PRODUCTO pr ON pr.id = fcp.producto_fk\n"
					+ " INNER JOIN MARCA_CAT2 mr ON mr.id = pr.marca_cat2_fk\n"
					+ " INNER JOIN GRUPO_CAT4 gr ON gr.id = pr.grupo_cat4_fk\n"
					+ " INNER JOIN PLATAFORMA_CAT5 pl ON pl.id = pr.plataforma_cat5_fk\n"
					+ " INNER JOIN MES_SOP mes ON mes.id = fcp.mes_sop\n" + "LEFT JOIN (   \n"
					+ " SELECT fp.forecast_gerente, fp.producto_fk, fp.mes_sop, fc.retailer_fk \n"
					+ " from FORECAST_PRODUCTO fp inner join FORECAST fc ON fc.id = fp.forecast_fk \n"
					+ " where fc.mes_fk = ?3 -1 and fc.retailer_fk IN ?1  and  fc.marca_fk IN  " + marca + "   \n"
					+ "\n"
					+ ")fma ON fma.producto_fk = pr.id AND fma.mes_sop = fcp.mes_sop AND fma.retailer_fk = fc.retailer_fk "
					+ " WHERE  fc.retailer_fk IN ?1 AND fc.marca_fk IN " + marca + " AND\n" + mesesFuturos
					+ estadosCerradoRetailer + "\n"
					+ " GROUP BY pr.marca_cat2_fk+pr.grupo_cat4_fk+pr.plataforma_cat5_fk, mr.nombre + ' ' + gr.nombre + ' ' + pl.nombre,pl.nombre, fcp.mes_sop, mes.nombre, pr.marca_cat2_fk, fc.retailer_fk\n"
					+ " ORDER BY id_categoria, retailer_fk, mes_sop, prod\n";
		}
		return qlString;
	}

	/***
	 * 
	 * @return
	 */
	@SuppressWarnings("null")
	public int consultarParametroMeses() {
		Integer parametroValor = null;
		try {
			Query query = em.createNativeQuery("SELECT valor FROM parametro WHERE nombre = 'MESES_FORECAST' ");
			return Integer.valueOf((String) query.getSingleResult());
		} catch (Exception e) {
			log.error("Error en la consulta de retailer otros", e);
		}
		return parametroValor;
	}

	@SuppressWarnings("unchecked")
	/**
	 * 
	 * @param mesesForecastParametro
	 * @param id
	 * @return
	 */
	public List<String> consultarNombresMeses(int mesesForecastParametro, Long id) {
		List<String> nombresMeses = null;
		try {
			Query query = em.createNativeQuery("SELECT m.nombre FROM Mes_Sop m WHERE (m.id >= ?1 AND m.id < ?2) ");

			query.setParameter(1, id);
			query.setParameter(2, id + mesesForecastParametro);

			nombresMeses = query.getResultList();
		} catch (Exception e) {
			log.error("Error en la consulta de retailer otros", e);
		}
		return nombresMeses;
	}

	/**
	 * 
	 * @param forecast
	 * @param estadoS
	 * @param usuario
	 * @param nombreMeses
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardarForecastMeses(ForecastXMesVO forecast, String estadoS, UsuarioVO usuario,
			List<String> nombreMeses) {

		Forecast forecastDB;
		EstadoForecast estado;

		for (MarcaGrupoForecast objGrupoMarcas : forecast.getMarcaGrupoForecast()) {
			if (objGrupoMarcas.getIdForecast() != null && objGrupoMarcas.getIdForecast().intValue() != 0) {
				TypedQuery<Forecast> q = em.createQuery(SELECT_FORECAST_ID, Forecast.class);
				q.setParameter("id", objGrupoMarcas.getIdForecast());
				q.setMaxResults(1);
				forecastDB = q.getSingleResult();

				forecastDB.setFechaModifica(new Date());
				forecastDB.setUsuarioModifica(usuario.getUserName());
				forecastDB.setForecastCompania(String.valueOf(forecast.isFueProcesadoXCompania()));
			} else {

				forecastDB = new Forecast();

				forecastDB.setCompaniaFk(new Compania(forecast.getCompania()));
				forecastDB.setFechaCrea(new Date());
				forecastDB.setMarcaFk(new MarcaCat2(objGrupoMarcas.getIdMarca()));
				forecastDB.setMesFk(new MesSop(forecast.getIdMes()));
				forecastDB.setRetailerFk(new Retailer(forecast.getRetailer()));
				forecastDB.setUsuarioCrea(usuario.getUserName());
				forecastDB.setForecastCompania(String.valueOf(forecast.isFueProcesadoXCompania()));

			}

			estado = new EstadoForecast(estadoS);

			forecastDB.setEstadoFk(estado);

			try {
				forecastDB = em.merge(forecastDB);
				em.flush();

				if (!estadoS.equals(ConstantesDemand.EST_FORECAST_CNFG)) {

					Query queryDelete = em
							.createNativeQuery("delete from FORECAST_PRODUCTO where forecast_fk=" + forecastDB.getId());
					queryDelete.executeUpdate();
					em.flush();

					for (ProductosForecastMesVO productoMes : forecast.getProductosForecast()) {

						if (!productoMes.isEsProdcuto()) {
							continue;
						}

						for (String mes : nombreMeses) {
							if (productoMes.getIdMarca().equals(objGrupoMarcas.getIdMarca())) {
								ForecastProducto forecastProductoDB = new ForecastProducto();

								if (productoMes.getInformacionForecastMes().get(mes).getIdForecastProducto() != null) {
									forecastProductoDB.setId(
											productoMes.getInformacionForecastMes().get(mes).getIdForecastProducto());
								}
								forecastProductoDB.setCogs(productoMes.getInformacionForecastMes().get(mes).getCogs());
								forecastProductoDB
										.setForecastKam(productoMes.getInformacionForecastMes().get(mes).getForecast());
								forecastProductoDB
										.setMargenKam(productoMes.getInformacionForecastMes().get(mes).getMargen());
								forecastProductoDB.setMesSop(
										new MesSop(productoMes.getInformacionForecastMes().get(mes).getIdMes()));
								forecastProductoDB.setPrecioRetailer(
										productoMes.getInformacionForecastMes().get(mes).getPrecioRetailer());
								forecastProductoDB.setProductoFk(new Producto(productoMes.getIdProducto()));
								forecastProductoDB.setSellinSugerido(
										productoMes.getInformacionForecastMes().get(mes).getSellInPropuesto());
								forecastProductoDB.setTotalforcastkam(
										productoMes.getInformacionForecastMes().get(mes).getTotalForecastKam());
								forecastProductoDB.setTotalsellinSugerido(
										productoMes.getInformacionForecastMes().get(mes).getTotalPropuesto());
								forecastProductoDB.setForecastFk(new Forecast(forecastDB.getId()));
								forecastProductoDB.setForecastGerente(
										productoMes.getInformacionForecastMes().get(mes).getForecastGerente() == null
												|| productoMes.getInformacionForecastMes().get(mes).getForecastGerente()
														.intValue() == 0
																? productoMes.getInformacionForecastMes().get(mes)
																		.getForecast()
																: productoMes.getInformacionForecastMes().get(mes)
																		.getForecastGerente());
								forecastProductoDB.setTotalForecastGerente(productoMes.getInformacionForecastMes()
										.get(mes).getTotalForecastGerente() == null
										|| productoMes.getInformacionForecastMes().get(mes).getTotalForecastGerente()
												.intValue() == 0
														? productoMes.getInformacionForecastMes().get(mes)
																.getTotalForecastKam()
														: productoMes.getInformacionForecastMes().get(mes)
																.getTotalForecastGerente());
								forecastProductoDB.setMargenGerente(
										productoMes.getInformacionForecastMes().get(mes).getMargenGerente() == null
												|| productoMes.getInformacionForecastMes().get(mes).getMargenGerente()
														.intValue() == 0
																? productoMes.getInformacionForecastMes().get(mes)
																		.getMargen()
																: productoMes.getInformacionForecastMes().get(mes)
																		.getMargenGerente());

								em.merge(forecastProductoDB);
								@SuppressWarnings("unused")
								ForecastProducto fp = new ForecastProducto();
								if (null != forecastProductoDB.getId()) {
									fp = em.find(ForecastProducto.class, forecastProductoDB.getId());
								}
							}
						}

					}
				}

			} catch (Exception e) {
				log.error("Error guardando forecast", e);
			}
		}

	}

	/**
	 * 
	 * @param forecast
	 * @param estadoS
	 * @param usuario
	 * @param nombreMeses
	 * @param productoCategoria
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardarForecastMeses(ForecastXMesVO forecast, String estadoS, UsuarioVO usuario,
			List<String> nombreMeses, Map<String, ForecastXMesVO> productoCategoria) {

		Forecast forecastDB = new Forecast();
		EstadoForecast estado;

		for (RetailerGrupoForecast objGrupoRetailer : forecast.getRetailerGrupoForecast()) {
			for (MarcaGrupoForecast objGrupoMarcas : forecast.getMarcaGrupoForecast()) {
				for (Map.Entry<String, ForecastXMesVO> entry : productoCategoria.entrySet()) {
					if (objGrupoMarcas.getIdMarca().equals(entry.getValue().getMarca())
							&& objGrupoRetailer.getIdRetailer().equals(entry.getValue().getRetailer())) {
						if (entry.getValue().getIdForecast() != null
								&& entry.getValue().getIdForecast().intValue() != 0) {
							TypedQuery<Forecast> q = em.createQuery(SELECT_FORECAST_ID, Forecast.class);
							q.setParameter("id", entry.getValue().getIdForecast());
							q.setMaxResults(1);
							forecastDB = q.getSingleResult();

							forecastDB.setFechaModifica(new Date());
							forecastDB.setUsuarioModifica(usuario.getUserName());
							forecastDB.setForecastCompania(String.valueOf(entry.getValue().isFueProcesadoXCompania()));

							if (!forecastDB.getEstadoFk().getCodigo().equals(ConstantesDemand.EST_FORECAST_DEVL_KAM)) {
								estado = new EstadoForecast(estadoS);
								forecastDB.setEstadoFk(estado);
							}
						} else {

							forecastDB = new Forecast();

							forecastDB.setCompaniaFk(new Compania(entry.getValue().getCompania()));
							forecastDB.setFechaCrea(new Date());
							forecastDB.setMarcaFk(new MarcaCat2(objGrupoMarcas.getIdMarca()));
							forecastDB.setMesFk(new MesSop(entry.getValue().getIdMes()));
							forecastDB.setRetailerFk(new Retailer(entry.getValue().getRetailer()));
							forecastDB.setUsuarioCrea(usuario.getUserName());
							forecastDB.setForecastCompania(String.valueOf(entry.getValue().isFueProcesadoXCompania()));

							estado = new EstadoForecast(estadoS);
							forecastDB.setEstadoFk(estado);

						}
						break;
					}
				}

				try {
					forecastDB = em.merge(forecastDB);
					em.flush();

					if (!estadoS.equals(ConstantesDemand.EST_FORECAST_CNFG)) {
						for (Map.Entry<String, ForecastXMesVO> entry : productoCategoria.entrySet()) {
							if (objGrupoMarcas.getIdMarca().equals(entry.getValue().getMarca())
									&& objGrupoRetailer.getIdRetailer().equals(entry.getValue().getRetailer())) {
								for (ProductosForecastMesVO productoMes : entry.getValue().getProductosForecast()) {
									for (String mes : nombreMeses) {
										if (productoMes.getIdMarca().equals(objGrupoMarcas.getIdMarca()) && productoMes
												.getIdRetailer().equals(objGrupoRetailer.getIdRetailer())) {
											ForecastProducto forecastProductoDB = new ForecastProducto();

											if (productoMes.getInformacionForecastMes().get(mes)
													.getIdForecastProducto() != null) {
												forecastProductoDB.setId(productoMes.getInformacionForecastMes()
														.get(mes).getIdForecastProducto());
											}
											forecastProductoDB.setCogs(
													productoMes.getInformacionForecastMes().get(mes).getCogs());
											forecastProductoDB.setForecastKam(
													productoMes.getInformacionForecastMes().get(mes).getForecast());
											forecastProductoDB.setMargenKam(
													productoMes.getInformacionForecastMes().get(mes).getMargen());
											forecastProductoDB.setMesSop(new MesSop(
													productoMes.getInformacionForecastMes().get(mes).getIdMes()));
											forecastProductoDB.setPrecioRetailer(productoMes.getInformacionForecastMes()
													.get(mes).getPrecioRetailer());
											forecastProductoDB.setProductoFk(new Producto(productoMes.getIdProducto()));
											forecastProductoDB.setSellinSugerido(productoMes.getInformacionForecastMes()
													.get(mes).getSellInPropuesto());
											forecastProductoDB.setTotalforcastkam(productoMes
													.getInformacionForecastMes().get(mes).getTotalForecastKam());
											forecastProductoDB.setTotalsellinSugerido(productoMes
													.getInformacionForecastMes().get(mes).getTotalPropuesto());
											forecastProductoDB.setForecastFk(new Forecast(forecastDB.getId()));
											if (estadoS.equals(ConstantesDemand.EST_FORECAST_INI)
													|| estadoS.equals(ConstantesDemand.EST_FORECAST_ELSE)) {
												forecastProductoDB.setForecastGerente(
														productoMes.getInformacionForecastMes().get(mes).getForecast());
												forecastProductoDB.setTotalForecastGerente(productoMes
														.getInformacionForecastMes().get(mes).getTotalForecastKam());
											} else {
												forecastProductoDB
														.setForecastGerente(productoMes.getInformacionForecastMes()
																.get(mes).getForecastGerente() == null
																|| productoMes.getInformacionForecastMes().get(mes)
																		.getForecastGerente().intValue() == 0
																				? productoMes
																						.getInformacionForecastMes()
																						.get(mes).getForecast()
																				: productoMes
																						.getInformacionForecastMes()
																						.get(mes).getForecastGerente());
												forecastProductoDB
														.setTotalForecastGerente(productoMes.getInformacionForecastMes()
																.get(mes).getTotalForecastGerente() == null
																|| productoMes.getInformacionForecastMes().get(mes)
																		.getTotalForecastGerente().intValue() == 0
																				? productoMes
																						.getInformacionForecastMes()
																						.get(mes).getTotalForecastKam()
																				: productoMes
																						.getInformacionForecastMes()
																						.get(mes)
																						.getTotalForecastGerente());
											}

											forecastProductoDB.setMargenGerente(productoMes.getInformacionForecastMes()
													.get(mes).getMargenGerente() == null
													|| productoMes.getInformacionForecastMes().get(mes)
															.getMargenGerente().intValue() == 0
																	? productoMes.getInformacionForecastMes().get(mes)
																			.getMargen()
																	: productoMes.getInformacionForecastMes().get(mes)
																			.getMargenGerente());
											forecastProductoDB.setInventarioWH(
													productoMes.getInformacionForecastMes().get(mes).getInventarioWH());
											forecastProductoDB.setTransitoPais(
													productoMes.getInformacionForecastMes().get(mes).getTransito());

											forecastProductoDB.setEstadoProducto(productoMes.getEstadoProducto());
											forecastProductoDB.setEditarKam(
													productoMes.getInformacionForecastMes().get(mes).getEditarKam());
											em.merge(forecastProductoDB);

										}
									}
								}
							}
						}
					}

				} catch (Exception e) {
					log.error("Error guardando forecast", e);
				}
			}
		}
	}

	/**
	 * 
	 * @param forecast
	 * @param companiaSeleccionado
	 * @param idMes
	 */
	public void generarPreciosReatilerOtro(Map<String, ForecastXMesVO> forecast, Compania companiaSeleccionado,
			Long idMes) {

		Retailer retailerOtros = consultarRetailerOtrosForecastRetailer(companiaSeleccionado);
		MesSop mes = em.find(MesSop.class, idMes);
		List<String> prodRetailer = new ArrayList<>();

		for (Map.Entry<String, ForecastXMesVO> entry : forecast.entrySet()) {
			for (ProductosForecastMesVO item : entry.getValue().getProductosForecast()) {
				if (!item.isEsProdcuto()) {
					continue;
				}
				if (prodRetailer.contains(item.getIdProducto())) {
					continue;
				}
				ProductoXRetailerXMes productoXRetailerXMesOtros = new ProductoXRetailerXMes();
				productoXRetailerXMesOtros.setMesFk(mes);
				productoXRetailerXMesOtros.setProductoFk(new Producto(item.getIdProducto()));
				productoXRetailerXMesOtros.setRetailerFk(retailerOtros);
				productoXRetailerXMesOtros.setEtapa_fk(new EstadoForecast(ConstantesDemand.EST_FORECAST_INI));
				productoXRetailerXMesOtros
						.setPrecioRetailer(item.getInformacionForecastMes().get(mes.getNombre()).getPrecioSinTasa());
				BigDecimal srpSinIVA = item.getInformacionForecastMes().get(mes.getNombre()).getSrpSinIva();
				productoXRetailerXMesOtros.setSrpSinIva(srpSinIVA);
				productoXRetailerXMesOtros.setEtapa_fk(new EstadoForecast(ConstantesDemand.EST_FORECAST_ELSE));
				prodRetailer.add(item.getIdProducto());

				try {
					em.persist(productoXRetailerXMesOtros);
					em.flush();

				} catch (Exception e) {
					log.error("Ya existe el producto x mes ");
				}
			}
		}
	}

	/**
	 * Metodo de notificación para Forecast Retailer. Solo se envia la notificación
	 * para el Grupo de Marca y Retailer cerrado.
	 * 
	 * @param mesSeleccionado
	 * @param forecast
	 * @param oCompany
	 */
	public void notificarCreacionForecastRetailer(MesSop mesSeleccionado, ForecastXMesVO forecast, Compania oCompany) {

		List<String> idGrupoRetailer = new ArrayList<>();

		for (ProductosForecastMesVO item : forecast.getProductosForecast()) {
			if (item.isEsTotalMarca() && item.getIdRetailer() != null) {
				String[] division = item.getIdProducto().split("-");
				GrupoMarca gMarcas = moduloGeneralEJB.getGrupoByMarca(division[0]);
				if (idGrupoRetailer.contains(gMarcas.getId() + "-" + item.getIdRetailer())) {
					continue;
				}

				moduloGeneralEJB.notifica(oCompany, gMarcas.getNombre(), Arrays.asList(item.getIdRetailer()),
						mesSeleccionado, "Forecast Retailer - ",
						"Estimado (a) <br /><br /> Proceso Forecast Retailer ha finalizado con fecha: ", "",
						ConstantesDemand.ETAPA_FORECAST_RETAILER, false);
				moduloGeneralEJB.notifica(oCompany, gMarcas.getNombre(), Arrays.asList(item.getIdRetailer()),
						mesSeleccionado, "Forecast Retailer - ",
						"Estimado (a) <br /><br /> Proceso Forecast Retailer ha finalizado con fecha: ", "",
						ConstantesDemand.ETAPA_ELEVACION_SELLIN, true);
				idGrupoRetailer.add(gMarcas.getId() + "-" + item.getIdRetailer());
			}
		}
	}

	/**
	 * Metodo de notificación para Forecast Gerente, solo se envia la notificación
	 * por el Grupo de Marca cerrado.
	 * 
	 * @param forecast
	 * @param grupoMarcaSeleccionadaIde
	 * @param mesSeleccionado
	 * @param companiaSeleccionada
	 */
	public void notificarConfirmacionForecastCompania(ForecastXMesVO forecast, String grupoMarcaSeleccionadaIde,
			MesSop mesSeleccionado, Compania companiaSeleccionada) {

		List<Integer> idGrupoRetailer = new ArrayList<>();

		for (ProductosForecastMesVO item : forecast.getProductosForecast()) {
			if (item.isEsTotalMarca() && !item.getIdProducto().equals("00")) {
				String[] division = item.getIdProducto().split("-");
				GrupoMarca gMarcas = moduloGeneralEJB.getGrupoByMarca(division[0]);
				if (idGrupoRetailer.contains(gMarcas.getId())) {
					continue;
				}

				moduloGeneralEJB.notifica(companiaSeleccionada, gMarcas.getNombre(), null, mesSeleccionado,
						"Forecast Compañía - ",
						"Estimado (a) <br /><br /> Proceso Forecast Compañia ha finalizado con fecha: ",
						"Favor iniciar el proceso Forecast General. ", ConstantesDemand.ETAPA_FORECAST_COMPANIA, true);
				moduloGeneralEJB.notifica(companiaSeleccionada, gMarcas.getNombre(), null, mesSeleccionado,
						"Forecast Compañía - ",
						"Estimado (a) <br /><br /> Proceso Forecast Compañia ha finalizado con fecha: ",
						"Favor iniciar el proceso Forecast General. ", ConstantesDemand.ETAPA_FORECAST_GENERAL, false);
				idGrupoRetailer.add(gMarcas.getId());
			}
		}
	}

	/**
	 * 
	 * @param mesSeleccionado
	 * @param grupoMarcaSeleccionadaIde
	 * @param forecast
	 */
	public void notificarConfirmacionForecastGeneral(MesSop mesSeleccionado, String grupoMarcaSeleccionadaIde,
			ForecastXMesVO forecast) {

		List<Integer> idGrupo = new ArrayList<>();

		for (ProductosForecastMesVO item : forecast.getProductosForecast()) {
			if (item.isEsTotalMarca() && !item.getIdProducto().equals("00")) {
				String[] division = item.getIdProducto().split("-");
				GrupoMarca gMarcas = moduloGeneralEJB.getGrupoByMarca(division[0]);
				if (idGrupo.contains(gMarcas.getId())) {
					continue;
				}
				log.info("ENVIA EMAIL AAA ::: " + gMarcas.getNombre());
				idGrupo.add(gMarcas.getId());
			}
		}
	}

	/**
	 * 
	 * @param companiaSeleccionada
	 * @param mesSeleccionado
	 * @param grupoMarcas
	 * @param isGeneral
	 * @return
	 */
	public boolean consultarForecastCerrado(Compania companiaSeleccionada, MesSop mesSeleccionado, String grupoMarcas,
			boolean isGeneral) {

		String qCompania = "";
		if (companiaSeleccionada != null)
			qCompania = "f.companiaFk = :compania AND";

		String queryStr = "SELECT f FROM Forecast f WHERE " + qCompania + " f.mesFk = :mes " + "and f.marcaFk.id IN "
				+ grupoMarcas + " AND (f.estadoFk.codigo = 'CNFD' "
				+ (!isGeneral ? "OR f.estadoFk.codigo = 'CNFG')" : ")");
		TypedQuery<Forecast> query = em.createQuery(queryStr, Forecast.class);
		if (companiaSeleccionada != null)
			query.setParameter("compania", companiaSeleccionada);
		query.setParameter("mes", mesSeleccionado);

		return !query.getResultList().isEmpty();
	}

	/**
	 * 
	 * @param companiaSeleccionada
	 * @param mesSeleccionado
	 * @param marcaSeleccionada
	 * @param listaRetailers
	 * @return
	 */
	public boolean consultarForecastRetailer(Compania companiaSeleccionada, MesSop mesSeleccionado,
			String marcaSeleccionada, int listaRetailers) {
		TypedQuery<String> query = em.createQuery(
				"SELECT distinct f.retailerFk.id FROM Forecast f WHERE f.companiaFk = :compania AND f.mesFk = :mes "
						+ "and f.marcaFk.id IN " + marcaSeleccionada,
				String.class);
		query.setParameter("compania", companiaSeleccionada);
		query.setParameter("mes", mesSeleccionado);

		return query.getResultList().size() == listaRetailers;
	}

	/**
	 * Metodo para construir y totalizar las filas de Total General. Total a nivel
	 * de marca y el llamado al totalizar retailer. Este metodo solo aplica a las
	 * pantallas a las cuales se les agrega la opción de filtro multiple por
	 * Retailer.
	 * 
	 * @param listaFC
	 * @param mes
	 * @param mesesForecastParametro
	 * @param idMarcas
	 * @param idsRetailer
	 * @param isElevacion
	 * @return
	 */
	private List<ItemForecastRetailerVO> totalizarMarcaNivelCompania(List<ItemForecastRetailerVO> listaFC, MesSop mes,
			int mesesForecastParametro, List<String> idMarcas, List<String> idsRetailer, boolean isElevacion) {

		Long idMesActual = mes.getId();
		Long idMesFinal = mes.getId() + mesesForecastParametro;
		List<ItemForecastRetailerVO> resultado = new ArrayList<>();
		Long sellin = 0L;
		Long forecastKam = 0L;
		Long forecastGerente = 0L;
		Long gerenteAnterior = 0L;
		BigDecimal totalPropuesto = BigDecimal.ZERO;
		BigDecimal totalKam = BigDecimal.ZERO;
		BigDecimal totalInvWH = BigDecimal.ZERO;
		BigDecimal totalGerenteAnterior = BigDecimal.ZERO;
		BigDecimal totalGerente = BigDecimal.ZERO;
		BigDecimal totalTransito = BigDecimal.ZERO;
		int categ = 0;
		boolean existsMarca = false;

		for (String objRetailer : idsRetailer) {
			for (String objMarcas : idMarcas) {
				MarcaCat2 oMarca = moduloGeneralEJB.consultarPorMarca(objMarcas);
				for (Long i = idMesActual; i <= idMesFinal; i++) {
					ItemForecastRetailerVO item = new ItemForecastRetailerVO();
					for (ItemForecastRetailerVO objItems : listaFC) {
						if (!objItems.isEsProducto() && objItems.getIdMes().equals(i)
								&& objItems.getMarcaCategoria().equals(objMarcas)
								&& objItems.getIdRetailer().equals(objRetailer)) {
							sellin = sellin
									+ (objItems.getSellInPropuesto() != null ? objItems.getSellInPropuesto() : 0L);
							forecastKam = forecastKam
									+ (objItems.getForecastKam() != null ? objItems.getForecastKam() : 0L);
							forecastGerente = forecastGerente
									+ (objItems.getForecastGerente() != null ? objItems.getForecastGerente() : 0L);
							totalPropuesto = totalPropuesto
									.add(objItems.getTotalPropuesto() != null ? objItems.getTotalPropuesto()
											: BigDecimal.ZERO);
							totalKam = totalKam
									.add(objItems.getTotalForecastKam() != null ? objItems.getTotalForecastKam()
											: BigDecimal.ZERO);
							totalGerente = totalGerente
									.add(objItems.getTotalForecastGerente() != null ? objItems.getTotalForecastGerente()
											: BigDecimal.ZERO);
							totalInvWH = totalInvWH.add(
									objItems.getInventarioWH() != null ? objItems.getInventarioWH() : BigDecimal.ZERO);
							totalTransito = totalTransito
									.add(objItems.getTransito() != null ? objItems.getTransito() : BigDecimal.ZERO);
							totalGerenteAnterior = totalGerenteAnterior
									.add(objItems.getTotalForecastGerenteAnterior() != null
											? objItems.getTotalForecastGerenteAnterior()
											: BigDecimal.ZERO);
							gerenteAnterior = gerenteAnterior + (objItems.getForecastGerenteAnterior() != null
									? objItems.getForecastGerenteAnterior()
									: 0L);
							item.setNombre(objItems.getNombre());
							existsMarca = true;
						}
					}

					if (!existsMarca) {
						continue;
					}

					item.setIdCategoria(objMarcas);
					item.setIdProducto("0");
					item.setIdRetailer(objRetailer);
					item.setEsProducto(false);
					item.setEsTotalMarca(true);
					item.setCategoriaProducto("Total " + oMarca.getNombre());
					item.setSellInPropuesto(sellin);
					item.setPrecioRetailer(BigDecimal.ZERO);
					item.setTotalPropuesto(totalPropuesto);
					item.setCogs(BigDecimal.ZERO);
					item.setForecastKam(forecastKam);
					item.setTotalForecastKam(totalKam);
					item.setForecastGerente(forecastGerente);
					item.setTotalForecastGerente(totalGerente);
					item.setInventarioWH(totalInvWH);
					item.setTransito(totalTransito);
					item.setTotalForecastGerenteAnterior(totalGerenteAnterior);
					item.setForecastGerenteAnterior(gerenteAnterior);
					item.setMargenGerente(BigDecimal.ZERO);
					item.setIdMes(i);
					item.setIdMarca("0");
					if (item.getIdMes().equals(mes.getId())) {
						item.setEsReal(true);
					} else {
						item.setEsReal(false);
					}

					sellin = 0L;
					forecastKam = 0L;
					forecastGerente = 0L;
					totalPropuesto = BigDecimal.ZERO;
					totalKam = BigDecimal.ZERO;
					totalGerente = BigDecimal.ZERO;
					totalInvWH = BigDecimal.ZERO;
					totalTransito = BigDecimal.ZERO;
					totalGerenteAnterior = BigDecimal.ZERO;
					gerenteAnterior = 0L;
					resultado.add(item);
					existsMarca = false;
				}
				existsMarca = false;
			}
			existsMarca = false;
		}

		List<ItemForecastRetailerVO> totRetailer = totalizarRetailer(resultado, idMesActual, idMesFinal, categ,
				idsRetailer);
		List<ItemForecastRetailerVO> grupoMarca = totalizarGrupoMarca(resultado, idMesActual, idMesFinal, categ);

		for (ItemForecastRetailerVO objItRet : totRetailer) {
			objItRet.setEsTotalMarca(true);
			objItRet.setEsProducto(false);
			grupoMarca.add(objItRet);
			for (ItemForecastRetailerVO objItems : resultado) {
				if (objItems.getIdMes().equals(objItRet.getIdMes())
						&& objItems.getIdRetailer().equals(objItRet.getIdCategoria())) {
					objItems.setEsTotalMarca(true);
					objItems.setEsProducto(false);
					grupoMarca.add(objItems);
					for (ItemForecastRetailerVO objItem : listaFC) {
						if (!objItem.isEsProducto() && objItems.getIdCategoria().equals(objItem.getMarcaCategoria())
								&& objItems.getIdMes().equals(objItem.getIdMes())
								&& objItem.getIdRetailer().equals(objItRet.getIdCategoria())) {
							objItem.setEsTotalMarca(false);
							grupoMarca.add(objItem);
						} else {
							if (objItems.getIdCategoria().equals(objItem.getIdMarca())
									&& objItems.getIdMes().equals(objItem.getIdMes())
									&& objItem.getIdRetailer().equals(objItRet.getIdCategoria())) {
								objItem.setEsTotalMarca(false);
								grupoMarca.add(objItem);
							}
						}
					}
				}
			}
		}

		if (isElevacion) {
			grupoMarca.removeAll(totRetailer);
		}

		return grupoMarca;
	}

	/**
	 * 
	 * @param resultado
	 * @param idMesActual
	 * @param idMesFinal
	 * @param categ
	 * @return
	 */
	private List<ItemForecastRetailerVO> totalizarGrupoMarca(List<ItemForecastRetailerVO> resultado, Long idMesActual,
			Long idMesFinal, int categ) {

		Long sellin = 0L;
		Long forecastKam = 0L;
		Long forecastGerente = 0L;
		Long gerenteAnterior = 0L;
		BigDecimal totalPropuesto = BigDecimal.ZERO;
		BigDecimal totalKam = BigDecimal.ZERO;
		BigDecimal totalGerente = BigDecimal.ZERO;
		BigDecimal totalGerenteAnterior = BigDecimal.ZERO;
		List<ItemForecastRetailerVO> totalGrupo = new ArrayList<>();
		boolean existsMarca = false;

		for (Long i = idMesActual; i <= idMesFinal; i++) {
			ItemForecastRetailerVO item = new ItemForecastRetailerVO();
			for (ItemForecastRetailerVO objItems : resultado) {
				if (objItems.getIdMes().equals(i)) {
					sellin = sellin + objItems.getSellInPropuesto();
					forecastKam = forecastKam + objItems.getForecastKam();
					forecastGerente = forecastGerente + objItems.getForecastGerente();
					totalPropuesto = totalPropuesto.add(objItems.getTotalPropuesto());
					totalKam = totalKam.add(objItems.getTotalForecastKam());
					totalGerente = totalGerente
							.add(objItems.getTotalForecastGerente() != null ? objItems.getTotalForecastGerente()
									: BigDecimal.ZERO);

					totalGerenteAnterior = totalGerenteAnterior.add(objItems.getTotalForecastGerenteAnterior() != null
							? objItems.getTotalForecastGerenteAnterior()
							: BigDecimal.ZERO);
					gerenteAnterior = gerenteAnterior
							+ (objItems.getForecastGerenteAnterior() != null ? objItems.getForecastGerenteAnterior()
									: 0L);
					item.setNombre(objItems.getNombre());
					existsMarca = true;
				}
			}

			if (!existsMarca) {
				continue;
			}

			item.setIdCategoria("00");
			item.setIdProducto("0");
			item.setEsProducto(false);
			item.setEsTotalMarca(true);
			item.setPosicionCategoria(0);
			item.setCategoriaProducto("TOTAL");
			item.setSellInPropuesto(sellin);
			item.setPrecioRetailer(BigDecimal.ZERO);
			item.setTotalPropuesto(totalPropuesto);
			item.setCogs(BigDecimal.ZERO);
			item.setForecastKam(forecastKam);
			item.setTotalForecastKam(totalKam);
			item.setForecastGerente(forecastGerente);
			item.setTotalForecastGerente(totalGerente);
			item.setMargenGerente(BigDecimal.ZERO);
			item.setTotalForecastGerenteAnterior(totalGerenteAnterior);
			item.setForecastGerenteAnterior(gerenteAnterior);
			item.setIdMes(i);
			item.setIdMarca("0");
			if (item.getIdMes().equals(idMesActual)) {
				item.setEsReal(true);
			} else {
				item.setEsReal(false);
			}
			sellin = 0L;
			forecastKam = 0L;
			forecastGerente = 0L;
			totalPropuesto = BigDecimal.ZERO;
			totalKam = BigDecimal.ZERO;
			totalGerente = BigDecimal.ZERO;
			totalGerenteAnterior = BigDecimal.ZERO;
			gerenteAnterior = 0L;
			totalGrupo.add(item);
			existsMarca = false;
		}
		return totalGrupo;
	}

	/**
	 * 
	 * @param marca
	 * @param compania
	 * @param mes
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> devolverIdMarcasForecast(String marca, Compania compania, MesSop mes) {

		String companiaOrCNFG = compania == null ? "AND (estado_fk='CNFD' OR estado_fk = 'CNFG')"
				: "AND compania_fk = " + compania.getId() + "";

		String marcasForecast = "SELECT distinct marca_fk, '0' FROM FORECAST WHERE  marca_fk IN " + marca + "  "
				+ companiaOrCNFG + " AND mes_fk = ?1";
		Query query = em.createNativeQuery(marcasForecast);
		query.setParameter(1, mes.getId());
		List<Object[]> objMarcasFore = query.getResultList();
		List<String> listaMarcas = new ArrayList<>();
		for (Object[] objs : objMarcasFore) {
			listaMarcas.add((String) objs[0]);
		}

		if (listaMarcas.isEmpty()) {
			marcasForecast = "SELECT id, '0' FROM MARCA_CAT2 WHERE id IN " + marca;
			query = em.createNativeQuery(marcasForecast);
			objMarcasFore = query.getResultList();
			for (Object[] objs : objMarcasFore) {
				listaMarcas.add((String) objs[0]);
			}
		}
		return listaMarcas;
	}

	/**
	 * 
	 * @param oCategoria
	 * @param productos
	 */
	public void actualizarCategoriaForecast(ProductosForecastMesVO oCategoria, ForecastXMesVO productos) {

		for (Map.Entry<String, InformacionForecastXProductoVO> entry : oCategoria.getInformacionForecastMes()
				.entrySet()) {
			entry.getValue().setInventarioWH(BigDecimal.ZERO);
			entry.getValue().setTransito(BigDecimal.ZERO);
			entry.getValue().setSellInPropuesto(0L);
			entry.getValue().setTotalForecastGerenteAnterior(BigDecimal.ZERO);
			entry.getValue().setForecast(0L);
			entry.getValue().setTotalPropuesto(BigDecimal.ZERO);
			entry.getValue().setTotalForecastKam(BigDecimal.ZERO);
			entry.getValue().setTotalForecastGerente(BigDecimal.ZERO);
			entry.getValue().setForecastGerente(0L);
		}
		if (productos.getProductosForecast() != null) {
			for (ProductosForecastMesVO oProd : productos.getProductosForecast()) {
				if (oProd != null) {
					if (oProd.isEsProdcuto()) {
						for (Map.Entry<String, InformacionForecastXProductoVO> entry : oProd.getInformacionForecastMes()
								.entrySet()) {

							oCategoria.getInformacionForecastMes().get(entry.getKey()).setInventarioWH(
									oCategoria.getInformacionForecastMes().get(entry.getKey()).getInventarioWH()
											.add(entry.getValue().getInventarioWH() != null
													? entry.getValue().getInventarioWH()
													: BigDecimal.ZERO));

							oCategoria.getInformacionForecastMes().get(entry.getKey()).setTransito(
									oCategoria.getInformacionForecastMes().get(entry.getKey()).getTransito()
											.add(entry.getValue().getTransito() != null ? entry.getValue().getTransito()
													: BigDecimal.ZERO));

							oCategoria.getInformacionForecastMes().get(entry.getKey()).setSellInPropuesto(
									oCategoria.getInformacionForecastMes().get(entry.getKey()).getSellInPropuesto()
											+ (entry.getValue().getSellInPropuesto() != null
													? entry.getValue().getSellInPropuesto()
													: 0L));

							oCategoria.getInformacionForecastMes().get(entry.getKey())
									.setTotalForecastGerenteAnterior(oCategoria.getInformacionForecastMes()
											.get(entry.getKey()).getTotalForecastGerenteAnterior()
											.add(entry.getValue().getTotalForecastGerenteAnterior() != null
													? entry.getValue().getTotalForecastGerenteAnterior()
													: BigDecimal.ZERO));

							oCategoria.getInformacionForecastMes().get(entry.getKey()).setForecast(oCategoria
									.getInformacionForecastMes().get(entry.getKey()).getForecast()
									+ (entry.getValue().getForecast() != null ? entry.getValue().getForecast() : 0L));

							oCategoria.getInformacionForecastMes().get(entry.getKey())
									.setTotalPropuesto(oCategoria.getInformacionForecastMes().get(entry.getKey())
											.getTotalPropuesto()
											.add(entry.getValue().getTotalPropuesto() != null
													? entry.getValue().getTotalPropuesto()
													: BigDecimal.ZERO));

							oCategoria.getInformacionForecastMes().get(entry.getKey())
									.setTotalForecastKam(oCategoria.getInformacionForecastMes().get(entry.getKey())
											.getTotalForecastKam()
											.add(entry.getValue().getTotalForecastKam() != null
													? entry.getValue().getTotalForecastKam()
													: BigDecimal.ZERO));

							oCategoria.getInformacionForecastMes().get(entry.getKey())
									.setTotalForecastGerente(oCategoria.getInformacionForecastMes().get(entry.getKey())
											.getTotalForecastGerente()
											.add(entry.getValue().getTotalForecastGerente() != null
													? entry.getValue().getTotalForecastGerente()
													: BigDecimal.ZERO));
							oCategoria.getInformacionForecastMes().get(entry.getKey()).setForecastGerente(
									oCategoria.getInformacionForecastMes().get(entry.getKey()).getForecastGerente()
											+ (entry.getValue().getForecastGerente() != null
													? entry.getValue().getForecastGerente()
													: 0L));

						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param forecast
	 * @param usuario
	 */
	public void guardarEstadoCompania(List<Forecast> forecast, UsuarioVO usuario) {

		try {
			for (Forecast objFore : forecast) {
				TypedQuery<Forecast> q = em.createQuery(SELECT_FORECAST_ID, Forecast.class);
				q.setParameter("id", objFore.getId());
				q.setMaxResults(1);
				Forecast forecastDB = q.getSingleResult();

				forecastDB.setFechaModifica(new java.util.Date());
				forecastDB.setUsuarioModifica(usuario.getUserName());
				forecastDB.setForecastCompania(String.valueOf(true));
				EstadoForecast estado = new EstadoForecast(ConstantesDemand.EST_FORECAST_CNFG);
				forecastDB.setEstadoFk(estado);
				em.merge(forecastDB);
				em.flush();
			}
		} catch (Exception e) {
			log.error("[guardarEsta]", e);
		}
	}

	/**
	 * Consulta el estado del producto en la tabla FORECAST_PRODUCTO a nivel de
	 * compañia para determinar si es estado descontinuado,varios o current
	 *
	 * @param idProducto
	 * @param marca
	 * @param compania
	 * @param mes
	 * 
	 * @return V:Varios, C:Current, D:Descontinuado
	 * 
	 */
	@SuppressWarnings("unchecked")
	public String buscarEstadoProdPorCompania(String idProducto, String marca, String compania, Long mes) {
		try {
			String queryStr = "SELECT distinct fp.estadoProducto FROM FORECAST f \n"
					+ "INNER JOIN FORECAST_PRODUCTO fp ON fp.forecast_fk=f.id \n" + "WHERE f.marca_fk IN " + marca
					+ " and mes_fk= ?1 and fp.producto_fk= ?2 and f.compania_fk= ?3 ";
			Query query = em.createNativeQuery(queryStr);
			query.setParameter(1, mes);
			query.setParameter(2, idProducto);
			query.setParameter(3, compania);
			List<Character> estado = query.getResultList();

			if (estado.size() > 1) {
				// Si los tipos de estado son diferentes se marca V: varios. Para pintar el
				// producto en naranja
				return "V";
			} else {
				if (estado.isEmpty()) {
					return "";
				} else {
					if (estado.get(0) == null) {
						return "";
					} else {
						return estado.get(0).toString();
					}
				}
			}
		} catch (Exception e) {
			log.error("[buscarEstadoProdPorCompania] ERROR en metodo buscarEstadoProdPorCompania() ", e);
			return "";
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardarForecastMeses(ProductosForecastMesVO item, List<String> nombreMeses, Retailer retailer,
			UsuarioVO usuario) {

		Forecast forecastDB;

		if (retailer != null) {
			TypedQuery<Forecast> q = em.createQuery(
					"SELECT f FROM Forecast f WHERE f.retailerFk = :retailer AND f.marcaFk.id = :marca AND f.mesFk.id = :mes",
					Forecast.class);
			q.setParameter("retailer", retailer);
			q.setParameter("marca", item.getIdMarca());
			q.setParameter("mes", item.getInformacionForecastMes().get(nombreMeses.get(0)).getIdMes());
			q.setMaxResults(1);
			forecastDB = q.getSingleResult();

			forecastDB.setFechaModifica(new Date());
			forecastDB.setUsuarioModifica(usuario.getUserName());
			forecastDB.setForecastCompania(String.valueOf(true));

			try {
				forecastDB = em.merge(forecastDB);
				em.flush();

				for (String mes : nombreMeses) {
					ForecastProducto forecastProductoDB = new ForecastProducto();

					if (item.getInformacionForecastMes().get(mes).getIdForecastProducto() != null) {
						forecastProductoDB.setId(item.getInformacionForecastMes().get(mes).getIdForecastProducto());
					}

					forecastProductoDB.setCogs(item.getInformacionForecastMes().get(mes).getCogs());
					forecastProductoDB.setForecastKam(item.getInformacionForecastMes().get(mes).getForecast());
					forecastProductoDB.setMargenKam(item.getInformacionForecastMes().get(mes).getMargen());
					forecastProductoDB.setMesSop(new MesSop(item.getInformacionForecastMes().get(mes).getIdMes()));
					forecastProductoDB.setPrecioRetailer(item.getInformacionForecastMes().get(mes).getPrecioRetailer());
					forecastProductoDB.setProductoFk(new Producto(item.getIdProducto()));
					forecastProductoDB
							.setSellinSugerido(item.getInformacionForecastMes().get(mes).getSellInPropuesto());
					forecastProductoDB
							.setTotalforcastkam(item.getInformacionForecastMes().get(mes).getTotalForecastKam());
					forecastProductoDB
							.setTotalsellinSugerido(item.getInformacionForecastMes().get(mes).getTotalPropuesto());
					forecastProductoDB.setForecastFk(new Forecast(forecastDB.getId()));
					forecastProductoDB.setEditarGerente(item.getInformacionForecastMes().get(mes).getEditarGerente());
					forecastProductoDB.setEditarKam(item.getInformacionForecastMes().get(mes).getEditarKam());

					if (forecastProductoDB.getEditarGerente() != null && forecastProductoDB.getEditarGerente()) {
						forecastProductoDB
								.setForecastGerente(item.getInformacionForecastMes().get(mes).getForecastGerente());
						forecastProductoDB.setTotalForecastGerente(
								item.getInformacionForecastMes().get(mes).getTotalForecastGerente());
					} else {
						forecastProductoDB.setForecastGerente(
								item.getInformacionForecastMes().get(mes).getForecastGerente() == null || item
										.getInformacionForecastMes().get(mes).getForecastGerente().intValue() == 0
												? item.getInformacionForecastMes().get(mes).getForecast()
												: item.getInformacionForecastMes().get(mes).getForecastGerente());
						forecastProductoDB.setTotalForecastGerente(
								item.getInformacionForecastMes().get(mes).getTotalForecastGerente() == null || item
										.getInformacionForecastMes().get(mes).getTotalForecastGerente().intValue() == 0
												? item.getInformacionForecastMes().get(mes).getTotalForecastKam()
												: item.getInformacionForecastMes().get(mes).getTotalForecastGerente());
					}

					forecastProductoDB
							.setMargenGerente(item.getInformacionForecastMes().get(mes).getMargenGerente() == null
									|| item.getInformacionForecastMes().get(mes).getMargenGerente().intValue() == 0
											? item.getInformacionForecastMes().get(mes).getMargen()
											: item.getInformacionForecastMes().get(mes).getMargenGerente());
					forecastProductoDB.setInventarioWH(item.getInformacionForecastMes().get(mes).getInventarioWH());
					forecastProductoDB.setTransitoPais(item.getInformacionForecastMes().get(mes).getTransito());
					forecastProductoDB.setEstadoProducto(item.getEstadoProducto());
					em.merge(forecastProductoDB);
				}
			} catch (Exception e) {
				log.error("Error guardando forecast", e);
			}
		}
	}

	/**
	 * Este metodo realiza el llamado a un SP que unicamente realiza la consulta de
	 * FG teniendo en cuenta la compañía o si el mes a consultar es el real. Este
	 * metodo se adiciona para dar solucion al aumento en el performance para
	 * ciertos filtros.
	 * 
	 * @param companiaSeleccionada
	 * @param mesSeleccionado
	 * @param marcas
	 * @param idGrupo
	 * @return
	 * @throws ExcepcionSOP
	 */
	public List<ItemForecastRetailerVO> consultarForecastGeneralCompañia(Compania companiaSeleccionada,
			MesSop mesSeleccionado, String marcas, String idGrupo, boolean isCerrado) throws ExcepcionSOP {

		MesSop mesAnterior;
		boolean isActual = false;
		Calendar fecha = Calendar.getInstance();

		if (mesSeleccionado.getMes().equals(1)) {
			mesAnterior = moduloGeneralEJB.consultarMesSOP(12, mesSeleccionado.getAnio() - 1);
		} else {
			mesAnterior = moduloGeneralEJB.consultarMesSOP(mesSeleccionado.getMes() - 1, mesSeleccionado.getAnio());
		}

		if (mesAnterior == null) {
			throw new ExcepcionSOP("No existe en el sistema un mes anterior a " + mesSeleccionado.getNombre() + " de "
					+ mesSeleccionado.getAnio());
		}

		if (fecha.get(Calendar.MONTH + 1) == mesSeleccionado.getMes()
				&& fecha.get(Calendar.YEAR) == mesSeleccionado.getAnio())
			isActual = true;

		Query finQuery = em.createNativeQuery("exec SOPP_ConsultaForecastGeneral ?,?,?,?,?,?,?,?");
		finQuery.setParameter(1, ConstantesDemand.EST_FORECAST_CNFG);
		finQuery.setParameter(2, ConstantesDemand.EST_FORECAST_CNFD);
		finQuery.setParameter(3, mesSeleccionado.getId());
		finQuery.setParameter(4, mesAnterior.getId());
		finQuery.setParameter(5, marcas);
		finQuery.setParameter(6, companiaSeleccionada != null ? companiaSeleccionada.getId() : 'M');
		finQuery.setParameter(7, isActual);
		finQuery.setParameter(8, isCerrado ? "S" : "N");

		List lQuery = finQuery.getResultList();
		Iterator it = lQuery.iterator();
		List<String> marcaQuery = new ArrayList<>();
		List<ItemForecastRetailerVO> listaResultado = new ArrayList<>();

		while (it.hasNext()) {
			Object[] obj = (Object[]) it.next();
			ItemForecastRetailerVO item = new ItemForecastRetailerVO();

			item.setIdCategoria((String) obj[0]);
			item.setCategoriaProducto((String) obj[1]);
			item.setIdMes(obj[2] != null ? ((BigDecimal) obj[2]).longValue() : 0L);
			item.setPrecioRetailer(obj[3] != null ? ((BigDecimal) obj[3]) : BigDecimal.ZERO);
			item.setCogs(obj[4] != null ? ((BigDecimal) obj[4]) : BigDecimal.ZERO);
			item.setForecastGerente(obj[5] != null ? ((BigDecimal) obj[5]).longValue() : 0L);
			item.setTotalForecastGerente(obj[6] != null ? ((BigDecimal) obj[6]) : BigDecimal.ZERO);
			item.setMargenGerente(obj[7] != null ? ((BigDecimal) obj[7]) : BigDecimal.ZERO);
			item.setMargen(obj[7] != null ? ((BigDecimal) obj[7]) : BigDecimal.ZERO);

			item.setMesAnterior(obj[8] != null ? ((BigDecimal) obj[8]).longValue() : 0L);
			item.setPrecioRetailerAnterior(obj[9] != null ? (BigDecimal) obj[9] : BigDecimal.ZERO);
			item.setCogsAnterior(obj[10] != null ? (BigDecimal) obj[10] : BigDecimal.ZERO);
			item.setForecastGerenteAnterior(obj[11] != null ? ((BigDecimal) obj[11]).longValue() : 0L);
			item.setTotalForecastGerenteAnterior(obj[12] != null ? (BigDecimal) obj[12] : BigDecimal.ZERO);
			item.setNombre((String) obj[13]);
			item.setIdMarca((String) obj[14]);
			item.setMarcaCategoria((String) obj[14]);
			item.setEsTotalMarca(false);

			if (!marcaQuery.contains(item.getIdMarca()))
				marcaQuery.add(item.getIdMarca());

			if (item.getIdMes().equals(mesSeleccionado.getId())) {
				item.setEsReal(true);
			} else {
				item.setEsReal(false);
			}

			listaResultado.add(item);
		}

		return !listaResultado.isEmpty() ? totalizarMarcaNivelCompania(listaResultado, mesSeleccionado, 7, marcaQuery)
				: new ArrayList<ItemForecastRetailerVO>();
	}

	/**
	 * Construir la fila por cada retailer consultado. Se totaliza todas las marcas
	 * por retailer para el total por mes. El metodo solo aplica para las pantallas
	 * con filtro de Retailer multiple.
	 * 
	 * @param resultado
	 * @param idMesActual
	 * @param idMesFinal
	 * @param categ
	 * @param idsRetailer
	 * @return
	 */
	private List<ItemForecastRetailerVO> totalizarRetailer(List<ItemForecastRetailerVO> resultado, Long idMesActual,
			Long idMesFinal, int categ, List<String> idsRetailer) {

		Long sellin = 0L;
		Long forecastKam = 0L;
		Long forecastGerente = 0L;
		Long gerenteAnterior = 0L;
		BigDecimal totalPropuesto = BigDecimal.ZERO;
		BigDecimal totalKam = BigDecimal.ZERO;
		BigDecimal totalGerente = BigDecimal.ZERO;
		BigDecimal totalGerenteAnterior = BigDecimal.ZERO;
		List<ItemForecastRetailerVO> totalRetailer = new ArrayList<>();
		boolean existsMarca = false;

		Collections.sort(idsRetailer);
		for (String objRetailer : idsRetailer) {
			Retailer oRetailer = moduloGeneralEJB.consultarRetailerById(objRetailer);
			for (Long i = idMesActual; i <= idMesFinal; i++) {
				ItemForecastRetailerVO item = new ItemForecastRetailerVO();
				for (ItemForecastRetailerVO objItems : resultado) {
					if (objItems.getIdMes().equals(i) && objItems.getIdRetailer().equals(objRetailer)) {
						sellin = sellin + (objItems.getSellInPropuesto() != null ? objItems.getSellInPropuesto() : 0L);
						forecastKam = forecastKam
								+ (objItems.getForecastKam() != null ? objItems.getForecastKam() : 0L);
						forecastGerente = forecastGerente
								+ (objItems.getForecastGerente() != null ? objItems.getForecastGerente() : 0L);
						totalPropuesto = totalPropuesto.add(
								objItems.getTotalPropuesto() != null ? objItems.getTotalPropuesto() : BigDecimal.ZERO);
						totalKam = totalKam.add(objItems.getTotalForecastKam() != null ? objItems.getTotalForecastKam()
								: BigDecimal.ZERO);
						totalGerente = totalGerente
								.add(objItems.getTotalForecastGerente() != null ? objItems.getTotalForecastGerente()
										: BigDecimal.ZERO);
						totalGerenteAnterior = totalGerenteAnterior
								.add(objItems.getTotalForecastGerenteAnterior() != null
										? objItems.getTotalForecastGerenteAnterior()
										: BigDecimal.ZERO);
						gerenteAnterior = gerenteAnterior
								+ (objItems.getForecastGerenteAnterior() != null ? objItems.getForecastGerenteAnterior()
										: 0L);
						item.setNombre(objItems.getNombre());
						existsMarca = true;
					}
				}

				if (!existsMarca) {
					continue;
				}

				item.setIdCategoria(objRetailer);
				item.setIdProducto("0");
				item.setEsProducto(false);
				item.setEsTotalMarca(true);
				item.setCategoriaProducto(objRetailer + "-" + oRetailer.getNombreComercial() + " Total");
				item.setSellInPropuesto(sellin);
				item.setPrecioRetailer(BigDecimal.ZERO);
				item.setTotalPropuesto(totalPropuesto);
				item.setCogs(BigDecimal.ZERO);
				item.setForecastKam(forecastKam);
				item.setTotalForecastKam(totalKam);
				item.setForecastGerente(forecastGerente);
				item.setTotalForecastGerente(totalGerente);
				item.setTotalForecastGerenteAnterior(totalGerenteAnterior);
				item.setForecastGerenteAnterior(gerenteAnterior);
				item.setMargenGerente(BigDecimal.ZERO);
				item.setIdMes(i);
				item.setIdMarca("0");
				if (item.getIdMes().equals(idMesActual)) {
					item.setEsReal(true);
				} else {
					item.setEsReal(false);
				}

				sellin = 0L;
				forecastKam = 0L;
				forecastGerente = 0L;
				totalPropuesto = BigDecimal.ZERO;
				totalKam = BigDecimal.ZERO;
				totalGerente = BigDecimal.ZERO;
				totalGerenteAnterior = BigDecimal.ZERO;
				gerenteAnterior = 0L;
				totalRetailer.add(item);
				existsMarca = false;
			}
			existsMarca = false;
		}
		return totalRetailer;
	}

	/**
	 * Totalizar a nivel de marca y el total general. Este metodo solo aplica para
	 * las pantallas en donde no se tienen en cuenta el Retailer o no existe el
	 * filtro de Retailer multiple.
	 * 
	 * @param listaFC
	 * @param mes
	 * @param mesesForecastParametro
	 * @param idMarcas
	 * @return
	 */
	private List<ItemForecastRetailerVO> totalizarMarcaNivelCompania(List<ItemForecastRetailerVO> listaFC, MesSop mes,
			int mesesForecastParametro, List<String> idMarcas) {

		Long idMesActual = mes.getId();
		Long idMesFinal = mes.getId() + mesesForecastParametro;
		List<ItemForecastRetailerVO> resultado = new ArrayList<>();
		Long sellin = 0L;
		Long forecastKam = 0L;
		Long forecastGerente = 0L;
		Long gerenteAnterior = 0L;
		BigDecimal totalPropuesto = BigDecimal.ZERO;
		BigDecimal totalKam = BigDecimal.ZERO;
		BigDecimal totalInvWH = BigDecimal.ZERO;
		BigDecimal totalGerenteAnterior = BigDecimal.ZERO;
		BigDecimal totalGerente = BigDecimal.ZERO;
		BigDecimal totalTransito = BigDecimal.ZERO;
		int categ = 0;
		boolean existsMarca = false;

		for (String objMarcas : idMarcas) {
			MarcaCat2 oMarca = moduloGeneralEJB.consultarPorMarca(objMarcas);
			for (Long i = idMesActual; i <= idMesFinal; i++) {
				ItemForecastRetailerVO item = new ItemForecastRetailerVO();
				for (ItemForecastRetailerVO objItems : listaFC) {
					if (!objItems.isEsProducto() && objItems.getIdMes().equals(i)
							&& objItems.getMarcaCategoria().equals(objMarcas)) {
						sellin = sellin + (objItems.getSellInPropuesto() != null ? objItems.getSellInPropuesto() : 0L);
						forecastKam = forecastKam
								+ (objItems.getForecastKam() != null ? objItems.getForecastKam() : 0L);
						forecastGerente = forecastGerente
								+ (objItems.getForecastGerente() != null ? objItems.getForecastGerente() : 0L);
						totalPropuesto = totalPropuesto.add(
								objItems.getTotalPropuesto() != null ? objItems.getTotalPropuesto() : BigDecimal.ZERO);
						totalKam = totalKam.add(objItems.getTotalForecastKam() != null ? objItems.getTotalForecastKam()
								: BigDecimal.ZERO);
						totalGerente = totalGerente
								.add(objItems.getTotalForecastGerente() != null ? objItems.getTotalForecastGerente()
										: BigDecimal.ZERO);
						totalInvWH = totalInvWH
								.add(objItems.getInventarioWH() != null ? objItems.getInventarioWH() : BigDecimal.ZERO);
						totalTransito = totalTransito
								.add(objItems.getTransito() != null ? objItems.getTransito() : BigDecimal.ZERO);
						totalGerenteAnterior = totalGerenteAnterior
								.add(objItems.getTotalForecastGerenteAnterior() != null
										? objItems.getTotalForecastGerenteAnterior()
										: BigDecimal.ZERO);
						gerenteAnterior = gerenteAnterior
								+ (objItems.getForecastGerenteAnterior() != null ? objItems.getForecastGerenteAnterior()
										: 0L);
						item.setNombre(objItems.getNombre());
						existsMarca = true;
					}
				}

				if (!existsMarca) {
					continue;
				}

				item.setIdCategoria(objMarcas);
				item.setIdProducto("0");
				item.setEsProducto(false);
				item.setEsTotalMarca(true);
				item.setCategoriaProducto("Total " + oMarca.getNombre());
				item.setSellInPropuesto(sellin);
				item.setPrecioRetailer(BigDecimal.ZERO);
				item.setTotalPropuesto(totalPropuesto);
				item.setCogs(BigDecimal.ZERO);
				item.setForecastKam(forecastKam);
				item.setTotalForecastKam(totalKam);
				item.setForecastGerente(forecastGerente);
				item.setTotalForecastGerente(totalGerente);
				item.setInventarioWH(totalInvWH);
				item.setTransito(totalTransito);
				item.setTotalForecastGerenteAnterior(totalGerenteAnterior);
				item.setForecastGerenteAnterior(gerenteAnterior);
				item.setMargenGerente(BigDecimal.ZERO);
				item.setIdMes(i);
				item.setIdMarca("0");
				if (item.getIdMes().equals(mes.getId())) {
					item.setEsReal(true);
				} else {
					item.setEsReal(false);
				}

				sellin = 0L;
				forecastKam = 0L;
				forecastGerente = 0L;
				totalPropuesto = BigDecimal.ZERO;
				totalKam = BigDecimal.ZERO;
				totalGerente = BigDecimal.ZERO;
				totalInvWH = BigDecimal.ZERO;
				totalTransito = BigDecimal.ZERO;
				totalGerenteAnterior = BigDecimal.ZERO;
				gerenteAnterior = 0L;
				resultado.add(item);
				existsMarca = false;
			}
			existsMarca = false;
		}

		List<ItemForecastRetailerVO> grupoMarca = totalizarGrupoMarca(resultado, idMesActual, idMesFinal, categ);

		for (ItemForecastRetailerVO objItems : resultado) {
			objItems.setEsTotalMarca(true);
			objItems.setEsProducto(false);
			grupoMarca.add(objItems);
			for (ItemForecastRetailerVO objItem : listaFC) {
				if (!objItem.isEsProducto() && objItems.getIdCategoria().equals(objItem.getMarcaCategoria())
						&& objItems.getIdMes().equals(objItem.getIdMes())) {
					objItem.setEsTotalMarca(false);
					grupoMarca.add(objItem);
				} else {
					if (objItems.getIdCategoria().equals(objItem.getIdMarca())
							&& objItems.getIdMes().equals(objItem.getIdMes())) {
						objItem.setEsTotalMarca(false);
						grupoMarca.add(objItem);
					}
				}
			}
		}

		return grupoMarca;
	}

	/**
	 * 
	 * @param inCompanhas
	 * @param inMes
	 * @param selectedRetailer
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ForecastVO> consultarForecastCompanhia(List<String> inCompanhas, List<Long> inMes,
			List<String> inMarcas) {
		List<ForecastVO> listForescast = new ArrayList<>();

		try {
			String companhia = listToText(inCompanhas, null);
			String marcas = listToText(inMarcas, "%3s");
			String meses = listToText(inMes, null);
			String queryStr = "SELECT MCA.nombre as Marca," // 0
					+ "F.compania_fk," // 1
					+ "F.retailer_fk," // 2
					+ "R.nombre_comercial," // 3
					+ "f.usuario_crea," // 4
					+ "F.fecha_crea," // 5
					+ "MSOP.anio," // 6
					+ "MSOP.nombre," // 7
					+ "f.estado_fk," // 8
					+ "ESF.nombre as trad " // 9
					+ "FROM FORECAST F" + " INNER JOIN MARCA_CAT2 MCA ON MCA.id=F.marca_fk "
					+ " INNER JOIN MARCA_GRUPO MG ON MCA.id = MG.idMarca"
					+ " INNER JOIN MES_SOP MSOP ON MSOP.id = f.mes_fk "
					+ " INNER JOIN RETAILER R ON R.id=F.retailer_fk "
					+ " INNER JOIN ESTADO_FORECAST ESF ON ESF.codigo = F.estado_fk " + " WHERE F.compania_fk IN  "
					+ companhia + " AND  MG.idGrupo IN " + marcas + " AND MSOP.id in " + meses + " ORDER BY MG.idGrupo";

			System.out.println("SQ -> " + queryStr);
			Query query = em.createNativeQuery(queryStr);
			List<Object[]> resultados = query.getResultList();

			if (!resultados.isEmpty()) {
				for (Object[] obj : resultados) {
					ForecastVO f = new ForecastVO();
					f.setMarca((String) obj[0]);
					f.setCompania((String) obj[1]);
					f.setRetailer((String) obj[2]);
					f.setDescripcionRetailer((String) obj[3]);
					f.setUsuario((String) obj[4]);
					try {
						f.setFechaCreacion((Date) obj[5]);
					} catch (Exception e) {
						log.error("[consultarForecastCompañia] Error obteniendo la fecha -> " + obj[5], e);
						f.setFechaCreacion(new Date());
					}
					String anhoMes = Integer.toString((int) obj[6]) + "-" + (String) obj[7];
					f.setAnhoMes(anhoMes);
					f.setEstado((String) obj[8]);
					f.setTraduccion((String) obj[9]);
					listForescast.add(f);
				}
			}
		} catch (Exception e) {
			log.error("[consultarForecastCompañia] Error realizando la consulta", e);
		}

		return listForescast;
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	public String listToText(List list, String format) {
		String resultado = "(";
		for (Object e : list) {
			String elemt = "";

			if (e instanceof String) {
				elemt = (String) e;
				if (format != null) {
					elemt = String.format(format, elemt).replace(' ', '0');
				}
			} else if (e instanceof Long) {
				elemt = Long.toString((long) e);
			}

			resultado = resultado.concat("'".concat(elemt).concat("',"));
		}
		resultado = resultado.substring(0, resultado.length() - 1).concat(")");
		return resultado;
	}
}
