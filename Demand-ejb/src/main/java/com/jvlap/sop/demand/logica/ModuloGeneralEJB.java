/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.demand.logica.utilitarios.FuncionesUtilitariasDemand;
import com.jvlap.sop.demand.logica.vos.ForecastXMesVO;
import com.jvlap.sop.demand.logica.vos.InformacionForecastXProductoVO;
import com.jvlap.sop.demand.logica.vos.ItemForecastCompaniaVO;
import com.jvlap.sop.demand.logica.vos.ItemForecastRetailerVO;
import com.jvlap.sop.demand.logica.vos.MarcaGrupoForecast;
import com.jvlap.sop.demand.logica.vos.ProductosForecastMesVO;
import com.jvlap.sop.demand.logica.vos.RetailerGrupoForecast;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.EstadoForecast;
import com.jvlat.sop.entidadessop.entidades.EtapasXRol;
import com.jvlat.sop.entidadessop.entidades.Forecast;
import com.jvlat.sop.entidadessop.entidades.GrupoCat4;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MarcaGrupo;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Producto;
import com.jvlat.sop.entidadessop.entidades.ProductoXCompania;
import com.jvlat.sop.entidadessop.entidades.ProductoXRetailerXMes;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.entidadessop.entidades.Usuario;
import com.jvlat.sop.entidadessop.entidades.UsuariosXRol;
import com.jvlat.sop.login.vos.UsuarioVO;

@SuppressWarnings("rawtypes")
@Stateless
public class ModuloGeneralEJB extends AbstractFacade {
	@PersistenceContext(unitName = "EntidadesSOPPU")
	private EntityManager em;

	static Logger log = LogManager.getLogger(ModuloGeneralEJB.class.getName());

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	/**
	 * 
	 * @return
	 */
	public List<Compania> consultarCompanias() {
		String queryStr = "SELECT c FROM Compania c ORDER BY c.nombre";
		TypedQuery<Compania> query = this.em.createQuery(queryStr, (Class) Compania.class);
		return query.getResultList();
	}

	/**
	 * 
	 * @param idCompania
	 * @return
	 */
	public Compania consultarCompaniasID(String idCompania) {
		return (Compania) this.em.createNamedQuery("Compania.findById").setParameter("id", (Object) idCompania)
				.getSingleResult();
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Retailer> consultarRetailers() {
		String queryStr = "SELECT r FROM Retailer r ORDER BY r.razonSocial";
		TypedQuery<Retailer> query = this.em.createQuery(queryStr, (Class) Retailer.class);
		return query.getResultList();
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<MarcaCat2> consultarMarcas() {
		String queryStr = "SELECT m FROM MarcaCat2 m ORDER BY m.nombre";
		TypedQuery<MarcaCat2> query = this.em.createQuery(queryStr, (Class) MarcaCat2.class);
		return query.getResultList();
	}

	/**
	 * 
	 * @param idMarca
	 * @return
	 */
	public MarcaCat2 consultarMarca(String idMarca) {
		try {
			String queryStr = "SELECT m FROM MarcaCat2 m WHERE m.id=" + idMarca;
			TypedQuery<MarcaCat2> query = (TypedQuery<MarcaCat2>) this.em.createQuery(queryStr,
					(Class) MarcaCat2.class);
			return (MarcaCat2) query.getSingleResult();
		} catch (NoResultException e) {
			return new MarcaCat2();
		}
	}

	public MarcaCat2 consultarPorMarca(String idMarca) {
		try {
			List<MarcaCat2> marcas = this.consultarMarcas();
			for (MarcaCat2 objMarca : marcas) {
				if (objMarca.getId().equals(idMarca)) {
					return objMarca;
				}
			}
		} catch (NoResultException e) {
			log.info("[consultarPorMarca] No se encontr\u00f3 informaci\u00f3n: {}", new Object[] { e.getMessage() });
		}
		return null;
	}

	public List<GrupoMarca> consultarGrupoMarcas() {
		String queryStr = "SELECT m FROM GrupoMarca m ORDER BY m.nombre";
		TypedQuery<GrupoMarca> query = (TypedQuery<GrupoMarca>) this.em.createQuery(queryStr, (Class) GrupoMarca.class);
		return (List<GrupoMarca>) query.getResultList();
	}

	public GrupoMarca consultarGrupoMarcaID(String idGrupo) {
		String queryStr = "SELECT m FROM GrupoMarca m WHERE m.id = " + idGrupo;
		TypedQuery<GrupoMarca> query = (TypedQuery<GrupoMarca>) this.em.createQuery(queryStr, (Class) GrupoMarca.class);
		return (GrupoMarca) query.getSingleResult();
	}

	public List<GrupoMarca> listaGrupoMarca() {
		List<GrupoMarca> out = null;
		try {
			Query query = this.em.createQuery("SELECT c FROM GrupoMarca c ORDER BY c.nombre");
			return (List<GrupoMarca>) query.getResultList();
		} catch (NoResultException e) {
			log.debug("[listaGrupoMarca] No se encontr\u00f3 informaci\u00f3n:{}", new Object[] { e.getMessage() });
			return out;
		}
	}

	public String getMarcasByIDGrupoMarca(String idGrupoMarca) {
		List<MarcaGrupo> listado = this.buscaMarcaGrupoPorIdGrupoMarca(idGrupoMarca);
		return this.getMarcaSeparadoComasYParentesis(listado);
	}

	public List<MarcaGrupo> buscaMarcaGrupoPorIdGrupoMarca(String idGrupoMarca) {
		List<MarcaGrupo> out = null;
		try {
			return (List<MarcaGrupo>) this.em.createNamedQuery("MarcaGrupo.findByIdGrupo")
					.setParameter("idGrupo", (Object) Long.valueOf(idGrupoMarca)).getResultList();
		} catch (NoResultException e) {
			log.info("[ buscaMarcaGrupoPorIdGrupoMarca] No se encontr\u00f3 informaci\u00f3n:{} ",
					new Object[] { e.getMessage() });
			return out;
		}
	}

	public GrupoMarca getGrupoByMarca(String idMarca) {
		try {
			MarcaGrupo oMarGrup = (MarcaGrupo) this.em.createNamedQuery("MarcaGrupo.findByIdMarca")
					.setParameter("c", (Object) idMarca).getSingleResult();
			return this.consultarGrupoMarcaID(Long.toString(oMarGrup.getMarcaGrupoPK().getIdGrupo()));
		} catch (NoResultException e) {
			log.info("[ getGrupoByMarca] No se encontr\u00f3 informaci\u00f3n:{} ", new Object[] { e.getMessage() });
			return null;
		}
	}

	public List<MarcaCat2> buscaMarcaPorIdGrupoMarca(String idGrupoMarca) {
		List<MarcaCat2> listMarca = null;
		try {
			List<MarcaGrupo> listMarcaGrupo = (List<MarcaGrupo>) this.em.createNamedQuery("MarcaGrupo.findByIdGrupo")
					.setParameter("idGrupo", (Object) Long.valueOf(idGrupoMarca)).getResultList();
			listMarca = new ArrayList<MarcaCat2>();
			for (MarcaGrupo marcaGrupo : listMarcaGrupo) {
				MarcaCat2 marca = this.consultarMarca(marcaGrupo.getMarcaGrupoPK().getIdMarca());
				listMarca.add(marca);
			}
			return listMarca;
		} catch (NoResultException e) {
			log.info("[ buscaMarcaPorIdGrupoMarca] No se encontr\u00f3 informaci\u00f3n:{} ",
					new Object[] { e.getMessage() });
			return listMarca;
		}
	}

	public List<String> getMarcaGrupoPorIdGrupoMarca(List<String> listIdGrupoMarca) {
		List<String> listResultado = null;
		try {
			List<MarcaGrupo> listMarcaGrupo = new ArrayList<MarcaGrupo>();
			for (String idGrupoMarca : listIdGrupoMarca) {
				List<MarcaGrupo> listAux = (List<MarcaGrupo>) this.em.createNamedQuery("MarcaGrupo.findByIdGrupo")
						.setParameter("idGrupo", (Object) Long.valueOf(idGrupoMarca)).getResultList();
				listMarcaGrupo.addAll(listAux);
			}
			listResultado = new ArrayList<String>();
			for (MarcaGrupo marcaGrupo : listMarcaGrupo) {
				listResultado.add(marcaGrupo.getMarcaGrupoPK().getIdMarca());
			}
			return listResultado;
		} catch (NoResultException e) {
			log.info("[getMarcaGrupoPorIdGrupoMarca] No se encontr\u00f3 informaci\u00f3n:{} ",
					new Object[] { e.getMessage() });
			return listResultado;
		}
	}

	public String getMarcaSeparadoComasYParentesis(List<MarcaGrupo> listadoMarcaGrupos) {
		String resultado = "(";
		for (MarcaGrupo marcaGrupo : listadoMarcaGrupos) {
			resultado = resultado.concat("'".concat(marcaGrupo.getMarcaGrupoPK().getIdMarca()).concat("',"));
		}
		resultado = resultado.substring(0, resultado.length() - 1).concat(")");
		return resultado;
	}

	public List<Long> consultarAniosCalendario() {
		String queryStr = "SELECT DISTINCT c.anio FROM Calendario c  ORDER BY c.anio ASC";
		TypedQuery<Long> query = (TypedQuery<Long>) this.em.createQuery(queryStr, (Class) Long.class);
		return query.getResultList();
	}

	public List<Long> consultarMesesCalendario() {
		String queryStr = "SELECT DISTINCT c.mes FROM Calendario c ORDER BY c.mes ASC";
		TypedQuery<Long> query = (TypedQuery<Long>) this.em.createQuery(queryStr, (Class) Long.class);
		return query.getResultList();
	}

	/**
	 * 
	 * @param anioSeleccionado
	 * @return
	 */
	public List<Integer> consultarMesesCalendario(Long anioSeleccionado) {
		String queryStr = "SELECT DISTINCT c.mes FROM Calendario c WHERE c.anio = :anio ORDER BY c.mes ASC";
		TypedQuery<Integer> query = this.em.createQuery(queryStr, (Class) Integer.class);
		query.setParameter("anio", (Object) anioSeleccionado);
		return query.getResultList();
	}

	/**
	 * 
	 * @return
	 */
	public List<Long> consultarSemanasCalendario() {
		String queryStr = "SELECT DISTINCT c.semana FROM Calendario c ORDER BY c.semana ASC";
		TypedQuery<Long> query = (TypedQuery<Long>) this.em.createQuery(queryStr, (Class) Long.class);
		return query.getResultList();
	}

	public List<Long> consultarSemanasCalendario(Long anioSeleccionado, Long mesSeleccionado) {
		List<Long> meses = null;
		if (mesSeleccionado != null) {
			String queryStr = "SELECT DISTINCT c.semana FROM Calendario c WHERE c.anio = :anio AND c.mes = :mes ORDER BY c.semana ASC";
			TypedQuery<Long> query = (TypedQuery<Long>) this.em.createQuery(queryStr, (Class) Long.class);
			query.setParameter("anio", (Object) anioSeleccionado);
			query.setParameter("mes", (Object) mesSeleccionado.intValue());
			meses = query.getResultList();
		}
		return meses;
	}

	public Forecast consultarForecast(Retailer retailer, String marca, MesSop mes, String estadoForecast) {
		try {
			Forecast out = null;
			String queryStr = "SELECT f FROM Forecast f WHERE f.retailerFk = :retailer AND f.mesFk = :mes and f.marcaFk.id IN "
					+ marca;
			if (estadoForecast != null) {
				queryStr += " and f.estadoFk = :estado";
			}
			TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
			query.setParameter("retailer", (Object) retailer);
			query.setParameter("mes", (Object) mes);
			if (estadoForecast != null) {
				query.setParameter("estado", (Object) this.buscaEstadoPorCodigo(estadoForecast));
			}
			List<Forecast> result = query.getResultList();
			if (!result.isEmpty()) {
				out = result.get(0);
				for (Forecast r : result) {
					System.out.println(r.getRetailerFk().getId() + "'" + r.getEstadoFk().getNombre() + "'");
					if (r.getEstadoFk().getNombre().equals("ABIERTO KAM")) {
						out.setEstadoFk(r.getEstadoFk());
					}
				}
			}
			return out;
		} catch (NoResultException e) {
			log.info("[consultarForecast] No se encontr\u00f3 informaci\u00f3n:{} ", new Object[] { e.getMessage() });
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> consultarForecastV1(Retailer retailer, String marca, MesSop mes, String estadoForecast) {
		try {
			Map out = new HashMap<String, Object>();
			String queryStr = "SELECT f FROM Forecast f WHERE f.retailerFk = :retailer AND f.mesFk = :mes and f.marcaFk.id IN "
					+ marca;
			if (estadoForecast != null) {
				queryStr += " and f.estadoFk = :estado";
			}
			TypedQuery<Forecast> query = this.em.createQuery(queryStr, Forecast.class);
			query.setParameter("retailer", retailer);
			query.setParameter("mes", (Object) mes);
			if (estadoForecast != null) {
				query.setParameter("estado", (Object) this.buscaEstadoPorCodigo(estadoForecast));
			}

			List<Forecast> result = query.getResultList();
			if (!result.isEmpty()) {
				out.put("FORECAST", result.get(0));
				EstadoForecast estado = result.get(0).getEstadoFk();
				for (Forecast r : result) {
					if (r.getEstadoFk().getNombre().contains("ABIERTO KAM")) {
						estado = r.getEstadoFk();
					}
				}
				out.put("ESTADO", estado);
			}
			return out;
		} catch (NoResultException e) {
			log.info("[consultarForecast] No se encontr\u00f3 informaci\u00f3n:{} ", new Object[] { e.getMessage() });
			return null;
		}
	}

	public List<Forecast> consultarForecast(String marca, MesSop mes, Compania compania) {
		String queryStr = "SELECT f FROM Forecast f WHERE f.mesFk = :mes and  f.estadoFk= 'CNFG' and f.marcaFk.id IN "
				+ marca;
		if (compania != null) {
			queryStr += " and f.companiaFk = :compania ";
		}
		TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
		query.setParameter("mes", (Object) mes);
		if (compania != null) {
			query.setParameter("compania", (Object) compania);
		}
		return query.getResultList();
	}

	public List<String> consultarForecastEstados(String marca, MesSop mes, Compania compania) {
		String queryStr = "SELECT f FROM Forecast f WHERE f.mesFk = :mes and  f.estadoFk != 'INIC' and f.marcaFk.id IN "
				+ marca + " and f.companiaFk = :compania ";
		TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
		query.setParameter("mes", (Object) mes);
		query.setParameter("compania", (Object) compania);
		List<Forecast> listEstados = query.getResultList();
		List<String> estados = new ArrayList<String>();
		for (Forecast forecast : listEstados) {
			estados.add(forecast.getEstadoFk().getCodigo());
		}
		return estados;
	}

	public Forecast consultarForecastByCompania(String marca, MesSop mes, List<Compania> compania, boolean isCerrado) {
		String queryStr = "SELECT f FROM Forecast f WHERE f.mesFk = :mes and f.marcaFk.id IN " + marca
				+ " and f.companiaFk IN :compania AND (f.estadoFk.id = 'CNFD' "
				+ (isCerrado ? ")" : "or f.estadoFk.id = 'CNFG')");
		TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
		query.setParameter("mes", (Object) mes);
		query.setParameter("compania", (Object) compania);
		List<Forecast> oForecast = query.getResultList();
		return oForecast.isEmpty() ? null : query.getResultList().get(0);
	}

	public List<Forecast> consultarForecastIN(Retailer retailer, String marcas, MesSop mes, String estadoForecast) {
		String queryStr = "SELECT f FROM Forecast f WHERE f.retailerFk = :retailer AND f.mesFk = :mes and f.marcaFk.id IN "
				+ marcas + " and f.estadoFk IN ('ELSE','DEVL','CERR')";
		TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
		query.setParameter("retailer", (Object) retailer);
		query.setParameter("mes", (Object) mes);
		List<Forecast> resp = query.getResultList();
		log.info("Retorna " + resp.size() + "Al consultar forecast a enviar");
		return resp;
	}

	public List<Forecast> consultarForecastXRetailersYMarcas(List<String> retailers, List<String> marcas, MesSop mes) {
		String queryStr = "SELECT f FROM Forecast f WHERE f.retailerFk.id IN :retailers AND f.mesFk = :mes  AND f.marcaFk.id IN :marcas AND f.retailerFk.nombreComercial not like '%otros%' AND estadoFk IN ('ELSE', 'DEVL')";
		TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
		query.setParameter("retailers", (Object) retailers);
		query.setParameter("marcas", (Object) marcas);
		query.setParameter("mes", (Object) mes);
		List<Forecast> resp = query.getResultList();
		return resp;
	}

	public boolean consultaRetailerDEVL_KAM(List<String> retailers, List<String> marcas, MesSop mes) {
		String queryStr = "SELECT f FROM Forecast f WHERE f.retailerFk.id IN :retailers AND f.mesFk = :mes  and f.marcaFk.id IN :marcas and f.estadoFk = 'DEVL_KAM' ";
		String msg = "[consultaRetailerDEVL_KAM]Consultar DEVL_KAM ::: retailers " + retailers + "  marcas " + marcas
				+ " mes " + mes;
		log.info(msg);
		TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
		query.setParameter("retailers", (Object) retailers);
		query.setParameter("marcas", (Object) marcas);
		query.setParameter("mes", (Object) mes);
		List<Forecast> resp = query.getResultList();
		return !resp.isEmpty();
	}

	public boolean consultaCompaniaRetailerDEVL_KAM(Compania companiaSeleccionada, List<String> marcas, MesSop mes) {
		String queryStr = "SELECT f FROM Forecast f WHERE f.companiaFk = :compania AND f.mesFk = :mes  and f.marcaFk.id IN :marcas and f.estadoFk IN ('DEVL_KAM','INIC') ";
		String msg = "Consultar DEVL_KAM ::: companiaSeleccionada " + companiaSeleccionada + "  marcas " + marcas
				+ " mes " + mes;
		log.info(msg);
		TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
		query.setParameter("compania", (Object) companiaSeleccionada);
		query.setParameter("marcas", (Object) marcas);
		query.setParameter("mes", (Object) mes);
		List<Forecast> resp = query.getResultList();
		return !resp.isEmpty();
	}

	public boolean consultarRetailersSinOtros(List<String> retailers) {
		try {
			String queryStr = "SELECT f FROM Retailer f WHERE f.id in :retailers AND f.nombreComercial not like '%otros%'";
			TypedQuery<Retailer> query = (TypedQuery<Retailer>) this.em.createQuery(queryStr, (Class) Retailer.class);
			query.setParameter("retailers", (Object) retailers);
			List<Retailer> resp = (List<Retailer>) query.getResultList();
			return !resp.isEmpty();
		} catch (Exception e) {
			log.error("consultarRetailersSinOtros ", (Throwable) e);
			return false;
		}
	}

	public MesSop consultarMesSOP(int mes, int anio) {
		String queryStr = "SELECT m FROM MesSop m WHERE m.mes = :mes and m.anio = :anio ";
		TypedQuery<MesSop> query = (TypedQuery<MesSop>) this.em.createQuery(queryStr, (Class) MesSop.class);
		query.setParameter("mes", (Object) mes);
		query.setParameter("anio", (Object) anio);
		MesSop mesSop = null;
		try {
			mesSop = (MesSop) query.getSingleResult();
		} catch (NoResultException ex) {
			log.error("[consultarMesSOP] No se enconetro el mes consultado");
		}
		return mesSop;
	}

	public MesSop consultarMesSOP(String mes, int anio) {
		String queryStr = "SELECT m FROM MesSop m WHERE m.nombre = :nombre and m.anio = :anio ";
		TypedQuery<MesSop> query = this.em.createQuery(queryStr, MesSop.class);
		query.setParameter("nombre", (Object) mes);
		query.setParameter("anio", (Object) anio);
		MesSop mesSop = null;
		try {
			mesSop = query.getSingleResult();
		} catch (NoResultException ex) {
			log.error("Error al consultar MesSOP por nombre y anio ", (Throwable) ex);
		}
		return mesSop;
	}

	public List<MesSop> consultarMesesSOP(boolean isMesesFutura) {
		Calendar c = Calendar.getInstance();
		Integer mes = c.get(2) + (isMesesFutura ? 2 : 1);
		Integer annio = c.get(1);
		if (mes > 12) {
			mes %= 12;
			++annio;
		}
		String queryStr = "SELECT m FROM MesSop m WHERE m.anio = (SELECT MIN(s.anio) FROM MesSop s)  or (m.anio = :annio AND m.mes <= :mes)  or (m.anio < :annio)  order by m.anio desc, m.mes desc ";
		TypedQuery<MesSop> query = (TypedQuery<MesSop>) this.em.createQuery(queryStr, (Class) MesSop.class);
		query.setParameter("mes", (Object) mes);
		query.setParameter("annio", (Object) annio);
		return (List<MesSop>) query.getResultList();
	}

	public List<MesSop> consultarMesesSOPListas() {
		Calendar c = Calendar.getInstance();
		Integer mes = c.get(2);
		Integer annio = c.get(1);
		String queryStr = "SELECT m FROM MesSop m WHERE (m.mes = :mes AND m.anio = :annio)  OR (m.anio = :annio AND m.mes > :mes)  OR (m.anio > :annio) ORDER BY m.anio, m.mes ASC ";
		TypedQuery<MesSop> query = (TypedQuery<MesSop>) this.em.createQuery(queryStr, (Class) MesSop.class);
		query.setParameter("mes", (Object) (mes + 1));
		query.setParameter("annio", (Object) annio);
		return (List<MesSop>) query.setMaxResults(18).getResultList();
	}

	public MesSop consultaMesSop(Long mesId) {
		return (MesSop) this.em.find((Class) MesSop.class, (Object) mesId);
	}

	public MesSop consultaMesSopPorID(Long mesId) {
		try {
			return (MesSop) this.em.createNamedQuery("MesSop.findById").setParameter("id", (Object) mesId)
					.getSingleResult();
		} catch (NoResultException e) {
			log.error("[consultaMesSopPorID] No se encontr\u00f3 informaci\u00f3n");
			return null;
		}
	}

	public ProductoXRetailerXMes consultarProductoRetailerMes(Producto producto, Retailer retailer, MesSop mes) {
		String queryStr = "SELECT prm FROM ProductoXRetailerXMes prm WHERE prm.productoFk = :producto and prm.retailerFk = :retailer and prm.mesFk = :mes";
		TypedQuery<ProductoXRetailerXMes> query = (TypedQuery<ProductoXRetailerXMes>) this.em.createQuery(queryStr,
				(Class) ProductoXRetailerXMes.class);
		query.setParameter("producto", (Object) producto);
		query.setParameter("retailer", (Object) retailer);
		query.setParameter("mes", (Object) mes);
		ProductoXRetailerXMes resp = null;
		try {
			resp = (ProductoXRetailerXMes) query.getSingleResult();
		} catch (NoResultException ex) {
			resp = null;
		}
		return resp;
	}

	public List<Retailer> consultarRetailers(String companiaId) {
		String qlString = "SELECT r FROM Retailer r WHERE r.statusRe='1' AND r.companiaFk.id = :compania AND r.nombreComercial IS NOT NULL ORDER BY r.nombreComercial";
		if (companiaId.equals("00070")) {
			return new ArrayList<Retailer>();
		}
		return (List<Retailer>) this.em.createQuery(qlString).setParameter("compania", (Object) companiaId)
				.getResultList();
	}

	public List<Retailer> consultarAllRetailers(String inCompaniaId) {
		String qlString = "SELECT r FROM Retailer r WHERE  r.companiaFk.id = :compania AND r.nombreComercial IS NOT NULL ORDER BY r.nombreComercial";
		if (inCompaniaId.equals("00070")) {
			return new ArrayList<Retailer>();
		}
		return (List<Retailer>) this.em.createQuery(qlString).setParameter("compania", (Object) inCompaniaId)
				.getResultList();
	}

	public List<Retailer> consultarRetailersNombre(String companiaId) {
		String qlString = "SELECT r FROM Retailer r WHERE r.companiaFk.nombre = :compania ORDER BY r.nombreComercial";
		return (List<Retailer>) this.em.createQuery(qlString).setParameter("compania", (Object) companiaId)
				.getResultList();
	}

	public List<String> leeDatosServidorCorreo(List<String> datosServidor) {
		String select = "SELECT valor FROM PARAMETRO WHERE nombre = ?1";
		try {
			Query host = this.em.createNativeQuery(select).setParameter(1, (Object) "MAIL_HOST");
			Query puerto = this.em.createNativeQuery(select).setParameter(1, (Object) "MAIL_PORT");
			Query user = this.em.createNativeQuery(select).setParameter(1, (Object) "MAIL_USER");
			Query pass = this.em.createNativeQuery(select).setParameter(1, (Object) "MAIL_PASS");
			Query from = this.em.createNativeQuery(select).setParameter(1, (Object) "MAIL_FROM");
			datosServidor.add((String) host.getSingleResult());
			datosServidor.add((String) puerto.getSingleResult());
			datosServidor.add((String) user.getSingleResult());
			datosServidor.add((String) pass.getSingleResult());
			datosServidor.add((String) from.getSingleResult());
		} catch (Exception ex) {
			log.error("Error en Consulta de datos servidor de correo para notificaci\u00f3n \n", (Throwable) ex);
		}
		return datosServidor;
	}

	public void notifica(Compania compania, String subject, String body, String body2, String etapa,
			boolean needCompania) {
		FuncionesUtilitariasDemand fud = new FuncionesUtilitariasDemand();
		List<Usuario> listaUsuariosNotificarTM = new ArrayList<Usuario>();
		List<Long> listRolTM = new ArrayList<Long>();
		List<String> datosServidorCorreo = new ArrayList<String>();
		try {
			datosServidorCorreo = this.leeDatosServidorCorreo(datosServidorCorreo);
			Query query = this.em.createQuery("SELECT e FROM EtapasXRol e WHERE e.etapa = :etapa");
			query.setParameter("etapa", (Object) etapa);
			query.setHint("javax.persistence.cache.storeMode", (Object) "REFRESH");
			List<EtapasXRol> listaEtapasXRolTM = (List<EtapasXRol>) query.getResultList();
			for (EtapasXRol exr : listaEtapasXRolTM) {
				if (!listRolTM.contains(exr.getIdRol().getId())) {
					listRolTM.add(exr.getIdRol().getId());
				}
			}
			Query query2 = this.em.createQuery("SELECT u FROM UsuariosXRol u WHERE u.idRol.id in ?1");
			query2.setParameter(1, (Object) listRolTM);
			query2.setHint("javax.persistence.cache.storeMode", (Object) "REFRESH");
			List<UsuariosXRol> listaUsuariosXRolTM = (List<UsuariosXRol>) query2.getResultList();
			for (UsuariosXRol uxr : listaUsuariosXRolTM) {
				log.info("[notifica] usaurios 1 {}", new Object[] { uxr.getIdUsuario().getCompaniaId() });
				if (uxr.getIdUsuario().getCompaniaId() != null) {
					if (uxr.getIdUsuario().getEmail() == null) {
						continue;
					}
					if (needCompania) {
						if (!uxr.getIdUsuario().getCompaniaId().getId().equals(compania.getId())
								|| listaUsuariosNotificarTM.contains(uxr.getIdUsuario())) {
							continue;
						}
						log.info("[notifica] Agrego metodo_1 if {}", new Object[] { uxr.getIdUsuario() });
						listaUsuariosNotificarTM.add(uxr.getIdUsuario());
					} else {
						if (listaUsuariosNotificarTM.contains(uxr.getIdUsuario())) {
							continue;
						}
						log.info("[notifica] Agrego metodo1 else {}", new Object[] { uxr.getIdUsuario() });
						listaUsuariosNotificarTM.add(uxr.getIdUsuario());
					}
				}
			}
			if (!listaUsuariosNotificarTM.isEmpty()) {
				fud.notificarTerminoFase((List) listaUsuariosNotificarTM, (List) datosServidorCorreo, subject, body,
						body2);
			} else {
				log.info(" [notifica]No se encontraron usuarios a notificar");
			}
		} catch (NoResultException e) {
			log.info("[notifica] No se encontr\u00f3 informaci\u00f3n:{}", new Object[] { e.getMessage() });
		} catch (Exception e2) {
			log.error(" [notifica] No se encontr\u00f3 informaci\u00f3n:", (Throwable) e2);
		}
	}

	public void notifica(Compania compania, String marca, List<String> listRetailer, MesSop mesSop, String subject,
			String body, String body2, String etapa, boolean needCompania) {
		FuncionesUtilitariasDemand fud = new FuncionesUtilitariasDemand();
		List<Usuario> listaUsuariosNotificarTM = new ArrayList<Usuario>();
		List<Long> listRolTM = new ArrayList<Long>();
		List<String> datosServidorCorreo = new ArrayList<String>();
		try {
			datosServidorCorreo = this.leeDatosServidorCorreo(datosServidorCorreo);
			Query query = this.em.createQuery("SELECT e FROM EtapasXRol e WHERE e.etapa = :etapa");
			query.setParameter("etapa", (Object) etapa);
			query.setHint("javax.persistence.cache.storeMode", (Object) "REFRESH");
			List<EtapasXRol> listaEtapasXRolTM = (List<EtapasXRol>) query.getResultList();
			for (EtapasXRol exr : listaEtapasXRolTM) {
				if (!listRolTM.contains(exr.getIdRol().getId())) {
					listRolTM.add(exr.getIdRol().getId());
				}
			}
			Query query2 = this.em.createQuery("SELECT u FROM UsuariosXRol u WHERE u.idRol.id in ?1");
			query2.setParameter(1, (Object) listRolTM);
			query2.setHint("javax.persistence.cache.storeMode", (Object) "REFRESH");
			List<UsuariosXRol> listaUsuariosXRolTM = (List<UsuariosXRol>) query2.getResultList();
			for (UsuariosXRol uxr : listaUsuariosXRolTM) {
				if (uxr.getIdUsuario().getEmail() == null) {
					continue;
				}
				boolean existe = this.existeUsuarioCompania(uxr.getIdUsuario(), compania);
				if (needCompania) {
					if (!existe || listaUsuariosNotificarTM.contains(uxr.getIdUsuario())) {
						continue;
					}
					listaUsuariosNotificarTM.add(uxr.getIdUsuario());
				} else if (!listaUsuariosNotificarTM.contains(uxr.getIdUsuario()) && existe) {
					listaUsuariosNotificarTM.add(uxr.getIdUsuario());
				} else {
					if (listaUsuariosNotificarTM.contains(uxr.getIdUsuario()) || compania != null) {
						continue;
					}
					listaUsuariosNotificarTM.add(uxr.getIdUsuario());
				}
			}
			if (!listaUsuariosNotificarTM.isEmpty()) {
				fud.notificarTerminoFase((List) listaUsuariosNotificarTM, (List) datosServidorCorreo, subject, body,
						body2, compania, marca, (List) listRetailer, mesSop);
			} else {
				log.info("[notifica] No se encontraron usuarios a notificar");
			}
		} catch (NoResultException e) {
			log.info("[notifica]No se encontr\u00f3 informaci\u00f3n:{}", new Object[] { e.getMessage() });
		} catch (Exception e2) {
			log.error("[notifica]No se encontr\u00f3 informaci\u00f3n:", (Throwable) e2);
		}
	}

	public Usuario buscaUsuarioLogueado(Usuario usuarioLogueado) {
		try {
			return (Usuario) this.em.createNamedQuery("Usuario.findById")
					.setParameter("id", (Object) usuarioLogueado.getId()).getSingleResult();
		} catch (NoResultException e) {
			log.info("[buscaUsuarioLogueado]No se encontr\u00f3 informaci\u00f3n:{}", new Object[] { e.getMessage() });
			return null;
		}
	}

	public Usuario buscaUsuarioPorId(Usuario usuario) {
		log.info("[buscaUsuarioPorId] Ingreso ===================  buscaUsuarioLogueado  ========================");
		try {
			Usuario u = (Usuario) this.em.createNamedQuery("Usuario.findById")
					.setParameter("id", (Object) usuario.getId()).getSingleResult();
			log.info(
					"[buscaUsuarioPorId] Consultar ===================  buscaUsuarioLogueado  ======================== {}",
					new Object[] { u.getNombre() });
			log.info(
					"[buscaUsuarioPorId] Consultar ===================  buscaUsuarioLogueado  ======================== {}",
					new Object[] { u.getUsuariosXRolList() });
			return u;
		} catch (NoResultException e) {
			log.info("[buscaUsuarioPorId]No se encontr\u00f3 informaci\u00f3n:{}", new Object[] { e.getMessage() });
			return null;
		}
	}

	public Compania buscaCompaniaPorId(String companiaId) {
		try {
			return (Compania) this.em.createNamedQuery("Compania.findById").setParameter("id", (Object) companiaId)
					.getSingleResult();
		} catch (NoResultException e) {
			log.info("[buscaCompaniaPorId]No se encontr\u00f3 informaci\u00f3n:{}", new Object[] { e.getMessage() });
			return null;
		}
	}

	public EstadoForecast buscaEstadoPorCodigo(String estado) {
		try {
			return (EstadoForecast) this.em.createNamedQuery("EstadoForecast.findByCodigo")
					.setParameter("codigo", (Object) estado).getSingleResult();
		} catch (NoResultException e) {
			log.info("[EstadoForecast]No se encontr\u00f3 informaci\u00f3n:{}", new Object[] { e.getMessage() });
			return null;
		}
	}

	public Producto buscaProductoPorCodigo(Producto producto) {
		try {
			return (Producto) this.em.createNamedQuery("Producto.findById")
					.setParameter("id", (Object) producto.getId()).getSingleResult();
		} catch (NoResultException e) {
			log.info("[buscaProductoPorCodigo]No se encontr\u00f3 informaci\u00f3n:{}",
					new Object[] { e.getMessage() });
			return null;
		}
	}

	public String buscaEstadoProductoPorNombre(String nombreProducto) {
		try {
			Producto productoFinal;
			Query query = this.em.createQuery("SELECT p FROM Producto p WHERE p.descripcion = :descripcion");
			query.setParameter("descripcion", (Object) nombreProducto);
			List<Producto> productoTM = query.getResultList();
			if (!productoTM.isEmpty()) {
				productoFinal = productoTM.get(0);
				return productoFinal.getFlagEstado();
			}
			return null;
		} catch (NoResultException e) {
			log.debug("No se encontró información:" + e.getMessage());
			return null;
		}
	}

	public ProductoXCompania buscaCompaniaProducto(Producto prd, Compania compania) {
		try {
			Query query = this.em.createQuery(
					"SELECT p FROM ProductoXCompania p WHERE p.productoFk = :idProducto AND p.companiaFk = :idComp");
			query.setParameter("idProducto", (Object) prd);
			query.setParameter("idComp", (Object) compania);
			return (ProductoXCompania) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public ProductoXCompania buscaCompaniaProducto(String prd, String compania) {
		Query query = this.em.createQuery(
				"SELECT p FROM ProductoXCompania p WHERE p.productoFk.id = :idProducto AND p.companiaFk.id = :idComp");
		query.setParameter("idProducto", (Object) prd);
		query.setParameter("idComp", (Object) compania);
		List<ProductoXCompania> prodCompania = (List<ProductoXCompania>) query.getResultList();
		if (!prodCompania.isEmpty()) {
			return prodCompania.get(0);
		}
		return new ProductoXCompania();
	}

	@SuppressWarnings("unchecked")
	public String buscarEstadoDescontinuadoProducto(String producto, String companiaSeleccionadaIde,
			String estadoProducto) {
		List<Producto> productoTM;
		Producto productoFinal = new Producto();

		try {
			Query query = this.em.createQuery("SELECT p FROM Producto p WHERE p.upc = :upc");
			query.setParameter("upc", (Object) producto);
			productoTM = query.getResultList();
			if (!productoTM.isEmpty()) {
				productoFinal = productoTM.get(0);
			}

			ProductoXCompania prodCompania = this.buscaCompaniaProducto(productoFinal.getId(), companiaSeleccionadaIde);

			if (prodCompania == null) {
				return estadoProducto;
			}

			if (prodCompania.getEstadoDes() != null && prodCompania.getEstadoDes().equals("D")) {
				return prodCompania.getEstadoDes();
			}

			return estadoProducto;
		} catch (NoResultException e) {
			log.debug("No se encontró información:" + e.getMessage());
			return null;
		}
	}

	public int consultarMesesFuturos(Long idForecast) {
		try {
			Query query = this.em.createNativeQuery(
					"SELECT COUNT(distinct fcp.mes_sop) num_meses FROM  FORECAST_PRODUCTO fcp WHERE fcp.forecast_fk = ?1 ");
			query.setParameter(1, idForecast);
			return (int) query.getSingleResult();
		} catch (Exception e) {
			log.error("[consultarMesesFuturos] Error en la consulta de retailer otros", (Throwable) e);
			return 0;
		}
	}

	public int consultarMeses(boolean evaluaEstado, Long idForecast) {
		if (!evaluaEstado) {
			return this.consultarParametroMeses();
		}
		return (idForecast != 0L) ? this.consultarMesesFuturos(idForecast) : this.consultarParametroMeses();
	}

	public int consultarParametroMeses() {
		Integer parametroValor = null;
		try {
			Query query = this.em.createNativeQuery("SELECT valor FROM parametro WHERE nombre = 'MESES_FORECAST' ");
			parametroValor = Integer.valueOf((String) query.getSingleResult());
		} catch (Exception e) {
			log.error("Error en la consulta de retailer otros", (Throwable) e);
		}
		return parametroValor;
	}

	public List<String> consultarNombresMeses(int mesesForecastParametro, Long id) {
		List<String> nombresMeses = null;
		try {
			Query query = this.em.createNativeQuery("SELECT m.nombre FROM Mes_Sop m WHERE (m.id >= ?1 AND m.id < ?2) ");
			query.setParameter(1, id);
			query.setParameter(2, (id + mesesForecastParametro));
			nombresMeses = (List<String>) query.getResultList();
		} catch (Exception e) {
			log.error("Error en la consulta de retailer otros", (Throwable) e);
		}
		return nombresMeses;
	}

	public Map<String, UnidadesdeSupply> consultarUnidadesSupply(String idCategoria, int idMes, String idCompania,
			String idMarca) {
		Map<String, UnidadesdeSupply> objItemUnidadesSupply = new HashMap<String, UnidadesdeSupply>();
		String whereCompania = "";
		if (idCompania != null) {
			whereCompania = "fc.compania_fk = ?4 AND ";
		}
		Query query = this.em.createNativeQuery(
				" SELECT p.marca_cat2_fk+p.grupo_cat4_fk+p.plataforma_cat5_fk categoria, \n       SUM(f.uni_faltan) uni_faltan,\n       SUM(f.uni_desp) uni_desp,\n       f.mes_fk,\n       SUM(f.precio) precio,\n       SUM(f.cogs) cogs\n FROM FORECAST_COMPANIA f \n INNER JOIN PRODUCTO p ON p.id = f.producto_fk  WHERE id_padre IN (         SELECT fc.id FROM FORECAST_COMPANIA fc\n         WHERE  "
						+ whereCompania + "\n             fc.marca_fk IN " + idMarca
						+ " AND\n             fc.mes_fk = ?2 AND\n             fc.id = fc.id_padre\n )AND\n p.marca_cat2_fk+p.grupo_cat4_fk+p.plataforma_cat5_fk = ?1\n \n \n GROUP BY mes_fk, p.marca_cat2_fk, p.grupo_cat4_fk, p.plataforma_cat5_fk");
		query.setParameter(1, idCategoria);
		query.setParameter(2, idMes);
		if (idCompania != null) {
			query.setParameter(4, idCompania);
		}
		List<Object[]> resultados = (List<Object[]>) query.getResultList();
		for (Object[] obj : resultados) {
			UnidadesdeSupply objItem = new UnidadesdeSupply();
			String llave = obj[0] + "-" + obj[3];
			objItem.setIdCategoria((String) obj[0]);
			objItem.setUnidadesFaltantes(Long.parseLong(obj[1].toString()));
			objItem.setUnidadesDespachadas(Long.parseLong(obj[2].toString()));
			objItem.setMes(Long.parseLong(obj[3].toString()));
			objItem.setPreciRetailer((BigDecimal) obj[4]);
			objItem.setCogs((BigDecimal) obj[5]);
			objItemUnidadesSupply.put(llave, objItem);
		}
		return objItemUnidadesSupply;
	}

	public ForecastXMesVO armarListaPantallaNMeses(List<ItemForecastRetailerVO> productosForecast,
			List<String> nombreMeses) {
		ForecastXMesVO forecast = new ForecastXMesVO();
		List<ProductosForecastMesVO> productosxMes = new ArrayList<ProductosForecastMesVO>();
		List<MarcaGrupoForecast> gruposMarcas = new ArrayList<MarcaGrupoForecast>();
		List<RetailerGrupoForecast> gruposRetailers = new ArrayList<RetailerGrupoForecast>();
		if (productosForecast != null && !productosForecast.isEmpty()) {
			for (ItemForecastRetailerVO forecastDB : productosForecast) {
				if (forecastDB.getIdProducto() != null && !forecastDB.getIdProducto().equals("0")) {
					forecast.setCompania(forecastDB.getIdCompania());
					forecast.setMarca(forecastDB.getIdMarca());
					forecast.setRetailer(forecastDB.getIdRetailer());
					forecast.setDescripcionRetailer(forecastDB.getDescripcionRetailer());
					forecast.setIdForecast(forecastDB.getIdForecast());
					forecast.setIdMes(forecastDB.getIdMes());
					forecast.setEstado(forecastDB.getEstadoForecast());
					break;
				}
			}
			int contador = 0;
			int indiceCategoria = 0;
			for (ItemForecastRetailerVO forecastDB2 : productosForecast) {
				ProductosForecastMesVO productoForecastxMes = new ProductosForecastMesVO();
				productoForecastxMes.setNombreProducto(forecastDB2.getCategoriaProducto());
				productoForecastxMes.setBudgetCategory(forecastDB2.getCategoriaNombre());
				productoForecastxMes.setIdProducto(
						forecastDB2.isEsProducto() ? forecastDB2.getIdProducto() : forecastDB2.getIdCategoria());
				if (!forecastDB2.isEsTotalMarca() && !forecastDB2.isEsProducto()) {
					productoForecastxMes
							.setIdProducto(forecastDB2.getIdCategoria() + "-" + forecastDB2.getIdRetailer());
				}
				if (forecastDB2.isEsTotalMarca() && forecastDB2.getIdRetailer() != null
						&& !forecastDB2.isEsProducto()) {
					productoForecastxMes
							.setIdProducto(forecastDB2.getIdCategoria() + "-" + forecastDB2.getIdRetailer());
				}
				if (!productosxMes.contains(productoForecastxMes)) {
					productoForecastxMes.setEsProdcuto(forecastDB2.isEsProducto());
					productoForecastxMes.setEsTotalMarca(forecastDB2.isEsTotalMarca());
					productoForecastxMes.setIndiceTabla(forecastDB2.getIndiceTabla());
					productoForecastxMes.setIdMarca(forecastDB2.getIdMarca());
					productoForecastxMes.setIdMarcaCategoria(forecastDB2.getMarcaCategoria());
					productoForecastxMes.setIdCategoria(forecastDB2.getIdCategoria());
					productoForecastxMes.setEstadoProducto(forecastDB2.getEstadoProducto());
					productoForecastxMes.setIdRetailer(forecastDB2.getIdRetailer());
					if (forecastDB2.isEsProducto()) {
						productoForecastxMes.setUpc(forecastDB2.getUpc());
						productoForecastxMes.setModelo(forecastDB2.getModelo());
					}
					if (!forecastDB2.isEsProducto()) {
						indiceCategoria = contador;
					}
					productoForecastxMes.setPosicionCategoria(indiceCategoria);
					productosxMes.add(productoForecastxMes);
					++contador;
				}
				MarcaGrupoForecast objMarcas = new MarcaGrupoForecast();
				objMarcas.setIdMarca(forecastDB2.getIdMarca());
				if (!gruposMarcas.contains(objMarcas) && !objMarcas.getIdMarca().equals("0")) {
					objMarcas.setIdForecast(forecastDB2.getIdForecast());
					gruposMarcas.add(objMarcas);
				}
				RetailerGrupoForecast objRetailers = new RetailerGrupoForecast();
				if (forecastDB2.getIdRetailer() != null) {
					objRetailers.setIdRetailer(forecastDB2.getIdRetailer());
					if (gruposRetailers.contains(objRetailers) || objRetailers.getIdRetailer().equals("0")) {
						continue;
					}
					objRetailers.setIdForecast(forecastDB2.getIdForecast());
					gruposRetailers.add(objRetailers);
				}
			}
			forecast.setMarcaGrupoForecast((List) gruposMarcas);
			forecast.setRetailerGrupoForecast((List) gruposRetailers);
			for (ProductosForecastMesVO productoMes : productosxMes) {
				for (ItemForecastRetailerVO forecastDB3 : productosForecast) {
					String idProducto = forecastDB3.isEsProducto() ? forecastDB3.getIdProducto()
							: forecastDB3.getIdCategoria();
					if (!forecastDB3.isEsTotalMarca() && !forecastDB3.isEsProducto()) {
						idProducto = forecastDB3.getIdCategoria() + "-" + forecastDB3.getIdRetailer();
					}
					if (forecastDB3.isEsTotalMarca() && forecastDB3.getIdRetailer() != null
							&& !forecastDB3.isEsProducto()) {
						idProducto = forecastDB3.getIdCategoria() + "-" + forecastDB3.getIdRetailer();
					}
					if (productoMes.getIdProducto().equals(idProducto)) {
						InformacionForecastXProductoVO infoForecast = new InformacionForecastXProductoVO();
						infoForecast.setIdForecastProducto(forecastDB3.getIdForecastProducto());
						infoForecast.setIdMes(forecastDB3.getIdMes());
						infoForecast.setForecast(Long
								.valueOf((forecastDB3.getForecastKam() == null) ? 0L : forecastDB3.getForecastKam()));
						infoForecast.setMargen(
								(forecastDB3.getMargen() == null) ? new BigDecimal(0) : forecastDB3.getMargen());
						infoForecast.setInventarioWH(forecastDB3.getInventarioWH());
						infoForecast.setTransito(forecastDB3.getTransito());
						infoForecast.setSellInPropuesto(
								Long.valueOf((forecastDB3.getSellInPropuesto() == null) ? Long.parseLong("0")
										: forecastDB3.getSellInPropuesto()));
						infoForecast.setTotalForecastGerenteAnterior(forecastDB3.getTotalForecastGerenteAnterior());
						infoForecast.setTotalForecastKam((forecastDB3.getTotalForecastKam() == null) ? new BigDecimal(0)
								: forecastDB3.getTotalForecastKam());
						infoForecast.setTotalPropuesto((forecastDB3.getTotalPropuesto() == null) ? new BigDecimal(0)
								: forecastDB3.getTotalPropuesto());
						infoForecast.setPrecioRetailer((forecastDB3.getPrecioRetailer() == null) ? new BigDecimal(0)
								: forecastDB3.getPrecioRetailer());
						infoForecast
								.setCogs((forecastDB3.getCogs() == null) ? new BigDecimal(0) : forecastDB3.getCogs());
						infoForecast.setForecastGerente(Long.valueOf(
								(forecastDB3.getForecastGerente() == null) ? 0L : forecastDB3.getForecastGerente()));
						infoForecast.setTotalForecastGerente(
								(forecastDB3.getTotalForecastGerente() == null) ? new BigDecimal(0)
										: forecastDB3.getTotalForecastGerente());
						infoForecast.setMargenGerente((forecastDB3.getMargenGerente() == null) ? new BigDecimal(0)
								: forecastDB3.getMargenGerente());
						infoForecast.setForecastGerenteAnterior(
								Long.valueOf((forecastDB3.getForecastGerenteAnterior() == null) ? 0L
										: forecastDB3.getForecastGerenteAnterior()));
						infoForecast.setSrpSinIva(new BigDecimal(0));
						infoForecast.setTotalForecastGerenteAnterior(
								(forecastDB3.getTotalForecastGerenteAnterior() == null) ? new BigDecimal(0)
										: forecastDB3.getTotalForecastGerenteAnterior());
						infoForecast.setPrecioSinTasa((forecastDB3.getPrecioSinTasa() == null) ? BigDecimal.ZERO
								: forecastDB3.getPrecioSinTasa());
						infoForecast.setEditarGerente(forecastDB3.getEditarGerente());
						infoForecast.setEditarKam(forecastDB3.getEditarKam());
						infoForecast.setNombreGrupo(forecastDB3.getNombreGrupo());
						infoForecast.setNombrePlataforma(forecastDB3.getNombrePlataforma());
						infoForecast.setRetailerId(forecastDB3.getIdRetailer());
						productoMes.agregarInfoMes(forecastDB3.getNombre(), infoForecast);
					}
				}
			}
			forecast.setProductosForecast((List) productosxMes);
		}
		return forecast;
	}

	public void actualizarInfoTabla(ProductosForecastMesVO item, ForecastXMesVO forecast, List<String> nombreMeses,
			boolean gerente, ForecastXMesVO productos, boolean reatailerSellin) {
		ProductosForecastMesVO itemCategoria = this.buscarCategoriaByRetailer(forecast, item);
		ProductosForecastMesVO prodForecast = this.buscarCategoriaTotalMarcaByRetailer(forecast, item);
		ProductosForecastMesVO prodForTotal = this.buscarCategoriaTotalGrupo(forecast);
		ProductosForecastMesVO prodForRetailer = this.buscarRetailerByCateg(forecast, item);
		Long sumaForecast = 0L;
		BigDecimal sumaTotalForecast = BigDecimal.ZERO;
		BigDecimal sumaMargen = BigDecimal.ZERO;
		Long sumaForecastGerente = 0L;
		BigDecimal sumaTotalForecastGerente = BigDecimal.ZERO;
		BigDecimal sumaMargenGerente = BigDecimal.ZERO;
		for (String mes : nombreMeses) {
			item.getInformacionForecastMes().get(mes).calcularTotalForecast();
			item.getInformacionForecastMes().get(mes).calcularMargen();
			if (!item.getInformacionForecastMes().get(mes).getForecast().toString()
					.equals(item.getInformacionForecastMes().get(mes).getTotalForecastGerenteAnterior().toString())) {
				item.getInformacionForecastMes().get(mes).setEditarKam(Boolean.valueOf(true));
			}
			if (gerente) {
				item.getInformacionForecastMes().get(mes).calcularTotalForecastGeneral();
				item.getInformacionForecastMes().get(mes).calcularMargenGeneral();
				if (!item.getInformacionForecastMes().get(mes).getForecastGerente().toString()
						.equals(item.getInformacionForecastMes().get(mes).getForecast().toString())) {
					item.getInformacionForecastMes().get(mes).setEditarGerente(Boolean.valueOf(true));
				}
			} else if (!item.getInformacionForecastMes().get(mes).getForecast().toString()
					.equals(item.getInformacionForecastMes().get(mes).getTotalForecastGerenteAnterior().toString())) {
				item.getInformacionForecastMes().get(mes).setEditarKam(Boolean.valueOf(true));
			}
			for (ProductosForecastMesVO productoForecast : productos.getProductosForecast()) {
				if (productoForecast.isEsProdcuto()) {
					sumaForecast += productoForecast.getInformacionForecastMes().get(mes).getForecast();
					sumaTotalForecast = sumaTotalForecast
							.add(productoForecast.getInformacionForecastMes().get(mes).getTotalForecastKam());
					sumaMargen = sumaMargen.add(productoForecast.getInformacionForecastMes().get(mes).getMargen());
					if (!gerente) {
						continue;
					}
					sumaForecastGerente += productoForecast.getInformacionForecastMes().get(mes).getForecastGerente();
					sumaTotalForecastGerente = sumaTotalForecastGerente
							.add(productoForecast.getInformacionForecastMes().get(mes).getTotalForecastGerente());
					sumaMargenGerente = sumaMargenGerente
							.add(productoForecast.getInformacionForecastMes().get(mes).getMargenGerente());
				}
			}
			Long categForecastKam = itemCategoria.getInformacionForecastMes().get(mes).getForecast();
			BigDecimal categTotalForecastKam = itemCategoria.getInformacionForecastMes().get(mes).getTotalForecastKam();
			itemCategoria.getInformacionForecastMes().get(mes).setForecast(sumaForecast);
			itemCategoria.getInformacionForecastMes().get(mes).setTotalForecastKam(sumaTotalForecast);
			itemCategoria.getInformacionForecastMes().get(mes).setMargen(sumaMargen);
			Long forecastGerente = itemCategoria.getInformacionForecastMes().get(mes).getForecastGerente();
			BigDecimal totalForecastGerente = itemCategoria.getInformacionForecastMes().get(mes)
					.getTotalForecastGerente();
			if (prodForecast != null) {
				Long forecastKamGrupo = prodForecast.getInformacionForecastMes().get(mes).getForecast();
				BigDecimal forecastKamTotalGrupo = BigDecimal.ZERO;
				if (reatailerSellin) {
					forecastKamTotalGrupo = prodForecast.getInformacionForecastMes().get(mes).getTotalForecastKam();
					prodForecast.getInformacionForecastMes().get(mes)
							.setTotalForecastKam(forecastKamTotalGrupo.subtract(categTotalForecastKam));
				}
				prodForecast.getInformacionForecastMes().get(mes)
						.setForecast(Long.valueOf(forecastKamGrupo - categForecastKam));
				prodForecast.getInformacionForecastMes().get(mes)
						.setForecast(Long.valueOf(prodForecast.getInformacionForecastMes().get(mes).getForecast()
								+ itemCategoria.getInformacionForecastMes().get(mes).getForecast()));
				prodForecast.getInformacionForecastMes().get(mes)
						.setTotalForecastKam(prodForecast.getInformacionForecastMes().get(mes).getTotalForecastKam()
								.add(itemCategoria.getInformacionForecastMes().get(mes).getTotalForecastKam()));
				if (prodForTotal != null) {
					log.info("PRODFORTOTAL GRUPO TOTAL ::: " + prodForTotal);
					if (reatailerSellin) {
						prodForTotal.getInformacionForecastMes().get(mes)
								.setTotalForecastKam(prodForTotal.getInformacionForecastMes().get(mes)
										.getTotalForecastKam().subtract(forecastKamTotalGrupo));
					}
					prodForTotal.getInformacionForecastMes().get(mes).setForecast(Long.valueOf(
							prodForTotal.getInformacionForecastMes().get(mes).getForecast() - forecastKamGrupo));
					prodForTotal.getInformacionForecastMes().get(mes)
							.setForecast(Long.valueOf(prodForTotal.getInformacionForecastMes().get(mes).getForecast()
									+ prodForecast.getInformacionForecastMes().get(mes).getForecast()));
					prodForTotal.getInformacionForecastMes().get(mes)
							.setTotalForecastKam(prodForTotal.getInformacionForecastMes().get(mes).getTotalForecastKam()
									.add(prodForecast.getInformacionForecastMes().get(mes).getTotalForecastKam()));
				}
				if (prodForRetailer != null) {
					log.info("PRODUCTO RET :: " + prodForRetailer);
					if (reatailerSellin) {
						log.info("REST :: " + forecastKamTotalGrupo);
						prodForRetailer.getInformacionForecastMes().get(mes)
								.setTotalForecastKam(prodForRetailer.getInformacionForecastMes().get(mes)
										.getTotalForecastKam().subtract(forecastKamTotalGrupo));
						log.info("REST 2 :: "
								+ prodForRetailer.getInformacionForecastMes().get(mes).getTotalForecastKam());
					}
					log.info("REST :: 3 " + forecastKamGrupo);
					prodForRetailer.getInformacionForecastMes().get(mes).setForecast(Long.valueOf(
							prodForRetailer.getInformacionForecastMes().get(mes).getForecast() - forecastKamGrupo));
					System.out
							.println("REST :: 4 " + prodForRetailer.getInformacionForecastMes().get(mes).getForecast());
					prodForRetailer.getInformacionForecastMes().get(mes)
							.setForecast(Long.valueOf(prodForRetailer.getInformacionForecastMes().get(mes).getForecast()
									+ prodForecast.getInformacionForecastMes().get(mes).getForecast()));
					System.out
							.println("REST :: 5 " + prodForRetailer.getInformacionForecastMes().get(mes).getForecast());
					prodForRetailer.getInformacionForecastMes().get(mes).setTotalForecastKam(
							prodForRetailer.getInformacionForecastMes().get(mes).getTotalForecastKam()
									.add(prodForecast.getInformacionForecastMes().get(mes).getTotalForecastKam()));
					log.info("REST 6 :: " + prodForRetailer.getInformacionForecastMes().get(mes).getTotalForecastKam());
				}
			}
			if (gerente) {
				itemCategoria.getInformacionForecastMes().get(mes).setForecastGerente(sumaForecastGerente);
				itemCategoria.getInformacionForecastMes().get(mes).setTotalForecastGerente(sumaTotalForecastGerente);
				itemCategoria.getInformacionForecastMes().get(mes).setMargenGerente(sumaMargenGerente);
				if (prodForecast != null) {
					Long forecastGerenteGrupo = prodForecast.getInformacionForecastMes().get(mes).getForecastGerente();
					BigDecimal totalForecastGerenteGrupo = prodForecast.getInformacionForecastMes().get(mes)
							.getTotalForecastGerente();
					prodForecast.getInformacionForecastMes().get(mes)
							.setForecastGerente(Long.valueOf(forecastGerenteGrupo - forecastGerente));
					prodForecast.getInformacionForecastMes().get(mes)
							.setTotalForecastGerente(totalForecastGerenteGrupo.subtract(totalForecastGerente));
					prodForecast.getInformacionForecastMes().get(mes).setForecastGerente(
							Long.valueOf(prodForecast.getInformacionForecastMes().get(mes).getForecastGerente()
									+ itemCategoria.getInformacionForecastMes().get(mes).getForecastGerente()));
					prodForecast.getInformacionForecastMes().get(mes)
							.setTotalForecastGerente(prodForecast.getInformacionForecastMes().get(mes)
									.getTotalForecastGerente()
									.add(itemCategoria.getInformacionForecastMes().get(mes).getTotalForecastGerente()));
					if (prodForTotal != null) {
						prodForTotal.getInformacionForecastMes().get(mes).setForecastGerente(
								Long.valueOf(prodForTotal.getInformacionForecastMes().get(mes).getForecastGerente()
										- forecastGerenteGrupo));
						prodForTotal.getInformacionForecastMes().get(mes)
								.setTotalForecastGerente(prodForTotal.getInformacionForecastMes().get(mes)
										.getTotalForecastGerente().subtract(totalForecastGerenteGrupo));
						prodForTotal.getInformacionForecastMes().get(mes).setForecastGerente(
								Long.valueOf(prodForTotal.getInformacionForecastMes().get(mes).getForecastGerente()
										+ prodForecast.getInformacionForecastMes().get(mes).getForecastGerente()));
						prodForTotal.getInformacionForecastMes().get(mes).setTotalForecastGerente(
								prodForTotal.getInformacionForecastMes().get(mes).getTotalForecastGerente().add(
										prodForecast.getInformacionForecastMes().get(mes).getTotalForecastGerente()));
					}
					if (prodForRetailer != null) {
						prodForRetailer.getInformacionForecastMes().get(mes).setForecastGerente(
								Long.valueOf(prodForRetailer.getInformacionForecastMes().get(mes).getForecastGerente()
										- forecastGerenteGrupo));
						prodForRetailer.getInformacionForecastMes().get(mes)
								.setTotalForecastGerente(prodForRetailer.getInformacionForecastMes().get(mes)
										.getTotalForecastGerente().subtract(totalForecastGerenteGrupo));
						prodForRetailer.getInformacionForecastMes().get(mes).setForecastGerente(
								Long.valueOf(prodForRetailer.getInformacionForecastMes().get(mes).getForecastGerente()
										+ prodForecast.getInformacionForecastMes().get(mes).getForecastGerente()));
						prodForRetailer.getInformacionForecastMes().get(mes).setTotalForecastGerente(
								prodForRetailer.getInformacionForecastMes().get(mes).getTotalForecastGerente().add(
										prodForecast.getInformacionForecastMes().get(mes).getTotalForecastGerente()));
					}
				}
			}
			sumaForecast = 0L;
			sumaTotalForecast = BigDecimal.ZERO;
			sumaMargen = BigDecimal.ZERO;
			sumaForecastGerente = 0L;
			sumaTotalForecastGerente = BigDecimal.ZERO;
			sumaMargenGerente = BigDecimal.ZERO;
		}
	}

	public ProductosForecastMesVO buscarCategoriaTotalMarca(ForecastXMesVO forecast, ProductosForecastMesVO item) {
		for (ProductosForecastMesVO prodForecast : forecast.getProductosForecast()) {
			if (prodForecast.isEsTotalMarca() && prodForecast.getIdProducto().equals(item.getIdMarca())) {
				return prodForecast;
			}
		}
		return null;
	}

	public ProductosForecastMesVO buscarCategoriaTotalMarcaByRetailer(ForecastXMesVO forecast,
			ProductosForecastMesVO item) {
		for (ProductosForecastMesVO prodForecast : forecast.getProductosForecast()) {
			if (prodForecast.isEsTotalMarca()
					&& prodForecast.getIdProducto().equals(item.getIdMarca() + "-" + item.getIdRetailer())) {
				return prodForecast;
			}
		}
		return null;
	}

	public ProductosForecastMesVO buscarCategoriaTotalGrupo(ForecastXMesVO forecast) {
		for (ProductosForecastMesVO prodForecast : forecast.getProductosForecast()) {
			if (prodForecast.isEsTotalMarca() && prodForecast.getIdProducto().equals("00")) {
				return prodForecast;
			}
		}
		return null;
	}

	public Boolean buscarEditarGerente(ForecastXMesVO forecast, String mes, ProductosForecastMesVO item) {
		for (ProductosForecastMesVO prodForecast : forecast.getProductosForecast()) {
			if (prodForecast.getIdProducto().equals(item.getIdCategoria())) {
				ModuloGeneralEJB.log
						.info("Entro if YES " + prodForecast.getInformacionForecastMes().get(mes).getEditarGerente());
				return prodForecast.getInformacionForecastMes().get(mes).getEditarGerente() != null
						&& prodForecast.getInformacionForecastMes().get(mes).getEditarGerente();
			}
		}
		return null;
	}

	public ProductosForecastMesVO buscarCategoria(ForecastXMesVO forecast, ProductosForecastMesVO item) {
		for (ProductosForecastMesVO prodForecast : forecast.getProductosForecast()) {
			if (prodForecast.getIdProducto().equals(item.getIdCategoria())) {
				return prodForecast;
			}
		}
		return null;
	}

	public ProductosForecastMesVO buscarCategoriaByRetailer(ForecastXMesVO forecast, ProductosForecastMesVO item) {
		for (ProductosForecastMesVO prodForecast : forecast.getProductosForecast()) {
			if (prodForecast.getIdProducto().equals(item.getIdCategoria() + "-" + item.getIdRetailer())) {
				return prodForecast;
			}
		}
		return null;
	}

	public ProductosForecastMesVO buscarRetailerByCateg(ForecastXMesVO forecast, ProductosForecastMesVO item) {
		for (ProductosForecastMesVO prodForecast : forecast.getProductosForecast()) {
			if (prodForecast.getIdProducto().equals(item.getIdRetailer()) && prodForecast.isEsTotalMarca()
					&& prodForecast.getIdRetailer() == null) {
				return prodForecast;
			}
		}
		return null;
	}

	public void cambiaEstadoProductos() {
		List<Producto> productosLanzamientoList = this.buscaProductoLanzamiento();
		if (productosLanzamientoList == null) {
			return;
		}
		for (Producto pr : productosLanzamientoList) {
			pr.setFlagEstado("C");
			this.em.merge(pr);
			log.info("Se cambia estado a C al Modelo:" + pr.getModelo());
		}
	}

	private List<Producto> buscaProductoLanzamiento() {
		Calendar fecha = Calendar.getInstance();
		Calendar fechaProducto = Calendar.getInstance();
		List<Producto> productosLanzamientoList = null;
		List<Producto> productosLanzamientoFinalList = new ArrayList<Producto>();
		try {
			Query query = this.em.createQuery("SELECT p FROM Producto p WHERE p.flagEstado = :flagEstado");
			query.setParameter("flagEstado", "L");
			productosLanzamientoList = (List<Producto>) query.getResultList();
			if (productosLanzamientoList == null) {
				return productosLanzamientoList;
			}
			for (Producto pr : productosLanzamientoList) {
				if (pr.getFechaLanzamiento() != null && pr.getSemanasLanzamiento() != null) {
					fechaProducto.setTime(pr.getFechaLanzamiento());
					fechaProducto.add(5, pr.getSemanasLanzamiento() * 7);
					if (!fechaProducto.before(fecha)) {
						continue;
					}
					productosLanzamientoFinalList.add(pr);
				}
			}
			return productosLanzamientoFinalList;
		} catch (NoResultException e) {
			log.debug("No se encontr\u00f3 informaci\u00f3n:" + e.getMessage());
			return productosLanzamientoList;
		}
	}

	public boolean verificarSiHuboEjecucionDiaria() {
		Query query = em
				.createNativeQuery("SELECT * FROM EJECUCIONES_ACTUALIZACION_PRODUCTO WHERE FECHA_EJECUCION = ?1");
		query.setParameter(1, new Date(new java.util.Date().getTime()));

		return query.getResultList().isEmpty();
	}

	public void registrarActualizacionProductos() {
		Query query = em
				.createNativeQuery("INSERT INTO EJECUCIONES_ACTUALIZACION_PRODUCTO (FECHA_EJECUCION) VALUES (?1)");
		query.setParameter(1, new Date(new java.util.Date().getTime()));

		query.executeUpdate();
	}

	public ForecastXMesVO armarListaSupplyPantalla(ForecastXMesVO forecast, List<String> nombreMeses,
			List<String> objMarcasForecast, Map<String, ForecastXMesVO> productos) {
		Map<String, ItemForecastCompaniaVO> informacionUnidades = this.obtenerUnidadesDespachadas(forecast);
		for (Map.Entry<String, ForecastXMesVO> entry : productos.entrySet()) {
			for (ProductosForecastMesVO objForecastProd : entry.getValue().getProductosForecast()) {
				if (objForecastProd.isEsProdcuto()) {
					for (String mes : nombreMeses) {
						if (informacionUnidades.containsKey(objForecastProd.getIdProducto() + "-"
								+ objForecastProd.getInformacionForecastMes().get(mes).getIdMes())) {
							objForecastProd.getInformacionForecastMes().get(mes)
									.setUnidadesDespachadas(informacionUnidades
											.get(objForecastProd.getIdProducto() + "-"
													+ objForecastProd.getInformacionForecastMes().get(mes).getIdMes())
											.getUni_pend());
							objForecastProd.getInformacionForecastMes().get(mes)
									.setUnidadesFaltantes(informacionUnidades
											.get(objForecastProd.getIdProducto() + "-"
													+ objForecastProd.getInformacionForecastMes().get(mes).getIdMes())
											.getUni_faltantes());
							objForecastProd.getInformacionForecastMes().get(mes)
									.setTotalUnidaDespachadas(informacionUnidades
											.get(objForecastProd.getIdProducto() + "-"
													+ objForecastProd.getInformacionForecastMes().get(mes).getIdMes())
											.getPrecioRetailer());
							objForecastProd.getInformacionForecastMes().get(mes)
									.setMargenUnidaDespachadas(informacionUnidades
											.get(objForecastProd.getIdProducto() + "-"
													+ objForecastProd.getInformacionForecastMes().get(mes).getIdMes())
											.getCogs());
						} else {
							objForecastProd.getInformacionForecastMes().get(mes)
									.setUnidadesDespachadas(Long.valueOf(0L));
							objForecastProd.getInformacionForecastMes().get(mes).setUnidadesFaltantes(
									objForecastProd.getInformacionForecastMes().get(mes).getForecastGerente());
							objForecastProd.getInformacionForecastMes().get(mes)
									.setTotalUnidaDespachadas(BigDecimal.ZERO);
							objForecastProd.getInformacionForecastMes().get(mes)
									.setMargenUnidaDespachadas(BigDecimal.ZERO);
						}
					}
				}
			}
		}
		return this.calculoTotalUnidades(forecast, nombreMeses, objMarcasForecast, productos);
	}

	public boolean verificarUnidadesDespachadas(ForecastXMesVO forecast) {
		String resultado = "(";
		for (MarcaGrupoForecast objGrupo : forecast.getMarcaGrupoForecast()) {
			resultado = resultado.concat("'".concat(objGrupo.getIdMarca()).concat("',"));
		}
		String marcas = resultado.substring(0, resultado.length() - 1).concat(")");
		log.info(
				"MARCA ::: " + marcas + " COMPANIA ::: " + forecast.getCompania() + " MES :::: " + forecast.getIdMes());
		Query query = this.em.createNativeQuery(
				"select producto_fk, uni_faltan , uni_desp, mes_fk, precio, cogs from FORECAST_COMPANIA where id_padre in (\nSELECT fc.id FROM Forecast_Compania  fc \n WHERE fc.compania_fk= ?1 and  fc.marca_Fk IN "
						+ marcas + " AND fc.mes_Fk=?3 and fc.id= fc.id_padre)\n order by producto_fk");
		query.setParameter(1, forecast.getCompania());
		query.setParameter(3, forecast.getIdMes());
		return !query.getResultList().isEmpty();
	}

	public Map<String, ItemForecastCompaniaVO> obtenerUnidadesDespachadas(ForecastXMesVO forecast) {
		String resultado = "(";
		for (MarcaGrupoForecast objGrupo : forecast.getMarcaGrupoForecast()) {
			resultado = resultado.concat("'".concat(objGrupo.getIdMarca()).concat("',"));
		}
		String marcas = resultado.substring(0, resultado.length() - 1).concat(")");
		Query query = this.em.createNativeQuery(
				"select producto_fk, uni_faltan , uni_desp, mes_fk, precio, cogs from FORECAST_COMPANIA where id_padre in (\nSELECT fc.id FROM Forecast_Compania  fc \n WHERE fc.compania_fk= ?1 and  fc.marca_Fk IN "
						+ marcas + " AND fc.mes_Fk=?3 and fc.id= fc.id_padre)\n order by producto_fk");
		query.setParameter(1, forecast.getCompania());
		query.setParameter(3, forecast.getIdMes());
		Map<String, ItemForecastCompaniaVO> informacionSupply = new LinkedHashMap<String, ItemForecastCompaniaVO>();
		List<Object[]> resultados = (List<Object[]>) query.getResultList();
		for (Object[] obj : resultados) {
			ItemForecastCompaniaVO objItem = new ItemForecastCompaniaVO();
			String llave = obj[0] + "-" + obj[3];
			objItem.setIdProducto((String) obj[0]);
			objItem.setUni_faltantes(Long.valueOf(Long.parseLong(obj[1].toString())));
			objItem.setUni_pend(Long.valueOf(Long.parseLong(obj[2].toString())));
			objItem.setIdMes(Long.valueOf(Long.parseLong(obj[3].toString())));
			objItem.setPrecioRetailer((BigDecimal) obj[4]);
			objItem.setCogs((BigDecimal) obj[5]);
			informacionSupply.put(llave, objItem);
		}
		return informacionSupply;
	}

	private ForecastXMesVO calculoTotalUnidades(ForecastXMesVO forecast, List<String> nombreMeses,
			List<String> calculoTotalUnidades, Map<String, ForecastXMesVO> productos) {
		Long uniFaltanTotal = 0L;
		Long uniDespacTotal = 0L;
		BigDecimal sumaTotalDespacho = BigDecimal.ZERO;
		BigDecimal sumaTotalMargen = BigDecimal.ZERO;
		ProductosForecastMesVO itemCategoria = null;
		for (String mes : nombreMeses) {
			for (ProductosForecastMesVO objCategoria : forecast.getProductosForecast()) {
				if (objCategoria.isEsTotalMarca()) {
					continue;
				}
				List<ProductosForecastMesVO> objProductos = (List<ProductosForecastMesVO>) productos
						.get(objCategoria.getIdProducto()).getProductosForecast();
				for (ProductosForecastMesVO objForecastProd : objProductos) {
					if (objForecastProd.isEsProdcuto()) {
						uniFaltanTotal += ((objForecastProd.getInformacionForecastMes().get(mes)
								.getUnidadesFaltantes() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getUnidadesFaltantes()
										: 0L);
						uniDespacTotal += ((objForecastProd.getInformacionForecastMes().get(mes)
								.getUnidadesDespachadas() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getUnidadesDespachadas()
										: 0L);
						sumaTotalDespacho = sumaTotalDespacho.add((objForecastProd.getInformacionForecastMes().get(mes)
								.getTotalUnidaDespachadas() != null)
										? objForecastProd.getInformacionForecastMes().get(mes)
												.getTotalUnidaDespachadas()
										: BigDecimal.ZERO);
						sumaTotalMargen = sumaTotalMargen.add((objForecastProd.getInformacionForecastMes().get(mes)
								.getMargenUnidaDespachadas() != null)
										? objForecastProd.getInformacionForecastMes().get(mes)
												.getMargenUnidaDespachadas()
										: BigDecimal.ZERO);
					}
				}
				objCategoria.getInformacionForecastMes().get(mes).setUnidadesFaltantes(uniFaltanTotal);
				objCategoria.getInformacionForecastMes().get(mes).setUnidadesDespachadas(uniDespacTotal);
				objCategoria.getInformacionForecastMes().get(mes).setTotalUnidaDespachadas(sumaTotalDespacho);
				objCategoria.getInformacionForecastMes().get(mes).setMargenUnidaDespachadas(sumaTotalMargen);
				uniFaltanTotal = 0L;
				uniDespacTotal = 0L;
				sumaTotalDespacho = BigDecimal.ZERO;
				sumaTotalMargen = BigDecimal.ZERO;
			}
		}
		return this.calcularTotalAMarca(forecast, nombreMeses, calculoTotalUnidades);
	}

	public void llenarUnidadesSupply(ForecastXMesVO forecast, List<String> nombreMeses, Compania idCompania,
			int mesId) {
		String resultado = "(";
		for (MarcaGrupoForecast objGrupo : forecast.getMarcaGrupoForecast()) {
			resultado = resultado.concat("'".concat(objGrupo.getIdMarca()).concat("',"));
		}
		String marcas = resultado.substring(0, resultado.length() - 1).concat(")");
		for (ProductosForecastMesVO categoria : forecast.getProductosForecast()) {
			if (!categoria.isEsTotalMarca()) {
				Map<String, UnidadesdeSupply> unidadesSupply = this.consultarUnidadesSupply(categoria.getIdProducto(),
						mesId, (idCompania != null) ? idCompania.getId() : null, marcas);
				for (String mes : nombreMeses) {
					if (unidadesSupply.containsKey(categoria.getIdProducto() + "-"
							+ categoria.getInformacionForecastMes().get(mes).getIdMes())) {
						categoria.getInformacionForecastMes().get(mes)
								.setUnidadesDespachadas(Long.valueOf(unidadesSupply
										.get(categoria.getIdProducto() + "-"
												+ categoria.getInformacionForecastMes().get(mes).getIdMes())
										.getUnidadesDespachadas()));
						categoria.getInformacionForecastMes().get(mes)
								.setUnidadesFaltantes(Long.valueOf(unidadesSupply
										.get(categoria.getIdProducto() + "-"
												+ categoria.getInformacionForecastMes().get(mes).getIdMes())
										.getUnidadesFaltantes()));
						categoria.getInformacionForecastMes().get(mes)
								.setTotalUnidaDespachadas(unidadesSupply
										.get(categoria.getIdProducto() + "-"
												+ categoria.getInformacionForecastMes().get(mes).getIdMes())
										.getPreciRetailer());
						categoria.getInformacionForecastMes().get(mes)
								.setMargenUnidaDespachadas(
										unidadesSupply
												.get(categoria.getIdProducto() + "-"
														+ categoria.getInformacionForecastMes().get(mes).getIdMes())
												.getCogs());
					} else {
						categoria.getInformacionForecastMes().get(mes).setUnidadesDespachadas(Long.valueOf(0L));
						categoria.getInformacionForecastMes().get(mes).setUnidadesFaltantes(
								categoria.getInformacionForecastMes().get(mes).getForecastGerente());
						categoria.getInformacionForecastMes().get(mes).setTotalUnidaDespachadas(BigDecimal.ZERO);
						categoria.getInformacionForecastMes().get(mes).setMargenUnidaDespachadas(BigDecimal.ZERO);
					}
				}
			}
		}
	}

	public Map<String, ItemForecastCompaniaVO> precioRetailerXCompania(ForecastXMesVO forecast) {
		Map<String, ItemForecastCompaniaVO> informacionPrecio = new LinkedHashMap<String, ItemForecastCompaniaVO>();
		Query query = this.em.createNativeQuery(
				"SELECT fp.producto_fk, SUM(fp.precio_retailer)/COUNT(f.id), fp.mes_sop,\n CAST(SUM(fp.cogs)/COUNT(f.id) AS decimal(18,0))\n FROM FORECAST f\n INNER JOIN FORECAST_PRODUCTO fp ON fp.forecast_fk = f.id\n WHERE f.compania_fk = ?1 AND\n f.mes_fk = ?2 AND\n f.marca_fk = ?3 AND f.estado_fk = 'CNFD' \n GROUP BY fp.producto_fk, fp.mes_sop");
		query.setParameter(1, forecast.getCompania());
		query.setParameter(2, forecast.getIdMes());
		query.setParameter(3, forecast.getMarca());
		List<Object[]> resultados = (List<Object[]>) query.getResultList();
		for (Object[] obj : resultados) {
			ItemForecastCompaniaVO objItem = new ItemForecastCompaniaVO();
			String llave = obj[0] + "-" + obj[2];
			objItem.setIdProducto((String) obj[0]);
			objItem.setPrecioRetailer((BigDecimal) obj[1]);
			objItem.setIdMes(Long.valueOf(Long.parseLong(obj[2].toString())));
			objItem.setCogs((BigDecimal) obj[3]);
			informacionPrecio.put(llave, objItem);
		}
		return informacionPrecio;
	}

	public boolean verificarSupply(int idMes, Compania idCompania, String idMarca) {
		String whereCompania = "";
		if (idCompania != null) {
			whereCompania = "fc.compania_fk = ?3 AND ";
		}
		Query query = this.em.createNativeQuery(
				" SELECT p.marca_cat2_fk + p.grupo_cat4_fk + p.plataforma_cat5_fk verificar_supply,  SUM(f.uni_faltan) uni_faltan, SUM(f.uni_desp) uni_desp,  f.mes_fk, SUM(f.precio) precio, SUM(f.cogs) cogs  FROM FORECAST_COMPANIA f \n INNER JOIN PRODUCTO p ON p.id = f.producto_fk  WHERE id_padre IN ( SELECT fc.id FROM FORECAST_COMPANIA fc\n WHERE  "
						+ whereCompania + " fc.marca_fk IN " + idMarca
						+ " AND  fc.mes_fk = ?1 AND  fc.id = fc.id_padre  )  GROUP BY mes_fk, p.marca_cat2_fk, p.grupo_cat4_fk, p.plataforma_cat5_fk");
		query.setParameter(1, idMes);
		if (idCompania != null) {
			query.setParameter(3, idCompania.getId());
		}
		return !query.getResultList().isEmpty();
	}

	public List<Compania> consultarCompaniasUsuario(UsuarioVO oUsuario) {
		List<Compania> oCompania = new ArrayList<Compania>();
		String queryConsulta = "SELECT id_usuario_fk, id_compania_fk FROM USUARIO_X_COMPANIA WHERE id_usuario_fk = "
				+ oUsuario.getId();
		Query query = this.em.createNativeQuery(queryConsulta);
		List<Object[]> objUsuario = (List<Object[]>) query.getResultList();
		for (Object[] objUser : objUsuario) {
			oCompania.add(this.consultarCompaniasID((String) objUser[1]));
		}
		return oCompania;
	}

	public boolean existeUsuarioCompania(Usuario oUsuario, Compania oCompania) {
		if (oCompania != null) {
			String queryConsulta = "SELECT id_usuario_fk, id_compania_fk FROM USUARIO_X_COMPANIA WHERE id_usuario_fk = "
					+ oUsuario.getId() + " AND id_compania_fk = " + oCompania.getId();
			Query query = this.em.createNativeQuery(queryConsulta);
			List<Object[]> objUsuario = (List<Object[]>) query.getResultList();
			return !objUsuario.isEmpty();
		}
		return false;
	}

	public ForecastXMesVO calcularTotalAMarca(ForecastXMesVO forecast, List<String> nombreMeses,
			List<String> nombreMarcas, List<String> selectedRetailer) {
		Long uniFaltanTotal = 0L;
		Long uniDespacTotal = 0L;
		BigDecimal sumaTotalDespacho = BigDecimal.ZERO;
		BigDecimal sumaTotalMargen = BigDecimal.ZERO;
		Long sellin = 0L;
		Long forecastKam = 0L;
		Long forecastGerente = 0L;
		Long gerenteAnterior = 0L;
		BigDecimal totalPropuesto = BigDecimal.ZERO;
		BigDecimal totalKam = BigDecimal.ZERO;
		BigDecimal totalInvWH = BigDecimal.ZERO;
		BigDecimal totalGerenteAnterior = BigDecimal.ZERO;
		BigDecimal totalGerente = BigDecimal.ZERO;
		BigDecimal totalTransito = BigDecimal.ZERO;
		for (String retailer : selectedRetailer) {
			for (String marca : nombreMarcas) {
				ProductosForecastMesVO itemCategoria = this.devolverCategoriaMarca(forecast, marca, retailer);
				for (String mes : nombreMeses) {
					for (ProductosForecastMesVO objForecastProd : forecast.getProductosForecast()) {
						if (objForecastProd == null) {
							log.info("ObjForecastProd  is null");
						}
						if (!objForecastProd.isEsProdcuto() && !objForecastProd.isEsTotalMarca()
								&& marca.equals(objForecastProd.getIdMarcaCategoria())
								&& objForecastProd.getIdRetailer().equals(retailer)
								&& objForecastProd.getInformacionForecastMes().containsKey(mes)) {
							uniFaltanTotal += ((objForecastProd.getInformacionForecastMes().get(mes)
									.getUnidadesFaltantes() != null)
											? objForecastProd.getInformacionForecastMes().get(mes)
													.getUnidadesFaltantes()
											: 0L);
							uniDespacTotal += ((objForecastProd.getInformacionForecastMes().get(mes)
									.getUnidadesDespachadas() != null)
											? objForecastProd.getInformacionForecastMes().get(mes)
													.getUnidadesDespachadas()
											: 0L);
							sumaTotalDespacho = sumaTotalDespacho.add((objForecastProd.getInformacionForecastMes()
									.get(mes).getTotalUnidaDespachadas() != null)
											? objForecastProd.getInformacionForecastMes().get(mes)
													.getTotalUnidaDespachadas()
											: BigDecimal.ZERO);
							sumaTotalMargen = sumaTotalMargen.add((objForecastProd.getInformacionForecastMes().get(mes)
									.getMargenUnidaDespachadas() != null)
											? objForecastProd.getInformacionForecastMes().get(mes)
													.getMargenUnidaDespachadas()
											: BigDecimal.ZERO);
							sellin += ((objForecastProd.getInformacionForecastMes().get(mes)
									.getSellInPropuesto() != null)
											? objForecastProd.getInformacionForecastMes().get(mes).getSellInPropuesto()
											: 0L);
							forecastKam += ((objForecastProd.getInformacionForecastMes().get(mes).getForecast() != null)
									? objForecastProd.getInformacionForecastMes().get(mes).getForecast()
									: 0L);
							forecastGerente += ((objForecastProd.getInformacionForecastMes().get(mes)
									.getForecastGerente() != null)
											? objForecastProd.getInformacionForecastMes().get(mes).getForecastGerente()
											: 0L);
							totalPropuesto = totalPropuesto.add(
									(objForecastProd.getInformacionForecastMes().get(mes).getTotalPropuesto() != null)
											? objForecastProd.getInformacionForecastMes().get(mes).getTotalPropuesto()
											: BigDecimal.ZERO);
							totalKam = totalKam.add(
									(objForecastProd.getInformacionForecastMes().get(mes).getTotalForecastKam() != null)
											? objForecastProd.getInformacionForecastMes().get(mes).getTotalForecastKam()
											: BigDecimal.ZERO);
							totalGerente = totalGerente.add((objForecastProd.getInformacionForecastMes().get(mes)
									.getTotalForecastGerente() != null)
											? objForecastProd.getInformacionForecastMes().get(mes)
													.getTotalForecastGerente()
											: BigDecimal.ZERO);
							totalInvWH = totalInvWH.add(
									(objForecastProd.getInformacionForecastMes().get(mes).getInventarioWH() != null)
											? objForecastProd.getInformacionForecastMes().get(mes).getInventarioWH()
											: BigDecimal.ZERO);
							totalTransito = totalTransito
									.add((objForecastProd.getInformacionForecastMes().get(mes).getTransito() != null)
											? objForecastProd.getInformacionForecastMes().get(mes).getTransito()
											: BigDecimal.ZERO);
							totalGerenteAnterior = totalGerenteAnterior.add((objForecastProd.getInformacionForecastMes()
									.get(mes).getTotalForecastGerenteAnterior() != null)
											? objForecastProd.getInformacionForecastMes().get(mes)
													.getTotalForecastGerenteAnterior()
											: BigDecimal.ZERO);
							gerenteAnterior += ((objForecastProd.getInformacionForecastMes().get(mes)
									.getForecastGerenteAnterior() != null)
											? objForecastProd.getInformacionForecastMes().get(mes)
													.getForecastGerenteAnterior()
											: 0L);
						}
					}
					if (itemCategoria != null) {
						if (itemCategoria.getInformacionForecastMes().containsKey(mes)) {
							itemCategoria.getInformacionForecastMes().get(mes).setUnidadesFaltantes(uniFaltanTotal);
							itemCategoria.getInformacionForecastMes().get(mes).setUnidadesDespachadas(uniDespacTotal);
							itemCategoria.getInformacionForecastMes().get(mes)
									.setTotalUnidaDespachadas(sumaTotalDespacho);
							itemCategoria.getInformacionForecastMes().get(mes)
									.setMargenUnidaDespachadas(sumaTotalMargen);
							itemCategoria.getInformacionForecastMes().get(mes).setSellInPropuesto(sellin);
							itemCategoria.getInformacionForecastMes().get(mes).setTotalPropuesto(totalPropuesto);
							itemCategoria.getInformacionForecastMes().get(mes).setForecast(forecastKam);
							itemCategoria.getInformacionForecastMes().get(mes).setTotalForecastKam(totalKam);
							itemCategoria.getInformacionForecastMes().get(mes).setForecastGerente(forecastGerente);
							itemCategoria.getInformacionForecastMes().get(mes).setTotalForecastGerente(totalGerente);
							itemCategoria.getInformacionForecastMes().get(mes).setInventarioWH(totalInvWH);
							itemCategoria.getInformacionForecastMes().get(mes).setTransito(totalTransito);
							itemCategoria.getInformacionForecastMes().get(mes)
									.setTotalForecastGerenteAnterior(totalGerenteAnterior);
							itemCategoria.getInformacionForecastMes().get(mes)
									.setForecastGerenteAnterior(gerenteAnterior);
						}
						uniFaltanTotal = 0L;
						uniDespacTotal = 0L;
						sumaTotalDespacho = BigDecimal.ZERO;
						sumaTotalMargen = BigDecimal.ZERO;
						sellin = 0L;
						forecastKam = 0L;
						forecastGerente = 0L;
						totalPropuesto = BigDecimal.ZERO;
						totalKam = BigDecimal.ZERO;
						totalGerente = BigDecimal.ZERO;
						totalInvWH = BigDecimal.ZERO;
						totalTransito = BigDecimal.ZERO;
						totalGerenteAnterior = BigDecimal.ZERO;
						gerenteAnterior = 0L;
					}
				}
			}
		}
		for (String retailer : selectedRetailer) {
			ProductosForecastMesVO itemCategoria = this.devolverCategoriaMarca(forecast, retailer);
			for (String mes2 : nombreMeses) {
				for (ProductosForecastMesVO objForecastProd2 : forecast.getProductosForecast()) {
					if (objForecastProd2 == null) {
						log.info("ObjForecastProd  is null");
					}
					if (!objForecastProd2.isEsProdcuto() && objForecastProd2.isEsTotalMarca()
							&& objForecastProd2.getIdRetailer() != null
							&& objForecastProd2.getInformacionForecastMes().containsKey(mes2)
							&& objForecastProd2.getIdRetailer().equals(retailer)) {
						uniFaltanTotal += ((objForecastProd2.getInformacionForecastMes().get(mes2)
								.getUnidadesFaltantes() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2).getUnidadesFaltantes()
										: 0L);
						uniDespacTotal += ((objForecastProd2.getInformacionForecastMes().get(mes2)
								.getUnidadesDespachadas() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2)
												.getUnidadesDespachadas()
										: 0L);
						sumaTotalDespacho = sumaTotalDespacho.add((objForecastProd2.getInformacionForecastMes()
								.get(mes2).getTotalUnidaDespachadas() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2)
												.getTotalUnidaDespachadas()
										: BigDecimal.ZERO);
						sumaTotalMargen = sumaTotalMargen.add((objForecastProd2.getInformacionForecastMes().get(mes2)
								.getMargenUnidaDespachadas() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2)
												.getMargenUnidaDespachadas()
										: BigDecimal.ZERO);
						sellin += ((objForecastProd2.getInformacionForecastMes().get(mes2).getSellInPropuesto() != null)
								? objForecastProd2.getInformacionForecastMes().get(mes2).getSellInPropuesto()
								: 0L);
						forecastKam += ((objForecastProd2.getInformacionForecastMes().get(mes2).getForecast() != null)
								? objForecastProd2.getInformacionForecastMes().get(mes2).getForecast()
								: 0L);
						forecastGerente += ((objForecastProd2.getInformacionForecastMes().get(mes2)
								.getForecastGerente() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2).getForecastGerente()
										: 0L);
						totalPropuesto = totalPropuesto.add(
								(objForecastProd2.getInformacionForecastMes().get(mes2).getTotalPropuesto() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2).getTotalPropuesto()
										: BigDecimal.ZERO);
						totalKam = totalKam.add(
								(objForecastProd2.getInformacionForecastMes().get(mes2).getTotalForecastKam() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2).getTotalForecastKam()
										: BigDecimal.ZERO);
						totalGerente = totalGerente.add((objForecastProd2.getInformacionForecastMes().get(mes2)
								.getTotalForecastGerente() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2)
												.getTotalForecastGerente()
										: BigDecimal.ZERO);
						totalGerenteAnterior = totalGerenteAnterior.add((objForecastProd2.getInformacionForecastMes()
								.get(mes2).getTotalForecastGerenteAnterior() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2)
												.getTotalForecastGerenteAnterior()
										: BigDecimal.ZERO);
						gerenteAnterior += ((objForecastProd2.getInformacionForecastMes().get(mes2)
								.getForecastGerenteAnterior() != null)
										? objForecastProd2.getInformacionForecastMes().get(mes2)
												.getForecastGerenteAnterior()
										: 0L);
					}
				}
				if (itemCategoria != null) {
					if (itemCategoria.getInformacionForecastMes().containsKey(mes2)) {
						itemCategoria.getInformacionForecastMes().get(mes2).setUnidadesFaltantes(uniFaltanTotal);
						itemCategoria.getInformacionForecastMes().get(mes2).setUnidadesDespachadas(uniDespacTotal);
						itemCategoria.getInformacionForecastMes().get(mes2).setTotalUnidaDespachadas(sumaTotalDespacho);
						itemCategoria.getInformacionForecastMes().get(mes2).setMargenUnidaDespachadas(sumaTotalMargen);
						itemCategoria.getInformacionForecastMes().get(mes2).setSellInPropuesto(sellin);
						itemCategoria.getInformacionForecastMes().get(mes2).setTotalPropuesto(totalPropuesto);
						itemCategoria.getInformacionForecastMes().get(mes2).setForecast(forecastKam);
						itemCategoria.getInformacionForecastMes().get(mes2).setTotalForecastKam(totalKam);
						itemCategoria.getInformacionForecastMes().get(mes2).setForecastGerente(forecastGerente);
						itemCategoria.getInformacionForecastMes().get(mes2).setTotalForecastGerente(totalGerente);
						itemCategoria.getInformacionForecastMes().get(mes2)
								.setTotalForecastGerenteAnterior(totalGerenteAnterior);
						itemCategoria.getInformacionForecastMes().get(mes2).setForecastGerenteAnterior(gerenteAnterior);
					}
					uniFaltanTotal = 0L;
					uniDespacTotal = 0L;
					sumaTotalDespacho = BigDecimal.ZERO;
					sumaTotalMargen = BigDecimal.ZERO;
					sellin = 0L;
					forecastKam = 0L;
					forecastGerente = 0L;
					totalPropuesto = BigDecimal.ZERO;
					totalKam = BigDecimal.ZERO;
					totalGerente = BigDecimal.ZERO;
					totalGerenteAnterior = BigDecimal.ZERO;
					gerenteAnterior = 0L;
				}
			}
		}
		ProductosForecastMesVO itemCategoria = this.devolverCategoriaMarca(forecast, "00");
		if (forecast.getProductosForecast().isEmpty()) {
			log.info("Produ forecadt i empty");
		}
		for (String mes3 : nombreMeses) {
			for (ProductosForecastMesVO objForecastProd3 : forecast.getProductosForecast()) {
				if (!objForecastProd3.isEsProdcuto() && objForecastProd3.isEsTotalMarca()
						&& !objForecastProd3.getIdProducto().equals("00") && objForecastProd3.getIdRetailer() != null
						&& objForecastProd3.getInformacionForecastMes().containsKey(mes3)) {
					uniFaltanTotal += ((objForecastProd3.getInformacionForecastMes().get(mes3)
							.getUnidadesFaltantes() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3).getUnidadesFaltantes()
									: 0L);
					uniDespacTotal += ((objForecastProd3.getInformacionForecastMes().get(mes3)
							.getUnidadesDespachadas() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3).getUnidadesDespachadas()
									: 0L);
					sumaTotalDespacho = sumaTotalDespacho.add(
							(objForecastProd3.getInformacionForecastMes().get(mes3).getTotalUnidaDespachadas() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3).getTotalUnidaDespachadas()
									: BigDecimal.ZERO);
					sumaTotalMargen = sumaTotalMargen.add(
							(objForecastProd3.getInformacionForecastMes().get(mes3).getMargenUnidaDespachadas() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3).getMargenUnidaDespachadas()
									: BigDecimal.ZERO);
					sellin += ((objForecastProd3.getInformacionForecastMes().get(mes3).getSellInPropuesto() != null)
							? objForecastProd3.getInformacionForecastMes().get(mes3).getSellInPropuesto()
							: 0L);
					forecastKam += ((objForecastProd3.getInformacionForecastMes().get(mes3).getForecast() != null)
							? objForecastProd3.getInformacionForecastMes().get(mes3).getForecast()
							: 0L);
					forecastGerente += ((objForecastProd3.getInformacionForecastMes().get(mes3)
							.getForecastGerente() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3).getForecastGerente()
									: 0L);
					totalPropuesto = totalPropuesto
							.add((objForecastProd3.getInformacionForecastMes().get(mes3).getTotalPropuesto() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3).getTotalPropuesto()
									: BigDecimal.ZERO);
					totalKam = totalKam
							.add((objForecastProd3.getInformacionForecastMes().get(mes3).getTotalForecastKam() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3).getTotalForecastKam()
									: BigDecimal.ZERO);
					totalGerente = totalGerente.add(
							(objForecastProd3.getInformacionForecastMes().get(mes3).getTotalForecastGerente() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3).getTotalForecastGerente()
									: BigDecimal.ZERO);
					totalGerenteAnterior = totalGerenteAnterior.add((objForecastProd3.getInformacionForecastMes()
							.get(mes3).getTotalForecastGerenteAnterior() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3)
											.getTotalForecastGerenteAnterior()
									: BigDecimal.ZERO);
					gerenteAnterior += ((objForecastProd3.getInformacionForecastMes().get(mes3)
							.getForecastGerenteAnterior() != null)
									? objForecastProd3.getInformacionForecastMes().get(mes3)
											.getForecastGerenteAnterior()
									: 0L);
				}
			}
			if (itemCategoria != null) {
				if (itemCategoria.getInformacionForecastMes().containsKey(mes3)) {
					itemCategoria.getInformacionForecastMes().get(mes3).setUnidadesFaltantes(uniFaltanTotal);
					itemCategoria.getInformacionForecastMes().get(mes3).setUnidadesDespachadas(uniDespacTotal);
					itemCategoria.getInformacionForecastMes().get(mes3).setTotalUnidaDespachadas(sumaTotalDespacho);
					itemCategoria.getInformacionForecastMes().get(mes3).setMargenUnidaDespachadas(sumaTotalMargen);
					itemCategoria.getInformacionForecastMes().get(mes3).setSellInPropuesto(sellin);
					itemCategoria.getInformacionForecastMes().get(mes3).setTotalPropuesto(totalPropuesto);
					itemCategoria.getInformacionForecastMes().get(mes3).setForecast(forecastKam);
					itemCategoria.getInformacionForecastMes().get(mes3).setTotalForecastKam(totalKam);
					itemCategoria.getInformacionForecastMes().get(mes3).setForecastGerente(forecastGerente);
					itemCategoria.getInformacionForecastMes().get(mes3).setTotalForecastGerente(totalGerente);
					itemCategoria.getInformacionForecastMes().get(mes3)
							.setTotalForecastGerenteAnterior(totalGerenteAnterior);
					itemCategoria.getInformacionForecastMes().get(mes3).setForecastGerenteAnterior(gerenteAnterior);
				}
				uniFaltanTotal = 0L;
				uniDespacTotal = 0L;
				sumaTotalDespacho = BigDecimal.ZERO;
				sumaTotalMargen = BigDecimal.ZERO;
				sellin = 0L;
				forecastKam = 0L;
				forecastGerente = 0L;
				totalPropuesto = BigDecimal.ZERO;
				totalKam = BigDecimal.ZERO;
				totalGerente = BigDecimal.ZERO;
				totalGerenteAnterior = BigDecimal.ZERO;
				gerenteAnterior = 0L;
			}
		}
		return forecast;
	}

	private ProductosForecastMesVO devolverCategoriaMarca(ForecastXMesVO forecast, String marca, String idRetailer) {
		for (ProductosForecastMesVO objForecastProd : forecast.getProductosForecast()) {
			if (objForecastProd.getIdProducto().equals(marca + "-" + idRetailer)
					&& objForecastProd.getIdRetailer().equals(idRetailer)) {
				return objForecastProd;
			}
		}
		return null;
	}

	private ProductosForecastMesVO devolverCategoriaMarca(ForecastXMesVO forecast, String marca) {
		for (ProductosForecastMesVO objForecastProd : forecast.getProductosForecast()) {
			if (objForecastProd.getIdProducto().equals(marca)) {
				return objForecastProd;
			}
		}
		return null;
	}

	public void guardarEstadoForecast(ForecastXMesVO forecast, String estadoS, UsuarioVO usuario,
			Map<String, ForecastXMesVO> productoCategoria) {

		Forecast forecastDB;
		EstadoForecast estado;
		boolean sigMarca = false;

		for (RetailerGrupoForecast objGrupoRetailer : forecast.getRetailerGrupoForecast()) {
			for (MarcaGrupoForecast objGrupoMarcas : forecast.getMarcaGrupoForecast()) {
				for (Map.Entry<String, ForecastXMesVO> entry : productoCategoria.entrySet()) {
					if (objGrupoMarcas.getIdMarca().equals(entry.getValue().getMarca()) && !sigMarca
							&& objGrupoRetailer.getIdRetailer().equals(entry.getValue().getRetailer())) {
						if (entry.getValue().getIdForecast() != null
								&& entry.getValue().getIdForecast().intValue() != 0) {
							TypedQuery<Forecast> q = em.createQuery("SELECT f FROM Forecast f WHERE f.id = :id",
									Forecast.class);
							q.setParameter("id", entry.getValue().getIdForecast());
							q.setMaxResults(1);
							forecastDB = (Forecast) q.getSingleResult();

							forecastDB.setFechaModifica(new java.util.Date());
							forecastDB.setUsuarioModifica(usuario.getUserName());
							forecastDB.setForecastCompania(String.valueOf(entry.getValue().isFueProcesadoXCompania()));
							estado = new EstadoForecast(estadoS);
							forecastDB.setEstadoFk(estado);
							em.merge(forecastDB);
							em.flush();
							sigMarca = true;
						}
					}
				}
				sigMarca = false;
			}
		}
	}

	public BigDecimal obtenerInvetariosProducto(String idProducto, String compania) {
		try {
			Query query = this.em.createNativeQuery(
					"SELECT sum(cantidad_disponible)+SUM(cantidad_transito) inventario FROM INVENTARIO \n INNER JOIN BODEGA b ON b.id= INVENTARIO.bodega_fk \n WHERE producto_fk=?1 and ((bodega_fk like '070MIA%' and bodega_fk != '070MIA02') or b.compania_fk=?2) group by producto_fk ");
			query.setParameter(1, idProducto);
			query.setParameter(2, compania);
			return (query.getResultList() != null) ? new BigDecimal(query.getSingleResult().toString())
					: BigDecimal.ZERO;
		} catch (NoResultException e2) {
			String msg = "No hay data de Inventario para PROD " + idProducto;
			log.error(msg);
			return BigDecimal.ZERO;
		} catch (Exception e) {
			log.error("Error en la consulta de obtenerInvetariosProducto", (Throwable) e);
			return BigDecimal.ZERO;
		}
	}

	public String buscarEstadoProdPorCompania(String producto, String marcaSeleccionadaIde, String idCompania,
			Long mesSeleccionadoIde, String retailer) {
		try {
			String queryStr = "SELECT distinct fp.estadoProducto FROM FORECAST f \nINNER JOIN FORECAST_PRODUCTO fp ON fp.forecast_fk=f.id \nWHERE f.marca_fk in "
					+ marcaSeleccionadaIde
					+ " and mes_fk= ?1 and fp.producto_fk= ?2 and f.compania_fk= ?3 and f.retailer_fk= ?4";
			String msg = "marcas " + marcaSeleccionadaIde + "  mes " + mesSeleccionadoIde + "  producto" + producto
					+ "  retailer " + retailer + " idCompania " + idCompania;
			log.info(msg);
			Query query = this.em.createNativeQuery(queryStr);
			query.setParameter(1, mesSeleccionadoIde);
			query.setParameter(2, producto.trim());
			query.setParameter(4, retailer);
			query.setParameter(3, idCompania);
			List<Character> estado = (List<Character>) query.getResultList();
			if (estado.size() > 1) {
				return "V";
			}
			return estado.get(0).toString();
		} catch (Exception e) {
			log.error("ERROR en metodo buscarEstadoProdPorCompania() ", (Throwable) e);
			return "";
		}
	}

	public String buscarEstadoProdElevacionInic(String producto, String marcaSeleccionadaIde, String idCompania,
			Long mesSeleccionadoIde, String retailer) {
		try {
			String queryStr = "SELECT distinct fp.estadoProducto FROM FORECAST f \nINNER JOIN FORECAST_PRODUCTO fp ON fp.forecast_fk=f.id \nWHERE f.marca_fk in "
					+ marcaSeleccionadaIde
					+ " and mes_fk= ?1 and fp.producto_fk= ?2 and f.compania_fk= ?3 and f.retailer_fk != ?4";
			Query query = this.em.createNativeQuery(queryStr);
			query.setParameter(1, mesSeleccionadoIde);
			query.setParameter(2, producto.trim());
			query.setParameter(4, retailer);
			query.setParameter(3, idCompania);
			List<Character> estado = (List<Character>) query.getResultList();
			if (estado.size() > 1) {
				return "V";
			}
			return estado.get(0).toString();
		} catch (Exception e) {
			log.error("ERROR en metodo buscarEstadoProdElevacionInic() ", (Throwable) e);
			return "";
		}
	}

	public GrupoCat4 getIdGrupoByIdProducto(String id) {
		GrupoCat4 grupo = null;
		try {
			String sql = "SELECT * FROM PRODUCTO WHERE id= ?";
			Query query = this.em.createNativeQuery(sql);
			query.setParameter(1, id);
			List<Producto> result = (List<Producto>) query.getResultList();
			if (!result.isEmpty()) {
				grupo = result.get(0).getGrupoCat4Fk();
			}
		} catch (Exception e) {
			log.error("Erro al consultar getNombreGrupoByIdProducto()", (Throwable) e);
		}
		return grupo;
	}

	public String getMarcasByIDGrupoMarca(List<String> selectedMarca) {
		List<MarcaGrupo> listado = new ArrayList<MarcaGrupo>();
		for (String idGrupoMarca : selectedMarca) {
			listado.addAll(this.buscaMarcaGrupoPorIdGrupoMarca(idGrupoMarca));
		}
		return this.getMarcaSeparadoComasYParentesis(listado);
	}

	public boolean consultarForecast(List<String> retailer, String marcas, MesSop mes, String estados) {
		String queryStr = "SELECT f FROM Forecast f WHERE f.retailerFk.id IN :retailer AND f.mesFk = :mes and f.marcaFk.id IN "
				+ marcas + " and f.estadoFk.id IN " + estados;
		TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
		query.setParameter("retailer", retailer);
		query.setParameter("mes", mes);
		List<Forecast> resp = query.getResultList();
		return !resp.isEmpty();
	}

	public List<String> getMarcasByGrupo(List<String> selectedMarca) {
		List<String> listMarcas = new ArrayList<String>();
		listMarcas.addAll(this.getMarcaGrupoPorIdGrupoMarca(selectedMarca));
		return listMarcas;
	}

	public Retailer consultarRetailerById(String idRetailer) {
		try {
			String qlString = "SELECT r FROM Retailer r WHERE r.id = :retailer";
			return (Retailer) this.em.createQuery(qlString).setParameter("retailer", idRetailer).getSingleResult();
		} catch (Exception e) {
			log.error("ERROR: Hubo un error en la consulta de Retailer por ID: ", (Throwable) e);
			return null;
		}
	}

	/**
	 * 
	 * @param retailer
	 * @param marcas
	 * @param mes
	 * @return
	 */
	public List<String> consultarForecastCerrados(List<String> retailer, String marcas, MesSop mes) {
		String queryStr = "SELECT f FROM Forecast f WHERE f.retailerFk.id IN :retailer AND f.mesFk = :mes and f.marcaFk.id IN "
				+ marcas + " and f.estadoFk.id IN ('CNFD','CNFG')";
		TypedQuery<Forecast> query = this.em.createQuery(queryStr, (Class) Forecast.class);
		query.setParameter("retailer", retailer);
		query.setParameter("mes", mes);
		List<Forecast> resp = query.getResultList();
		List<String> oIDRetailers = new ArrayList<>();
		for (Forecast oForecast : resp) {
			oIDRetailers.add(oForecast.getRetailerFk().getId());
		}
		return oIDRetailers;
	}

	/**
	 * 
	 * @param forecast
	 * @param nombreMeses
	 * @param nombreMarcas
	 * @return
	 */
	public ForecastXMesVO calcularTotalAMarca(ForecastXMesVO forecast, List<String> nombreMeses,
			List<String> nombreMarcas) {
		Long uniFaltanTotal = 0L;
		Long uniDespacTotal = 0L;
		BigDecimal sumaTotalDespacho = BigDecimal.ZERO;
		BigDecimal sumaTotalMargen = BigDecimal.ZERO;
		Long sellin = 0L;
		Long forecastKam = 0L;
		Long forecastGerente = 0L;
		Long gerenteAnterior = 0L;
		BigDecimal totalPropuesto = BigDecimal.ZERO;
		BigDecimal totalKam = BigDecimal.ZERO;
		BigDecimal totalInvWH = BigDecimal.ZERO;
		BigDecimal totalGerenteAnterior = BigDecimal.ZERO;
		BigDecimal totalGerente = BigDecimal.ZERO;
		BigDecimal totalTransito = BigDecimal.ZERO;
		for (String marca : nombreMarcas) {
			ProductosForecastMesVO itemCategoria = this.devolverCategoriaMarca(forecast, marca);
			for (String mes : nombreMeses) {
				for (ProductosForecastMesVO objForecastProd : forecast.getProductosForecast()) {
					if (objForecastProd == null) {
						log.info("ObjForecastProd  is null");
					}
					if (!objForecastProd.isEsProdcuto() && !objForecastProd.isEsTotalMarca()
							&& marca.equals(objForecastProd.getIdMarcaCategoria())
							&& objForecastProd.getInformacionForecastMes().containsKey(mes)) {
						uniFaltanTotal += ((objForecastProd.getInformacionForecastMes().get(mes)
								.getUnidadesFaltantes() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getUnidadesFaltantes()
										: 0L);
						uniDespacTotal += ((objForecastProd.getInformacionForecastMes().get(mes)
								.getUnidadesDespachadas() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getUnidadesDespachadas()
										: 0L);
						sumaTotalDespacho = sumaTotalDespacho.add((objForecastProd.getInformacionForecastMes().get(mes)
								.getTotalUnidaDespachadas() != null)
										? objForecastProd.getInformacionForecastMes().get(mes)
												.getTotalUnidaDespachadas()
										: BigDecimal.ZERO);
						sumaTotalMargen = sumaTotalMargen.add((objForecastProd.getInformacionForecastMes().get(mes)
								.getMargenUnidaDespachadas() != null)
										? objForecastProd.getInformacionForecastMes().get(mes)
												.getMargenUnidaDespachadas()
										: BigDecimal.ZERO);
						sellin += ((objForecastProd.getInformacionForecastMes().get(mes).getSellInPropuesto() != null)
								? objForecastProd.getInformacionForecastMes().get(mes).getSellInPropuesto()
								: 0L);
						forecastKam += ((objForecastProd.getInformacionForecastMes().get(mes).getForecast() != null)
								? objForecastProd.getInformacionForecastMes().get(mes).getForecast()
								: 0L);
						forecastGerente += ((objForecastProd.getInformacionForecastMes().get(mes)
								.getForecastGerente() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getForecastGerente()
										: 0L);
						totalPropuesto = totalPropuesto
								.add((objForecastProd.getInformacionForecastMes().get(mes).getTotalPropuesto() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getTotalPropuesto()
										: BigDecimal.ZERO);
						totalKam = totalKam.add(
								(objForecastProd.getInformacionForecastMes().get(mes).getTotalForecastKam() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getTotalForecastKam()
										: BigDecimal.ZERO);
						totalGerente = totalGerente.add(
								(objForecastProd.getInformacionForecastMes().get(mes).getTotalForecastGerente() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getTotalForecastGerente()
										: BigDecimal.ZERO);
						totalInvWH = totalInvWH
								.add((objForecastProd.getInformacionForecastMes().get(mes).getInventarioWH() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getInventarioWH()
										: BigDecimal.ZERO);
						totalTransito = totalTransito
								.add((objForecastProd.getInformacionForecastMes().get(mes).getTransito() != null)
										? objForecastProd.getInformacionForecastMes().get(mes).getTransito()
										: BigDecimal.ZERO);
						totalGerenteAnterior = totalGerenteAnterior.add((objForecastProd.getInformacionForecastMes()
								.get(mes).getTotalForecastGerenteAnterior() != null)
										? objForecastProd.getInformacionForecastMes().get(mes)
												.getTotalForecastGerenteAnterior()
										: BigDecimal.ZERO);
						gerenteAnterior += ((objForecastProd.getInformacionForecastMes().get(mes)
								.getForecastGerenteAnterior() != null)
										? objForecastProd.getInformacionForecastMes().get(mes)
												.getForecastGerenteAnterior()
										: 0L);
					}
				}
				if (itemCategoria != null) {
					if (itemCategoria.getInformacionForecastMes().containsKey(mes)) {
						itemCategoria.getInformacionForecastMes().get(mes).setUnidadesFaltantes(uniFaltanTotal);
						itemCategoria.getInformacionForecastMes().get(mes).setUnidadesDespachadas(uniDespacTotal);
						itemCategoria.getInformacionForecastMes().get(mes).setTotalUnidaDespachadas(sumaTotalDespacho);
						itemCategoria.getInformacionForecastMes().get(mes).setMargenUnidaDespachadas(sumaTotalMargen);
						itemCategoria.getInformacionForecastMes().get(mes).setSellInPropuesto(sellin);
						itemCategoria.getInformacionForecastMes().get(mes).setTotalPropuesto(totalPropuesto);
						itemCategoria.getInformacionForecastMes().get(mes).setForecast(forecastKam);
						itemCategoria.getInformacionForecastMes().get(mes).setTotalForecastKam(totalKam);
						itemCategoria.getInformacionForecastMes().get(mes).setForecastGerente(forecastGerente);
						itemCategoria.getInformacionForecastMes().get(mes).setTotalForecastGerente(totalGerente);
						itemCategoria.getInformacionForecastMes().get(mes).setInventarioWH(totalInvWH);
						itemCategoria.getInformacionForecastMes().get(mes).setTransito(totalTransito);
						itemCategoria.getInformacionForecastMes().get(mes)
								.setTotalForecastGerenteAnterior(totalGerenteAnterior);
						itemCategoria.getInformacionForecastMes().get(mes).setForecastGerenteAnterior(gerenteAnterior);
					}
					uniFaltanTotal = 0L;
					uniDespacTotal = 0L;
					sumaTotalDespacho = BigDecimal.ZERO;
					sumaTotalMargen = BigDecimal.ZERO;
					sellin = 0L;
					forecastKam = 0L;
					forecastGerente = 0L;
					totalPropuesto = BigDecimal.ZERO;
					totalKam = BigDecimal.ZERO;
					totalGerente = BigDecimal.ZERO;
					totalInvWH = BigDecimal.ZERO;
					totalTransito = BigDecimal.ZERO;
					totalGerenteAnterior = BigDecimal.ZERO;
					gerenteAnterior = 0L;
				}
			}
		}
		ProductosForecastMesVO itemCategoria = this.devolverCategoriaMarca(forecast, "00");
		if (forecast.getProductosForecast().isEmpty()) {
			log.info("Produ forecadt i empty");
		}
		for (String mes2 : nombreMeses) {
			for (ProductosForecastMesVO objForecastProd2 : forecast.getProductosForecast()) {
				if (!objForecastProd2.isEsProdcuto() && objForecastProd2.isEsTotalMarca()
						&& !objForecastProd2.getIdProducto().equals("00")
						&& objForecastProd2.getInformacionForecastMes().containsKey(mes2)) {
					uniFaltanTotal += ((objForecastProd2.getInformacionForecastMes().get(mes2)
							.getUnidadesFaltantes() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getUnidadesFaltantes()
									: 0L);
					uniDespacTotal += ((objForecastProd2.getInformacionForecastMes().get(mes2)
							.getUnidadesDespachadas() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getUnidadesDespachadas()
									: 0L);
					sumaTotalDespacho = sumaTotalDespacho.add(
							(objForecastProd2.getInformacionForecastMes().get(mes2).getTotalUnidaDespachadas() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getTotalUnidaDespachadas()
									: BigDecimal.ZERO);
					sumaTotalMargen = sumaTotalMargen.add(
							(objForecastProd2.getInformacionForecastMes().get(mes2).getMargenUnidaDespachadas() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getMargenUnidaDespachadas()
									: BigDecimal.ZERO);
					sellin += ((objForecastProd2.getInformacionForecastMes().get(mes2).getSellInPropuesto() != null)
							? objForecastProd2.getInformacionForecastMes().get(mes2).getSellInPropuesto()
							: 0L);
					forecastKam += ((objForecastProd2.getInformacionForecastMes().get(mes2).getForecast() != null)
							? objForecastProd2.getInformacionForecastMes().get(mes2).getForecast()
							: 0L);
					forecastGerente += ((objForecastProd2.getInformacionForecastMes().get(mes2)
							.getForecastGerente() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getForecastGerente()
									: 0L);
					totalPropuesto = totalPropuesto
							.add((objForecastProd2.getInformacionForecastMes().get(mes2).getTotalPropuesto() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getTotalPropuesto()
									: BigDecimal.ZERO);
					totalKam = totalKam
							.add((objForecastProd2.getInformacionForecastMes().get(mes2).getTotalForecastKam() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getTotalForecastKam()
									: BigDecimal.ZERO);
					totalGerente = totalGerente.add(
							(objForecastProd2.getInformacionForecastMes().get(mes2).getTotalForecastGerente() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getTotalForecastGerente()
									: BigDecimal.ZERO);
					totalInvWH = totalInvWH
							.add((objForecastProd2.getInformacionForecastMes().get(mes2).getInventarioWH() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getInventarioWH()
									: BigDecimal.ZERO);
					totalTransito = totalTransito
							.add((objForecastProd2.getInformacionForecastMes().get(mes2).getTransito() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2).getTransito()
									: BigDecimal.ZERO);
					totalGerenteAnterior = totalGerenteAnterior.add((objForecastProd2.getInformacionForecastMes()
							.get(mes2).getTotalForecastGerenteAnterior() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2)
											.getTotalForecastGerenteAnterior()
									: BigDecimal.ZERO);
					gerenteAnterior += ((objForecastProd2.getInformacionForecastMes().get(mes2)
							.getForecastGerenteAnterior() != null)
									? objForecastProd2.getInformacionForecastMes().get(mes2)
											.getForecastGerenteAnterior()
									: 0L);
				}
			}
			if (itemCategoria != null) {
				if (itemCategoria.getInformacionForecastMes().containsKey(mes2)) {
					itemCategoria.getInformacionForecastMes().get(mes2).setUnidadesFaltantes(uniFaltanTotal);
					itemCategoria.getInformacionForecastMes().get(mes2).setUnidadesDespachadas(uniDespacTotal);
					itemCategoria.getInformacionForecastMes().get(mes2).setTotalUnidaDespachadas(sumaTotalDespacho);
					itemCategoria.getInformacionForecastMes().get(mes2).setMargenUnidaDespachadas(sumaTotalMargen);
					itemCategoria.getInformacionForecastMes().get(mes2).setSellInPropuesto(sellin);
					itemCategoria.getInformacionForecastMes().get(mes2).setTotalPropuesto(totalPropuesto);
					itemCategoria.getInformacionForecastMes().get(mes2).setForecast(forecastKam);
					itemCategoria.getInformacionForecastMes().get(mes2).setTotalForecastKam(totalKam);
					itemCategoria.getInformacionForecastMes().get(mes2).setForecastGerente(forecastGerente);
					itemCategoria.getInformacionForecastMes().get(mes2).setTotalForecastGerente(totalGerente);
					itemCategoria.getInformacionForecastMes().get(mes2).setInventarioWH(totalInvWH);
					itemCategoria.getInformacionForecastMes().get(mes2).setTransito(totalTransito);
					itemCategoria.getInformacionForecastMes().get(mes2)
							.setTotalForecastGerenteAnterior(totalGerenteAnterior);
					itemCategoria.getInformacionForecastMes().get(mes2).setForecastGerenteAnterior(gerenteAnterior);
				}
				uniFaltanTotal = 0L;
				uniDespacTotal = 0L;
				sumaTotalDespacho = BigDecimal.ZERO;
				sumaTotalMargen = BigDecimal.ZERO;
				sellin = 0L;
				forecastKam = 0L;
				forecastGerente = 0L;
				totalPropuesto = BigDecimal.ZERO;
				totalKam = BigDecimal.ZERO;
				totalGerente = BigDecimal.ZERO;
				totalInvWH = BigDecimal.ZERO;
				totalTransito = BigDecimal.ZERO;
				totalGerenteAnterior = BigDecimal.ZERO;
				gerenteAnterior = 0L;
			}
		}
		return forecast;
	}

	public Usuario buscaUsuarioPorUserName(String inUserName) {
		try {
			Usuario u = (Usuario) this.em.createNamedQuery("Usuario.findByUserName")
					.setParameter("userName", inUserName).getSingleResult();
			return u;
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * 
	 * @param inIdCompania
	 * @return
	 */
	public List<String> consultarUsuarioCompania(String inIdCompania) {
		String sql = "SELECT U.user_Name FROM USUARIO_X_COMPANIA UXC INNER JOIN USUARIO U ON U.id = UXC.id_usuario_fk WHERE id_compania_fk = "
				+ inIdCompania + " ORDER BY U.user_Name";
		if (inIdCompania != null) {
			Query query = this.em.createNativeQuery(sql);
			return query.getResultList();
		}
		return new ArrayList<>();
	}

}