package com.jvlap.sop.demand.logica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.demand.logica.vos.ScheduleVO;

@SuppressWarnings("rawtypes")
@Stateless
public class ScheduledProcessesEJB extends AbstractFacade {

	private static final Logger LOGGER = LogManager.getLogger(ScheduledProcessesEJB.class.getName());

	@PersistenceContext(unitName = "EntidadesSOPPU")
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public boolean merge(ScheduleVO inScheduleVO) {
		try {
			if (inScheduleVO.getId() != null) {
				this.update(inScheduleVO);
			} else {
				this.insert(inScheduleVO);
			}
			return true;
		} catch (Exception e) {
			LOGGER.error("[merge] 1- Hubo un error ejecutando la sentencia INSERT", e);
			return false;
		}

	}

	/**
	 * 
	 * @param inScheduleVO
	 * @return
	 */
	public boolean merge(List<ScheduleVO> inScheduleVO) {
		boolean out = true;
		try {
			for (ScheduleVO s : inScheduleVO) {
				out = out && merge(s);
			}
			return out;
		} catch (Exception e) {
			LOGGER.error("[merge] L - Hubo un error ejecutando la sentencia INSERT", e);
			return false;
		}

	}

	/**
	 * 
	 * @param inParametros
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ScheduleVO> consultar(Map<String, List> inParametros) {
		List<ScheduleVO> out = new ArrayList<>();
		try {
			String sql = "SELECT ID, CODE ,MARCAS ,COMPANIAS, RETAILERS, MES, FECHA_EJECUCION, ESTADO,USER_ID, FECHA_CREACION, PROCESO, ORDEN FROM SCHEDULE WITH(nolock)";

			if (!inParametros.isEmpty()) {
				List<String> parametros = new ArrayList<>();

				for (Map.Entry<String, List> entry : inParametros.entrySet()) {
					List<Object> listValores = entry.getValue();
					List<String> listValoresText = new ArrayList<>();
					for (Object v : listValores) {
						String valor = "";

						if (v instanceof String) {
							valor = (String) v;
						}

						if (v instanceof Long) {
							valor = Long.toString((long) v);
						}

						if (v instanceof Integer) {
							valor = String.valueOf(v);
						}
						listValoresText.add("'" + valor + "'");
					}

					if (!entry.getKey().equals("FECHA_EJECUCION") && !entry.getKey().equals("FECHA_EJECUCION-P")) {
						parametros.add(
								entry.getKey().concat(" IN (").concat(String.join(",", listValoresText).concat(")")));
					} else {
						if (entry.getKey().equals("FECHA_EJECUCION")) {
							parametros.add(" CONVERT(Date,FECHA_EJECUCION) = " + listValoresText.get(0));

						}

						if (entry.getKey().equals("FECHA_EJECUCION-P")) {
							parametros.add(" CONVERT(DATETIME,FECHA_EJECUCION)  <= " + listValoresText.get(0));

						}
					}

				}

				sql = sql.concat(" WHERE ").concat(String.join(" AND ", parametros));

			}
			sql = sql.concat(" ORDER BY CODE,ORDEN");

			Query q = em.createNativeQuery(sql);
			List<Object[]> resultados = q.getResultList();

			for (Object[] obj : resultados) {
				ScheduleVO s = new ScheduleVO();
				s.setId(obj[0] != null ? ((BigDecimal) obj[0]).longValue() : null);
				s.setCode(obj[1] != null ? (String) obj[1] : null);
				s.setMarcas(obj[2] != null ? (String) obj[2] : null);
				s.setCompanias(obj[3] != null ? (String) obj[3] : null);
				s.setRetailers(obj[4] != null ? (String) obj[4] : null);
				s.setMes(obj[5] != null ? ((BigDecimal) obj[5]).longValue() : null);
				s.setFechaEjecucion(obj[6] != null ? (String) obj[6] : null);
				s.setEstado(obj[7] != null ? (String) obj[7] : null);
				s.setUsuario(obj[8] != null ? (String) obj[8] : null);
				s.setFechaCreacion(obj[9] != null ? (String) obj[9] : null);
				s.setProceso(obj[10] != null ? (String) obj[10] : null);
				s.setOrden(obj[11] != null ? (int) obj[11] : null);
				out.add(s);
			}
		} catch (

		Exception e) {
			LOGGER.error("[consultar] Hubo un error realizando la consulta de los procesos", e);

		}

		return out;
	}

	/**
	 * 
	 * @param inScheduleVO
	 * @throws SystemException
	 * @throws NotSupportedException
	 * @throws HeuristicRollbackException
	 * @throws HeuristicMixedException
	 * @throws RollbackException
	 * @throws IllegalStateException
	 * @throws SecurityException
	 */
	private void update(ScheduleVO inScheduleVO) {
		String sql = "UPDATE SCHEDULE SET CODE= ?,"// 1
				+ " MARCAS= ?,"// 2
				+ " COMPANIAS= ?,"// 3
				+ " RETAILERS= ?,"// 4
				+ " MES= ?,"// 5
				+ " FECHA_EJECUCION  = ?,"// 6
				+ " ESTADO= ?,"// 7
				+ " USER_ID= ?,"// 8
				+ " PROCESO = ?,"// 9
				+ " ORDEN = ?"// 10
				+ " WHERE ID = ? ";// 11;
		Query q = em.createNativeQuery(sql);
		q.setParameter(1, inScheduleVO.getCode());
		q.setParameter(2, inScheduleVO.getMarcas());
		q.setParameter(3, inScheduleVO.getCompanias());
		q.setParameter(4, inScheduleVO.getRetailers());
		q.setParameter(5, inScheduleVO.getMes());
		q.setParameter(6, inScheduleVO.getFechaEjecucion());
		q.setParameter(7, inScheduleVO.getEstado());
		q.setParameter(8, inScheduleVO.getUsuario());
		q.setParameter(9, inScheduleVO.getProceso());
		q.setParameter(10, inScheduleVO.getOrden());
		q.setParameter(11, inScheduleVO.getId());
		q.executeUpdate();

	}

	/**
	 * 
	 * @param inScheduleVO
	 */
	private void insert(ScheduleVO inScheduleVO) {

		String sql = "INSERT INTO SCHEDULE ( CODE , MARCAS , COMPANIAS , RETAILERS , MES , FECHA_EJECUCION , ESTADO , USER_ID , FECHA_CREACION, PROCESO, ORDEN ) "
				+ "    VALUES  (? ,? ,? ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?)";

		Query q = em.createNativeQuery(sql).setParameter(1, inScheduleVO.getCode())
				.setParameter(2, inScheduleVO.getMarcas()).setParameter(3, inScheduleVO.getCompanias())
				.setParameter(4, inScheduleVO.getRetailers()).setParameter(5, inScheduleVO.getMes())
				.setParameter(6, inScheduleVO.getFechaEjecucion()).setParameter(7, inScheduleVO.getEstado())
				.setParameter(8, inScheduleVO.getUsuario()).setParameter(9, inScheduleVO.getFechaCreacion())
				.setParameter(10, inScheduleVO.getProceso()).setParameter(11, inScheduleVO.getOrden());
		q.executeUpdate();

	}

	/**
	 * 
	 * @param code
	 * @param estado
	 */
	@Transactional
	public void actualizarEstadoProcesoXCode(String code, String estado) {
		String sql = "UPDATE  SCHEDULE SET   ESTADO= ?1  WHERE CODE = ?2";
		Query q = em.createNativeQuery(sql);
		q.setParameter(2, code);
		q.setParameter(1, estado);
		q.executeUpdate();
		em.flush();
		em.clear();

	}

	/**
	 * 
	 * @param id
	 * @param estado
	 */
	@Transactional
	public void actualizarEstadoProcesoXId(Long id, String estado) {
		String sql = "UPDATE SCHEDULE SET ESTADO= ?1  WHERE ID = ?2";
		Query q = em.createNativeQuery(sql);
		q.setParameter(2, id);
		q.setParameter(1, estado);
		q.executeUpdate();
		em.flush();
		em.clear();
	}

}
