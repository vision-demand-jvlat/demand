/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlap.sop.demand.constantes.ConstantesDemand;
import com.jvlap.sop.demand.logica.vos.ItemSemanasRequeridasVO;
import com.jvlap.sop.demand.logica.vos.SemanasRequeridasVO;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.entidadessop.entidades.SellInSugeridoCategoriaTMP;
import com.jvlat.sop.entidadessop.entidades.SellInSugeridoProductoTMP;
import com.jvlat.sop.entidadessop.entidades.Variacion;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author Seidor
 */
@SuppressWarnings("rawtypes")
@Singleton
public class SemanasRequeridasEJB extends AbstractFacade {

	@PersistenceContext(unitName = "EntidadesSOPPU")
	private EntityManager em;

	private BigDecimal totalSellInMesDos = new BigDecimal(0);
	private BigDecimal totalSellInMesTres = new BigDecimal(0);
	private BigDecimal totalSellInMesCuatro = new BigDecimal(0);
	private BigDecimal totalSellInMesCinco = new BigDecimal(0);
	private BigDecimal totalSellInMesSeis = new BigDecimal(0);
	private BigDecimal totalSellInMesSiete = new BigDecimal(0);
	private BigDecimal totalSellInMesOcho = new BigDecimal(0);
	private BigDecimal totalSellInMesNueve = new BigDecimal(0);
	private BigDecimal totalSellInMesDiez = new BigDecimal(0);
	private BigDecimal totalSellInMesOnce = new BigDecimal(0);
	private BigDecimal totalSellInMesDoce = new BigDecimal(0);

	@EJB
	ModuloGeneralEJB moduloGeneral;

	static Logger log = LogManager.getLogger(SemanasRequeridasEJB.class.getName());

	/**
	 * Consulta en el sistema las semanas realizar el calculo de sellin sugerido
	 *
	 * @param mesFind
	 * @param retailer
	 * @param marca
	 * @return
	 * @throws com.jvlap.sop.commonsop.excepciones.ExcepcionSOP
	 */
	public List<SemanasRequeridasVO> consultarSemanas(Long mesFind, String retailer, String marca, String compania,
			boolean isProducto, SemanasRequeridasVO oCategoria) throws Exception {

		try {
			return this.consultaResultadoSemanasRequeridas(retailer, marca, compania,
					moduloGeneral.consultaMesSop(mesFind), isProducto, oCategoria);

		} catch (Exception e) {
			log.error("Error en SemanasRequeridasEJB.consultarSemanas consulta", e);
			return new ArrayList<>();
		}
	}

	/**
	 * @author Manuel Cortes Granados
	 * @since 8 Junio 2017 10:50 AM
	 * @param mesFind
	 * @param retailer
	 * @param marca
	 * @param compania
	 * @param listMarcas
	 * @return
	 * @throws ExcepcionSOP
	 * @throws Exception
	 */
	public boolean recalcularSemanasAuxGrupoMarca(Long mesFind, String retailer, String marca, String compania,
			List<String> listMarcas) throws Exception {

		MesSop mesSop = moduloGeneral.consultaMesSop(mesFind);
		int anio = mesSop.getAnio();
		int mes = mesSop.getMes();
		boolean isValid = true;

		try {

			if (this.existeVariacionMes(mesSop, retailer, listMarcas)) {
				for (String marca_1 : listMarcas) {
					boolean aux = this.ejecutaProcedimientoBDSemanas(anio, mes, retailer,
							ConstantesDemand.CANTIDAD_MESE, marca_1, anio + "_" + mes, 1);
					isValid = (isValid && aux);
				}
			} else {
				throw new ExcepcionSOP("No se encontró variación para el mes a ejecutar");
			}

		} catch (Exception e) {
			log.error("Error en SemanasRequeridasEJB.consultarSemanas consulta ", e);
		}

		return isValid;
	}

	/**
	 * Valida si existe Variacion para el mes que se ejecutan las semanas requeridas
	 *
	 * @param anio
	 * @param mes
	 * @param retailer
	 * @param marca
	 * @return
	 */
	private boolean existeVariacionMes(MesSop mesSop, String retailer, List<String> marca) {
		try {
			Retailer retailerE = em.find(Retailer.class, retailer);
			try {
				String qlString = "SELECT v FROM Variacion v  WHERE v.mesFk = :mes AND v.marcaFk.id IN :marca AND v.retailerFk = :retailer";
				TypedQuery<Variacion> queryVariacion = em.createQuery(qlString, Variacion.class)
						.setParameter("mes", mesSop).setParameter("marca", marca).setParameter("retailer", retailerE);
				List<Variacion> v = queryVariacion.getResultList();
				return v.size() == marca.size();
			} catch (Exception e) {
				log.error("[existeVariacionMes] No se encontro Variacion para eliminar ", e);
				return false;
			}
		} catch (Exception e) {
			log.error("[existeVariacionMes] Fallo en el borrado de VARIACIONES ANTERIORES", e);
		}
		return false;
	}

	/**
	 * Ejecuta en el procedimiento almacenado en base de datos con los calculos de
	 * la variacion
	 *
	 * @return
	 */
	private boolean ejecutaProcedimientoBDSemanas(int anio, int mes, String retailer, int cantMeses, String marca,
			String mesBase, int flag) {
		return llamarProcedimientoBDSemanas(anio, mes, retailer, marca, mesBase, flag, cantMeses);
	}

	@Timeout
	@AccessTimeout(value = 60, unit = TimeUnit.MINUTES)
	private boolean llamarProcedimientoBDSemanas(int anio, int mes, String retailer, String marca, String mesBase,
			int flag, int cantMeses) {

		int contador = 0;
		while (contador < cantMeses) {
			StoredProcedureQuery storedProcQuery = em.createStoredProcedureQuery("SemanasRequeridas_Mes");
			storedProcQuery.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
			storedProcQuery.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
			storedProcQuery.registerStoredProcedureParameter("retailer", String.class, ParameterMode.IN);

			storedProcQuery.registerStoredProcedureParameter("marca", String.class, ParameterMode.IN);
			storedProcQuery.registerStoredProcedureParameter("mesBase", String.class, ParameterMode.IN);
			storedProcQuery.registerStoredProcedureParameter("flag", Integer.class, ParameterMode.IN);
			storedProcQuery.registerStoredProcedureParameter("sw", Integer.class, ParameterMode.IN);

			storedProcQuery.setParameter("anio", anio);
			storedProcQuery.setParameter("mes", mes);
			storedProcQuery.setParameter("retailer", retailer);
			storedProcQuery.setParameter("marca", marca);
			storedProcQuery.setParameter("mesBase", mesBase);
			storedProcQuery.setParameter("flag", flag);
			storedProcQuery.setParameter("sw", contador);

			storedProcQuery.execute();

			if (mes + 1 == 13) {
				mes = 1;
				anio = anio + 1;
			} else {
				mes = mes + 1;
			}
			contador++;
		}

		return true;
	}

	/**
	 * Ejecuta procedimiento para el cambio del valor de categoria
	 *
	 * @param mesFind
	 * @param retailer
	 * @param marca
	 * @param semReales
	 * @param semReq
	 * @param categoria
	 * @param flag
	 */
	public void calcularSemanasRequeridas(Long mesFind, String retailer, String marca, int flag) {

		try {
			int mesBase;
			int anioBase;
			// Llama procedimiento para mes actual
			MesSop mesSop = moduloGeneral.consultaMesSop(mesFind);
			int anio = mesSop.getAnio();
			int mes = mesSop.getMes();

			// Llama procedimiento para los meses siguientes
			// Actualiza mes y anio para el siguiente query
			mesBase = mes;
			anioBase = anio;
			if (mes + 1 == 13) {
				mes = 1;
				anio++;
			} else {
				mes++;
			}

			this.ejecutaProcedimientoBDSemanas(anio, mes, retailer, ConstantesDemand.CANTIDAD_MESE, marca,
					anioBase + "_" + mesBase, flag);

		} catch (Exception e) {
			log.error("Fallo en calculo de semanas requeridas ", e);
		}
	}

	/**
	 * Consultar semanas requeridas a nivel de categoria o productos para 12 meses.
	 *
	 * @param compania
	 * @param mesSop
	 * @param isProducto
	 * @param retailer
	 * @param marca
	 * @param oCategoria
	 * @return
	 * @throws java.lang.Exception
	 */
	public List<SemanasRequeridasVO> consultaResultadoSemanasRequeridas(String retailer, String marca, String compania,
			MesSop mesSop, boolean isProducto, SemanasRequeridasVO oCategoria) throws Exception {

		String qlString;
		if (!isProducto) {
			qlString = "SELECT r.id+'-'+r.nombre_comercial retailer," // 0
					+ " sc.categoria categoria," // 1
					+ " '0_'+ic.categoria categoria_producto," // 2
					+ " ROUND(sc.inventario,0) inventario ,"// 3
					+ " sc.semanas_reales,"// 4
					+ " sc.semanas_requeridas, " // 5
					+ " ROUND(sc.sellin,0) sell_in,"// 6
					+ " ROUND(vc.sell_out,0,0) sell_out," // 7
					+ " sc.id var_categ, NULL var_prod," // 8
					+ " NULL producto_fk," // 9
					+ " sc.flag_edit, " // 10
					+ "     ms.id idMes, " // 11
					+ "     ms.nombre, " // 12
					+ "     sc.categoria id, " // 13
					+ "     s.marca_fk " // 14
					+ " FROM SEMANAS_REQUERIDAS_TMP s WITH (NOLOCK),"
					+ "SELL_IN_SUGERIDO_CATEGORIA_TMP sc WITH (NOLOCK)," + "MES_SOP ms WITH (NOLOCK),"
					+ "I_VIEW_CATEGORIA ic WITH (NOLOCK)," + "VARIACION_CATEGORIA vc WITH (NOLOCK),"
					+ "VARIACION v WITH (NOLOCK),RETAILER r WITH (NOLOCK)"
					+ " WHERE ms.id between ?1 AND ?2 and s.id = sc.semanas_requeridas_fk and s.mes_fk = ms.id and"
					+ " s.retailer_fk = ?3 and s.marca_fk IN " + marca + " and"
					+ " sc.categoria = ic.id AND v.id = vc.variacion_fk and v.mes_fk = ms.id and v.retailer_fk = ?3 and v.marca_fk IN "
					+ marca + " and  vc.categoria = ic.id  and r.id = v.retailer_fk  ORDER BY categoria, idMes ";
		} else {
			qlString = "SELECT " + "r.id+'-'+r.nombre_comercial retailer," // 0
					+ "ic.marca_cat2_fk+ic.grupo_cat4_fk+ic.plataforma_cat5_fk categoria," // 1
					+ "descripcion categoria_producto," // 2
					+ " ROUND(sc.inventario,0) inventario,"// 3
					+ "sc.semanas_reales,"// 4
					+ "sc.semanas_requeridas," // 5
					+ " ROUND(sc.sellin,0) sell_in ,"// 6
					+ "ROUND(vc.sell_out,0,0) sell_out , " // 7
					+ " NULL var_categ,"// 8
					+ " sc.id var_prod," // 9
					+ "       ic.id, "// 10
					+ "sc.flag_edit," // 11
					+ "     ms.id idMes," // 12
					+ "     ms.nombre, " // 13
					+ "     '0'," // 14
					+ "     ic.marca_cat2_fk," // 16
					+ "     ic.UPC," // 16
					+ "     ic.modelo,"// 17
					+ "     ISNULL((SELECT TOP (1) pc.estado_des  FROM PRODUCTO_X_COMPANIA pc WHERE pc.producto_fk = ic.id AND compania_fk = ?4),NULL ) estadoDesc " // 18
					+ "FROM SEMANAS_REQUERIDAS_TMP s1 WITH (NOLOCK), SELL_IN_SUGERIDO_PRODUCTO_TMP sc WITH (NOLOCK),MES_SOP ms WITH (NOLOCK), PRODUCTO ic WITH (NOLOCK), VARIACION_PRODUCTO vc WITH (NOLOCK),"
					+ " VARIACION v WITH (NOLOCK),"
					+ "RETAILER r WITH (NOLOCK) WHERE ms.id between ?1 AND ?2 and s1.id = sc.semanas_requeridas_fk and s1.mes_fk = ms.id and s1.retailer_fk = ?3 and s1.marca_fk IN "
					+ marca
					+ " and sc.producto_fk = ic.id AND v.id = vc.variacion_fk and v.mes_fk = ms.id and v.retailer_fk = ?3 and v.marca_fk IN "
					+ marca
					+ " and vc.producto_fk = ic.id and r.id = v.retailer_fk and ic.marca_cat2_fk+ic.grupo_cat4_fk+ic.plataforma_cat5_fk = '"
					+ oCategoria.getCategoria() + "' ORDER BY ic.modelo ASC, idMes";
		}

		Query query = em.createNativeQuery(qlString);
		query.setParameter(3, retailer);
		query.setParameter(1, mesSop.getId());
		query.setParameter(2, mesSop.getId() + 11);
		if (isProducto)
			query.setParameter(4, compania);
		List<SemanasRequeridasVO> producto = new ArrayList<>();
		ItemSemanasRequeridasVO datosProducto;
		@SuppressWarnings("unchecked")
		List<Object[]> lista = query.getResultList();

		try {
			if (!lista.isEmpty()) {
				for (Object[] resultado : lista) {
					SemanasRequeridasVO objSemanas = new SemanasRequeridasVO();
					objSemanas.setCategoriaProducto((String) resultado[2]);
					objSemanas.setProducto(resultado[10] != null ? (String) resultado[10] : (String) resultado[1]);
					if (!producto.contains(objSemanas)) {
						objSemanas.setMesNombre((String) resultado[13]);
						objSemanas.setRetailer((String) resultado[0]);
						objSemanas.setCategoria((String) resultado[1]);
						objSemanas.setEsProducto(resultado[10] != null);
						objSemanas.setMarca((String) resultado[15]);

						if (isProducto) {
							objSemanas.setUpc((String) resultado[16]);
							objSemanas.setModelo((String) resultado[17]);
							if (resultado[18] != null
									&& resultado[18].equals(ConstantesDemand.FLAG_ESTADO_DESCONTINUADO)) {

								BigDecimal inventario = moduloGeneral
										.obtenerInvetariosProducto(objSemanas.getProducto(), compania);
								if (inventario.compareTo(BigDecimal.ZERO) <= 0) {
									continue;
								}
							}
						}
						producto.add(objSemanas);
					}
				}

				for (SemanasRequeridasVO objProducto : producto) {
					for (Object[] resultado : lista) {
						if (objProducto.getProducto().equals(
								objProducto.isEsProducto() ? (String) resultado[10] : resultado[14].toString())) {
							datosProducto = new ItemSemanasRequeridasVO();
							datosProducto.setInventarioInicial((BigDecimal) resultado[3]);
							datosProducto.setSemanasReales((BigDecimal) resultado[4]);
							datosProducto.setSemanasRequeridas((BigDecimal) resultado[5]);
							datosProducto
									.setSellIn((BigDecimal) (resultado[6] != null ? resultado[6] : BigDecimal.ZERO));
							datosProducto
									.setSellOut((BigDecimal) (resultado[7] != null ? resultado[7] : BigDecimal.ZERO));
							datosProducto.setVarCateg((BigDecimal) resultado[8]);
							datosProducto.setVarProd((BigDecimal) resultado[9]);
							int flagTM = (Integer) (resultado[11] != null ? resultado[11] : 0);
							datosProducto.setFlagEdit(flagTM);
							datosProducto.setMes(new Integer(resultado[12].toString()));
							datosProducto.setEsActual(resultado[12].toString().equals(mesSop.getId().toString()));

							objProducto.agregarInfoMes((String) resultado[13], datosProducto);

						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Error en SemanasRequeridasEJB.consultaResultadoSemanas ", e);
			throw e;
		}

		if (isProducto) {
			return actualizarCategoriasSemanas(producto, oCategoria);
		}
		return producto;
	}

	/**
	 * Actualizar categorias, columnas Sellin, SellOut y Inventario Inicial
	 *
	 * @param productos
	 * @param oCategoria
	 * @return
	 */
	public List<SemanasRequeridasVO> actualizarCategoriasSemanas(List<SemanasRequeridasVO> productos,
			SemanasRequeridasVO oCategoria) {

		for (Map.Entry<String, ItemSemanasRequeridasVO> entry : oCategoria.getInformacionSemanasMes().entrySet()) {
			entry.getValue().setSellIn(BigDecimal.ZERO);
			entry.getValue().setSellOut(BigDecimal.ZERO);
			entry.getValue().setInventarioInicial(BigDecimal.ZERO);
		}

		for (SemanasRequeridasVO productoMap : productos) {
			if (productoMap.isEsProducto()) {
				for (Map.Entry<String, ItemSemanasRequeridasVO> entry : productoMap.getInformacionSemanasMes()
						.entrySet()) {
					if (oCategoria.getInformacionSemanasMes().containsKey(entry.getKey())) {
						oCategoria.getInformacionSemanasMes().get(entry.getKey())
								.setSellIn(oCategoria.getInformacionSemanasMes().get(entry.getKey()).getSellIn()
										.add(entry.getValue().getSellIn() != null ? entry.getValue().getSellIn()
												: BigDecimal.ZERO));
						oCategoria.getInformacionSemanasMes().get(entry.getKey())
								.setSellOut(oCategoria.getInformacionSemanasMes().get(entry.getKey()).getSellOut()
										.add(entry.getValue().getSellOut() != null ? entry.getValue().getSellOut()
												: BigDecimal.ZERO));
						oCategoria.getInformacionSemanasMes().get(entry.getKey()).setInventarioInicial(
								oCategoria.getInformacionSemanasMes().get(entry.getKey()).getInventarioInicial()
										.add(entry.getValue().getInventarioInicial() != null
												? entry.getValue().getInventarioInicial()
												: BigDecimal.ZERO));
					}
				}
			}
		}
		return productos;
	}

	/**
	 * Guardar las semanas requeridas de inventario configuradas por el usaurio
	 *
	 * @param compania
	 * @param mesFind
	 * @param retailer
	 * @param marca
	 * @param usuarioLogueado
	 * @param grupoMarcas
	 * @return
	 * @throws com.jvlap.sop.commonsop.excepciones.ExcepcionSOP
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@AccessTimeout(value = 60, unit = TimeUnit.MINUTES)
	public String guardarSemanasRequeridas(Compania compania, Long mesFind, String retailer, String marca,
			UsuarioVO usuarioLogueado, String grupoMarcas) throws ExcepcionSOP {

		MesSop mesSop;
		try {
			mesSop = moduloGeneral.consultaMesSop(mesFind);
			int anio = mesSop.getAnio();
			int mes = mesSop.getMes();

			StoredProcedureQuery storedProcQuery = em.createStoredProcedureQuery("SemanasRequeridas_Guardar");

			storedProcQuery.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
			storedProcQuery.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
			storedProcQuery.registerStoredProcedureParameter("retailer", String.class, ParameterMode.IN);
			storedProcQuery.registerStoredProcedureParameter("marca", String.class, ParameterMode.IN);
			storedProcQuery.registerStoredProcedureParameter("cantidad", Integer.class, ParameterMode.IN);
			log.info("anio: {}", anio);
			log.info("mes: {}", mes);
			log.info("retailer : {}", retailer);
			log.info("marca : {}", marca);
			log.info("cantidad " + ConstantesDemand.CANTIDAD_MESE);
			storedProcQuery.setParameter("anio", anio);
			storedProcQuery.setParameter("mes", mes);
			storedProcQuery.setParameter("retailer", retailer);
			storedProcQuery.setParameter("marca", marca);
			storedProcQuery.setParameter("cantidad", ConstantesDemand.CANTIDAD_MESE);

			storedProcQuery.execute();
		} catch (Exception e) {
			log.error("Error en SemanasRequeridas.guardarSemanasRequeridas", e);
			throw new ExcepcionSOP("Error guardando semanas requeridas ", e);
		}

		return "exitoso";
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	/**
	 * Actualizo el valor del semenas requerias y el SellIn de la Categoria
	 * seleccionada
	 *
	 * @param sellInSugeCate
	 */
	public void actualizarSellInCategoria(SellInSugeridoCategoriaTMP sellInSugeCate) {

		try {
			if (sellInSugeCate != null) {
				Query query = em.createQuery("UPDATE SellInSugeridoCategoriaTMP s SET s.semanasRequeridas = ?1"
						+ ",s.sellin = ?2  WHERE s.id = ?3");
				query.setParameter(1, sellInSugeCate.getSemanasRequeridas());
				query.setParameter(2, sellInSugeCate.getSellin());
				query.setParameter(3, sellInSugeCate.getId());
				query.executeUpdate();
			}

		} catch (Exception e) {
			log.error(
					"[actualizarSellInCategoria] Fallo Actualizacion de las Sem Req. y SellIN Metodo actualizarSellInCategoria(ArrayList<SellInSugeridoCategoria> sellInSugeCate)",
					e);
		}
	}

	/**
	 * Actualizo el valor del semenas requerias y el SellIn del Producto
	 * seleccionado
	 *
	 * @param sellInSugeProdu
	 */
	public void actualizarSellInProducto(SellInSugeridoProductoTMP sellInSugeProdu) {

		try {
			if (sellInSugeProdu != null) {
				Query query = em.createQuery("UPDATE SellInSugeridoProductoTMP s SET s.semanasRequeridas = ?1"
						+ ",s.sellin = ?2  WHERE s.id = ?3");
				query.setParameter(1, sellInSugeProdu.getSemanasRequeridas());
				query.setParameter(2, sellInSugeProdu.getSellin());
				query.setParameter(3, sellInSugeProdu.getId());
				query.executeUpdate();
			}

		} catch (Exception e) {
			log.error(
					"[actualizarSellInProducto] Fallo Actualizacion de las Sem Req. y SellIN Metodo actualizarSellInProducto(ArrayList<SellInSugeridoCategoria> sellInSugeCate)",
					e);
		}
	}

	/**
	 * valido si el producto no tiene Sell Out, validar si el campo sellOut es Cero
	 * --> obtener los 6 ultimos meses promedio
	 *
	 * @param idMes
	 * @param retailer
	 * @param oCategoria
	 * @param oProductos
	 */
	public void validarSellOt(Long idMes, SemanasRequeridasVO oCategoria, List<SemanasRequeridasVO> oProductos) {

		SellInSugeridoProductoTMP sellInSugeProdu;
		List<String> listID = new ArrayList<>();
		String idSellinMes = "(";
		List<String> nombreMeses = moduloGeneral.consultarNombresMeses(12, idMes);

		for (SemanasRequeridasVO objProd : oProductos) {
			sellInSugeProdu = new SellInSugeridoProductoTMP();
			for (Map.Entry<String, ItemSemanasRequeridasVO> entry : objProd.getInformacionSemanasMes().entrySet()) {
				if (entry.getKey().equals(nombreMeses.get(0))) {
					ItemSemanasRequeridasVO oActual = objProd.getInformacionSemanasMes().get(entry.getKey());
					sellInSugeProdu.setSellin(oActual.getSellIn());
					sellInSugeProdu.setInventario(oActual.getInventarioInicial());
					sellInSugeProdu.setSemanasReales(oActual.getSemanasReales());
				}

				if (objProd.getInformacionSemanasMes().get(entry.getKey()).getSellOut()
						.compareTo(BigDecimal.ZERO) <= 0) {
					listID.add(objProd.getInformacionSemanasMes().get(entry.getKey()).getVarProd().toString());
					objProd.getInformacionSemanasMes().get(entry.getKey()).setSellIn(sellInSugeProdu.getSellin());
					objProd.getInformacionSemanasMes().get(entry.getKey())
							.setInventarioInicial(sellInSugeProdu.getInventario());
					objProd.getInformacionSemanasMes().get(entry.getKey())
							.setSemanasReales(sellInSugeProdu.getSemanasReales());
				}
			}
			idSellinMes = idSellinMes.concat(String.join(",", listID)).concat(")");
			actualizarSellInSugeridoProductoTMP(sellInSugeProdu, idSellinMes);
			idSellinMes = "(";
			listID = new ArrayList<>();
		}
	}

	/**
	 * Metodo que actualiza el sellin de la categoria
	 *
	 * @param oCategoria
	 * @param nombreMeses
	 * @throws com.jvlap.sop.commonsop.excepciones.ExcepcionSOP
	 */
	public void guardarSellOutCategoria(SemanasRequeridasVO oCategoria, List<String> nombreMeses) throws Exception {

		SellInSugeridoCategoriaTMP sellInSugeCate = new SellInSugeridoCategoriaTMP();

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(0))) {
			sellInSugeCate.setInventario(
					oCategoria.getInformacionSemanasMes().get(nombreMeses.get(0)).getInventarioInicial());
			sellInSugeCate
					.setSemanasReales(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(0)).getSemanasReales());
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(1))) {
			sellInSugeCate.setSellin(totalSellInMesDos);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(1)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(1))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesDos = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(2))) {
			sellInSugeCate.setSellin(totalSellInMesTres);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(2)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(2))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesTres = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(3))) {
			sellInSugeCate.setSellin(totalSellInMesCuatro);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(3)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(3))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesCuatro = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(4))) {
			sellInSugeCate.setSellin(totalSellInMesCinco);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(4)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(4))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesCinco = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(5))) {
			sellInSugeCate.setSellin(totalSellInMesSeis);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(5)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(5))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesSeis = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(6))) {
			sellInSugeCate.setSellin(totalSellInMesSiete);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(6)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(6))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesSiete = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(7))) {
			sellInSugeCate.setSellin(totalSellInMesOcho);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(7)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(7))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesOcho = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(8))) {
			sellInSugeCate.setSellin(totalSellInMesNueve);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(8)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(8))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesNueve = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(9))) {
			sellInSugeCate.setSellin(totalSellInMesDiez);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(9)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(9))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesDiez = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(10))) {
			sellInSugeCate.setSellin(totalSellInMesOnce);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(10)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(10))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesOnce = new BigDecimal(0);
		}

		if (oCategoria.getInformacionSemanasMes().containsKey(nombreMeses.get(11))) {
			sellInSugeCate.setSellin(totalSellInMesDoce);
			sellInSugeCate
					.setId(oCategoria.getInformacionSemanasMes().get(nombreMeses.get(11)).getVarCateg().longValue());
			oCategoria.getInformacionSemanasMes().get(nombreMeses.get(11))
					.setSemanasReales(sellInSugeCate.getSemanasReales());
			actualizarSellInSugeridoCategoriaTMP(sellInSugeCate);
			totalSellInMesDoce = new BigDecimal(0);
		}
	}

	/**
	 * Actualizo el valor del Inv. inicial, semenas reales y el SellIn de la
	 * Categoria seleccionada
	 *
	 * @param sellInSugeCate
	 */
	public void actualizarSellInSugeridoCategoriaTMP(SellInSugeridoCategoriaTMP sellInSugeCate) {

		try {
			if (sellInSugeCate != null) {
				Query query = em.createQuery("UPDATE SellInSugeridoCategoriaTMP s SET s.semanasReales = ?1, "
						+ " s.inventario = ?2, s.sellin = ?3  WHERE s.id = ?4");
				query.setParameter(1, sellInSugeCate.getSemanasReales());
				query.setParameter(2, sellInSugeCate.getInventario());
				query.setParameter(3, sellInSugeCate.getSellin());
				query.setParameter(4, sellInSugeCate.getId());
				query.executeUpdate();
			}

		} catch (Exception e) {
			log.error("Fallo Actualizacion del Metodo actualizarSellInSugeridoCategoriaTMP()", e);
		}
	}

	/**
	 * Actualizo el valor del Inv. inicial, semenas reales y el SellIn del Producto
	 * seleccionado
	 *
	 * @param sellInSugeProdu
	 */

	public void actualizarSellInSugeridoProductoTMP(SellInSugeridoProductoTMP sellInSugeProdu, String idSellinMes) {

		try {
			if (sellInSugeProdu != null && idSellinMes.length() > 2) {
				Query query = em.createQuery("UPDATE SellInSugeridoProductoTMP s SET s.semanasReales = ?1, "
						+ " s.inventario = ?2, s.sellin = ?3  WHERE s.id IN " + idSellinMes);
				query.setParameter(1, sellInSugeProdu.getSemanasReales());
				query.setParameter(2, sellInSugeProdu.getInventario());
				query.setParameter(3, sellInSugeProdu.getSellin());
				query.executeUpdate();
			}

		} catch (Exception e) {
			log.error("Fallo Actualizacion del Metodo SellInSugeridoProductoTMP()", e);
		}
	}

	/**
	 * 
	 * @param compania
	 * @param mesFind
	 * @param retailer
	 * @param grupoMarcas
	 */
	public void notificarCreacionSemanasRequeridas(Compania compania, Long mesFind, List<String> listRetailer,
			List<String> listGrupoMarcas) {
		MesSop mesSop = moduloGeneral.consultaMesSop(mesFind);

		for (String grupoMarcas : listGrupoMarcas) {
			GrupoMarca gMarcas = moduloGeneral.consultarGrupoMarcaID(grupoMarcas);

			moduloGeneral.notifica(compania, gMarcas.getNombre(), listRetailer, mesSop, "Semanas Requeridas - ",
					"Estimado (a) <br/><br/> El Proceso Semanas Requeridas ha finalizado con fecha: ",
					"Favor iniciar el proceso de Forecast Retailer. ", ConstantesDemand.ETAPA_SEMANAS_REQUERIDAS,
					false);
			moduloGeneral.notifica(compania, gMarcas.getNombre(), listRetailer, mesSop, "Semanas Requeridas - ",
					"Estimado (a) <br/><br/> El Proceso Semanas Requeridas ha finalizado con fecha: ",
					"Favor iniciar el proceso de Forecast Retailer. ", ConstantesDemand.ETAPA_FORECAST_RETAILER, true);
		}
	}

	/**
	 * 
	 * @param categoria
	 * @param listCategorias
	 * @return
	 */
	public SemanasRequeridasVO buscarCategoria(String categoria, List<SemanasRequeridasVO> listCategorias) {

		for (SemanasRequeridasVO oCategoria : listCategorias) {
			if (oCategoria.getCategoria().equals(categoria))
				return oCategoria;
		}
		return null;
	}

	/**
	 * 
	 * @param compaTM
	 * @param mesSeleccionado
	 * @param retailerSeleccionadoIde
	 * @param listMarcas
	 * @param usuarioLogueado
	 * @param grupoMarcaSeleccionadaIde
	 */
	public synchronized void guardarSemanasRequeridas(Compania compaTM, Long mesSeleccionado,
			List<String> listRetailerSeleccionadoIde, UsuarioVO usuarioLogueado,
			List<String> listGrupoMarcaSeleccionadaIde) {
		try {
			for (String retailerSeleccionadoIde : listRetailerSeleccionadoIde) {
				for (String grupoMarcaSeleccionadaIde : listGrupoMarcaSeleccionadaIde) {
					this.guardarSemanasRequeridas(compaTM, mesSeleccionado, retailerSeleccionadoIde,
							grupoMarcaSeleccionadaIde, usuarioLogueado, grupoMarcaSeleccionadaIde);
				}
			}

			this.notificarCreacionSemanasRequeridas(compaTM, mesSeleccionado, listRetailerSeleccionadoIde,
					listGrupoMarcaSeleccionadaIde);
		} catch (Exception e) {
			log.error("[guardarSemanasRequeridas] ERROR SINCRONIZANDO Y GUARDANDO SEMANAS ", e);
		}
	}

	/**
	 * 
	 * @param mesFind
	 * @param retailer
	 * @param marca
	 * @param compania
	 * @param listMarcas
	 * @param idGrupo
	 * @param prodCategoria
	 * @param productos
	 * @return
	 */
	public synchronized List<SemanasRequeridasVO> procesarSemanasRequeridas(Long mesFind, String retailer, String marca,
			String compania, List<String> listMarcas, Map<String, List<SemanasRequeridasVO>> prodCategoria,
			List<SemanasRequeridasVO> productos) {
		try {
			boolean isValid = this.recalcularSemanasAuxGrupoMarca(mesFind, retailer, marca, compania, listMarcas);
			if (isValid) {

				productos = this.consultarSemanas(mesFind, retailer, marca, compania, false, null);

				for (SemanasRequeridasVO oCategoria : productos) {
					List<SemanasRequeridasVO> oProductos = this.consultarSemanas(mesFind, oCategoria.getRetailer(),
							marca, compania, true, oCategoria);
					this.validarSellOt(mesFind, oCategoria, oProductos);
					this.actualizarCategoriasSemanas(oProductos, oCategoria);
					prodCategoria.put(oCategoria.getCategoria(), oProductos);
				}
				return productos;
			}
		} catch (Exception e) {
			log.error("[procesarSemanasRequeridas] ERROR SINCRONIZANDO Y PROCESANDO SEMANAS REQUERIDAS ", e);
		}
		return productos;
	}
}
