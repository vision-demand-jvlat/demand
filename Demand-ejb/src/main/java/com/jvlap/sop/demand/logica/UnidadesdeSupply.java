
package com.jvlap.sop.demand.logica;

import java.math.BigDecimal;

/**
 *
 * @author Seidor
 */
public class UnidadesdeSupply {

    private String idCategoria;
    private long  unidadesFaltantes;
    private long  unidadesDespachadas;
    private BigDecimal preciRetailer;
    private BigDecimal cogs;
    private long mes;

    public long getUnidadesFaltantes() {
        return unidadesFaltantes;
    }

    public void setUnidadesFaltantes(long unidades_Faltantes) {
        this.unidadesFaltantes = unidades_Faltantes;
    }

    public long getUnidadesDespachadas() {
        return unidadesDespachadas;
    }

    public void setUnidadesDespachadas(long unidades_Despachadas) {
        this.unidadesDespachadas = unidades_Despachadas;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public BigDecimal getPreciRetailer() {
        return preciRetailer;
    }

    public void setPreciRetailer(BigDecimal preciRetailer) {
        this.preciRetailer = preciRetailer;
    }

    public BigDecimal getCogs() {
        return cogs;
    }

    public void setCogs(BigDecimal cogs) {
        this.cogs = cogs;
    }

    public long getMes() {
        return mes;
    }

    public void setMes(long mes) {
        this.mes = mes;
    }
}
