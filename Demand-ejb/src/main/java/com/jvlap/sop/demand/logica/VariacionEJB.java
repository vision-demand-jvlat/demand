/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.demand.constantes.ConstantesDemand;
import com.jvlap.sop.demand.logica.vos.VariacionSellOutVO;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.VariacionCategoria;
import com.jvlat.sop.entidadessop.entidades.VariacionProducto;

/**
 *
 * @author Seidor
 */
@SuppressWarnings("rawtypes")
@Singleton
public class VariacionEJB extends AbstractFacade {

	@PersistenceContext(unitName = "EntidadesSOPPU")
	private EntityManager em;

	@EJB
	ModuloGeneralEJB moduloGeneral;

	private static final Logger LOG = LogManager.getLogger(VariacionEJB.class.getName());

	/**
	 * Constructor por defecto
	 */
	public VariacionEJB() {
		LOG.info("[VariacionEJB] Inicializando EJB Variacion");
	}

	@SuppressWarnings("unchecked")
	public VariacionEJB(Class entityClass) {
		super(entityClass);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	/**
	 * @author Manuel Cortes Granados
	 * @since 7 Junio 2017 3:52 PM
	 * @param idGrupoMarca
	 * @return
	 */
	public String getMarcasByIDGrupoMarca(String idGrupoMarca) {
		return moduloGeneral.getMarcasByIDGrupoMarca(idGrupoMarca);
	}

	/**
	 * 
	 * @param idGrupoMarca
	 * @return
	 */
	public String getMarcasByIDGrupoMarca(List<String> listGrupoMarca) {
		return moduloGeneral.getMarcasByIDGrupoMarca(listGrupoMarca);
	}

	/**
	 * @author Manuel Cortes Granados
	 * @since 8 Junio 2017 9:12 AM
	 * @param idGrupoMarca
	 * @return
	 */
	public List<String> getMarcaGrupoPorIdGrupoMarca(String idGrupoMarca) {
		return moduloGeneral.getMarcaGrupoPorIdGrupoMarca(Arrays.asList(idGrupoMarca));
	}

	/**
	 * Metodo encargado de ejecutar el proceso de variacion en base de datos
	 *
	 * @param mesFind
	 * @param retailer
	 * @param marca
	 * @param isProducto
	 * @return
	 */
	public List<VariacionSellOutVO> consultarVariacion(Long mesFind, List<String> listRetailer, String marca,
			boolean isProducto, String categoria) {
		MesSop mesSop = moduloGeneral.consultaMesSop(mesFind);
		int anio = mesSop.getAnio();
		int mes = mesSop.getMes();
		List<VariacionSellOutVO> out = new ArrayList<>();
		try {
			for (String retailer : listRetailer) {
				List<VariacionSellOutVO> aux = consultaResultadoVariacion(anio, mes, retailer, marca, isProducto,
						categoria);
				out.addAll(aux);
			}
		} catch (Exception e) {
			LOG.error("Error en VariacionEJB.consultarVariacion consulta ", e);
		}

		return out;
	}

	/**
	 * @author Manuel Cortes Granados
	 * @since 8 Junio 2-17 9:22 AM
	 * @param marcasGrupo
	 * @param mesFind
	 * @param retailer
	 * @param compania
	 * @param idGrupo
	 * @return
	 * @throws Exception
	 */
	public List<VariacionSellOutVO> concatenarRecalcularVariacion(String marcasGrupo, Long mesFind,
			List<String> listRetailer, String compania, List<String> listidGrupo) throws Exception {
		MesSop mesSop = moduloGeneral.consultaMesSop(mesFind);

		for (String marca : listidGrupo) {
			for (String retailer : listRetailer) {
				recalcularVariacion(mesFind, retailer, marca, compania, mesSop.getAnio(), mesSop.getMes());
			}
		}

		List<VariacionSellOutVO> out = new ArrayList<>();
		for (String r : listRetailer) {
			List<VariacionSellOutVO> aux = consultaResultadoVariacion(mesSop.getAnio(), mesSop.getMes(), r, marcasGrupo,
					false, null);
			out.addAll(aux);
		}
		return out;
	}

	/**
	 * Ejecuta el procedimiento de variacion el sistema
	 * 
	 * 
	 * @param mesFind
	 * @param retailer
	 * @param marca
	 * @param compania
	 * @param anio
	 * @param mes
	 * @return
	 */
	@Timeout
	@AccessTimeout(value = 30, unit = TimeUnit.MINUTES)
	public boolean recalcularVariacion(Long mesFind, String retailer, String marca, String compania, int anio,
			int mes) {
		long codigo = System.currentTimeMillis();

		LOG.info("[recalcularVariacion] {} - INICIO DE EJECUTAR EL SP SINCRONIZADO ", codigo);

		StoredProcedureQuery storedProcQuery = em.createStoredProcedureQuery("[Variacion_Forecast]");

		storedProcQuery.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("retailer", String.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("cantidad", Integer.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("marca", String.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("pCompania", String.class, ParameterMode.IN);
		String r = retailer.trim();
		storedProcQuery.setParameter("anio", anio);
		storedProcQuery.setParameter("mes", mes);
		storedProcQuery.setParameter("retailer", r);
		storedProcQuery.setParameter("cantidad", ConstantesDemand.CANTIDAD_MESE);
		storedProcQuery.setParameter("marca", marca);
		storedProcQuery.setParameter("pCompania", compania);

		String datos = "ANIO : " + anio + ", MES : " + mes + ", RETAILER:: " + retailer + ", CANTIDAD : "
				+ ConstantesDemand.CANTIDAD_MESE + ", MARCA:" + marca + ", COMPANIA : " + compania;

		LOG.info("[recalcularVariacion] {} - DATOS DE EJECUTAR EL SP SINCRONIZADO: {} ", codigo, datos);

		storedProcQuery.execute();
		LOG.info("[recalcularVariacion] {} - TERMINO DE EJECUTAR EL SP SINCRONIZADO ", codigo);
		return true;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private boolean llamarProcedimientoBDVariacion(int anio, int mes, String retailer, int cantidad_meses, String marca,
			String compania, int contador) {
		StoredProcedureQuery storedProcQuery = em.createStoredProcedureQuery("[Variacion_Forecast]");

		storedProcQuery.registerStoredProcedureParameter("anio", Integer.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("mes", Integer.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("retailer", String.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("cantidad", Integer.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("marca", String.class, ParameterMode.IN);
		storedProcQuery.registerStoredProcedureParameter("pCompania", String.class, ParameterMode.IN);

		storedProcQuery.setParameter("anio", anio);
		storedProcQuery.setParameter("mes", mes);
		storedProcQuery.setParameter("retailer", retailer);
		storedProcQuery.setParameter("cantidad", cantidad_meses);
		storedProcQuery.setParameter("marca", marca);
		storedProcQuery.setParameter("pCompania", compania);
		return (boolean) storedProcQuery.execute();
	}

	/**
	 * Consulta en base de datos el resultado de la ejecucion del procedimiento de
	 * base de datos
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<VariacionSellOutVO> consultaResultadoVariacion(int anio, int mes, String retailer, String marca,
			boolean isProducto, String categoria) throws Exception {

		retailer = ("('").concat(retailer).concat("')");

		List<VariacionSellOutVO> listaVariacion = new ArrayList<>();
		String qlString = "";

		if (isProducto) {
			qlString = "SELECT r.id+'-'+r.nombre_comercial retailer,"
					+ "p.marca_cat2_fk+p.grupo_cat4_fk+p.plataforma_cat5_fk categoria,"
					+ " p.descripcion categoria_producto," + " vp.sell_out_ant," + "vp.porcentaje_base,"
					+ " vp.sell_out_base," + " vp.porcentaje_modificado," + " vp.sell_out," + " NULL var_categ,"
					+ " vp.id var_prod," + " p.UPC," + " p.modelo"
					+ " FROM VARIACION v WITH (NOLOCK),VARIACION_PRODUCTO vp WITH (NOLOCK),"
					+ " MES_SOP ms WITH (NOLOCK),PRODUCTO p WITH (NOLOCK)," + " RETAILER r WITH (NOLOCK) "
					+ " WHERE  vp.variacion_fk = v.id and  ms.anio = " + anio + " and ms.mes = " + mes
					+ " and v.mes_fk = ms.id and  vp.producto_fk = p.id AND v.retailer_fk IN " + retailer
					+ " and v.marca_fk IN " + marca
					+ " and r.id = v.retailer_fk and p.marca_cat2_fk+p.grupo_cat4_fk+p.plataforma_cat5_fk = '"
					+ categoria + "' \n" + "ORDER BY r.id,p.modelo ASC";
		} else {
			qlString = "SELECT r.id+'-'+r.nombre_comercial retailer," + " vc.categoria categoria,"
					+ "'0_'+ic.categoria categoria_producto," + " vc.sell_out_ant," + " vc.porcentaje_base,"
					+ " vc.sell_out_base," + " vc.porcentaje_modificado," + " vc.sell_out," + " vc.id var_categ,"
					+ " NULL var_prod"
					+ " FROM VARIACION v WITH (NOLOCK),VARIACION_CATEGORIA vc WITH (NOLOCK), MES_SOP ms WITH (NOLOCK),I_VIEW_CATEGORIA ic WITH (NOLOCK) ,RETAILER r WITH (NOLOCK)"
					+ " WHERE vc.variacion_fk = v.id and ms.anio = " + anio + " and ms.mes = " + mes
					+ " and v.mes_fk = ms.id and ic.id = vc.categoria and v.retailer_fk IN " + retailer
					+ " and v.marca_fk IN " + marca + "  and r.id = v.retailer_fk ORDER BY r.id,vc.categoria";
		}

		List<Object[]> lista = em.createNativeQuery(qlString).getResultList();

		for (Object[] resultado : lista) {
			VariacionSellOutVO variacionVO = new VariacionSellOutVO();
			try {
				variacionVO.setRetailer((String) resultado[0]);
				variacionVO.setCategoria((String) resultado[1]);
				variacionVO.setCategoriaProducto((String) resultado[2]);
				variacionVO.setSellOutAnt((BigDecimal) resultado[3]);
				variacionVO.setPorcentajeBase((BigDecimal) resultado[4]);
				variacionVO.setSellOutBase((BigDecimal) resultado[5]);
				variacionVO.setPorcentajeModificado((BigDecimal) resultado[6]);
				variacionVO.setSellOut((BigDecimal) resultado[7]);
				variacionVO.setVarCateg((BigDecimal) resultado[8]);
				variacionVO.setVarProd((BigDecimal) resultado[9]);

				if (isProducto) {
					variacionVO.setUpc((String) resultado[10]);
					variacionVO.setModelo((String) resultado[11]);
				}

			} catch (Exception e) {
				LOG.error("[consultaResultadoVariacion] Error realizando la consulta del resultado variacion", e);
			}

			listaVariacion.add(variacionVO);
		}
		return listaVariacion;

	}

	/**
	 * Almacena en base de datos las modificaciones realizadas por el usuario
	 *
	 * @param lCategoria
	 * @param lProductos
	 * @return
	 * @throws java.lang.Exception
	 */
	public String guardarModificacionVariacion(List<VariacionSellOutVO> lCategoria,
			Map<String, List<VariacionSellOutVO>> lProductos) throws Exception {

		for (VariacionSellOutVO elemento : lCategoria) {
			VariacionCategoria variacionCategoria;
			variacionCategoria = em.find(VariacionCategoria.class, elemento.getVarCateg().longValue());
			variacionCategoria.setPorcentajeModificado(
					elemento.getPorcentajeModificado() != null ? elemento.getPorcentajeModificado() : BigDecimal.ZERO); // ERROR
																														// CON
																														// 50
																														// USUARIOS
																														// NULL
																														// POINTER
			variacionCategoria.setSellOut(elemento.getSellOut() != null ? elemento.getSellOut() : BigDecimal.ZERO);
			em.merge(variacionCategoria);
		}

		for (Map.Entry<String, List<VariacionSellOutVO>> entry : lProductos.entrySet()) {
			for (VariacionSellOutVO prod : entry.getValue()) {
				VariacionProducto variacionProducto;
				variacionProducto = em.find(VariacionProducto.class, prod.getVarProd().longValue());
				variacionProducto.setPorcentajeModificado(
						prod.getPorcentajeModificado() != null ? prod.getPorcentajeModificado() : BigDecimal.ZERO);
				variacionProducto.setSellOut(prod.getSellOut() != null ? prod.getSellOut() : BigDecimal.ZERO);
				em.merge(variacionProducto);
			}
		}

		return "exitoso";
	}

	public void guardarModificacionVariacion(VariacionSellOutVO variacionVO, List<VariacionSellOutVO> prodCat) {

		VariacionCategoria variacionCategoria;
		variacionCategoria = em.find(VariacionCategoria.class, variacionVO.getVarCateg().longValue());
		variacionCategoria.setPorcentajeModificado(
				variacionVO.getPorcentajeModificado() != null ? variacionVO.getPorcentajeModificado()
						: BigDecimal.ZERO);
		variacionCategoria.setSellOut(variacionVO.getSellOut() != null ? variacionVO.getSellOut() : BigDecimal.ZERO);
		em.merge(variacionCategoria);

		for (VariacionSellOutVO prod : prodCat) {
			VariacionProducto variacionProducto;
			variacionProducto = em.find(VariacionProducto.class, prod.getVarProd().longValue());
			variacionProducto.setPorcentajeModificado(
					prod.getPorcentajeModificado() != null ? prod.getPorcentajeModificado() : BigDecimal.ZERO);
			variacionProducto.setSellOut(prod.getSellOut() != null ? prod.getSellOut() : BigDecimal.ZERO);
			em.merge(variacionProducto);
		}
	}

	/**
	 * 
	 * @param marcasGrupo
	 * @param mesFind
	 * @param retailer
	 * @param listMarcas
	 * @param compania
	 * @param listIdGrupo
	 * @param prodCategoria
	 * @return
	 */
	// TODO
	public synchronized List<VariacionSellOutVO> procesarVariacion(String marcasGrupo, Long mesFind,
			List<String> retailer, String compania, List<String> listIdGrupo,
			Map<String, List<VariacionSellOutVO>> prodCategoria) {
		List<VariacionSellOutVO> productos = new ArrayList<>();
		try {
			productos = concatenarRecalcularVariacion(marcasGrupo, mesFind, retailer, compania, listIdGrupo);

			for (VariacionSellOutVO oCategoria : productos) {
				prodCategoria.put(oCategoria.getCategoria(),
						this.consultarVariacion(mesFind, retailer, marcasGrupo, true, oCategoria.getCategoria()));
			}

		} catch (Exception e) {
			LOG.error("[procesarVariacion] ERROR SINCORONIZANDO METODO PROCESAR VARIACION:: ", e);
		}
		return productos;
	}

}
