/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.utilitarios;

import com.jvlat.sop.commonsop.mail.EmailUtil;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.entidadessop.entidades.Usuario;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author iviasus
 */
public class FuncionesUtilitariasDemand {

	static Logger log = LogManager.getLogger(FuncionesUtilitariasDemand.class.getName());

	/**
	 * Obtiene el dia o el mes o el año de la fecha
	 *
	 * @param fecha
	 * @param objeto DD, MM, YYYY
	 * @return
	 */
	public static int obtenerDiaMesAnio(Date fecha, String objeto) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		int resp = 0;

		if (objeto.equals("DD")) {
			resp = calendar.get(Calendar.DAY_OF_MONTH);
		} else if (objeto.equals("MM")) {
			resp = calendar.get(Calendar.MONTH) + 1;
		} else if (objeto.equals("YYYY")) {
			resp = calendar.get(Calendar.YEAR);
		}

		return resp;

	}

	/**
	 * Confirma el despacho en el sistema
	 *
	 * @param listUsuarioNotificar
	 * @param datosServidor
	 * @return
	 * @throws javax.mail.MessagingException
	 */
	public String notificarTerminoFase(List<Usuario> listUsuarioNotificar, List<String> datosServidor, String subject,
			String body, String body2) throws Exception {

		String resultado = "";
		try {
			// recorre los usuarios a notificar en

			// Envia correos
			String correos = "";
			String[] to = null;

			for (Usuario usu : listUsuarioNotificar) {
				correos = correos + usu.getEmail() + ";";
			}
			if (correos.contains(";")) {
				to = correos.split(";");
			} else {
				to = new String[] { correos };
			}
			log.info("Se enviara correo a las direcciones " + Arrays.toString(to));
			log.info("host " + datosServidor.get(0));
			log.info("port " + datosServidor.get(1));
			log.info("user " + datosServidor.get(2));
			log.info("pass " + datosServidor.get(3));
			log.info("from " + datosServidor.get(4));

			EmailUtil emailUtil = new EmailUtil(datosServidor.get(0), datosServidor.get(1), datosServidor.get(2),
					datosServidor.get(3), datosServidor.get(4));
//            resultado = emailUtil.sendEmail(to, "Semanas Requeridas ", "Proceso Semanas Requeridas Finalizado con fecha: " + new Date(), "");
			resultado = emailUtil.sendEmail(to, subject, body + new Date() + ". " + body2, null);
		} catch (Exception e) {
			System.out.println("Error en envio de correo " + body + " " + e);
			throw new Exception("Error en envio de correo " + body, e);
		}
		return resultado;
	}

	public String notificarTerminoFase(List<Usuario> listUsuarioNotificar, List<String> datosServidor, String subject,
			String body, String body2, Compania compania, String marca, List<String> listRetailer, MesSop mesSop)
			throws Exception {

		String resultado = "";
		try {
			// recorre los usuarios a notificar en

			StringBuilder listCorreos = new StringBuilder();

			String[] to = null;

			for (Usuario usu : listUsuarioNotificar) {
				listCorreos.append(usu.getEmail() + ";");
			}

			String correos = listCorreos.toString();
			if (correos.contains(";")) {
				to = correos.split(";");
			} else {
				to = new String[] { correos };
			}

			EmailUtil emailUtil = new EmailUtil(datosServidor.get(0), datosServidor.get(1), datosServidor.get(2),
					datosServidor.get(3), datosServidor.get(4));
			if (listRetailer != null && !listRetailer.isEmpty()) {

				String retailer = String.join("/", listRetailer);
				resultado = emailUtil.sendEmail(to,
						subject + compania.getNombre() + " - " + marca + " - " + retailer + " - " + mesSop.getAnio()
								+ " " + mesSop.getNombre(),
						body + new Date() + ". <br /><br /> " + body2 + "<br /> Gracias, <br /><br /> Saludos ", null);
			} else {
				if (compania != null) {

					resultado = emailUtil.sendEmail(to,
							subject + compania.getNombre() + " - " + marca + " - " + mesSop.getAnio() + " "
									+ mesSop.getNombre(),
							body + new Date() + ". <br /><br /> " + body2 + "<br /> Gracias, <br /><br /> Saludos ",
							null);
				} else {

					resultado = emailUtil.sendEmail(to,
							subject + marca + " - " + mesSop.getAnio() + " " + mesSop.getNombre(),
							body + new Date() + ". <br /><br /> " + body2 + "<br /> Gracias, <br /><br /> Saludos ",
							null);
				}
			}

		} catch (Exception e) {
			throw new Exception("Error en envio de correo " + body, e);
		}
		return resultado;
	}

	// =rdenar map
	public static Map<String, Retailer> sortByComparator(Map<String, Retailer> unsortMap, final boolean order) {

		List<Entry<String, Retailer>> list = new LinkedList<Entry<String, Retailer>>(unsortMap.entrySet());

		// Sorting the list based on values
		Collections.sort(list, new Comparator<Entry<String, Retailer>>() {
			public int compare(Entry<String, Retailer> o1, Entry<String, Retailer> o2) {
				if (order) {
					return o1.getValue().getNombreComercial().compareTo(o2.getValue().getNombreComercial());
				} else {
					return o2.getValue().getNombreComercial().compareTo(o1.getValue().getNombreComercial());

				}
			}
		});

		// Maintaining insertion order with the help of LinkedList
		Map<String, Retailer> sortedMap = new LinkedHashMap<String, Retailer>();
		for (Entry<String, Retailer> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	/**
	 * 
	 * @param listUsuarioNotificar
	 * @param datosServidor
	 * @param subject
	 * @param body
	 * @param body2
	 */
	public void enviarCorreo(List<Usuario> listUsuarioNotificar, List<String> datosServidor, String subject,
			String body) {

		try {
			List<String> correos = new ArrayList<>();
			String[] to = null;

			for (Usuario usu : listUsuarioNotificar) {
				correos.add(usu.getEmail());
			}

			if (!correos.isEmpty()) {
				to = correos.toArray(new String[correos.size()]);
				EmailUtil emailUtil = new EmailUtil(datosServidor.get(0), datosServidor.get(1), datosServidor.get(2),
						datosServidor.get(3), datosServidor.get(4));
				emailUtil.sendEmail(to, subject, body, null);
			}

		} catch (Exception e) {
			log.error("[enviarCorreo] Error en envio de correo ", e);
		}
	}

}
