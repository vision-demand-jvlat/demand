/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Seidor
 */
public class FilasArchivoForecastVO {
    
    private String age;    
    private String mes;   
    private String retailer;    
    private String marca;   
    private String categoria;  
    private String codigo;  
    private String sku;  
    private String upc;   
    private String descripcion;
    private String forecastKam;
    private Map<Long, BigDecimal> valoresMeses = new LinkedHashMap<>();
    private String linea;
    private BigDecimal precioRetailer;

    public void agregarMeses(Long idMes, BigDecimal valor){
        valoresMeses.put(idMes, valor);
    }
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getRetailer() {
        return retailer;
    }

    public void setRetailer(String retailer) {
        this.retailer = retailer;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getForecastKam() {
        return forecastKam;
    }

    public void setForecastKam(String forecastKam) {
        this.forecastKam = forecastKam;
    }

    public Map<Long, BigDecimal> getValoresMeses() {
        return valoresMeses;
    }

    public void setValoresMeses(Map<Long, BigDecimal> valoresMeses) {
        this.valoresMeses = valoresMeses;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    @Override
    public String toString() {
        return "FilasArchivoForecastVO{" + "age=" + age + ", mes=" + mes + ", retailer=" + retailer + ", marca=" + marca + ", categoria=" + categoria + ", codigo=" + codigo + ", sku=" + sku + ", upc=" + upc + ", descripcion=" + descripcion + ", forecastKam=" + forecastKam + ", valoresMeses=" + valoresMeses + ", linea=" + linea + '}';
    }

    public BigDecimal getPrecioRetailer() {
        return precioRetailer;
    }

    public void setPrecioRetailer(BigDecimal precioRetailer) {
        this.precioRetailer = precioRetailer;
    }    
}
