/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ogomez
 */
@SuppressWarnings("serial")
public class ForecastVO implements Serializable {

	private Long id;
	private Long idMes;
	private String compania;
	private String marca;
	private String retailer;
	private String descripcionRetailer;
	private String usuario;
	private Date fechaCreacion;
	private String anhoMes;
	private String estado;
	private String traduccion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdMes() {
		return idMes;
	}

	public void setIdMes(Long idMes) {
		this.idMes = idMes;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania != null ? compania.trim() : null;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca != null ? marca.trim() : null;
	}

	public String getRetailer() {
		return retailer;
	}

	public void setRetailer(String retailer) {
		this.retailer = retailer != null ? retailer.trim() : null;
	}

	public String getDescripcionRetailer() {
		return descripcionRetailer;
	}

	public void setDescripcionRetailer(String descripcionRetailer) {
		this.descripcionRetailer = descripcionRetailer != null ? descripcionRetailer.trim() : null;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario != null ? usuario.trim() : null;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getAnhoMes() {
		return anhoMes;
	}

	public void setAnhoMes(String anhoMes) {
		this.anhoMes = anhoMes != null ? anhoMes.trim() : null;

	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado != null ? estado.trim() : null;
	}

	public String getTraduccion() {
		return traduccion;
	}

	public void setTraduccion(String traduccion) {
		this.traduccion = traduccion != null ? traduccion.trim() : null;
	}

	@Override
	public String toString() {
		return "ForecastVO [id=" + id + ", idMes=" + idMes + ", compania=" + compania + ", marca=" + marca
				+ ", retailer=" + retailer + ", descripcionRetailer=" + descripcionRetailer + ", usuario=" + usuario
				+ ", fechaCreacion=" + fechaCreacion + ", anhoMes=" + anhoMes + ", estado=" + estado + ", traduccion="
				+ traduccion + "]";
	}
}
