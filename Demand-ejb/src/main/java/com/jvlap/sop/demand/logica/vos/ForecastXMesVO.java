/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author iviasus
 */
public class ForecastXMesVO implements Serializable {

	private Long idForecast;
	private Long idMes;
	private String compania;
	private String marca;
	private String retailer;
	private String descripcionRetailer;
	private String estado;
	private boolean fueProcesadoXCompania;
	private List<ProductosForecastMesVO> productosForecast;
	private List<MarcaGrupoForecast> marcaGrupoForecast;
	private List<RetailerGrupoForecast> retailerGrupoForecast;

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getRetailer() {
		return retailer;
	}

	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}

	public Long getIdForecast() {
		return idForecast;
	}

	public void setIdForecast(Long idForecast) {
		this.idForecast = idForecast;
	}

	public List<ProductosForecastMesVO> getProductosForecast() {
		return productosForecast;
	}

	public void setProductosForecast(List<ProductosForecastMesVO> productosForecast) {
		this.productosForecast = productosForecast;
	}

	public Long getIdMes() {
		return idMes;
	}

	public void setIdMes(Long idMes) {
		this.idMes = idMes;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public boolean isFueProcesadoXCompania() {
		return fueProcesadoXCompania;
	}

	public void setFueProcesadoXCompania(boolean fueProcesadoXCompania) {
		this.fueProcesadoXCompania = fueProcesadoXCompania;
	}

	public List<MarcaGrupoForecast> getMarcaGrupoForecast() {
		return marcaGrupoForecast;
	}

	public void setMarcaGrupoForecast(List<MarcaGrupoForecast> marcaGrupoForecast) {
		this.marcaGrupoForecast = marcaGrupoForecast;
	}

	public Object getVarCateg() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public List<RetailerGrupoForecast> getRetailerGrupoForecast() {
		return retailerGrupoForecast;
	}

	public void setRetailerGrupoForecast(List<RetailerGrupoForecast> retailerGrupoForecast) {
		this.retailerGrupoForecast = retailerGrupoForecast;
	}

	public String getDescripcionRetailer() {
		return descripcionRetailer != null ? descripcionRetailer.replace(",", " ") : null;
	}

	public void setDescripcionRetailer(String descripcionRetailer) {
		this.descripcionRetailer = descripcionRetailer != null ? descripcionRetailer.replace(",", " ") : null;
	}

}
