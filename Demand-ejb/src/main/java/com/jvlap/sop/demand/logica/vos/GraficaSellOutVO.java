/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.math.BigDecimal;

/**
 *
 * @author Roberto Osorio
 */
public class GraficaSellOutVO {
    private String mes;
    private BigDecimal sellout1;
    private BigDecimal sellout2;
    private BigDecimal sellout3;

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public BigDecimal getSellout1() {
        return sellout1;
    }

    public void setSellout1(BigDecimal sellout1) {
        this.sellout1 = sellout1;
    }

    public BigDecimal getSellout2() {
        return sellout2;
    }

    public void setSellout2(BigDecimal sellout2) {
        this.sellout2 = sellout2;
    }

    public BigDecimal getSellout3() {
        return sellout3;
    }

    public void setSellout3(BigDecimal sellout3) {
        this.sellout3 = sellout3;
    }
    
}
