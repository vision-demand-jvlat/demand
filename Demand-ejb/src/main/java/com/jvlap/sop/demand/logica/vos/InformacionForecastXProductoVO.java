/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.math.BigDecimal;

/**
 *
 * @author iviasus
 */
public class InformacionForecastXProductoVO {

	private Long sellInPropuesto;
	private Long forecast;
	private Long forecastGerente;
	private Long forecastGerenteAnterior;
	private Long idForecastProducto;
	private Long idMes;
	private BigDecimal totalForecastGerenteAnterior;

	private BigDecimal totalPropuesto;
	private BigDecimal totalForecastKam;
	private BigDecimal totalForecastGerente;
	private BigDecimal margenGerente;
	private BigDecimal margen;
	private BigDecimal precioRetailer;
	private BigDecimal cogs;
	private BigDecimal srpSinIva;
	private BigDecimal inventarioWH;
	private BigDecimal transito;

	// Campos Unidades de Supply
	private Long unidadesDespachadas;
	private Long unidadesFaltantes;
	private BigDecimal totalUnidaDespachadas;
	private BigDecimal margenUnidaDespachadas;
	private BigDecimal precioSinTasa;
	private String upc;
	private String modelo;
	private Boolean editarGerente;
	private Boolean editarKam;
	private String nombreGrupo;
	private String nombrePlataforma;
	private String retailerId;

	public Long getSellInPropuesto() {
		return sellInPropuesto;
	}

	public void setSellInPropuesto(Long sellInPropuesto) {
		this.sellInPropuesto = sellInPropuesto;
	}

	public Long getForecast() {
		return forecast;
	}

	public void setForecast(Long forecast) {
		this.forecast = forecast;
	}

	public BigDecimal getTotalPropuesto() {
		return totalPropuesto;
	}

	public void setTotalPropuesto(BigDecimal totalPropuesto) {
		this.totalPropuesto = totalPropuesto;
	}

	public BigDecimal getTotalForecastKam() {
		return totalForecastKam;
	}

	public void setTotalForecastKam(BigDecimal totalForecastKam) {
		this.totalForecastKam = totalForecastKam;
	}

	public BigDecimal getMargen() {
		return margen;
	}

	public void setMargen(BigDecimal margen) {
		this.margen = margen;
	}

	public BigDecimal getPrecioRetailer() {
		return precioRetailer;
	}

	public void setPrecioRetailer(BigDecimal precioRetailer) {
		this.precioRetailer = precioRetailer;
	}

	public BigDecimal getCogs() {
		return cogs;
	}

	public void setCogs(BigDecimal cogs) {
		this.cogs = cogs;
	}

	public Long getIdForecastProducto() {
		return idForecastProducto;
	}

	public void setIdForecastProducto(Long idForecastProducto) {
		this.idForecastProducto = idForecastProducto;
	}

	public Long getIdMes() {
		return idMes;
	}

	public void setIdMes(Long idMes) {
		this.idMes = idMes;
	}

	public BigDecimal getSrpSinIva() {
		return srpSinIva;
	}

	public void setSrpSinIva(BigDecimal srpSinIva) {
		this.srpSinIva = srpSinIva;
	}

	public BigDecimal getInventarioWH() {
		return inventarioWH;
	}

	public void setInventarioWH(BigDecimal inventarioWH) {
		this.inventarioWH = inventarioWH;
	}

	public Long getForecastGerente() {
		return forecastGerente;
	}

	public void setForecastGerente(Long forecastGerente) {
		this.forecastGerente = forecastGerente;
	}

	public BigDecimal getTotalForecastGerente() {
		return totalForecastGerente;
	}

	public void setTotalForecastGerente(BigDecimal totalForecastGerente) {
		this.totalForecastGerente = totalForecastGerente;
	}

	public BigDecimal getMargenGerente() {
		return margenGerente;
	}

	public void setMargenGerente(BigDecimal margenGerente) {
		this.margenGerente = margenGerente;
	}

	public Long getForecastGerenteAnterior() {
		return forecastGerenteAnterior;
	}

	public void setForecastGerenteAnterior(Long forecastGerenteAnterior) {
		this.forecastGerenteAnterior = forecastGerenteAnterior;
	}

	public BigDecimal getTotalForecastGerenteAnterior() {
		return totalForecastGerenteAnterior;
	}

	public void setTotalForecastGerenteAnterior(BigDecimal totalForecastGerenteAnterior) {
		this.totalForecastGerenteAnterior = totalForecastGerenteAnterior;
	}

	public Long getUnidadesDespachadas() {
		return unidadesDespachadas;
	}

	public void setUnidadesDespachadas(Long unidadesDespachadas) {
		this.unidadesDespachadas = unidadesDespachadas;
	}

	public Long getUnidadesFaltantes() {
		return unidadesFaltantes;
	}

	public void setUnidadesFaltantes(Long unidadesFaltantes) {
		this.unidadesFaltantes = unidadesFaltantes;
	}

	public BigDecimal getTotalUnidaDespachadas() {
		return totalUnidaDespachadas;
	}

	public void setTotalUnidaDespachadas(BigDecimal totalUnidaDespachadas) {
		this.totalUnidaDespachadas = totalUnidaDespachadas;
	}

	public BigDecimal getMargenUnidaDespachadas() {
		return margenUnidaDespachadas;
	}

	public void setMargenUnidaDespachadas(BigDecimal margenUnidaDespachadas) {
		this.margenUnidaDespachadas = margenUnidaDespachadas;
	}

	public void calcularTotalForecast() {
		System.out.println("Precio: " + precioRetailer + " forecast " + forecast);
		totalForecastKam = precioRetailer.multiply(BigDecimal.valueOf(forecast));
	}

	public void calcularMargen() {
		margen = totalForecastKam.subtract(cogs.multiply(BigDecimal.valueOf(forecast)));
	}

	public void calcularTotalForecastGeneral() {
		System.out.println("Gerente Precio: " + precioRetailer + " forecast " + forecast);
		totalForecastGerente = precioRetailer.multiply(BigDecimal.valueOf(forecastGerente));
	}

	public void calcularMargenGeneral() {
		margenGerente = totalForecastGerente.subtract(cogs.multiply(BigDecimal.valueOf(forecastGerente)));
	}

	public BigDecimal getPrecioSinTasa() {
		return precioSinTasa;
	}

	public void setPrecioSinTasa(BigDecimal precioSinTasa) {
		this.precioSinTasa = precioSinTasa;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public BigDecimal getTransito() {
		return transito;
	}

	public void setTransito(BigDecimal transito) {
		this.transito = transito;
	}

	public Boolean getEditarGerente() {
		return editarGerente;
	}

	public void setEditarGerente(Boolean editarGerente) {
		this.editarGerente = editarGerente;
	}

	public Boolean getEditarKam() {
		return editarKam;
	}

	public void setEditarKam(Boolean editarKam) {
		this.editarKam = editarKam;
	}

	public String getNombreGrupo() {
		return nombreGrupo;
	}

	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}

	public String getNombrePlataforma() {
		return nombrePlataforma;
	}

	public void setNombrePlataforma(String nombrePlataforma) {
		this.nombrePlataforma = nombrePlataforma;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	@Override
	public String toString() {
		return "InformacionForecastXProductoVO [sellInPropuesto=" + sellInPropuesto + ", forecast=" + forecast
				+ ", forecastGerente=" + forecastGerente + ", forecastGerenteAnterior=" + forecastGerenteAnterior
				+ ", idForecastProducto=" + idForecastProducto + ", idMes=" + idMes + ", totalForecastGerenteAnterior="
				+ totalForecastGerenteAnterior + ", totalPropuesto=" + totalPropuesto + ", totalForecastKam="
				+ totalForecastKam + ", totalForecastGerente=" + totalForecastGerente + ", margenGerente="
				+ margenGerente + ", margen=" + margen + ", precioRetailer=" + precioRetailer + ", cogs=" + cogs
				+ ", srpSinIva=" + srpSinIva + ", inventarioWH=" + inventarioWH + ", transito=" + transito
				+ ", unidadesDespachadas=" + unidadesDespachadas + ", unidadesFaltantes=" + unidadesFaltantes
				+ ", totalUnidaDespachadas=" + totalUnidaDespachadas + ", margenUnidaDespachadas="
				+ margenUnidaDespachadas + ", precioSinTasa=" + precioSinTasa + ", upc=" + upc + ", modelo=" + modelo
				+ ", editarGerente=" + editarGerente + ", editarKam=" + editarKam + ", nombreGrupo=" + nombreGrupo
				+ ", nombrePlataforma=" + nombrePlataforma + ", retailerId=" + retailerId + "]";
	}

}
