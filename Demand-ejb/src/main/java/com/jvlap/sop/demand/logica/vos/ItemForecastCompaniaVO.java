/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.math.BigDecimal;

/**
 *
 * @author hp pc
 */
public class ItemForecastCompaniaVO {
    
    private String idProducto;
    private BigDecimal precioRetailer;
    private Long idMes;
    private BigDecimal cogs;
    private Long uni_pend;
    private Long uni_faltantes;
    
    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public BigDecimal getPrecioRetailer() {
        return precioRetailer;
    }

    public void setPrecioRetailer(BigDecimal precioRetailer) {
        this.precioRetailer = precioRetailer;
    }

    public Long getIdMes() {
        return idMes;
    }

    public void setIdMes(Long idMes) {
        this.idMes = idMes;
    }

    public BigDecimal getCogs() {
        return cogs;
    }

    public void setCogs(BigDecimal cogs) {
        this.cogs = cogs;
    }

    public Long getUni_pend() {
        return uni_pend;
    }

    public void setUni_pend(Long uni_pend) {
        this.uni_pend = uni_pend;
    }

    public Long getUni_faltantes() {
        return uni_faltantes;
    }

    public void setUni_faltantes(Long uni_faltantes) {
        this.uni_faltantes = uni_faltantes;
    }
    
    
}
