/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author iviasus
 */
public class ItemForecastGeneralVO implements Serializable {
    
     private String idCategoria;
     private String categoria;
     private Long forecastActual;
     private Long forecastAnterior;
     private Long mesActual;
     private Long mesAnterior;
     private BigDecimal forecastActualUSD;
     private BigDecimal forecastAnteriorUSD;
     private BigDecimal precioRetailerActual;
     private BigDecimal precioRetailerAnterior;
     private BigDecimal cogsActual;
     private BigDecimal cogsAnterior;
     private BigDecimal margen;
     
     /* Campo para agrupar productos por categoria */
    private String indiceTabla;
    
    public Long getIdMes() {
        return idMes;
    }

    public void setIdMes(Long idMes) {
        this.idMes = idMes;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEsReal() {
        return esReal;
    }

    /* N-meses Futuros y Reales */
    public void setEsReal(boolean esReal) {
        this.esReal = esReal;
    }

    private Long idMes;
    private String nombre;
    private boolean esReal;

    public ItemForecastGeneralVO() {
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Long getForecastActual() {
        return forecastActual;
    }

    public void setForecastActual(Long forecastActual) {
        this.forecastActual = forecastActual;
    }

    public Long getForecastAnterior() {
        return forecastAnterior;
    }

    public void setForecastAnterior(Long forecastAnterior) {
        this.forecastAnterior = forecastAnterior;
    }

    public BigDecimal getForecastActualUSD() {
        return forecastActualUSD;
    }

    public void setForecastActualUSD(BigDecimal forecastActualUSD) {
        this.forecastActualUSD = forecastActualUSD;
    }

    public BigDecimal getForecastAnteriorUSD() {
        return forecastAnteriorUSD;
    }

    public void setForecastAnteriorUSD(BigDecimal forecastAnteriorUSD) {
        this.forecastAnteriorUSD = forecastAnteriorUSD;
    }

    public BigDecimal getPrecioRetailerActual() {
        return precioRetailerActual;
    }

    public void setPrecioRetailerActual(BigDecimal precioRetailerActual) {
        this.precioRetailerActual = precioRetailerActual;
    }

    public BigDecimal getPrecioRetailerAnterior() {
        return precioRetailerAnterior;
    }

    public void setPrecioRetailerAnterior(BigDecimal precioRetailerAnterior) {
        this.precioRetailerAnterior = precioRetailerAnterior;
    }

    public BigDecimal getCogsActual() {
        return cogsActual;
    }

    public void setCogsActual(BigDecimal cogsActual) {
        this.cogsActual = cogsActual;
    }

    public BigDecimal getCogsAnterior() {
        return cogsAnterior;
    }

    public void setCogsAnterior(BigDecimal cogsAnterior) {
        this.cogsAnterior = cogsAnterior;
    }

    public BigDecimal getMargen() {
        return margen;
    }

    public void setMargen(BigDecimal margen) {
        this.margen = margen;
    }

    public Long getMesActual() {
        return mesActual;
    }

    public void setMesActual(Long mesActual) {
        this.mesActual = mesActual;
    }

    public Long getMesAnterior() {
        return mesAnterior;
    }

    public void setMesAnterior(Long mesAnterior) {
        this.mesAnterior = mesAnterior;
    }

    public String getIndiceTabla() {
        return indiceTabla;
    }

    public void setIndiceTabla(String indiceTabla) {
        this.indiceTabla = indiceTabla;
    }
     
     
     
}
