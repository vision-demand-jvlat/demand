/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author iviasus
 */
public class ItemForecastRetailerVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idProducto;
	private String idCategoria;

	private String idMarca;
	private String idCompania;
	private String idRetailer;
	private String descripcionRetailer;
	private String categoriaProducto;
	private String categoriaNombre;
	private Long sellInPropuesto;
	private Long forecast;
	private BigDecimal precioRetailer;
	private BigDecimal totalPropuesto;
	private BigDecimal totalForecast;
	private BigDecimal totalForecastKam;
	private BigDecimal totalForecastGerente;
	private BigDecimal margen;
	private BigDecimal margenGerente;
	private boolean esProducto = false;
	private int posicionCategoria;
	private BigDecimal cogs;
	private BigDecimal idProductoPorRetailerPorMes;
	private BigDecimal inventarioWH;
	private BigDecimal transito;

	private BigDecimal precioRetailerAnterior;
	private Long mesAnterior;
	private BigDecimal cogsAnterior;
	private Long forecastGerenteAnterior;
	private BigDecimal totalForecastGerenteAnterior;

	/* Campos para Forecast Compañia */
	private Long idForecast;
	private Long idForecastProducto;
	private Long forecastKam;
	private Boolean editarKam;
	private Long forecastGerente;

	/* Campo para agrupar productos por categoria */
	private String indiceTabla;

	/* Control para ejecutar cálculo en metodos get */
	private boolean calculoPantalla = false;

	private String estadoForecast;

	/* N-meses Futuros y Reales */
	private Long idMes;
	private String nombre;
	private boolean esReal;
	/* ediciones pantalla */
	private Long unidadesDespachadas;
	private Long unidadesFaltantes;

	private BigDecimal precioSinTasa;
	private boolean esTotalMarca;
	private String marcaCategoria;

	private String upc;
	private String modelo;

	private String estadoProducto;
	private Boolean editarGerente;

	private String nombreGrupo;
	private String nombrePlataforma;

	public String getCategoriaNombre() {
		return categoriaNombre;
	}

	public Long getUnidadesDespachadas() {
		return unidadesDespachadas;
	}

	public void setUnidadesDespachadas(Long unidadesDespachadas) {
		this.unidadesDespachadas = unidadesDespachadas;
	}

	public Long getUnidadesFaltantes() {
		return unidadesFaltantes;
	}

	public void setUnidadesFaltantes(Long unidadesFaltantes) {
		this.unidadesFaltantes = unidadesFaltantes;
	}

	public ItemForecastRetailerVO() {

	}

	public String getCategoriaProducto() {
		return categoriaProducto;
	}

	public void setCategoriaProducto(String categoriaProducto) {
		this.categoriaProducto = categoriaProducto;
	}

	public Long getSellInPropuesto() {
		return sellInPropuesto;
	}

	public void setSellInPropuesto(Long sellInPropuesto) {
		this.sellInPropuesto = sellInPropuesto;
	}

	public Long getForecast() {
		return forecast;
	}

	public void setForecast(Long forecast) {
		this.forecast = forecast;
	}

	public BigDecimal getPrecioRetailerAnterior() {
		return precioRetailerAnterior;
	}

	public void setPrecioRetailerAnterior(BigDecimal precioRetailerAnterior) {
		this.precioRetailerAnterior = precioRetailerAnterior;
	}

	public BigDecimal getCogsAnterior() {
		return cogsAnterior;
	}

	public void setCogsAnterior(BigDecimal cogsAnterior) {
		this.cogsAnterior = cogsAnterior;
	}

	public Long getMesAnterior() {
		return mesAnterior;
	}

	public void setMesAnterior(Long mesAnterior) {
		this.mesAnterior = mesAnterior;
	}

	public Long getForecastGerenteAnterior() {
		return forecastGerenteAnterior;
	}

	public void setForecastGerenteAnterior(Long forecastGerenteAnterior) {
		this.forecastGerenteAnterior = forecastGerenteAnterior;
	}

	public BigDecimal getTotalForecastGerenteAnterior() {
		return totalForecastGerenteAnterior;
	}

	public void setTotalForecastGerenteAnterior(BigDecimal totalForecastGerenteAnterior) {
		this.totalForecastGerenteAnterior = totalForecastGerenteAnterior;
	}

	public BigDecimal getTotalPropuesto() {
		if (esProducto && calculoPantalla) {
			if (this.sellInPropuesto != null && this.precioRetailer != null) {
				BigDecimal sellInPropuestoBig = BigDecimal.valueOf(this.sellInPropuesto);
				this.totalPropuesto = this.precioRetailer.multiply(sellInPropuestoBig);
			} else {
				this.totalPropuesto = BigDecimal.ZERO;
			}
		}

		return totalPropuesto;
	}

	public void setTotalPropuesto(BigDecimal totalPropuesto) {
		this.totalPropuesto = totalPropuesto;
	}

	public BigDecimal getTotalForecast() {

		if (esProducto && calculoPantalla) {
			if (this.forecast != null && this.precioRetailer != null) {
				BigDecimal forecastBig = BigDecimal.valueOf(this.forecast);
				this.totalForecast = this.precioRetailer.multiply(forecastBig);
			} else {
				this.totalForecast = BigDecimal.ZERO;
			}
		}

		return totalForecast;
	}

	public void setTotalForecast(BigDecimal totalForecast) {
		this.totalForecast = totalForecast;
	}

	public BigDecimal getMargen() {

		if (esProducto && calculoPantalla) {
			if (this.totalForecast != null && this.cogs != null && this.forecast != null) {
				this.margen = this.totalForecast.subtract(this.cogs.multiply(BigDecimal.valueOf(this.forecast)));
			} else {
				this.margen = BigDecimal.ZERO;
			}
		}

		return this.margen;
	}

	public BigDecimal getTotalForecastKam() {
		if (esProducto && calculoPantalla) {
			if (this.forecastKam != null && this.precioRetailer != null) {
				BigDecimal forecastBig = BigDecimal.valueOf(this.forecastKam);
				this.totalForecastKam = this.precioRetailer.multiply(forecastBig);
			} else {
				this.totalForecastKam = BigDecimal.ZERO;
			}
		}
		return totalForecastKam;
	}

	public void setTotalForecastKam(BigDecimal totalForecastKam) {
		this.totalForecastKam = totalForecastKam;
	}

	public BigDecimal getTotalForecastGerente() {
		if (esProducto && calculoPantalla) {
			if (this.forecastGerente != null && this.precioRetailer != null) {
				BigDecimal forecastBig = BigDecimal.valueOf(this.forecastGerente);
				this.totalForecastGerente = this.precioRetailer.multiply(forecastBig);
			} else {
				this.totalForecastGerente = BigDecimal.ZERO;
			}
		}

		return totalForecastGerente;
	}

	public void setTotalForecastGerente(BigDecimal totalForecastGerente) {
		this.totalForecastGerente = totalForecastGerente;
	}

	public BigDecimal getMargenGerente() {
		if (esProducto && calculoPantalla) {
			if (this.totalForecastGerente != null && this.cogs != null && this.forecastGerente != null) {
				this.margenGerente = this.totalForecastGerente
						.subtract(this.cogs.multiply(BigDecimal.valueOf(this.forecastGerente)));
			} else {
				this.margenGerente = BigDecimal.ZERO;
			}
		}

		return margenGerente;
	}

	public void setMargenGerente(BigDecimal margenGerente) {
		this.margenGerente = margenGerente;
	}

	public void setMargen(BigDecimal margen) {
		this.margen = margen;
	}

	public boolean isEsProducto() {
		return esProducto;
	}

	public void setEsProducto(boolean esProducto) {
		this.esProducto = esProducto;
	}

	public String getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(String idCategoria) {
		this.idCategoria = idCategoria;
	}

	public BigDecimal getPrecioRetailer() {
		return precioRetailer;
	}

	public void setPrecioRetailer(BigDecimal precioRetailer) {
		this.precioRetailer = precioRetailer;
	}

	public int getPosicionCategoria() {
		return posicionCategoria;
	}

	public void setPosicionCategoria(int posicionCategoria) {
		this.posicionCategoria = posicionCategoria;
	}

	public BigDecimal getCogs() {
		return cogs;
	}

	public void setCogs(BigDecimal cogs) {
		this.cogs = cogs;
	}

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public Long getIdForecastProducto() {
		return idForecastProducto;
	}

	public void setIdForecastProducto(Long idForecastProducto) {
		this.idForecastProducto = idForecastProducto;
	}

	public Boolean getEditarKam() {
		return editarKam;
	}

	public void setEditarKam(Boolean editarKam) {
		this.editarKam = editarKam;
	}

	public Long getForecastKam() {
		return forecastKam;
	}

	public void setForecastKam(Long forecastKam) {
		this.forecastKam = forecastKam;
	}

	public Long getForecastGerente() {
		return forecastGerente;
	}

	public void setForecastGerente(Long forecastGerente) {
		this.forecastGerente = forecastGerente;
	}

	public Long getIdForecast() {
		return idForecast;
	}

	public void setIdForecast(Long idForecast) {
		this.idForecast = idForecast;
	}

	public String getIndiceTabla() {
		return indiceTabla;
	}

	public void setIndiceTabla(String indiceTabla) {
		this.indiceTabla = indiceTabla;
	}

	public boolean isCalculoPantalla() {
		return calculoPantalla;
	}

	public void setCalculoPantalla(boolean calculoPantalla) {
		this.calculoPantalla = calculoPantalla;
	}

	public BigDecimal getIdProductoPorRetailerPorMes() {
		return idProductoPorRetailerPorMes;
	}

	public void setIdProductoPorRetailerPorMes(BigDecimal idProductoPorRetailerPorMes) {
		this.idProductoPorRetailerPorMes = idProductoPorRetailerPorMes;
	}

	public String getEstadoForecast() {
		return estadoForecast;
	}

	public void setEstadoForecast(String estadoForecast) {
		this.estadoForecast = estadoForecast;
	}

	public Long getIdMes() {
		return idMes;
	}

	public void setIdMes(Long idMes) {
		this.idMes = idMes;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEsReal() {
		return esReal;
	}

	public void setEsReal(boolean esReal) {
		this.esReal = esReal;
	}

	public String getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(String idMarca) {
		this.idMarca = idMarca;
	}

	public String getIdCompania() {
		return idCompania;
	}

	public void setIdCompania(String idCompania) {
		this.idCompania = idCompania;
	}

	public String getIdRetailer() {
		return idRetailer;
	}

	public void setIdRetailer(String idRetailer) {
		this.idRetailer = idRetailer;
	}

	public BigDecimal getPrecioSinTasa() {
		return precioSinTasa;
	}

	public void setPrecioSinTasa(BigDecimal precioSinTasa) {
		this.precioSinTasa = precioSinTasa;
	}

	public boolean isEsTotalMarca() {
		return esTotalMarca;
	}

	public void setEsTotalMarca(boolean esTotalMarca) {
		this.esTotalMarca = esTotalMarca;
	}

	public String getMarcaCategoria() {
		return marcaCategoria;
	}

	public void setMarcaCategoria(String marcaCategoria) {
		this.marcaCategoria = marcaCategoria;
	}

	public BigDecimal getInventarioWH() {
		return inventarioWH;
	}

	public void setInventarioWH(BigDecimal inventarioWH) {
		this.inventarioWH = inventarioWH;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public BigDecimal getTransito() {
		return transito;
	}

	public void setTransito(BigDecimal transito) {
		this.transito = transito;
	}

	public String getEstadoProducto() {
		return estadoProducto;
	}

	public void setEstadoProducto(String estadoProducto) {
		this.estadoProducto = estadoProducto;
	}

	public Boolean getEditarGerente() {
		return editarGerente;
	}

	public void setEditarGerente(Boolean editarGerente) {
		this.editarGerente = editarGerente;
	}

	public String getNombreGrupo() {
		return nombreGrupo;
	}

	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}

	public String getNombrePlataforma() {
		return nombrePlataforma;
	}

	public void setNombrePlataforma(String nombrePlataforma) {
		this.nombrePlataforma = nombrePlataforma;
	}

	public String getDescripcionRetailer() {
		return descripcionRetailer;
	}

	public void setDescripcionRetailer(String descripcionRetailer) {
		this.descripcionRetailer = descripcionRetailer;
	}

	public void setCategoriaNombre(String categoriaNombre) {
		this.categoriaNombre = categoriaNombre;
	}

}
