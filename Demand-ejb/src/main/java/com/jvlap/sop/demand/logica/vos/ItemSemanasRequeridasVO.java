/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Seidor
 */
public class ItemSemanasRequeridasVO implements Serializable {

    private BigDecimal varCateg;
    private BigDecimal varProd;
    private BigDecimal inventarioInicial;
    private BigDecimal sellOut;
    private BigDecimal semanasReales;
    private BigDecimal semanasRequeridas;
    private BigDecimal sellIn;
    private int flagEdit;
    private boolean esActual;
    private int mes;
    private String upc;
    private String modelo;

    public BigDecimal getVarCateg() {
        return varCateg;
    }

    public void setVarCateg(BigDecimal varCateg) {
        this.varCateg = varCateg;
    }

    public BigDecimal getVarProd() {
        return varProd;
    }

    public void setVarProd(BigDecimal varProd) {
        this.varProd = varProd;
    }

    public BigDecimal getInventarioInicial() {
        return inventarioInicial;
    }

    public void setInventarioInicial(BigDecimal inventarioInicial) {
        this.inventarioInicial = inventarioInicial;
    }

    public BigDecimal getSellOut() {
        return sellOut;
    }

    public void setSellOut(BigDecimal sellOut) {
        this.sellOut = sellOut;
    }

    public BigDecimal getSemanasReales() {
        return semanasReales;
    }

    public void setSemanasReales(BigDecimal semanasReales) {
        this.semanasReales = semanasReales;
    }

    public BigDecimal getSemanasRequeridas() {
        return semanasRequeridas;
    }

    public void setSemanasRequeridas(BigDecimal semanasRequeridas) {
        this.semanasRequeridas = semanasRequeridas;
    }

    public BigDecimal getSellIn() {
        return sellIn;
    }

    public void setSellIn(BigDecimal sellIn) {
        this.sellIn = sellIn;
    }

    public int getFlagEdit() {
        return flagEdit;
    }

    public void setFlagEdit(int flagEdit) {
        this.flagEdit = flagEdit;
    }

    public boolean isEsActual() {
        return esActual;
    }

    public void setEsActual(boolean esActual) {
        this.esActual = esActual;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}
