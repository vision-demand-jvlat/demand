/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.util.Objects;

/**
 *
 * @author hp pc
 */
public class MarcaGrupoForecast {
    
    private String idMarca;
    private Long idForecast;

    public String getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(String idMarca) {
        this.idMarca = idMarca;
    }

    public Long getIdForecast() {
        return idForecast;
    }

    public void setIdForecast(Long idForecast) {
        this.idForecast = idForecast;
    }    
    
    @Override
    public boolean equals(Object obj) { 
         if(obj == null) return false;
         else if (!(obj instanceof MarcaGrupoForecast)) return false;
         else if (this.idMarca == null) return false;
         return this.idMarca.equals(((MarcaGrupoForecast)obj).getIdMarca());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.idMarca);
        return hash;
    }
}
