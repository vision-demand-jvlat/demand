/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.math.BigDecimal;

/**
 *
 * @author Roberto Osorio
 */
public class ParametrosDTO {

    private BigDecimal id;
    private String categoria;
    private String nombreCategoria;
    private BigDecimal sell_out;
    private String compania_fk;
    private String retailer_fk;
    private String mesAnio;

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getCompania_fk() {
        return compania_fk;
    }

    public void setCompania_fk(String compania_fk) {
        this.compania_fk = compania_fk;
    }

    public String getRetailer_fk() {
        return retailer_fk;
    }

    public void setRetailer_fk(String retailer_fk) {
        this.retailer_fk = retailer_fk;
    }

    public String getMesAnio() {
        return mesAnio;
    }

    public void setMesAnio(String mesAnio) {
        this.mesAnio = mesAnio;
    }

    public BigDecimal getSell_out() {
        return sell_out;
    }

    public void setSell_out(BigDecimal sell_out) {
        this.sell_out = sell_out;
    }

}
