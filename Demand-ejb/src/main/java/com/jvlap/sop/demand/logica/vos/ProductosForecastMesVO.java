/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author iviasus
 */
public class ProductosForecastMesVO {

	private String idProducto;
	private String nombreProducto;
	private String indiceTabla;
	private boolean esProdcuto;
	private boolean esTotalMarca;
	private int posicionCategoria;
	private String idMarca;
	private String idMarcaCategoria;
	private String idCategoria;
	private String upc;
	private String modelo;
	private String estadoProducto;
	private Long editarKam;
	private String idRetailer;
	private Map<String, InformacionForecastXProductoVO> informacionForecastMes;
	private String budgetCategory;

	public ProductosForecastMesVO() {
		informacionForecastMes = new LinkedHashMap<>();
	}

	public void agregarInfoMes(String mes, InformacionForecastXProductoVO infoMes) {
		informacionForecastMes.put(mes, infoMes);
	}

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public String getIndiceTabla() {
		return indiceTabla;
	}

	public void setIndiceTabla(String indiceTabla) {
		this.indiceTabla = indiceTabla;
	}

	public boolean isEsProdcuto() {
		return esProdcuto;
	}

	public void setEsProdcuto(boolean esProdcuto) {
		this.esProdcuto = esProdcuto;
	}

	public int getPosicionCategoria() {
		return posicionCategoria;
	}

	public void setPosicionCategoria(int posicionCategoria) {
		this.posicionCategoria = posicionCategoria;
	}

	public Map<String, InformacionForecastXProductoVO> getInformacionForecastMes() {
		return informacionForecastMes;
	}

	public void setInformacionForecastMes(Map<String, InformacionForecastXProductoVO> informacionForecastMes) {
		this.informacionForecastMes = informacionForecastMes;
	}

	public String getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(String idMarca) {
		this.idMarca = idMarca;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		else if (!(obj instanceof ProductosForecastMesVO))
			return false;
		else if (this.idProducto == null)
			return false;
		return this.idProducto.equals(((ProductosForecastMesVO) obj).getIdProducto());
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + Objects.hashCode(this.idProducto);
		return hash;
	}

	public boolean isEsTotalMarca() {
		return esTotalMarca;
	}

	public void setEsTotalMarca(boolean esTotalMarca) {
		this.esTotalMarca = esTotalMarca;
	}

	public String getIdMarcaCategoria() {
		return idMarcaCategoria;
	}

	public void setIdMarcaCategoria(String idMarcaCategoria) {
		this.idMarcaCategoria = idMarcaCategoria;
	}

	public String getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(String idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getEstadoProducto() {
		return estadoProducto;
	}

	public void setEstadoProducto(String estadoProducto) {
		this.estadoProducto = estadoProducto;
	}

	public Long getEditarKam() {
		return editarKam;
	}

	public void setEditarKam(Long editarKam) {
		this.editarKam = editarKam;
	}

	public String getIdRetailer() {
		return idRetailer;
	}

	public void setIdRetailer(String idRetailer) {
		this.idRetailer = idRetailer;
	}

	public String getBudgetCategory() {
		return budgetCategory;
	}

	public void setBudgetCategory(String budgetCategory) {
		this.budgetCategory = budgetCategory;
	}

}
