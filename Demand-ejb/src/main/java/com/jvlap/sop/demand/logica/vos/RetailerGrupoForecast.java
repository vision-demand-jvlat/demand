/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.util.Objects;

/**
 *
 * @author User
 */
public class RetailerGrupoForecast {
    
    private String idRetailer;
    private Long idForecast;

    public String getIdRetailer() {
        return idRetailer;
    }

    public void setIdRetailer(String idRetailer) {
        this.idRetailer = idRetailer;
    }

    public Long getIdForecast() {
        return idForecast;
    }

    public void setIdForecast(Long idForecast) {
        this.idForecast = idForecast;
    }

    @Override
    public boolean equals(Object obj) { 
         if(obj == null) return false;
         else if (!(obj instanceof RetailerGrupoForecast)) return false;
         else if (this.idRetailer == null) return false;
         return this.idRetailer.equals(((RetailerGrupoForecast)obj).getIdRetailer());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.idRetailer);
        return hash;
    }
}
