package com.jvlap.sop.demand.logica.vos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ScheduleVO {
	private Long id;
	private String code;
	private String marcas;
	private String companias;
	private String retailers;
	private Long mes;
	private String fechaEjecucion;
	private String estado;
	private String usuario;
	private String fechaCreacion;
	private String proceso;
	private int orden;

	/********************************************
	 * 
	 **********************************************/

	public ScheduleVO(Long id, String code, String marcas, String companias, String retailers, Long mes,
			String fechaEjecucion, String estado, String usuario, String proceso, int orden) {
		super();
		this.id = id;
		this.code = code.length() > 19 ? code.substring(0, 19) : code;
		this.marcas = marcas;
		this.companias = companias;
		this.retailers = retailers;
		this.mes = mes;
		this.fechaEjecucion = fechaEjecucion;
		this.estado = estado;
		this.usuario = usuario;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		this.fechaCreacion = df.format(Calendar.getInstance().getTime());
		this.proceso = proceso;
		this.orden = orden;
	}

	public ScheduleVO() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		this.fechaCreacion = df.format(Calendar.getInstance().getTime());
	}

	/**********************
	 * 
	 **********************************/
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMarcas() {
		return marcas;
	}

	public void setMarcas(String marcas) {
		this.marcas = marcas;
	}

	public String getCompanias() {
		return companias;
	}

	public void setCompanias(String companias) {
		this.companias = companias;
	}

	public String getRetailers() {
		return retailers;
	}

	public void setRetailers(String retailers) {
		this.retailers = retailers;
	}

	public Long getMes() {
		return mes;
	}

	public void setMes(Long mes) {
		this.mes = mes;
	}

	public String getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(String fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code.length() > 19 ? code.substring(0, 19) : code;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

}
