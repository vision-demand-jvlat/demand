/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author Seidor
 */
public class SemanasRequeridasVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String retailer;
	private String mesNombre;
	private String categoria;
	private String categoriaProducto;
	private String indiceTabla;
	private String producto;
	private boolean esProducto;
	private String marca;
	private String upc;
	private String modelo;
	private Map<String, ItemSemanasRequeridasVO> informacionSemanasMes;

	public SemanasRequeridasVO(String retailer, String mesNombre, String categoria, String categoriaProducto,
			String indiceTabla, String producto, boolean esProducto, String marca, String upc, String modelo) {
		super();
		this.retailer = retailer;
		this.mesNombre = mesNombre;
		this.categoria = categoria;
		this.categoriaProducto = categoriaProducto;
		this.indiceTabla = indiceTabla;
		this.producto = producto;
		this.esProducto = esProducto;
		this.marca = marca;
		this.upc = upc;
		this.modelo = modelo;
		informacionSemanasMes = new LinkedHashMap<>();
	}

	public SemanasRequeridasVO() {
		informacionSemanasMes = new LinkedHashMap<>();
	}

	public void agregarInfoMes(String mes, ItemSemanasRequeridasVO infoMes) {
		informacionSemanasMes.put(mes, infoMes);
	}

	public String getMesNombre() {
		return mesNombre;
	}

	public void setMesNombre(String mesNombre) {
		this.mesNombre = mesNombre;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getCategoriaProducto() {
		return categoriaProducto;
	}

	public void setCategoriaProducto(String categoriaProducto) {
		this.categoriaProducto = categoriaProducto;
	}

	public String getIndiceTabla() {
		return indiceTabla;
	}

	public void setIndiceTabla(String indiceTabla) {
		this.indiceTabla = indiceTabla;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public Map<String, ItemSemanasRequeridasVO> getInformacionSemanasMes() {
		return informacionSemanasMes;
	}

	public void setInformacionSemanasMes(Map<String, ItemSemanasRequeridasVO> informacionSemanasMes) {
		this.informacionSemanasMes = informacionSemanasMes;
	}

	public boolean isEsProducto() {
		return esProducto;
	}

	public void setEsProducto(boolean esProducto) {
		this.esProducto = esProducto;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getRetailer() {
		return retailer;
	}

	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		else if (!(obj instanceof SemanasRequeridasVO))
			return false;
		else if (this.producto == null)
			return false;
		return this.producto.equals(((SemanasRequeridasVO) obj).getProducto());
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + Objects.hashCode(this.producto);
		return hash;
	}

	@Override
	public String toString() {
		return "SemanasRequeridasVO [retailer=" + retailer + ", mesNombre=" + mesNombre + ", categoria=" + categoria
				+ ", categoriaProducto=" + categoriaProducto + ", indiceTabla=" + indiceTabla + ", producto=" + producto
				+ ", esProducto=" + esProducto + ", marca=" + marca + ", upc=" + upc + ", modelo=" + modelo
				+ ", informacionSemanasMes=" + informacionSemanasMes + "]";
	}

}
