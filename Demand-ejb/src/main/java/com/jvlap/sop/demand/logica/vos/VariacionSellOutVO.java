/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.logica.vos;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Seidor
 */
public class VariacionSellOutVO implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	private String retailer;
	private String categoria;
	private String categoriaProducto;
	private BigDecimal sellOutAnt;
	private BigDecimal porcentajeBase;
	private BigDecimal sellOutBase;
	private BigDecimal porcentajeModificado;
	private BigDecimal sellOut;
	private BigDecimal varCateg;
	private BigDecimal varProd;
	private String indiceTabla;
	private String upc;
	private String modelo;

	public VariacionSellOutVO(String retailer, String categoria, String categoriaProducto, BigDecimal sellOutAnt,
			BigDecimal porcentajeBase, BigDecimal sellOutBase, BigDecimal porcentajeModificado, BigDecimal sellOut,
			BigDecimal varCateg, BigDecimal varProd, String indiceTabla, String upc, String modelo) {
		super();
		this.retailer = retailer;
		this.categoria = categoria;
		this.categoriaProducto = categoriaProducto;
		this.sellOutAnt = sellOutAnt;
		this.porcentajeBase = porcentajeBase;
		this.sellOutBase = sellOutBase;
		this.porcentajeModificado = porcentajeModificado;
		this.sellOut = sellOut;
		this.varCateg = varCateg;
		this.varProd = varProd;
		this.indiceTabla = indiceTabla;
		this.upc = upc;
		this.modelo = modelo;
	}

	public VariacionSellOutVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getCategoriaProducto() {
		return categoriaProducto;
	}

	public void setCategoriaProducto(String categoriaProducto) {
		this.categoriaProducto = categoriaProducto;
	}

	public BigDecimal getSellOutAnt() {
		return sellOutAnt;
	}

	public void setSellOutAnt(BigDecimal sellOutAnt) {
		this.sellOutAnt = sellOutAnt;
	}

	public BigDecimal getPorcentajeBase() {
		return porcentajeBase;
	}

	public void setPorcentajeBase(BigDecimal porcentajeBase) {
		this.porcentajeBase = porcentajeBase;
	}

	public BigDecimal getSellOutBase() {
		return sellOutBase;
	}

	public void setSellOutBase(BigDecimal sellOutBase) {
		this.sellOutBase = sellOutBase;
	}

	public BigDecimal getPorcentajeModificado() {
		return porcentajeModificado;
	}

	public void setPorcentajeModificado(BigDecimal porcentajeModificado) {
		this.porcentajeModificado = porcentajeModificado;
	}

	public BigDecimal getSellOut() {
		return sellOut;
	}

	public void setSellOut(BigDecimal sellOut) {
		this.sellOut = sellOut;
	}

	public BigDecimal getVarCateg() {
		return varCateg;
	}

	public void setVarCateg(BigDecimal varCateg) {
		this.varCateg = varCateg;
	}

	public BigDecimal getVarProd() {
		return varProd;
	}

	public void setVarProd(BigDecimal varProd) {
		this.varProd = varProd;
	}

	public String getIndiceTabla() {
		return indiceTabla;
	}

	public void setIndiceTabla(String indiceTabla) {
		this.indiceTabla = indiceTabla;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getRetailer() {
		return retailer;
	}

	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}

}
