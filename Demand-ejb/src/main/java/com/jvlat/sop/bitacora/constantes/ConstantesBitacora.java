/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlat.sop.bitacora.constantes;

/**
 *
 * @author Seidor
 */
public class ConstantesBitacora {

	private ConstantesBitacora() {
		throw new IllegalStateException("Utility class");
	}

	public static final String DEMAND_VARIACION = "VARIACION";
	public static final String DEMAND_SEMANAS = "SEMANAS_REQ";
	public static final String DEMAND_FORECAST_RETAILER = "FORECAST_RETAILER";

	public static final String SUPPLY_PLAN_DEP = "PLAN_DESPACHO";
	public static final String SUPPLY_PSI_COMPANIA = "PSI_COMPANIA";

	public static final String FORECAST_COMPANIA = "FORECAST_COMPANIA";
	public static final String FORECAST_GENERAL = "FORECAST_GENERAL";

}
