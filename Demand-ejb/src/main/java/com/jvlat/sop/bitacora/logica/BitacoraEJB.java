/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlat.sop.bitacora.logica;

import com.jvlap.sop.demand.logica.AbstractFacade;
import com.jvlat.sop.entidadessop.entidades.Bitacora;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author Seidor
 */
@Stateless
public class BitacoraEJB extends AbstractFacade{
    
    @PersistenceContext(unitName = "EntidadesSOPPU")
    private EntityManager em;        
    
    static Logger log = LogManager.getLogger(BitacoraEJB.class.getName());

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    /**
     * Consulta la lista de registros de bitacora por modulo y fecha
     * @param modulo
     * @param mes
     * @return 
     */
    public List<Bitacora> consultarBitacora(String modulo, MesSop mes){
        List<Bitacora> lista = new ArrayList<>();
        Query query;
        try{
            query = em.createQuery("SELECT b FROM Bitacora b WHERE b.modulo = :modulo and b.mesFk = :mes");
            query.setParameter("modulo", modulo);
            query.setParameter("mes", mes);
            return query.getResultList();
        }catch(Exception e){
            log.error("Error en consultar Bitacora", e);
        }
        
        return lista;
    }
    
    /**
     * Guarda registro de bitacora
     * @param bitacora
     * @return 
     */
    public String guardarRegistroBitacora(Bitacora bitacora){
        
        try{
            bitacora.setFechaCrea(new Date());
            em.merge(bitacora);
        }catch(Exception e){
            log.error("Error en guardar Bitacora", e);
        }
        
        return "";
    }
    
}
