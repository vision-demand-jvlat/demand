/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlat.sop.login.constantes;

/**
 *
 * @author Roberto Osorio
 */
public final class ConstantesLogin {

  public final static String LDAP_INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
    
    ////////////////////////////////////
    public final static String LDAP_PROVIDER_URL = "ldap://192.168.30.5:389";  //1
    public final static String LDAP_SECURITY_AUTHENTICATION = "simple"; //1
    public final static String LDAP_SECURITY_PRINCIPAL = "CN=Administrador,CN=Users,DC=dominio,DC=seidor";  //1
    public final static String LDAP_SECURITY_CREDENTIALS = "S31dorC0L";  //1
    ////////////////////////////
//    public final static String LDAP_DOMINIO = "@dominio.seidor";
    public final static String LDAP_SEARCH_FILTER = "(&(objectClass=user))";
    public final static String LDAP_SEARCH_BASE = "CN=Users,DC=dominio,DC=seidor";  //1
//    public final static String LDAP_SECURITY_PROTOCOL= "ssl";
//    public final static String LDAP_SECURITY_PROTOCOL, "simple";
    public final static String LOGIN_USUARIO = "loginUsuario";

    private ConstantesLogin() {
    }

}
