/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlat.sop.login.logica;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlat.sop.entidadessop.entidades.Funcionalidad;
import com.jvlat.sop.entidadessop.entidades.FuncionalidadXRol;
import com.jvlat.sop.entidadessop.entidades.LdapCredenciales;
import com.jvlat.sop.entidadessop.entidades.Rol;
import com.jvlat.sop.entidadessop.entidades.Usuario;
import com.jvlat.sop.entidadessop.entidades.UsuariosXRol;
import com.jvlat.sop.login.constantes.ConstantesLogin;
import com.jvlat.sop.login.vos.LdapVO;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author Roberto Osorio
 */
@SuppressWarnings("rawtypes")
@Stateless
public class LoginEJB extends AbstractFacade {

	static Logger log = LogManager.getLogger(LoginEJB.class.getName());
	private Usuario usuarioLogueado;
	private LdapCredenciales ldapCredenciales;

	@PersistenceContext(unitName = "EntidadesSOPPU")
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public LoginEJB() {

	}

	@SuppressWarnings("unchecked")
	public LoginEJB(Class entityClass) {
		super(entityClass);
	}

	@PostConstruct
	public void init() {
		usuarioLogueado = new Usuario();
		ldapCredenciales = (LdapCredenciales) em.createNamedQuery("LdapCredenciales.findById")
				.setParameter("id", new Long(1)).getSingleResult();
	}

	/**
	 * 
	 * @param user
	 * @param pass
	 * @param usuarioVO
	 * @return
	 */
	public List<Funcionalidad> validar(String user, String pass, UsuarioVO usuarioVO) {
		Usuario usuario;
		boolean result = true;
		List<FuncionalidadXRol> funcionalidadXRolListTM;
		List<Funcionalidad> funcionalidadesUsuarioMenu = new ArrayList<>();
		try {
			usuario = (Usuario) em.createNamedQuery("Usuario.findByUserName").setParameter("userName", user)
					.getSingleResult();
			LdapVO ldapVO = new LdapVO();
			if (ldapVO.validaUsuarioLdap(user, pass, ldapCredenciales)) {
				usuarioVO.setId(new Long(usuario.getId()));
				usuarioVO.setNombre(usuario.getNombre());
				usuarioVO.setUserName(usuario.getUserName());

				// verificar
				usuarioVO.setLoginUsuario(ConstantesLogin.LOGIN_USUARIO);

				usuarioLogueado.setId(usuario.getId());
				usuarioLogueado.setNombre(usuario.getNombre());
				usuarioLogueado.setUserName(usuario.getUserName());

				List<UsuariosXRol> usuariosXRolListTM = buscarRolXUsuario(usuario);
				if (usuariosXRolListTM != null) {
					for (UsuariosXRol usuariosXRol : usuariosXRolListTM) {
						funcionalidadXRolListTM = buscarFuncionalidadesXRol(usuariosXRol.getIdRol());
						if (funcionalidadXRolListTM != null) {
							for (FuncionalidadXRol funcioXRol : funcionalidadXRolListTM) {
								if (!funcionalidadesUsuarioMenu.contains(funcioXRol.getIdFuncionalidad())) {
									funcionalidadesUsuarioMenu.add(funcioXRol.getIdFuncionalidad());
								}
							}
						} else {
							result = false;
						}
					}
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		} catch (NoResultException e) {
			result = false;
		}
		if (result) {
			return funcionalidadesUsuarioMenu;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @param usuarioVO
	 * @return
	 */
	public Usuario verificaExistenciaUsuario(UsuarioVO usuarioVO) {
		try {
			return (Usuario) em.createNamedQuery("Usuario.findByUserName")
					.setParameter("userName", usuarioVO.getUserName()).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Rol> buscaListaRoles() {
		try {
			return em.createNamedQuery("Rol.findAll").getResultList();
		} catch (NoResultException e) {
			log.info("[buscaListaRoles] No se encontró información:{}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param rol
	 * @return
	 */
	public Rol buscaRolPorNombre(Rol rol) {
		try {
			return (Rol) em.createNamedQuery("Rol.findByNombre").setParameter("nombre", rol.getNombre())
					.getSingleResult();
		} catch (NoResultException e) {
			log.info("[buscaRolPorNombre] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Usuario> buscaListaUsuarios() {
		try {
			return em.createNamedQuery("Usuario.findAll").getResultList();
		} catch (NoResultException e) {
			log.info("[buscaListaUsuarios] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param usuario
	 * @return
	 */
	public Usuario buscaUsuarioPorNombre(Usuario usuario) {
		try {
			return (Usuario) em.createNamedQuery("Usuario.findByNombre").setParameter("nombre", usuario.getNombre())
					.getSingleResult();
		} catch (NoResultException e) {
			log.info("[buscaUsuarioPorNombre] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param usuariosXRol
	 * @return
	 * @throws ExcepcionSOP
	 */
	public UsuariosXRol buscaUsuarioRol(UsuariosXRol usuariosXRol) throws ExcepcionSOP {
		try {
			Query query = em
					.createQuery("SELECT r FROM UsuariosXRol r WHERE r.idRol= :idRol AND r.idUsuario= :idUsuario");
			query.setParameter("idRol", usuariosXRol.getIdRol());
			query.setParameter("idUsuario", usuariosXRol.getIdUsuario());
			UsuariosXRol usuariosXRolTM = (UsuariosXRol) query.getSingleResult();
			if (usuariosXRolTM == null) {
				throw new ExcepcionSOP("Error No se encontró información:", "", 1);
			} else {
				return usuariosXRolTM;
			}

		} catch (NoResultException e) {
			log.info("[buscaUsuarioRol] No se encontró información: {}", e.getMessage());
			throw new ExcepcionSOP("Error No se encontró información:", "", 1);
		}
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<UsuariosXRol> buscaListaUsuariosXRol() {
		try {
			return em.createNamedQuery("UsuariosXRol.findAll").getResultList();
		} catch (NoResultException e) {
			log.info("[UsuariosXRol] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param usuario
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<UsuariosXRol> buscarRolXUsuario(Usuario usuario) {
		try {
			Query query = em.createQuery("SELECT u FROM UsuariosXRol u WHERE u.idUsuario.id= :idUsuario");
			query.setParameter("idUsuario", usuario.getId());
			return query.getResultList();
		} catch (NoResultException e) {
			log.info("[buscarRolXUsuario] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Funcionalidad> buscarListaFuncionalidades() {
		try {
			return em.createNamedQuery("Funcionalidad.findAll").getResultList();
		} catch (NoResultException e) {
			log.info("[buscarListaFuncionalidades] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param nombreFuncionalidad
	 * @return
	 */
	public Funcionalidad buscarFuncionalidadPorNombre(String nombreFuncionalidad) {
		try {
			return (Funcionalidad) em.createNamedQuery("Funcionalidad.findByNombre")
					.setParameter("nombre", nombreFuncionalidad).getSingleResult();
		} catch (NoResultException e) {
			log.info("[buscarFuncionalidadPorNombre] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param funcionalidad
	 * @param rol
	 * @return
	 * @throws ExcepcionSOP
	 */
	public FuncionalidadXRol buscaFuncionalidadRol(Funcionalidad funcionalidad, Rol rol) throws ExcepcionSOP {
		try {
			Query query = em.createQuery(
					"SELECT f FROM FuncionalidadXRol f WHERE f.idFuncionalidad= :idFuncionalidad AND f.idRol= :idRol");
			query.setParameter("idFuncionalidad", funcionalidad);
			query.setParameter("idRol", rol);

			FuncionalidadXRol funcionalidadXRolTM = (FuncionalidadXRol) query.getSingleResult();
			if (funcionalidadXRolTM == null) {
				throw new ExcepcionSOP("Error No se encontró información:", "", 1);
			} else {
				return funcionalidadXRolTM;
			}
		} catch (NoResultException e) {
			log.info("[buscaFuncionalidadRol] No se encontró información: {}", e.getMessage());
			throw new ExcepcionSOP("Error No se encontró información:", "", 1);
		}
	}

	/**
	 * 
	 * @param rol
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<FuncionalidadXRol> buscarFuncionalidadesXRol(Rol rol) {
		try {
			Query query = em.createQuery(
					"SELECT f FROM FuncionalidadXRol f WHERE f.idRol.nombre= :idRol ORDER BY f.idFuncionalidad.orden");
			query.setParameter("idRol", rol.getNombre());
			return query.getResultList();
		} catch (NoResultException e) {
			log.info("[buscarFuncionalidadesXRol] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	public Usuario getUsuarioLogueado() {
		return usuarioLogueado;
	}

	public void setUsuarioLogueado(Usuario usuarioLogueado) {
		this.usuarioLogueado = usuarioLogueado;
	}
}
