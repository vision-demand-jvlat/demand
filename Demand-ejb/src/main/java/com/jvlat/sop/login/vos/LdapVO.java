/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlat.sop.login.vos;

import com.jvlat.sop.entidadessop.entidades.LdapCredenciales;
import static com.jvlat.sop.login.constantes.ConstantesLogin.LDAP_INITIAL_CONTEXT_FACTORY;
import static com.jvlat.sop.login.constantes.ConstantesLogin.LDAP_SEARCH_BASE;
import static com.jvlat.sop.login.constantes.ConstantesLogin.LDAP_SEARCH_FILTER;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 *
 * @author
 */
public class LdapVO {

	public boolean validaUsuarioLdap(String user, String passw, LdapCredenciales ldapCredenciales) {

		Hashtable<String, String> env = new Hashtable(11);
		env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_INITIAL_CONTEXT_FACTORY);
		env.put(Context.PROVIDER_URL, ldapCredenciales.getLdapProviderUrl());
		env.put(Context.SECURITY_AUTHENTICATION, ldapCredenciales.getLdapSecurityAuthentication());
//       env.put(Context.SECURITY_AUTHENTICATION, "DIGEST-MD5"); // No other SALS  
		env.put(Context.SECURITY_PRINCIPAL, user + ldapCredenciales.getLdapDomain());
		env.put(Context.SECURITY_CREDENTIALS, passw);

		return findUsuarioLdap(env);
	}

	public List<UsuarioVO> obtenerListaUsuariosLdap(LdapCredenciales ldapCredenciales) {

		Hashtable<String, String> env = new Hashtable();
//         env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_INITIAL_CONTEXT_FACTORY);
		env.put(Context.PROVIDER_URL, ldapCredenciales.getLdapProviderUrl());
		env.put(Context.SECURITY_AUTHENTICATION, ldapCredenciales.getLdapSecurityAuthentication());
		env.put(Context.SECURITY_PRINCIPAL, ldapCredenciales.getLdapSecurityPrincipal());
		env.put(Context.SECURITY_CREDENTIALS, ldapCredenciales.getLdapSecurityCredentials());
//        env.put(Context.SECURITY_AUTHENTICATION, LDAP_SECURITY_AUTHENTICATION);
//        env.put(Context.SECURITY_PRINCIPAL, LDAP_SECURITY_PRINCIPAL);
//        env.put(Context.SECURITY_CREDENTIALS, LDAP_SECURITY_CREDENTIALS);
//        env.put(Context.PROVIDER_URL, LDAP_PROVIDER_URL);
//        ensures that objectSID attribute values
//        will be returned as a byte[] instead of a String
//        env.put("java.naming.ldap.attributes.binary", "objectSID");
		return findAllLdapUser(env);

	}

	private SearchResult findAccountByAccountName(DirContext ctx, String ldapSearchBase, String accountName)
			throws NamingException {

		String searchFilter = "(&(objectClass=user)(sAMAccountName=" + accountName + "))";

		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		NamingEnumeration<SearchResult> results = ctx.search(ldapSearchBase, searchFilter, searchControls);

		SearchResult searchResult = null;
		if (results.hasMoreElements()) {
			searchResult = (SearchResult) results.nextElement();

			// make sure there is not another item available, there should be only 1 match
			if (results.hasMoreElements()) {
				System.err.println("Matched multiple users for the accountName: " + accountName);
				return null;
			}
		}

		return searchResult;
	}

	private String findGroupBySID(DirContext ctx, String ldapSearchBase, String sid) throws NamingException {

		String searchFilter = "(&(objectClass=group)(objectSid=" + sid + "))";

		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		NamingEnumeration<SearchResult> results = ctx.search(ldapSearchBase, searchFilter, searchControls);

		if (results.hasMoreElements()) {
			SearchResult searchResult = (SearchResult) results.nextElement();

			// make sure there is not another item available, there should be only 1 match
			if (results.hasMoreElements()) {
				System.err.println("Matched multiple groups for the group with SID: " + sid);
				return null;
			} else {
				return (String) searchResult.getAttributes().get("sAMAccountName").get();
			}
		}
		return null;
	}

	private String getPrimaryGroupSID(SearchResult srLdapUser) throws NamingException {
		byte[] objectSID = (byte[]) srLdapUser.getAttributes().get("objectSid").get();
		String strPrimaryGroupID = (String) srLdapUser.getAttributes().get("primaryGroupID").get();

		String strObjectSid = decodeSID(objectSID);

		return strObjectSid.substring(0, strObjectSid.lastIndexOf('-') + 1) + strPrimaryGroupID;
	}

	/**
	 * The binary data is in the form: byte[0] - revision level byte[1] - count of
	 * sub-authorities byte[2-7] - 48 bit authority (big-endian) and then count x 32
	 * bit sub authorities (little-endian)
	 *
	 * The String value is: S-Revision-Authority-SubAuthority[n]...
	 *
	 * Based on code from here -
	 * http://forums.oracle.com/forums/thread.jspa?threadID=1155740&tstart=0
	 */
	private static String decodeSID(byte[] sid) {

		final StringBuilder strSid = new StringBuilder("S-");

		// get version
		final int revision = sid[0];
		strSid.append(Integer.toString(revision));

		// next byte is the count of sub-authorities
		final int countSubAuths = sid[1] & 0xFF;

		// get the authority
		long authority = 0;
		// String rid = "";
		for (int i = 2; i <= 7; i++) {
			authority |= ((long) sid[i]) << (8 * (5 - (i - 2)));
		}
		strSid.append("-");
		strSid.append(Long.toHexString(authority));

		// iterate all the sub-auths
		int offset = 8;
		int size = 4; // 4 bytes for each sub auth
		for (int j = 0; j < countSubAuths; j++) {
			long subAuthority = 0;
			for (int k = 0; k < size; k++) {
				subAuthority |= (long) (sid[offset + k] & 0xFF) << (8 * k);
			}
			strSid.append("-");
			strSid.append(subAuthority);
			offset += size;
		}
		return strSid.toString();
	}

	private List<UsuarioVO> findAllLdapUser(Hashtable<String, String> ldapEnv) {
		try {
			List<UsuarioVO> usuarioList = new ArrayList();
			UsuarioVO usuarioVO;
			InitialDirContext ldapContext = new InitialDirContext(ldapEnv);

//             Create the search controls       
			SearchControls searchCtls = new SearchControls();

//            Specify the attributes to return
//            String returnedAtts[] = {"sn", "givenName", "userPrincipalName"};
			String returnedAtts[] = { "sn", "uid" };
//            searchCtls.setReturningAttributes(returnedAtts);

//            Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

//            specify the LDAP search filter
			String searchFilter = LDAP_SEARCH_FILTER;

//            Specify the Base for the search
			String searchBase = LDAP_SEARCH_BASE;
//            initialize counter to total the results
			int totalResults = 0;
//            Search for objects using the filter

			NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, "(objectclass=user)", searchCtls);

			while (answer.hasMoreElements()) {
				SearchResult sr = answer.next();
				totalResults++;
				Attributes attrs = sr.getAttributes();

				usuarioVO = new UsuarioVO();
				String nombreApellidoUsuario = "";
				try {
					if (attrs.get("givenName").get() != null) {
						nombreApellidoUsuario = "" + attrs.get("givenName").get();
					}
				} catch (Exception ex) {

				}

				try {
					if (attrs.get("sn").get() != null) {
						if (nombreApellidoUsuario != null) {
							nombreApellidoUsuario = nombreApellidoUsuario + " " + attrs.get("sn").get();
						} else {
							nombreApellidoUsuario = "" + attrs.get("sn").get();
						}
					}
				} catch (Exception ex) {

				}

				usuarioVO.setNombre(nombreApellidoUsuario);
				usuarioVO.setUserName("" + attrs.get("cn").get());
				usuarioList.add(usuarioVO);
			}
//            System.out.println("Total results: " + totalResults);
			ldapContext.close();
			return usuarioList;
		} catch (NamingException ne) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	private boolean findUsuarioLdap(Hashtable<String, String> ldapEnv) {
		boolean result;
		try {
			DirContext ctx = new InitialDirContext(ldapEnv);
			ctx.close();
			result = true;
		} catch (NamingException ne) {
			result = false;
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

}
