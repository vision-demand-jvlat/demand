/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlat.sop.login.vos;

import com.jvlat.sop.entidadessop.entidades.Compania;

/**
 *
 * @author Roberto Osorio
 */
public class UsuarioVO {

    private Long id;
    private String nombre;
    private String userName;
    private String loginUsuario;
    
    private Compania companiaId;

    public Compania getCompaniaId() {
        return companiaId;
    }

    public void setCompaniaId(Compania companiaId) {
        this.companiaId = companiaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }




    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoginUsuario() {
        return loginUsuario;
    }

    public void setLoginUsuario(String loginUsuario) {
        this.loginUsuario = loginUsuario;
    }
    
}
