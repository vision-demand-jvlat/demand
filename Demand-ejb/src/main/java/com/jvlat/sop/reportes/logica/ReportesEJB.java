/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlat.sop.reportes.logica;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.demand.logica.ForecastEJB;
import com.jvlap.sop.demand.logica.vos.CategoriasVO;
import com.jvlap.sop.demand.logica.vos.GraficaSellOutVO;
import com.jvlap.sop.demand.logica.vos.InvRetailerPuntoVentaVO;
import com.jvlap.sop.demand.logica.vos.ParametrosDTO;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.GrupoCat4;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.PuntoDeVenta;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.entidadessop.entidades.Usuario;

/**
 *
 * @author Roberto Osorio
 */
@SuppressWarnings("rawtypes")
@Stateless
public class ReportesEJB extends AbstractFacade {

	static final String COD_COMPANIA_00070 = "00070";
	static final String COD_COMPANIA_00072 = "00072";
	static final String COD_COMPANIA_00074 = "00074";

	static Logger log = LogManager.getLogger(ReportesEJB.class.getName());
	@SuppressWarnings("unused")
	private Usuario usuarioLogueado;

	@PersistenceContext(unitName = "EntidadesSOPPU")
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public ReportesEJB() {
	}

	@SuppressWarnings("unchecked")
	public ReportesEJB(Class entityClass) {
		super(entityClass);
	}

	@PostConstruct
	public void init() {
		usuarioLogueado = new Usuario();

	}

	/**
	 * 
	 * @param idRetailer
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PuntoDeVenta> buscaPuntoVentaPorRetailer(String idRetailer) {
		try {
			Query query = em
					.createQuery("SELECT p FROM PuntoDeVenta p WHERE p.retailerFk.id = :retailerID ORDER BY p.nombre");
			query.setParameter("retailerID", idRetailer);
			return query.getResultList();
		} catch (NoResultException e) {
			log.error("[buscaPuntoVentaPorRetailer] No se encontró información:", e);
			return new ArrayList<>();
		}
	}

	/**
	 *
	 * @param idCompania
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Retailer> buscarListaRetailersPorCompania(String idCompania) {

		try {
			String idCompaniaTM = idCompania;
			Query query;

			if (idCompania.equals(COD_COMPANIA_00074)) {
				idCompania = COD_COMPANIA_00070;
				query = em.createQuery(
						"SELECT r FROM Retailer r WHERE r.companiaFk.id = :idCompania AND r.canalDistribucion ='BRD' ORDER BY r.nombreComercial");
			} else if (idCompania.equals(COD_COMPANIA_00072)) {
				idCompania = COD_COMPANIA_00070;
				query = em.createQuery(
						"SELECT r FROM Retailer r WHERE r.companiaFk.id = :idCompania AND r.canalDistribucion !='BRD' ORDER BY r.nombreComercial");
			} else {
				query = em.createQuery(
						"SELECT r FROM Retailer r WHERE r.companiaFk.id = :idCompania ORDER BY r.nombreComercial");
			}

			if (idCompaniaTM.equals(COD_COMPANIA_00070)) {
				return new ArrayList<>();
			}
			query.setParameter("idCompania", idCompania);
			return query.getResultList();
		} catch (NoResultException e) {
			log.debug("[buscarListaRetailersPorCompania] No se encontró información:{}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Compania> listaCompanias() {
		try {
			Query query = em.createQuery("SELECT c FROM Compania c ORDER BY c.nombre");
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	/**
	 * 
	 * @return
	 */
	public List<MesSop> listaMesSop() {
		try {
			Query query = em.createQuery("SELECT m FROM MesSop m ORDER BY m.anio desc, m.mes desc, m.nombre");
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<GrupoCat4> listaGrupoCat() {
		try {
			Query query = em.createQuery("SELECT g FROM GrupoCat4 g ");
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CategoriasVO> listaCategorias() {
		try {
			String queryStr = " SELECT distinct m.id+g.id+pl.id codigo,  m.nombre+g.nombre+pl.nombre FROM PRODUCTO p INNER JOIN MARCA_CAT2 m ON p.marca_cat2_fk = m.id  INNER JOIN GRUPO_CAT4 g ON p.grupo_cat4_fk = g.id INNER JOIN PLATAFORMA_CAT5 pl ON p.plataforma_cat5_fk = pl.id ";
			Query query = em.createNativeQuery(queryStr);
			List<Object[]> resultados = query.getResultList();
			CategoriasVO categorias;
			List<CategoriasVO> cat = new ArrayList<>();

			for (Object[] obj : resultados) {
				categorias = new CategoriasVO();
				categorias.setId((String) obj[0]);
				categorias.setNombre((String) obj[1]);
				cat.add(categorias);
			}
			return cat;
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}

	@SuppressWarnings("unchecked")
	public List<PuntoDeVenta> listaPuntoVenta() {
		try {
			Query query = em.createQuery("SELECT p FROM PuntoDeVenta p ");
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Connection obtenerConnection() {
		return em.unwrap(java.sql.Connection.class);
	}

	/**
	 * 
	 * @param idCompania
	 * @param retailerId
	 * @param mesesAnio
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ParametrosDTO> datosReporteVariacionCategoria(String idCompania, String retailerId, Long mesesAnio) {
		List<ParametrosDTO> listParametrosDTO = new ArrayList<>();
		ParametrosDTO parametrosDTO;

		try {
			ForecastEJB forecastEJB = new ForecastEJB();
			String sqlComRet = "";
			sqlComRet = forecastEJB.validarCompaniaXRetailers(idCompania);

			for (Long i = mesesAnio; i < mesesAnio + 5; i++) {
				String queryStr = "SELECT v.id,vc.categoria,ivc.categoria nombreCategoria, "
						+ "floor(round(isnull(vc.sell_out,0),0)) sell_out, " + "v.compania_fk,v.retailer_fk, "
						+ "CASE \n" + "WHEN LEN(cast(m.mes as varchar(4))) > 1 \n"
						+ "THEN cast(m.mes as varchar(4))+' / ' + cast(m.anio as varchar(4)) \n"
						+ "ELSE '0'+cast(m.mes as varchar(4))+' / ' + cast(m.anio as varchar(4)) \n" + "END mesAnio "
						+ "FROM variacion v " + "INNER JOIN VARIACION_CATEGORIA vc ON v.id=vc.variacion_fk "
						+ "INNER JOIN MES_SOP m ON v.mes_fk=m.id "
						+ "INNER JOIN I_VIEW_CATEGORIA ivc ON vc.categoria=ivc.id "
						+ "INNER JOIN RETAILER ret ON (ret.id = v.retailer_fk)" /* Agregado mejora #85 JA 23-11-2016 */
						+ "WHERE " + "v.compania_fk IN (?1) " + "AND v.retailer_fk IN (?2) " + "AND v.mes_fk IN (?3) "
						+ "AND isnull(vc.sell_out,0) > 0" + sqlComRet;

				Query query = em.createNativeQuery(queryStr);
				if (idCompania.equals(COD_COMPANIA_00072) || idCompania.equals(COD_COMPANIA_00074)) {
					idCompania = COD_COMPANIA_00070;
					query.setParameter(1, idCompania);
				} else {
					query.setParameter(1, idCompania);
				}
				query.setParameter(2, retailerId);
				query.setParameter(3, i);
				List<Object[]> resultados = query.getResultList();

				for (Object[] obj : resultados) {
					parametrosDTO = new ParametrosDTO();
					parametrosDTO.setId((BigDecimal) obj[0]);
					parametrosDTO.setCategoria((String) obj[1]);
					parametrosDTO.setNombreCategoria((String) obj[2]);
					parametrosDTO.setSell_out((BigDecimal) obj[3]);
					parametrosDTO.setCompania_fk((String) obj[4]);
					parametrosDTO.setRetailer_fk((String) obj[5]);
					parametrosDTO.setMesAnio((String) obj[6]);
					listParametrosDTO.add(parametrosDTO);
				}
			}
			return listParametrosDTO;
		} catch (NoResultException e) {
			log.debug("[datosReporteVariacionCategoria] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<GraficaSellOutVO> datosReporteGraficarVariacion(String idCompania, String marca, String categoria) {
		List<GraficaSellOutVO> listGraficaSellOutVO = new ArrayList<>();
		GraficaSellOutVO graficaSellOutVO;
		try {
			List<Object[]> resultados = em.createNativeQuery("{call SOPP_RptGrapVarSellOut(?,?,?)}")
					.setParameter(1, idCompania).setParameter(2, marca).setParameter(3, categoria).getResultList();

			for (Object[] obj : resultados) {
				graficaSellOutVO = new GraficaSellOutVO();
				graficaSellOutVO.setMes((String) obj[0]);
				graficaSellOutVO.setSellout1((BigDecimal) obj[1]);
				graficaSellOutVO.setSellout2((BigDecimal) obj[1]);
				graficaSellOutVO.setSellout3((BigDecimal) obj[1]);
				listGraficaSellOutVO.add(graficaSellOutVO);
			}
			return listGraficaSellOutVO;
		} catch (NoResultException e) {
			log.debug("[datosReporteGraficarVariacion] No se encontró información: {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 
	 * @param idCompania
	 * @param retailerId
	 * @param mesesAnio
	 * @param puntoVentasSeleccionados
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<InvRetailerPuntoVentaVO> datosReporteInvRetailerPuntoVenta(String idCompania, String retailerId,
			Long mesesAnio, String[] puntoVentasSeleccionados) {
		List<InvRetailerPuntoVentaVO> listInvRetailerPuntoVentaVO = new ArrayList<>();
		InvRetailerPuntoVentaVO invRetailerPuntoVentaVO;

		StringBuilder puntoVentaString = new StringBuilder();

		for (int k = 0; k < puntoVentasSeleccionados.length; k++) {
			puntoVentaString.append(puntoVentasSeleccionados[k] + ",");
		}

		try {
			ForecastEJB forecastEJB = new ForecastEJB();
			String sqlComRet = "";
			sqlComRet = forecastEJB.validarCompaniaXRetailers(idCompania);

			String queryStr = "select max(ivc.categoria) categoria, sum(s.stock) stock " + "from SELL_OUT s "
					+ "inner join PRODUCTO p on s.producto_fk=p.id "
					+ "inner join PUNTO_DE_VENTA pv on s.punto_de_venta_fk=pv.codigo "
					+ "inner join RETAILER ret on pv.retailer_fk=ret.id "
					+ "inner join COMPANIA c on ret.compania_fk=c.id "
					+ "inner join I_VIEW_CATEGORIA ivc on p.marca_cat2_fk+p.grupo_cat4_fk+p.plataforma_cat5_fk=ivc.id "
					+ "inner join CALENDARIO cal on s.calendario_fk=cal.id "
					+ "INNER JOIN MES_SOP m ON cal.mes=m.mes and cal.anio=m.anio " + "where ret.compania_fk=?1 "
					+ "and ret.id=?2 " + "and m.id=?3  " + sqlComRet
					+ "and s.punto_de_venta_fk IN (select rtrim(ltrim(splitdata)) splitdata "
					+ "from [dbo].[fnSplitString]( substring(?4,1,LEN(?4)-1) , ',' ) ) "
					+ "group by p.marca_cat2_fk+p.grupo_cat4_fk+p.plataforma_cat5_fk ";

			Query query = em.createNativeQuery(queryStr);
			if (idCompania.equals(COD_COMPANIA_00072) || idCompania.equals(COD_COMPANIA_00074)) {
				idCompania = COD_COMPANIA_00070;
				query.setParameter(1, idCompania);
			} else {
				query.setParameter(1, idCompania);
			}
			query.setParameter(2, retailerId);
			query.setParameter(3, mesesAnio);
			query.setParameter(4, puntoVentaString.toString());

			List<Object[]> resultados = query.getResultList();

			for (Object[] obj : resultados) {
				invRetailerPuntoVentaVO = new InvRetailerPuntoVentaVO();
				invRetailerPuntoVentaVO.setCategoria((String) obj[0]);
				invRetailerPuntoVentaVO.setStock((BigDecimal) obj[1]);
				listInvRetailerPuntoVentaVO.add(invRetailerPuntoVentaVO);
			}
			return listInvRetailerPuntoVentaVO;
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * 
	 * @param idRetailer
	 * @return
	 */

	public Retailer buscarRetailerXId(String idRetailer) {

		try {
			String qlString = "SELECT r FROM Retailer r WHERE r.id = :retailer";
			return (Retailer) em.createQuery(qlString).setParameter("retailer", idRetailer).getSingleResult();
		} catch (Exception e) {
			log.error("ERROR: Hubo un error en la consulta de Retailer por ID: ", e);
			return null;
		}
	}

}
