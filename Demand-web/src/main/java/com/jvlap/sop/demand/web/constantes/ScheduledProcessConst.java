package com.jvlap.sop.demand.web.constantes;

public class ScheduledProcessConst {

	private ScheduledProcessConst() {
		throw new IllegalStateException("Utility class");
	}

	public static final String PROCESO_1 = "PROC_1";
	public static final String PROCESO_2 = "PROC_2";

	public static final String ESTADO_PROGRAMADO = "PROGRAMADO";
	public static final String ESTADO_EJECUTANDO = "EJECUTANDO";
	public static final String ESTADO_TERMINADO = "TERMINADO";
	public static final String ESTADO_ERROR = "ERROR";
	public static final String ESTADO_CANCELADO = "CANCELADO";
	public static final String ESTADO_EN_PROCESO = "EN_PROCESO";
	public static final String ESTADO_INDEFINIDO = "INDEFINIDO";
	public static final String SIN_DATOS = "SIN_DATOS";

}
