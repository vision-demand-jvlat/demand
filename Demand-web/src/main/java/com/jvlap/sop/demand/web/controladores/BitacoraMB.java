/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.controladores;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlat.sop.bitacora.logica.BitacoraEJB;
import com.jvlat.sop.entidadessop.entidades.Bitacora;
import com.jvlat.sop.entidadessop.entidades.MesSop;

/**
 *
 * @author Seidor
 */
@SuppressWarnings("serial")
@Named(value = "bitacoraMB")
@ViewScoped
public class BitacoraMB implements Serializable {
	private static Logger log = LogManager.getLogger(BitacoraMB.class.getName());

	@EJB
	private BitacoraEJB bitacoraEJB;

	private Bitacora bitacora;
	private List<Bitacora> bitacoraLista;

	/**
	 * Constructor
	 */
	public BitacoraMB() {
		//
	}

	@PostConstruct
	public void init() {
		bitacora = new Bitacora();
	}

	public String consultarBitacora() {
		HttpSession session = Util.getSession();
		// Consulta Bitacora
		try {
			String modulo = (String) session.getAttribute("modulo_bitacora");
			MesSop mes = (MesSop) session.getAttribute("mes_bitacora");
			bitacoraLista = bitacoraEJB.consultarBitacora(modulo, mes);
		} catch (Exception e) {
			log.error("Error en consulta de registros bitacora ", e);
		}
		return "";
	}

	public List<Bitacora> getBitacoraLista() {
		return bitacoraLista;
	}

	public void setBitacoraLista(List<Bitacora> bitacoraLista) {
		this.bitacoraLista = bitacoraLista;
	}

	public Bitacora getBitacora() {
		return bitacora;
	}

	public void setBitacora(Bitacora bitacora) {
		this.bitacora = bitacora;
	}

}
