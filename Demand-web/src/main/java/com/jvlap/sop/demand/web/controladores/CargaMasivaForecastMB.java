package com.jvlap.sop.demand.web.controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import com.jvlap.sop.demand.logica.CargaMasivaEJB;
import com.jvlap.sop.demand.logica.ForecastEJB;
import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.web.utilitarios.Mensajes;
import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author Seidor
 */

@SuppressWarnings("serial")
@Named("cargaMasivaForecastMB")
@ViewScoped
@ManagedBean
public class CargaMasivaForecastMB implements Serializable {

	private static Logger log = LogManager.getLogger(CargaMasivaForecastMB.class.getName());

	@EJB
	ModuloGeneralEJB moduloGeneralEJB;

	@EJB
	ForecastEJB forecastEJB;

	@EJB
	CargaMasivaEJB cargaMasivaEJB;

	private List<String> listaErrores;
	private UsuarioVO usuarioLogueado;

	private List<Compania> companias = new ArrayList<>();
	private Compania companiaSeleccionada;
	private LinkedHashMap<String, Compania> companiasMap = new LinkedHashMap<>();
	private String companiaSeleccionadaIde;

	@PostConstruct
	public void init() {

		usuarioLogueado = getUsuario();
		companias = moduloGeneralEJB.consultarCompaniasUsuario(usuarioLogueado);

		for (Compania compania : companias) {
			companiasMap.put(compania.getId(), compania);
		}

	}

	/**
	 * 
	 * @return
	 */
	public UsuarioVO getUsuario() {
		if (usuarioLogueado == null) {
			usuarioLogueado = new UsuarioVO();
			HttpSession session = Util.getSession();
			usuarioLogueado.setId((Long) session.getAttribute("usrLogueadoId"));
			usuarioLogueado.setNombre((String) session.getAttribute("usrLogueadoNom"));
			usuarioLogueado.setUserName((String) session.getAttribute("usrLogueadoUserNom"));
			return usuarioLogueado;
		} else {
			return usuarioLogueado;
		}
	}

	/**
	 * 
	 * @param event
	 */
	public void handleFileUpload(FileUploadEvent event) {

		try {
			if (companiaSeleccionada == null) {
				FacesContext.getCurrentInstance().addMessage("Cargar",
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Compañia es requerida ", "Error"));
				return;
			}
			if (!event.getFile().getFileName().endsWith(".csv")) {
				Mensajes.mostrarMensajeError("Archivo no Permitido. Solamente se permite un archivo tipo CSV");
				return;
			}

			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);
			listaErrores = new ArrayList<>();

			cargaMasivaEJB.cargarArchivo(event.getFile().getInputstream(), listaErrores, companiaSeleccionada.getId(),
					usuarioLogueado.getNombre());

			if (listaErrores.isEmpty()) {
				FacesContext.getCurrentInstance().addMessage("Cargar",
						new FacesMessage("Se cargó la información con éxito"));
			} else {
				errorCargaMasiva(listaErrores);
			}
		} catch (Exception e) {
			log.error("[handleFileUpload] Hubo un error", e);
			FacesContext.getCurrentInstance().addMessage("Cargar",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al procesar el archivo CSV ", "Error"));
		}
	}

	/**
	 * 
	 */
	public void accion() {
		companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);
	}

	/**
	 * 
	 * @param listaErrores
	 */
	public void errorCargaMasiva(List<String> listaErrores) {
		Mensajes.mostrarMensajeError("Error cargando el archivo");
		RequestContext context2 = RequestContext.getCurrentInstance();
		context2.execute("PF('errorCargaDialog').show()");
		context2.update("mainForm:tableErrors");
	}

	// GETTERS Y SETTERS

	public List<String> getListaErrores() {
		return listaErrores;
	}

	public void setListaErrores(List<String> listaErrores) {
		this.listaErrores = listaErrores;
	}

	public UsuarioVO getUsuarioLogueado() {
		return usuarioLogueado;
	}

	public void setUsuarioLogueado(UsuarioVO usuarioLogueado) {
		this.usuarioLogueado = usuarioLogueado;
	}

	public List<Compania> getCompanias() {
		return companias;
	}

	public void setCompanias(List<Compania> companias) {
		this.companias = companias;
	}

	public Compania getCompaniaSeleccionada() {
		return companiaSeleccionada;
	}

	public void setCompaniaSeleccionada(Compania companiaSeleccionada) {
		this.companiaSeleccionada = companiaSeleccionada;
	}

	public LinkedHashMap<String, Compania> getCompaniasMap() {
		return companiasMap;
	}

	public void setCompaniasMap(LinkedHashMap<String, Compania> companiasMap) {
		this.companiasMap = companiasMap;
	}

	public String getCompaniaSeleccionadaIde() {
		return companiaSeleccionadaIde;
	}

	public void setCompaniaSeleccionadaIde(String companiaSeleccionadaIde) {
		this.companiaSeleccionadaIde = companiaSeleccionadaIde;
	}

}
