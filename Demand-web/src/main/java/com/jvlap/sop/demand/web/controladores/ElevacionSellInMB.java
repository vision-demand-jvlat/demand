/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.controladores;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.model.LazyDataModel;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlap.sop.demand.constantes.ConstantesDemand;
import com.jvlap.sop.demand.logica.ElevacionSellInEJB;
import com.jvlap.sop.demand.logica.ForecastEJB;
import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.vos.ForecastXMesVO;
import com.jvlap.sop.demand.logica.vos.ItemForecastRetailerVO;
import com.jvlap.sop.demand.logica.vos.ProductosForecastMesVO;
import com.jvlap.sop.demand.web.excel.GestorExcelForecast;
import com.jvlap.sop.demand.web.lazymodel.ForecastLazyModel;
import com.jvlap.sop.demand.web.utilitarios.Mensajes;
import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.Forecast;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author Seidor
 */
@SuppressWarnings("serial")
@Named(value = "elevacionSellInMB")
@ViewScoped
public class ElevacionSellInMB implements Serializable {
	private static Logger log = LogManager.getLogger(ElevacionSellInMB.class.getName());

	@EJB
	ModuloGeneralEJB moduloGeneralEJB;

	@EJB
	private ElevacionSellInEJB elevacionEJB;

	@EJB
	ForecastEJB forecastEJB;

	private UsuarioVO usuario = null;
	private List<Compania> companias = new ArrayList<>();
	private Compania companiaSeleccionada;
	private LinkedHashMap<String, Compania> companiasMap = new LinkedHashMap<>();
	private String companiaSeleccionadaIde;

	// ---------------------------------------------------------------------------------------------------------------------------
	// MCG Controles relacionados con GrupoMarca
	// ---------------------------------------------------------------------------------------------------------------------------
	private List<GrupoMarca> grupoMarcas;
	private GrupoMarca grupoMarcaSeleccionada;
	private LinkedHashMap<String, GrupoMarca> grupoMarcasMap = new LinkedHashMap<>();
	private String grupoMarcaSeleccionadaIde;
	// ---------------------------------------------------------------------------------------------------------------------------

	private List<MarcaCat2> marcas;
	private MarcaCat2 marcaSeleccionada;
	private LinkedHashMap<String, MarcaCat2> marcasMap = new LinkedHashMap<>();
	private String marcaSeleccionadaIde;

	private List<Retailer> retailers;
	private Retailer retailerSeleccionado;
	private LinkedHashMap<String, Retailer> retailersMap = new LinkedHashMap<>();
	private String retailerSeleccionadoIde;

	private List<MesSop> meses;
	private MesSop mesSeleccionado;
	private Long mesSeleccionadoIde;
	private Map<Long, MesSop> mesesMap = new LinkedHashMap<>();

	private List<ItemForecastRetailerVO> productosForecast;

	private String estadoForecast = null;

	// Control para inactivar boton de edicion
	private boolean inactivarEdit;
	private ForecastXMesVO forecast;
	private List<String> nombreMeses;
	private boolean mostrarBotnConfirmar = false;
	private boolean unicaCompania = false;

	private LazyDataModel<ProductosForecastMesVO> lazyCategoria;
	private LazyDataModel<ProductosForecastMesVO> lazyProductos;
	private Map<String, ForecastXMesVO> productoCategoria;
	private String nombreCategoria;

	private List<String> selectItems;
	private boolean upc;
	private boolean modelo;
	private boolean descripcion;

	private boolean estadoCerrado;
	private String marcasGrupo;

	@PostConstruct
	public void init() {

		selectItems = new ArrayList<>();
		selectItems.add("Producto");
		usuario = getUsuario();
		upc = false;
		modelo = false;
		descripcion = true;

		companias = moduloGeneralEJB.consultarCompaniasUsuario(usuario);

		marcas = moduloGeneralEJB.consultarMarcas();
		meses = moduloGeneralEJB.consultarMesesSOP(false);
		grupoMarcas = moduloGeneralEJB.consultarGrupoMarcas();
		productoCategoria = new LinkedHashMap<>();

		for (Compania item : companias) {
			companiasMap.put(item.getId(), item);
		}

		for (GrupoMarca item : grupoMarcas) {
			grupoMarcasMap.put(String.valueOf(item.getId()), item);
		}

		for (MarcaCat2 item : marcas) {
			marcasMap.put(item.getId(), item);
		}

		for (MesSop item : meses) {
			mesesMap.put(item.getId(), item);
		}

		if (companias.size() == 1) {
			companiaSeleccionadaIde = companias.get(0).getId();
			unicaCompania = true;
		}
	}

	public UsuarioVO getUsuario() {

		if (usuario == null) {
			usuario = new UsuarioVO();
			HttpSession session = Util.getSession();
			usuario.setId((Long) session.getAttribute("usrLogueadoId"));
			usuario.setNombre((String) session.getAttribute("usrLogueadoNom"));
			usuario.setUserName((String) session.getAttribute("usrLogueadoUserNom"));
			return usuario;
		} else {
			return usuario;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void consultar() {

		try {
			grupoMarcaSeleccionada = grupoMarcasMap.get(grupoMarcaSeleccionadaIde);
			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);
			retailerSeleccionado = elevacionEJB.consultarRetailerOtrosPorCompania(companiaSeleccionada);
			boolean evaluaEstado = false;

			marcasGrupo = moduloGeneralEJB.getMarcasByIDGrupoMarca(grupoMarcaSeleccionadaIde);
			Forecast forecastDB = moduloGeneralEJB.consultarForecast(retailerSeleccionado, marcasGrupo, mesSeleccionado,
					null);
			if (forecastDB != null && (forecastDB.getEstadoFk().getCodigo().equals(ConstantesDemand.EST_FORECAST_CNFD)
					|| forecastDB.getEstadoFk().getCodigo().equals(ConstantesDemand.EST_FORECAST_CNFG)
					|| forecastDB.getEstadoFk().getCodigo().equals(ConstantesDemand.EST_FORECAST_CERR)
					|| forecastDB.getEstadoFk().getCodigo().equals(ConstantesDemand.EST_FORECAST_DEVL))) {
				evaluaEstado = true;
			}

			estadoCerrado = evaluaEstado;
			int mesesForecastParametro = moduloGeneralEJB.consultarMeses(evaluaEstado,
					(forecastDB != null ? forecastDB.getId() : 0L));
			nombreMeses = moduloGeneralEJB.consultarNombresMeses(mesesForecastParametro, mesSeleccionado.getId());
			List<String> listMarcas = moduloGeneralEJB
					.getMarcaGrupoPorIdGrupoMarca(Arrays.asList(grupoMarcaSeleccionadaIde));
			List<String> oIDRetailers = new ArrayList<>();
			oIDRetailers.add(retailerSeleccionado.getId());

			forecast = elevacionEJB.consultarElevacion(productosForecast, forecast, productoCategoria, mesSeleccionado,
					companiaSeleccionada, marcasGrupo, mesesForecastParametro, listMarcas, retailerSeleccionado,
					evaluaEstado, grupoMarcaSeleccionadaIde, nombreMeses);
			if (forecast != null) {
				forecast = moduloGeneralEJB.calcularTotalAMarca(forecast, nombreMeses, listMarcas, oIDRetailers);
				lazyCategoria = new ForecastLazyModel(forecast.getProductosForecast());
				mostrarBotnConfirmar = !forecast.getProductosForecast().isEmpty();
			} else {
				lazyCategoria = new ForecastLazyModel(new ArrayList());
				mostrarBotnConfirmar = false;
			}

			if (forecastDB != null) {
				estadoForecast = forecastDB.getEstadoFk().getCodigo();
				inactivarEdit = forecastDB.getEstadoFk().getCodigo().equals(ConstantesDemand.EST_FORECAST_ELSE);
				if (!estadoForecast.equals(ConstantesDemand.EST_FORECAST_ELSE)) {
					estadoForecast = ConstantesDemand.EST_FORECAST_CERR;
					inactivarEdit = true;
				} else {
					estadoForecast = ConstantesDemand.EST_FORECAST_INI;
					inactivarEdit = false;
				}
			} else {
				estadoForecast = ConstantesDemand.EST_FORECAST_INI;
				inactivarEdit = false;
			}
		} catch (Exception ex) {
			log.error("[consultar] Hubo un error ", ex);
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}
	}

	// Autor : Manuel Cortes Granados
	// Fecha : 17 Abril 2017 1:40 PM
	public void exportarAExcel() throws IOException {
		HSSFWorkbook workbook = new HSSFWorkbook();
		try {
			GestorExcelForecast exportarExcel = new GestorExcelForecast();
			if (forecast != null) {
				exportarExcel.generarArchivoExcelForecast(workbook, forecast, GestorExcelForecast.ELEVACION_SELLIN,
						true, productoCategoria, null);
			} else {
				throw new ExcepcionSOP("Debe primero generar la consulta antes de solicitar el archivo de excel.");
			}
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			externalContext.setResponseContentType("application/vnd.ms-excel");
			externalContext.setResponseHeader("Content-Disposition",
					"attachment; filename=\"ReporteElevacionSellin.xls\"");
			workbook.write(externalContext.getResponseOutputStream());
			facesContext.responseComplete();

		} catch (ExcepcionSOP ex) {
			log.error("[exportarAExcel] Hubo un error ", ex);
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} finally {
			workbook.close();
		}
	}

	/**
	 * Guarda forecast, en caso de existir lo modifica si el estado es INIC
	 */
	public void guardarParcial() {

		try {
			elevacionEJB.guardarForecastRetailerOtros(ConstantesDemand.EST_FORECAST_ELSE, forecast, nombreMeses,
					usuario, productoCategoria);

			limpiarCampos();
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje("msj_proc_exitoso"));
		} catch (Exception ex) {
			log.error("[guardarParcial] Hubo un error ", ex);
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

	}

	/**
	 * Guarda el forecast y lo deja en estado Cerrado
	 */
	public void guardarEnviar() {

		try {
			forecast.setFueProcesadoXCompania(true);
			moduloGeneralEJB.guardarEstadoForecast(forecast, ConstantesDemand.EST_FORECAST_CERR, usuario,
					productoCategoria);
			elevacionEJB.notificarCreacionElevationSellin(mesSeleccionado, grupoMarcaSeleccionadaIde,
					retailerSeleccionado);

			limpiarCampos();
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje("msj_proc_exitoso"));
		} catch (Exception ex) {
			log.error("[guardarEnviar] Hubo un error ", ex);
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void limpiarCampos() {
		marcaSeleccionadaIde = null;
		retailerSeleccionadoIde = null;
		inactivarEdit = true;
		mostrarBotnConfirmar = false;
		retailersMap.clear();
		forecast = null;
		estadoForecast = null;
		lazyCategoria = new ForecastLazyModel(new ArrayList());
		selectItems = null;

	}

	/**
	 * Carga la lista de retailers a partir de la compania seleccionada
	 */
	public void cargaListaRetailers() {

		try {
			retailersMap = new LinkedHashMap<>();

			if (companiaSeleccionadaIde != null) {
				retailers = moduloGeneralEJB.consultarRetailers(companiaSeleccionadaIde);
				for (Retailer item : retailers) {
					retailersMap.put(item.getId(), item);
				}
			}
		} catch (Exception e) {
			log.error("[cargaListaRetailers] Hubo un error ", e);

			Mensajes.mostrarMensajeErrorNoManejado(e, this.getClass());
		}
	}

	public void onCellEdit(CellEditEvent event) {

		ProductosForecastMesVO item = (ProductosForecastMesVO) ((DataTable) event.getComponent()).getRowData();
		moduloGeneralEJB.actualizarInfoTabla(item, forecast, nombreMeses, false,
				productoCategoria.get(item.getIdCategoria() + "-" + item.getIdRetailer()), true);
	}

	@SuppressWarnings("deprecation")
	public void cargarDialogo(ProductosForecastMesVO item) {

		RequestContext context2 = RequestContext.getCurrentInstance();
		nombreCategoria = item.getNombreProducto().replaceFirst("0_", "");
		lazyProductos = new ForecastLazyModel(productoCategoria.get(item.getIdProducto()).getProductosForecast());
		context2.execute("PF('carDialog').show()");
		verificarDimensionDatoMaestro();
	}

	public void selecion() {

		upc = selectItems.contains("UPC");
		descripcion = selectItems.contains("Producto");
		modelo = selectItems.contains("Modelo");
		verificarDimensionDatoMaestro();
	}

	public void verificarDimensionDatoMaestro() {

		if (upc == false && descripcion == false && modelo == false) {
			String script = "$('.dgProductos .ui-datatable-frozenlayout-left').css(\"width\", 0 + \"px\");\n"
					+ "$('.dgProductos .ui-datatable-frozen-container').css(\"width\", 0 + \"px\");\n";
			RequestContext.getCurrentInstance().execute(script);
		}
	}

	public ModuloGeneralEJB getModuloGeneralEJB() {
		return moduloGeneralEJB;
	}

	public void setModuloGeneralEJB(ModuloGeneralEJB moduloGeneralEJB) {
		this.moduloGeneralEJB = moduloGeneralEJB;
	}

	public void setUsuario(UsuarioVO usuario) {
		this.usuario = usuario;
	}

	public List<Compania> getCompanias() {
		return companias;
	}

	public void setCompanias(List<Compania> companias) {
		this.companias = companias;
	}

	public Compania getCompaniaSeleccionada() {
		return companiaSeleccionada;
	}

	public void setCompaniaSeleccionada(Compania companiaSeleccionada) {
		this.companiaSeleccionada = companiaSeleccionada;
	}

	public Map<String, Compania> getCompaniasMap() {
		return companiasMap;
	}

	public void setCompaniasMap(Map<String, Compania> companiasMap) {
		this.companiasMap = (LinkedHashMap<String, Compania>) companiasMap;
	}

	public String getCompaniaSeleccionadaIde() {
		return companiaSeleccionadaIde;
	}

	public void setCompaniaSeleccionadaIde(String companiaSeleccionadaIde) {
		this.companiaSeleccionadaIde = companiaSeleccionadaIde;
	}

	public List<MarcaCat2> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<MarcaCat2> marcas) {
		this.marcas = marcas;
	}

	public MarcaCat2 getMarcaSeleccionada() {
		return marcaSeleccionada;
	}

	public void setMarcaSeleccionada(MarcaCat2 marcaSeleccionada) {
		this.marcaSeleccionada = marcaSeleccionada;
	}

	public Map<String, MarcaCat2> getMarcasMap() {
		return marcasMap;
	}

	public void setMarcasMap(LinkedHashMap<String, MarcaCat2> marcasMap) {
		this.marcasMap = marcasMap;
	}

	public String getMarcaSeleccionadaIde() {
		return marcaSeleccionadaIde;
	}

	public void setMarcaSeleccionadaIde(String marcaSeleccionadaIde) {
		this.marcaSeleccionadaIde = marcaSeleccionadaIde;
	}

	public List<Retailer> getRetailers() {
		return retailers;
	}

	public void setRetailers(List<Retailer> retailers) {
		this.retailers = retailers;
	}

	public Retailer getRetailerSeleccionado() {
		return retailerSeleccionado;
	}

	public void setRetailerSeleccionado(Retailer retailerSeleccionado) {
		this.retailerSeleccionado = retailerSeleccionado;
	}

	public Map<String, Retailer> getRetailersMap() {
		return retailersMap;
	}

	public void setRetailersMap(LinkedHashMap<String, Retailer> retailersMap) {
		this.retailersMap = retailersMap;
	}

	public MesSop getMesSeleccionado() {
		return mesSeleccionado;
	}

	public void setMesSeleccionado(MesSop mesSeleccionado) {
		this.mesSeleccionado = mesSeleccionado;
	}

	public Long getMesSeleccionadoIde() {
		return mesSeleccionadoIde;
	}

	public void setMesSeleccionadoIde(Long mesSeleccionadoIde) {
		this.mesSeleccionadoIde = mesSeleccionadoIde;
	}

	public String getRetailerSeleccionadoIde() {
		return retailerSeleccionadoIde;
	}

	public void setRetailerSeleccionadoIde(String retailerSeleccionadoIde) {
		this.retailerSeleccionadoIde = retailerSeleccionadoIde;
	}

	public List<MesSop> getMeses() {
		return meses;
	}

	public void setMeses(List<MesSop> meses) {
		this.meses = meses;
	}

	public Map<Long, MesSop> getMesesMap() {
		return mesesMap;
	}

	public void setMesesMap(Map<Long, MesSop> mesesMap) {
		this.mesesMap = mesesMap;
	}

	public List<ItemForecastRetailerVO> getProductosForecast() {
		return productosForecast;
	}

	public void setProductosForecast(List<ItemForecastRetailerVO> productosForecast) {
		this.productosForecast = productosForecast;
	}

	public String getEstadoForecast() {
		return estadoForecast;
	}

	public void setEstadoForecast(String estadoForecast) {
		this.estadoForecast = estadoForecast;
	}

	public boolean isInactivarEdit() {
		return inactivarEdit;
	}

	public void setInactivarEdit(boolean inactivarEdit) {
		this.inactivarEdit = inactivarEdit;
	}

	public ForecastXMesVO getForecast() {
		return forecast;
	}

	public void setForecast(ForecastXMesVO forecast) {
		this.forecast = forecast;
	}

	public List<String> getNombreMeses() {
		return nombreMeses;
	}

	public void setNombreMeses(List<String> nombreMeses) {
		this.nombreMeses = nombreMeses;
	}

	public boolean isMostrarBotnConfirmar() {
		return mostrarBotnConfirmar;
	}

	public void setMostrarBotnConfirmar(boolean mostrarBotnConfirmar) {
		this.mostrarBotnConfirmar = mostrarBotnConfirmar;
	}

	public String color(String producto) {

		String esta;
		if (estadoCerrado) {
			esta = moduloGeneralEJB.buscarEstadoProdPorCompania(producto, marcasGrupo, companiaSeleccionada.getId(),
					mesSeleccionadoIde, retailerSeleccionado.getId());
		} else {
			esta = moduloGeneralEJB.buscarEstadoProdElevacionInic(producto, marcasGrupo, companiaSeleccionada.getId(),
					mesSeleccionadoIde, retailerSeleccionado.getId());
		}
		String color = "color: lightslategray";
		if (esta == null) {
			return color;
		} else if (esta.equals("D")) {
			return "color: red";
		} else if (esta.equals("V")) {
			return "color: orange; font-weight: bold;";
		} else if (esta.equals("L")) {
			return "color: lime";
		} else if (esta.equals("C")) {
			return color;
		} else {
			return color;
		}
	}

	public List<GrupoMarca> getGrupoMarcas() {
		return grupoMarcas;
	}

	public void setGrupoMarcas(List<GrupoMarca> grupoMarcas) {
		this.grupoMarcas = grupoMarcas;
	}

	public GrupoMarca getGrupoMarcaSeleccionada() {
		return grupoMarcaSeleccionada;
	}

	public void setGrupoMarcaSeleccionada(GrupoMarca grupoMarcaSeleccionada) {
		this.grupoMarcaSeleccionada = grupoMarcaSeleccionada;
	}

	public Map<String, GrupoMarca> getGrupoMarcasMap() {
		return grupoMarcasMap;
	}

	public void setGrupoMarcasMap(LinkedHashMap<String, GrupoMarca> grupoMarcasMap) {
		this.grupoMarcasMap = grupoMarcasMap;
	}

	public String getGrupoMarcaSeleccionadaIde() {
		return grupoMarcaSeleccionadaIde;
	}

	public void setGrupoMarcaSeleccionadaIde(String grupoMarcaSeleccionadaIde) {
		this.grupoMarcaSeleccionadaIde = grupoMarcaSeleccionadaIde;
	}

	public boolean isUnicaCompania() {
		return unicaCompania;
	}

	public void setUnicaCompania(boolean unicaCompania) {
		this.unicaCompania = unicaCompania;
	}

	public LazyDataModel<ProductosForecastMesVO> getLazyCategoria() {
		return lazyCategoria;
	}

	public void setLazyCategoria(LazyDataModel<ProductosForecastMesVO> lazyCategoria) {
		this.lazyCategoria = lazyCategoria;
	}

	public LazyDataModel<ProductosForecastMesVO> getLazyProductos() {
		return lazyProductos;
	}

	public void setLazyProductos(LazyDataModel<ProductosForecastMesVO> lazyProductos) {
		this.lazyProductos = lazyProductos;
	}

	public Map<String, ForecastXMesVO> getProductoCategoria() {
		return productoCategoria;
	}

	public void setProductoCategoria(Map<String, ForecastXMesVO> productoCategoria) {
		this.productoCategoria = productoCategoria;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public List<String> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<String> selectItems) {
		this.selectItems = selectItems;
	}

	public boolean isUpc() {
		return upc;
	}

	public void setUpc(boolean upc) {
		this.upc = upc;
	}

	public boolean isModelo() {
		return modelo;
	}

	public void setModelo(boolean modelo) {
		this.modelo = modelo;
	}

	public boolean isDescripcion() {
		return descripcion;
	}

	public void setDescripcion(boolean descripcion) {
		this.descripcion = descripcion;
	}

	public ElevacionSellInMB() {
		//
	}

	public boolean isEstadoCerrado() {
		return estadoCerrado;
	}

	public void setEstadoCerrado(boolean estadoCerrado) {
		this.estadoCerrado = estadoCerrado;
	}
}
