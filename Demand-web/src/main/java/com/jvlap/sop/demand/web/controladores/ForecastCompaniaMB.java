/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.controladores;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.xml.soap.SOAPException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.model.LazyDataModel;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlap.sop.demand.constantes.ConstantesDemand;
import com.jvlap.sop.demand.logica.ForecastEJB;
import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.vos.ForecastVO;
import com.jvlap.sop.demand.logica.vos.ForecastXMesVO;
import com.jvlap.sop.demand.logica.vos.ItemForecastRetailerVO;
import com.jvlap.sop.demand.logica.vos.ProductosForecastMesVO;
import com.jvlap.sop.demand.web.excel.GestorExcelForecast;
import com.jvlap.sop.demand.web.lazymodel.ForecastLazyModel;
import com.jvlap.sop.demand.web.utilitarios.Mensajes;
import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlat.sop.bitacora.constantes.ConstantesBitacora;
import com.jvlat.sop.bitacora.logica.BitacoraEJB;
import com.jvlat.sop.entidadessop.entidades.Bitacora;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.EstadoForecast;
import com.jvlat.sop.entidadessop.entidades.Forecast;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.entidadessop.entidades.Usuario;
import com.jvlat.sop.entidadessop.entidades.UsuariosXRol;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author iviasus
 */
@SuppressWarnings("serial")
@Named(value = "forecastCompaniaMB")
@ViewScoped
public class ForecastCompaniaMB implements Serializable {
	private static Logger log = LogManager.getLogger(ForecastCompaniaMB.class.getName());

	@EJB
	ModuloGeneralEJB moduloGeneralEJB;

	@EJB
	private ForecastEJB forecastEJB;

	@EJB
	private BitacoraEJB bitacoraEJB;

	private UsuarioVO usuario = null;
	private List<Compania> companias = new ArrayList<>();
	private Compania companiaSeleccionada;
	private Map<String, Compania> companiasMap = new LinkedHashMap<>();
	private String companiaSeleccionadaIde;

	// ---------------------------------------------------------------------------------------------------------------------------
	// Controles relacionados con GrupoMarca
	// ---------------------------------------------------------------------------------------------------------------------------
	private List<GrupoMarca> grupoMarcas;
	private GrupoMarca grupoMarcaSeleccionada;
	private LinkedHashMap<String, GrupoMarca> grupoMarcasMap = new LinkedHashMap<>();
	private String grupoMarcaSeleccionadaIde;
	private List<String> selectedMarca = new ArrayList<>();
	// ---------------------------------------------------------------------------------------------------------------------------

	private List<MarcaCat2> marcas;
	private MarcaCat2 marcaSeleccionada;
	private Map<String, MarcaCat2> marcasMap = new LinkedHashMap<>();
	private String marcaSeleccionadaIde;

	private List<MesSop> meses;
	private MesSop mesSeleccionado;
	private Long mesSeleccionadoIde;
	private Map<Long, MesSop> mesesMap = new LinkedHashMap<>();

	private List<Retailer> retailers;
	private Retailer retailerSeleccionado;
	private Map<String, Retailer> retailersMap = new LinkedHashMap<>();
	private String retailerSeleccionadoIde;
	private List<String> selectedRetailer = new ArrayList<>();

	private List<ItemForecastRetailerVO> productosForecast = new ArrayList<>();
	private ForecastXMesVO forecast;
	private boolean consultaRetailer;

	private String estadoForecast = null;
	private boolean estadoSupply = false;

	private boolean inactivarEdit;
	private boolean forecastNivelCompania = true;
	@SuppressWarnings("unused")
	private String forecastCompania = "";
	private Bitacora bitacora;
	private String observacion = "";
	private int mesesForecastParametro;
	private List<String> nombreMeses;
	private boolean mostrarBotones = false;
	private String mensajeForecast;
	private List<Retailer> retailerForecast = new ArrayList<>();
	private boolean unicaCompania = false;
	@SuppressWarnings("unused")
	private List<ProductosForecastMesVO> oProductosEdit = new ArrayList<>();

	private LazyDataModel<ProductosForecastMesVO> lazyModelCag;
	private LazyDataModel<ProductosForecastMesVO> lazyProductos;
	private Map<String, ForecastXMesVO> productoCategoria;
	private String nombreCategoria;

	boolean hayForecastDevlKam = false;
	boolean hayRetailersSinOtros = false;

	private List<String> selectItems;
	private boolean upc;
	private boolean modelo;
	private boolean descripcion;

	private static final String MSJ_PROC_EXITOSO = "msj_proc_exitoso";
	private static final String EXCP_NULL_PROD = "No hay productos para guardar";
	private static final String EXCP_RETAIL_NULL = "Debe seleccionar Retailer";
	private static final String EXCP_GRUPO_MARC_NULL = "Debe seleccionar GrupoMarca";
	private static final String EXCP_MES_NULL = "Debe seleccionar Mes";

	private boolean renderedExcel = true;

	/**
	 * Creates a new instance of ForecastCompaniaMB
	 */
	public ForecastCompaniaMB() {
		//
	}

	/**
	 * 
	 */
	@PostConstruct
	public void init() {

		selectItems = new ArrayList<>();
		selectItems.add("Producto");
		upc = false;
		modelo = false;
		descripcion = true;
		boolean esRolForecast = false;
		usuario = getUsuario();

		// Valida rol si es de tipo Forecast Retailer, sólo puede ver su propia compañía
		try {
			esRolForecast = validaRol(usuario);
		} catch (Exception e) {
			log.error("Error  Init", e);
		}

		if (esRolForecast) {
			companias = new ArrayList<>();
		} else {
			companias = moduloGeneralEJB.consultarCompaniasUsuario(usuario);
		}
		marcas = moduloGeneralEJB.consultarMarcas();
		meses = moduloGeneralEJB.consultarMesesSOP(false);
		grupoMarcas = moduloGeneralEJB.consultarGrupoMarcas();
		productoCategoria = new LinkedHashMap<>();

		for (Compania item : companias) {
			companiasMap.put(item.getId(), item);
		}
		for (GrupoMarca item : grupoMarcas) {
			grupoMarcasMap.put(String.valueOf(item.getId()), item);
		}

		for (MarcaCat2 item : marcas) {
			marcasMap.put(item.getId(), item);
		}

		for (MesSop item : meses) {
			mesesMap.put(item.getId(), item);
		}

		if (companias.size() == 1) {
			companiaSeleccionadaIde = companias.get(0).getId();
			cargaListaRetailers();
			unicaCompania = true;
		}
	}

	/**
	 * 
	 * @return
	 */
	public UsuarioVO getUsuario() {

		if (usuario == null) {
			usuario = new UsuarioVO();
			HttpSession session = Util.getSession();
			usuario.setId((Long) session.getAttribute("usrLogueadoId"));
			usuario.setNombre((String) session.getAttribute("usrLogueadoNom"));
			usuario.setUserName((String) session.getAttribute("usrLogueadoUserNom"));
			return usuario;
		} else {
			return usuario;
		}
	}

	/**
	 * 
	 * @param usuario
	 * @return
	 * @throws SOAPException
	 */
	private boolean validaRol(UsuarioVO usuario) throws SOAPException {
		Usuario usuarioTM = new Usuario();
		usuarioTM.setId(usuario.getId().intValue());
		List<UsuariosXRol> usuarioXRolTM;
		usuarioTM = moduloGeneralEJB.buscaUsuarioPorId(usuarioTM);
		usuarioXRolTM = usuarioTM.getUsuariosXRolList();
		boolean result = false;
		if (usuarioXRolTM != null && usuarioXRolTM.size() == 1) {
			for (UsuariosXRol uxr : usuarioXRolTM) {
				if (uxr.getIdRol().getNombre().equals(ConstantesDemand.ROL_FORECAST_COMPANIA)) {
					result = true;
					break;
				}
			}
		}

		usuario.setCompaniaId(usuarioTM.getCompaniaId());
		return result;
	}

	/**
		 * 
		 */
	public void consultar() {
		valRenderedExcel();
		productosForecast = new ArrayList<>();
		int estadoDEVL = 0;
		observacion = "";
		estadoSupply = false;
		retailerForecast.clear();
		forecast = null;
		estadoForecast = null;
		consultaRetailer = false;
		lazyModelCag = new ForecastLazyModel(new ArrayList<ProductosForecastMesVO>());
		hayForecastDevlKam = false;
		hayRetailersSinOtros = false;

		try {
			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);

			validarFiltros();

			String grupoMar = moduloGeneralEJB.getMarcasByIDGrupoMarca(selectedMarca);
			List<String> listMarcas = moduloGeneralEJB.getMarcasByGrupo(selectedMarca);
			mesesForecastParametro = moduloGeneralEJB.consultarMeses(false, 0L);
			boolean forecastCerrado = forecastEJB.consultarForecastCerrado(companiaSeleccionada, mesSeleccionado,
					grupoMar, false);
			if (selectedRetailer.isEmpty()) {
				this.consultaRetailer = false;
				productosForecast = forecastEJB.consultarForecastCompañia(companiaSeleccionada, mesSeleccionado,
						grupoMar, mesesForecastParametro, forecastCerrado, false, null, true);

				Forecast forecastDB;
				List<Retailer> retList = moduloGeneralEJB.consultarRetailers(companiaSeleccionada.getId());

				hayForecastDevlKam = moduloGeneralEJB.consultaCompaniaRetailerDEVL_KAM(companiaSeleccionada, listMarcas,
						mesSeleccionado);

				if (forecastEJB.consultarForecastRetailer(companiaSeleccionada, mesSeleccionado, grupoMar,
						retList.size())) {
					mensajeForecast = "Listo para cerrar el Forecast Compañia";
				} else {
					mensajeForecast = "Faltan Forecast Retailer para los siguientes Retailers: ";
				}

				forecast = moduloGeneralEJB.armarListaPantallaNMeses(productosForecast, nombreMeses);
				if (forecast.getProductosForecast() != null) {
					for (ProductosForecastMesVO oCategoria : forecast.getProductosForecast()) {
						if (!oCategoria.isEsTotalMarca()) {
							List<ItemForecastRetailerVO> listProductos = forecastEJB.consultarForecastCompañia(
									companiaSeleccionada, mesSeleccionado, grupoMar, mesesForecastParametro,
									forecastCerrado, true, oCategoria.getIdCategoria(), false);
							ForecastXMesVO prod = moduloGeneralEJB.armarListaPantallaNMeses(listProductos, nombreMeses);
							forecastEJB.actualizarCategoriaForecast(oCategoria, prod);
							productoCategoria.put(oCategoria.getIdProducto(), prod);
						}
					}
				}

				if (!productosForecast.isEmpty()) {
					for (Retailer ret : retList) {
						Map<String, Object> mpRetailer = moduloGeneralEJB.consultarForecastV1(ret, grupoMar,
								mesSeleccionado, null);

						forecastDB = (Forecast) mpRetailer.get("FORECAST");
						EstadoForecast estadoForecastAux = (EstadoForecast) mpRetailer.get("ESTADO");
						String descripcionEstado = "SIN FORECAST";
						if (forecastDB != null && estadoForecastAux != null) {
							estadoForecast = estadoForecastAux.getCodigo();
							descripcionEstado = estadoForecastAux.getNombre();
							if (descripcionEstado != null) {
								descripcionEstado = descripcionEstado.toUpperCase();
							}
							forecastCompania = forecastDB.getForecastCompania();

							if (estadoForecast.equals(ConstantesDemand.EST_FORECAST_DEVL)) {
								estadoDEVL++;
							}
						}

						ret.setNombreComercial(ret.getNombreComercial().concat(" / ").concat(descripcionEstado));
						retailerForecast.add(ret);

					}
				}
				inactivarEdit = false;
			} else {
				this.consultaRetailer = true;
				if (selectedRetailer.isEmpty()) {
					throw new ExcepcionSOP(EXCP_RETAIL_NULL);
				}

				hayRetailersSinOtros = moduloGeneralEJB.consultarRetailersSinOtros(selectedRetailer);

				if (forecastCerrado) {
					List<String> oIDRetailers = moduloGeneralEJB.consultarForecastCerrados(selectedRetailer, grupoMar,
							mesSeleccionado);
					if (!oIDRetailers.isEmpty()) {
						productosForecast = forecastEJB.consultarForecastCompaniaNivelRetailer(oIDRetailers,
								mesSeleccionado, grupoMar, mesesForecastParametro, false, null, true, forecastCerrado);
					}
				} else {
					productosForecast = forecastEJB.consultarForecastCompaniaNivelRetailer(selectedRetailer,
							mesSeleccionado, grupoMar, mesesForecastParametro, false, null, true, forecastCerrado);
				}

				forecast = moduloGeneralEJB.armarListaPantallaNMeses(productosForecast, nombreMeses);
				if (forecast.getProductosForecast() != null) {
					for (ProductosForecastMesVO oCategoria : forecast.getProductosForecast()) {
						if (!oCategoria.isEsTotalMarca()) {
							List<String> oIDRetailer = new ArrayList<>();
							oIDRetailer.add(oCategoria.getIdRetailer());
							List<ItemForecastRetailerVO> listProductos = forecastEJB
									.consultarForecastCompaniaNivelRetailer(oIDRetailer, mesSeleccionado, grupoMar,
											mesesForecastParametro, true, oCategoria.getIdCategoria(), false,
											forecastCerrado);
							ForecastXMesVO prod = moduloGeneralEJB.armarListaPantallaNMeses(listProductos, nombreMeses);
							forecastEJB.actualizarCategoriaForecast(oCategoria, prod);
							productoCategoria.put(oCategoria.getIdProducto(), prod);
						}
					}
				} else {
					FacesMessage msg = new FacesMessage(
							"No se encontraron resultados para la consulta, por favor verifique que haya cerrado un forecast para el retailer seleccionado.");
					msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}

				if (forecastCerrado) {
					estadoForecast = productosForecast.isEmpty() ? null : ConstantesDemand.EST_FORECAST_CERR;
					inactivarEdit = false;
				} else {
					estadoForecast = !productosForecast.isEmpty() ? ConstantesDemand.EST_FORECAST_INI : null;
					inactivarEdit = !productosForecast.isEmpty();
				}
			}

			nombreMeses = moduloGeneralEJB.consultarNombresMeses(mesesForecastParametro, mesSeleccionado.getId());
			if (forecast.getProductosForecast() != null)
				forecast = consultaRetailer
						? moduloGeneralEJB.calcularTotalAMarca(forecast, nombreMeses, listMarcas, selectedRetailer)
						: moduloGeneralEJB.calcularTotalAMarca(forecast, nombreMeses, listMarcas);

			List<String> objMarcasForecast = forecastEJB.devolverIdMarcasForecast(grupoMar, companiaSeleccionada,
					mesSeleccionado);

			if (selectedRetailer.isEmpty() && estadoForecast != null
					&& estadoForecast.equals(ConstantesDemand.EST_FORECAST_CNFD) && forecastCerrado
					&& forecast.getProductosForecast() != null) {
				forecast.setCompania(companiaSeleccionada.getId());
				forecast.setIdMes(mesSeleccionado.getId());
				if (moduloGeneralEJB.verificarUnidadesDespachadas(forecast)) {
					moduloGeneralEJB.armarListaSupplyPantalla(forecast, nombreMeses, objMarcasForecast,
							productoCategoria);
					estadoSupply = true;
				}

			}

			if (estadoForecast != null) {
				if ((estadoForecast.equals(ConstantesDemand.EST_FORECAST_ELSE)
						|| estadoForecast.equals(ConstantesDemand.EST_FORECAST_CERR)) && !forecastCerrado) {
					estadoForecast = ConstantesDemand.EST_FORECAST_INI;
				} else {
					if ((estadoForecast.equals(ConstantesDemand.EST_FORECAST_CNFG))) {
						estadoForecast = ConstantesDemand.EST_FORECAST_CERR;
					}
				}
			}

			if (selectedRetailer.isEmpty()) {
				if (estadoDEVL > 0) {
					estadoForecast = ConstantesDemand.EST_FORECAST_DEVL;
				} else {
					if (forecastCerrado) {
						estadoForecast = ConstantesDemand.EST_FORECAST_CERR;
					}
				}
			}

			if ((estadoForecast != null && (estadoForecast.equals(ConstantesDemand.EST_FORECAST_CNFD)))
					|| (forecastCerrado && !productosForecast.isEmpty())) {
				estadoForecast = ConstantesDemand.EST_FORECAST_CERR;
			}

			if (estadoForecast != null && !estadoForecast.equals(ConstantesDemand.EST_FORECAST_CERR)
					&& !selectedRetailer.isEmpty()
					&& moduloGeneralEJB.consultarForecast(selectedRetailer, grupoMar, mesSeleccionado, "('DEVL')")) {
				estadoForecast = ConstantesDemand.EST_FORECAST_DEVL;
			}

			if (estadoForecast != null && estadoForecast.equals(ConstantesDemand.EST_FORECAST_DEVL_KAM)) {
				estadoForecast = ConstantesDemand.EST_FORECAST_DEVL;
			}

			if (selectedRetailer.isEmpty()) {
				List<String> estadoForecastsCompania = moduloGeneralEJB.consultarForecastEstados(grupoMar,
						mesSeleccionado, companiaSeleccionada);

				if (!estadoForecastsCompania.isEmpty()
						&& estadoForecastsCompania.contains(ConstantesDemand.EST_FORECAST_CNFD)
						|| estadoForecastsCompania.contains(ConstantesDemand.EST_FORECAST_CNFG)) {
					estadoForecast = ConstantesDemand.EST_FORECAST_CERR;
				} else if (!estadoForecastsCompania.isEmpty()
						&& estadoForecastsCompania.contains(ConstantesDemand.EST_FORECAST_DEVL)) {
					estadoForecast = ConstantesDemand.EST_FORECAST_DEVL;
				} else {
					estadoForecast = ConstantesDemand.EST_FORECAST_INI;
				}
			}

			if (forecast.getProductosForecast() != null)
				lazyModelCag = new ForecastLazyModel(forecast.getProductosForecast());

			mostrarBotones = (!productosForecast.isEmpty());

			try {
				HttpSession session = Util.getSession();

				session.setAttribute("modulo_bitacora", ConstantesBitacora.FORECAST_COMPANIA);
				session.setAttribute("mes_bitacora", mesesMap.get(mesSeleccionadoIde));

			} catch (Exception e) {
				log.error("Error en ForecastCompaniaMB.consultar () ", e);
			}

		} catch (ExcepcionSOP ex) {
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}
	}

	/**
	 * 
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public void exportarAExcel() throws IOException {
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			GestorExcelForecast exportarExcel = new GestorExcelForecast();
			String nombreFile = "";
			boolean generarFile = false;

			if (!renderedExcel) {
				if (forecast != null) {
					if (retailerSeleccionadoIde == null) {
						boolean estado = estadoSupply;
						exportarExcel.generarArchivoExcelForecast(workbook, forecast,
								GestorExcelForecast.FORECAST_COMPANIA_SIN_RETAILER, estado, productoCategoria, null);
					} else {
						exportarExcel.generarArchivoExcelForecast(workbook, forecast,
								GestorExcelForecast.FORECAST_COMPANIA_CON_RETAILER, false, productoCategoria, null);
					}
				} else {
					throw new ExcepcionSOP("Debe primero generar la consulta antes de solicitar el archivo de excel.");
				}

				nombreFile = "ReporteForecastCompania.xls";
				generarFile = true;
			} else {
				List<ForecastVO> listForecast = forecastEJB.consultarForecastCompanhia(
						Arrays.asList(companiaSeleccionadaIde), Arrays.asList(mesSeleccionadoIde), selectedMarca);
				if (!listForecast.isEmpty()) {
					exportarExcel.generarArchivoExcelForecast(workbook, forecast,
							GestorExcelForecast.ESTADO_FORECAST_RETAILER, false, null, listForecast);
					nombreFile = "EstadosForecastRetailer.xls";
					generarFile = true;
				} else {
					throw new ExcepcionSOP("No se encontro informacion segun los filtros seleccionandos.");
				}
			}
			if (generarFile) {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				ExternalContext externalContext = facesContext.getExternalContext();
				externalContext.setResponseContentType("application/vnd.ms-excel");
				externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + nombreFile + "\"");
				workbook.write(externalContext.getResponseOutputStream());
				facesContext.responseComplete();
			}

		} catch (ExcepcionSOP ex) {
			Mensajes.mostrarMensajeError(ex, this.getClass());
		}
	}

	public void devolver() {

		try {
			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);

			validarFiltros();

			if (selectedRetailer.isEmpty()) {
				throw new ExcepcionSOP(EXCP_RETAIL_NULL);
			}

			List<String> listMarcas = moduloGeneralEJB.getMarcasByGrupo(selectedMarca);
			boolean devolvio = forecastEJB.devolverForecastCompania(companiaSeleccionada, listMarcas, mesSeleccionado,
					usuario, selectedMarca, grupoMarcaSeleccionadaIde, selectedRetailer);

			if (devolvio) {
				Mensajes.mostrarMensajeInfo(Mensajes.getMensaje(MSJ_PROC_EXITOSO));
			} else {
				Mensajes.mostrarMensajeError("Error al devolver forecast, consulte con su administrador");
			}
			limpiarCampos();

		} catch (Exception ex) {
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}
	}

	/**
	 * Carga la lista de retailers a partir de la compania seleccionada
	 */
	public void cargaListaRetailers() {

		try {
			retailersMap = new HashMap<>();

			if (companiaSeleccionadaIde != null) {
				retailers = moduloGeneralEJB.consultarRetailers(companiaSeleccionadaIde);
				for (Retailer item : retailers) {
					retailersMap.put(item.getId(), item);
				}
				retailersMap = Util.sortByComparator(retailersMap, true);
			}
		} catch (Exception e) {
			Mensajes.mostrarMensajeErrorNoManejado(e, this.getClass());
		}
	}

	/**
	 * 
	 */
	public void guardarNivelRetailer() {

		try {
			grupoMarcaSeleccionada = grupoMarcasMap.get(grupoMarcaSeleccionadaIde);
			retailerSeleccionado = retailersMap.get(retailerSeleccionadoIde);
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);

			if (grupoMarcaSeleccionada == null) {
				throw new ExcepcionSOP(EXCP_GRUPO_MARC_NULL);
			}
			if (retailerSeleccionado == null) {
				throw new ExcepcionSOP(EXCP_RETAIL_NULL);
			}

			if (mesSeleccionado == null) {
				throw new ExcepcionSOP(EXCP_MES_NULL);
			}

			if (productosForecast == null || productosForecast.isEmpty()) {
				throw new ExcepcionSOP(EXCP_NULL_PROD);
			}

			forecast.setFueProcesadoXCompania(true);
			forecastEJB.guardarForecastMeses(forecast, forecast.getEstado(), usuario, nombreMeses, productoCategoria);
			limpiarCampos();
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje(MSJ_PROC_EXITOSO));

		} catch (ExcepcionSOP ex) {
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

	}

	/**
	 * 
	 */
	@SuppressWarnings("deprecation")
	public void guardarNivelCompanhia() {

		try {
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);
			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);

			validarFiltros();

			if (productosForecast == null || productosForecast.isEmpty()) {
				throw new ExcepcionSOP(EXCP_NULL_PROD);
			}

			String grupoM = moduloGeneralEJB.getMarcasByIDGrupoMarca(selectedMarca);
			List<Retailer> retList = moduloGeneralEJB.consultarRetailers(companiaSeleccionada.getId());
			for (Retailer ret : retList) {
				List<Forecast> forecastDB = moduloGeneralEJB.consultarForecastIN(ret, grupoM, mesSeleccionado, null);
				forecastEJB.guardarEstadoCompania(forecastDB, usuario);
			}

			forecastEJB.notificarConfirmacionForecastCompania(forecast, grupoMarcaSeleccionadaIde, mesSeleccionado,
					companiaSeleccionada);

			HttpSession session = Util.getSession();
			String modulo = (String) session.getAttribute("modulo_bitacora");
			MesSop mes = (MesSop) session.getAttribute("mes_bitacora");
			if (!observacion.equals("")) {
				bitacora = new Bitacora();
				bitacora.setModulo(modulo);
				bitacora.setMesFk(mes);
				bitacora.setObservacion(observacion);
				bitacora.setUsuarioCrea(usuario.getUserName());
				bitacoraEJB.guardarRegistroBitacora(bitacora);
			}

			RequestContext context2 = RequestContext.getCurrentInstance();
			context2.execute("PF('confirmarDialog').hide()");
			limpiarCampos();
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje(MSJ_PROC_EXITOSO));

		} catch (ExcepcionSOP ex) {
			log.error("[guardarNivelCompanhia] Hubo un error ", ex);
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			log.error("[guardarNivelCompanhia] Hubo un error ", ex);
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

	}

	/**
	 * Cambio por el requerimiento de GrupoMarca
	 */
	public void guardarParcialNivelCompanhia() {

		try {
			grupoMarcaSeleccionada = grupoMarcasMap.get(grupoMarcaSeleccionadaIde);
			retailerSeleccionado = retailersMap.get(retailerSeleccionadoIde);
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);
			if (companiaSeleccionadaIde.equals("00072") || companiaSeleccionadaIde.equals("00074")) {
				companiaSeleccionadaIde = "00070";
			}
			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);
			if (grupoMarcaSeleccionada == null) {
				throw new ExcepcionSOP(EXCP_GRUPO_MARC_NULL);
			}
			if (companiaSeleccionada == null) {
				throw new ExcepcionSOP("Debe seleccionar Compañía");
			}
			if (mesSeleccionado == null) {
				throw new ExcepcionSOP(EXCP_MES_NULL);
			}

			if (productosForecast == null || productosForecast.isEmpty()) {
				throw new ExcepcionSOP(EXCP_NULL_PROD);
			}
			boolean confirmarForecast = false;

			String marca = moduloGeneralEJB.getMarcasByIDGrupoMarca(grupoMarcaSeleccionadaIde);
			List<MarcaCat2> listMarcas = moduloGeneralEJB.buscaMarcaPorIdGrupoMarca(grupoMarcaSeleccionadaIde);
			forecastEJB.guardarForecastCompania(this.companiaSeleccionadaIde, confirmarForecast, companiaSeleccionada,
					marca, mesSeleccionado, usuario, listMarcas, mesesForecastParametro);

			limpiarCampos();
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje(MSJ_PROC_EXITOSO));

		} catch (ExcepcionSOP ex) {
			log.error("[guardarParcialNivelCompanhia] Hubo un error ", ex);
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			log.error("[guardarParcialNivelCompanhia] Hubo un error ", ex);
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

	}

	/**
	 * 
	 */
	public void limpiarCampos() {
		marcaSeleccionadaIde = null;
		productosForecast.clear();
		forecast = null;
		estadoForecast = null;
		observacion = null;
		mostrarBotones = false;
		consultaRetailer = false;
		lazyModelCag = new ForecastLazyModel(new ArrayList<ProductosForecastMesVO>());
		hayForecastDevlKam = false;
		hayRetailersSinOtros = false;
	}

	/**
	 * 
	 * @param event
	 */
	public void onCellEdit(CellEditEvent event) {
		if (forecast != null) {
			ProductosForecastMesVO item = (ProductosForecastMesVO) ((DataTable) event.getComponent()).getRowData();
			moduloGeneralEJB.actualizarInfoTabla(item, forecast, nombreMeses, true,
					productoCategoria.get(item.getIdCategoria() + "-" + item.getIdRetailer()), true);
			forecastEJB.guardarForecastMeses(item, nombreMeses, new Retailer(item.getIdRetailer()), usuario);
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje(MSJ_PROC_EXITOSO));
		}
	}

	/**
	 * 
	 * @param item
	 */
	@SuppressWarnings("deprecation")
	public void cargarDialogo(ProductosForecastMesVO item) {

		RequestContext context2 = RequestContext.getCurrentInstance();
		nombreCategoria = item.getNombreProducto().replace("0_", "");
		lazyProductos = new ForecastLazyModel(productoCategoria.get(item.getIdProducto()).getProductosForecast());
		context2.execute("PF('carDialog').show()");
		verificarDimensionDatoMaestro();
	}

	/**
	 * 
	 */
	public void selecion() {
		upc = selectItems.contains("UPC");
		descripcion = selectItems.contains("Producto");
		modelo = selectItems.contains("Modelo");
		verificarDimensionDatoMaestro();
	}

	/**
	 * 
	 */
	@SuppressWarnings("deprecation")
	public void verificarDimensionDatoMaestro() {

		if (!upc && !descripcion && !modelo) {
			String script = "$('.dgProductos .ui-datatable-frozenlayout-left').css(\"width\", 0 + \"px\");\n"
					+ "$('.dgProductos .ui-datatable-frozen-container').css(\"width\", 0 + \"px\");\n";
			RequestContext.getCurrentInstance().execute(script);
		}
	}

	/**
	 * 
	 * @throws ExcepcionSOP
	 */
	private void validarFiltros() throws ExcepcionSOP {
		if (companiaSeleccionada == null) {
			throw new ExcepcionSOP("Debe seleccionar Compañía");
		}

		if (selectedMarca.isEmpty()) {
			throw new ExcepcionSOP(EXCP_GRUPO_MARC_NULL);
		}

		if (mesSeleccionado == null) {
			throw new ExcepcionSOP(EXCP_MES_NULL);
		}
	}

	/**
	 * 
	 * @return
	 */
	public boolean isInactivarEdit() {
		return inactivarEdit;
	}

	public void setInactivarEdit(boolean inactivarEdit) {
		this.inactivarEdit = inactivarEdit;
	}

	public ModuloGeneralEJB getModuloGeneralEJB() {
		return moduloGeneralEJB;
	}

	public void setModuloGeneralEJB(ModuloGeneralEJB moduloGeneralEJB) {
		this.moduloGeneralEJB = moduloGeneralEJB;
	}

	public ForecastEJB getForecastEJB() {
		return forecastEJB;
	}

	public void setForecastEJB(ForecastEJB forecastEJB) {
		this.forecastEJB = forecastEJB;
	}

	public void setUsuario(UsuarioVO usuario) {
		this.usuario = usuario;
	}

	public List<Compania> getCompanias() {
		return companias;
	}

	public void setCompanias(List<Compania> companias) {
		this.companias = companias;
	}

	public Compania getCompaniaSeleccionada() {
		return companiaSeleccionada;
	}

	public void setCompaniaSeleccionada(Compania companiaSeleccionada) {
		this.companiaSeleccionada = companiaSeleccionada;
	}

	public Map<String, Compania> getCompaniasMap() {
		return companiasMap;
	}

	public void setCompaniasMap(Map<String, Compania> companiasMap) {
		this.companiasMap = companiasMap;
	}

	public String getCompaniaSeleccionadaIde() {
		return companiaSeleccionadaIde;
	}

	public void setCompaniaSeleccionadaIde(String companiaSeleccionadaIde) {
		this.companiaSeleccionadaIde = companiaSeleccionadaIde;
	}

	public List<MarcaCat2> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<MarcaCat2> marcas) {
		this.marcas = marcas;
	}

	public MarcaCat2 getMarcaSeleccionada() {
		return marcaSeleccionada;
	}

	public void setMarcaSeleccionada(MarcaCat2 marcaSeleccionada) {
		this.marcaSeleccionada = marcaSeleccionada;
	}

	public Map<String, MarcaCat2> getMarcasMap() {
		return marcasMap;
	}

	public void setMarcasMap(Map<String, MarcaCat2> marcasMap) {
		this.marcasMap = marcasMap;
	}

	public String getMarcaSeleccionadaIde() {
		return marcaSeleccionadaIde;
	}

	public void setMarcaSeleccionadaIde(String marcaSeleccionadaIde) {
		this.marcaSeleccionadaIde = marcaSeleccionadaIde;
	}

	public List<MesSop> getMeses() {
		return meses;
	}

	public void setMeses(List<MesSop> meses) {
		this.meses = meses;
	}

	public MesSop getMesSeleccionado() {
		return mesSeleccionado;
	}

	public void setMesSeleccionado(MesSop mesSeleccionado) {
		this.mesSeleccionado = mesSeleccionado;
	}

	public Long getMesSeleccionadoIde() {
		return mesSeleccionadoIde;
	}

	public void setMesSeleccionadoIde(Long mesSeleccionadoIde) {
		this.mesSeleccionadoIde = mesSeleccionadoIde;
	}

	public Map<Long, MesSop> getMesesMap() {
		return mesesMap;
	}

	public void setMesesMap(Map<Long, MesSop> mesesMap) {
		this.mesesMap = mesesMap;
	}

	public List<ItemForecastRetailerVO> getProductosForecast() {
		return productosForecast;
	}

	public void setProductosForecast(List<ItemForecastRetailerVO> productosForecast) {
		this.productosForecast = productosForecast;
	}

	public List<Retailer> getRetailers() {
		return retailers;
	}

	public void setRetailers(List<Retailer> retailers) {
		this.retailers = retailers;
	}

	public Retailer getRetailerSeleccionado() {
		return retailerSeleccionado;
	}

	public void setRetailerSeleccionado(Retailer retailerSeleccionado) {
		this.retailerSeleccionado = retailerSeleccionado;
	}

	public Map<String, Retailer> getRetailersMap() {
		return retailersMap;
	}

	public void setRetailersMap(Map<String, Retailer> retailersMap) {
		this.retailersMap = retailersMap;
	}

	public String getRetailerSeleccionadoIde() {
		return retailerSeleccionadoIde;
	}

	public void setRetailerSeleccionadoIde(String retailerSeleccionadoIde) {
		this.retailerSeleccionadoIde = retailerSeleccionadoIde;
	}

	public boolean isConsultaRetailer() {
		return consultaRetailer;
	}

	public void setConsultaRetailer(boolean consultaRetailer) {
		this.consultaRetailer = consultaRetailer;
	}

	public String getEstadoForecast() {
		return estadoForecast;
	}

	public void setEstadoForecast(String estadoForecast) {
		this.estadoForecast = estadoForecast;
	}

	public boolean isForecastNivelCompania() {
		return forecastNivelCompania;
	}

	public void setForecastNivelCompania(boolean forecastNivelCompania) {
		this.forecastNivelCompania = forecastNivelCompania;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public int getMesesForecastParametro() {
		return mesesForecastParametro;
	}

	public void setMesesForecastParametro(int mesesForecastParametro) {
		this.mesesForecastParametro = mesesForecastParametro;
	}

	public List<String> getNombreMeses() {
		return nombreMeses;
	}

	public void setNombreMeses(List<String> nombreMeses) {
		this.nombreMeses = nombreMeses;
	}

	public ForecastXMesVO getForecast() {
		return forecast;
	}

	public void setForecast(ForecastXMesVO forecast) {
		this.forecast = forecast;
	}

	public boolean isMostrarBotones() {
		return mostrarBotones;
	}

	public void setMostrarBotones(boolean mostrarBotones) {
		this.mostrarBotones = mostrarBotones;
	}

	public boolean getEstadoSupply() {
		return estadoSupply;
	}

	public void setEstadoSupply(boolean estadoSupply) {
		this.estadoSupply = estadoSupply;
	}

	public String getMensajeForecast() {
		return mensajeForecast;
	}

	public void setMensajeForecast(String mensajeForecast) {
		this.mensajeForecast = mensajeForecast;
	}

	public List<Retailer> getRetailerForecast() {
		return retailerForecast;
	}

	public void setRetailerForecast(List<Retailer> retailerForecast) {
		this.retailerForecast = retailerForecast;
	}

	public String color(String producto) {
		String colorDefault = "color: lightslategray";
		String esta = moduloGeneralEJB.buscaEstadoProductoPorNombre(producto);
		if (esta == null || esta.equals("C")) {
			esta = moduloGeneralEJB.buscarEstadoDescontinuadoProducto(producto, companiaSeleccionadaIde, esta);
		}
		if (esta == null) {
			return colorDefault;
		} else if (esta.equals("D")) {
			return "color: red";
		} else if (esta.equals("L")) {
			return "color: lime";
		} else if (esta.equals("C")) {
			return colorDefault;
		} else {
			return colorDefault;
		}
	}

	/**
	 * 
	 * @return
	 */
	public void valRenderedExcel() {
		renderedExcel = selectedRetailer.isEmpty();
	}

	public List<GrupoMarca> getGrupoMarcas() {
		return grupoMarcas;
	}

	public void setGrupoMarcas(List<GrupoMarca> grupoMarcas) {
		this.grupoMarcas = grupoMarcas;
	}

	public GrupoMarca getGrupoMarcaSeleccionada() {
		return grupoMarcaSeleccionada;
	}

	public void setGrupoMarcaSeleccionada(GrupoMarca grupoMarcaSeleccionada) {
		this.grupoMarcaSeleccionada = grupoMarcaSeleccionada;
	}

	public LinkedHashMap<String, GrupoMarca> getGrupoMarcasMap() {
		return grupoMarcasMap;
	}

	public void setGrupoMarcasMap(LinkedHashMap<String, GrupoMarca> grupoMarcasMap) {
		this.grupoMarcasMap = grupoMarcasMap;
	}

	public String getGrupoMarcaSeleccionadaIde() {
		return grupoMarcaSeleccionadaIde;
	}

	public void setGrupoMarcaSeleccionadaIde(String grupoMarcaSeleccionadaIde) {
		this.grupoMarcaSeleccionadaIde = grupoMarcaSeleccionadaIde;
	}

	public boolean isUnicaCompania() {
		return unicaCompania;
	}

	public void setUnicaCompania(boolean unicaCompania) {
		this.unicaCompania = unicaCompania;
	}

	public LazyDataModel<ProductosForecastMesVO> getLazyModelCag() {
		return lazyModelCag;
	}

	public void setLazyModelCag(LazyDataModel<ProductosForecastMesVO> lazyModelCag) {
		this.lazyModelCag = lazyModelCag;
	}

	public LazyDataModel<ProductosForecastMesVO> getLazyProductos() {
		return lazyProductos;
	}

	public void setLazyProductos(LazyDataModel<ProductosForecastMesVO> lazyProductos) {
		this.lazyProductos = lazyProductos;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public List<String> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<String> selectItems) {
		this.selectItems = selectItems;
	}

	public boolean isUpc() {
		return upc;
	}

	public void setUpc(boolean upc) {
		this.upc = upc;
	}

	public boolean isModelo() {
		return modelo;
	}

	public void setModelo(boolean modelo) {
		this.modelo = modelo;
	}

	public boolean isDescripcion() {
		return descripcion;
	}

	public void setDescripcion(boolean descripcion) {
		this.descripcion = descripcion;
	}

	public List<String> getSelectedRetailer() {
		return selectedRetailer;
	}

	public void setSelectedRetailer(List<String> selectedRetailer) {
		this.selectedRetailer = selectedRetailer;
	}

	public List<String> getSelectedMarca() {
		return selectedMarca;
	}

	public void setSelectedMarca(List<String> selectedMarca) {
		this.selectedMarca = selectedMarca;
	}

	public boolean isHayForecastDevlKam() {
		return hayForecastDevlKam;
	}

	public void setHayForecastDevlKam(boolean hayForecastDevlKam) {
		this.hayForecastDevlKam = hayForecastDevlKam;
	}

	public boolean isHayRetailersSinOtros() {
		return hayRetailersSinOtros;
	}

	public void setHayRetailersSinOtros(boolean hayRetailersSinOtros) {
		this.hayRetailersSinOtros = hayRetailersSinOtros;
	}

	public boolean isRenderedExcel() {
		return renderedExcel;
	}

	public void setRenderedExcel(boolean renderedExcel) {
		this.renderedExcel = renderedExcel;
	}

}
