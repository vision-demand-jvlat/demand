/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.controladores;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.model.LazyDataModel;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlap.sop.demand.constantes.ConstantesDemand;
import com.jvlap.sop.demand.logica.ForecastEJB;
import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.vos.ForecastXMesVO;
import com.jvlap.sop.demand.logica.vos.ItemForecastRetailerVO;
import com.jvlap.sop.demand.logica.vos.ProductosForecastMesVO;
import com.jvlap.sop.demand.web.excel.GestorExcelForecast;
import com.jvlap.sop.demand.web.lazymodel.ForecastLazyModel;
import com.jvlap.sop.demand.web.utilitarios.Mensajes;
import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlat.sop.bitacora.constantes.ConstantesBitacora;
import com.jvlat.sop.bitacora.logica.BitacoraEJB;
import com.jvlat.sop.entidadessop.entidades.Bitacora;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.Forecast;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author iviasus
 */
@SuppressWarnings("serial")
@Named(value = "forecastGeneralMB")
@ViewScoped
public class ForecastGeneralMB implements Serializable {

	private static Logger log = LogManager.getLogger(ForecastGeneralMB.class.getName());

	@EJB
	ModuloGeneralEJB moduloGeneralEJB;

	@EJB
	private ForecastEJB forecastEJB;

	@EJB
	private BitacoraEJB bitacoraEJB;

	private UsuarioVO usuario = null;
	private List<Compania> companias = new ArrayList<>();
	private Compania companiaSeleccionada;
	private Map<String, Compania> companiasMap = new LinkedHashMap<>();
	private String companiaSeleccionadaIde;
	private boolean unicaCompania = false;

	private List<MarcaCat2> marcas;
	private MarcaCat2 marcaSeleccionada;
	private Map<String, MarcaCat2> marcasMap = new LinkedHashMap<>();
	private String marcaSeleccionadaIde;

	// ---------------------------------------------------------------------------------------------------------------------------
	// MCG Controles relacionados con GrupoMarca
	// ---------------------------------------------------------------------------------------------------------------------------
	private List<GrupoMarca> grupoMarcas;
	private GrupoMarca grupoMarcaSeleccionada;
	private LinkedHashMap<String, GrupoMarca> grupoMarcasMap = new LinkedHashMap<>();
	private String grupoMarcaSeleccionadaIde;
	private List<String> selectedMarca = new ArrayList<>();
	// ---------------------------------------------------------------------------------------------------------------------------

	private List<MesSop> meses;
	private MesSop mesSeleccionado;
	private Long mesSeleccionadoIde;
	private Map<Long, MesSop> mesesMap = new LinkedHashMap<>();

	private List<ItemForecastRetailerVO> productosForecast = new ArrayList<>();
	private boolean consultaRetailer;

	private LazyDataModel<ProductosForecastMesVO> lazyModelCag;

	private String estadoForecast = null;

	private boolean mostrarBotnConfirmar = false;
	private boolean mostrarBotn = false;
	private Bitacora bitacora;
	private String observacion = "";
	private int mesesForecastParametro;
	private List<String> nombreMeses;

	private ForecastXMesVO forecast;
	boolean estadoSupply = false;

	public ForecastGeneralMB() {
		//
	}

	@PostConstruct
	public void init() {
		usuario = getUsuario();

		companias = moduloGeneralEJB.consultarCompaniasUsuario(usuario);
		marcas = moduloGeneralEJB.consultarMarcas();
		meses = moduloGeneralEJB.consultarMesesSOP(false);
		grupoMarcas = moduloGeneralEJB.consultarGrupoMarcas();

		for (Compania item : companias) {
			companiasMap.put(item.getId(), item);
		}
		for (GrupoMarca item : grupoMarcas) {
			grupoMarcasMap.put(String.valueOf(item.getId()), item);
		}
		for (MarcaCat2 item : marcas) {
			marcasMap.put(item.getId(), item);
		}
		for (MesSop item : meses) {
			mesesMap.put(item.getId(), item);
		}

		if (companias.size() == 1) {
			companiaSeleccionadaIde = companias.get(0).getId();
			unicaCompania = true;
		}

	}

	public UsuarioVO getUsuario() {

		if (usuario == null) {
			usuario = new UsuarioVO();
			HttpSession session = Util.getSession();
			usuario.setId((Long) session.getAttribute("usrLogueadoId"));
			usuario.setNombre((String) session.getAttribute("usrLogueadoNom"));
			usuario.setUserName((String) session.getAttribute("usrLogueadoUserNom"));

			return usuario;
		} else {
			return usuario;
		}

	}

	public void consultar() {

		try {
			observacion = "";
			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);

			estadoSupply = false;

			if (selectedMarca.isEmpty()) {
				throw new ExcepcionSOP("Debe seleccionar Marca");
			}
			if (mesSeleccionado == null) {
				throw new ExcepcionSOP("Debe seleccionar Mes");
			}
			productosForecast.clear();

			String marcasGrupo = moduloGeneralEJB.getMarcasByIDGrupoMarca(selectedMarca);
			List<String> nombreMarcas = moduloGeneralEJB.getMarcasByGrupo(selectedMarca);
			boolean forecastCerrado = forecastEJB.consultarForecastCerrado(companiaSeleccionada, mesSeleccionado,
					marcasGrupo, true);
			productosForecast = forecastEJB.consultarForecastGeneralCompañia(companiaSeleccionada, mesSeleccionado,
					String.join(",", selectedMarca), grupoMarcaSeleccionadaIde, forecastCerrado);

			if (productosForecast.isEmpty()) {
				FacesMessage msg = new FacesMessage(
						"No se encontraron resultados para la consulta, por favor verifique que haya cerrado un forecast para la compañía seleccionada.");
				msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

			mesesForecastParametro = moduloGeneralEJB.consultarMeses(false, 0L);
			nombreMeses = moduloGeneralEJB.consultarNombresMeses(mesesForecastParametro, mesSeleccionado.getId());

			forecast = moduloGeneralEJB.armarListaPantallaNMeses(productosForecast, nombreMeses);

			if (!productosForecast.isEmpty()) {
				lazyModelCag = new ForecastLazyModel(forecast.getProductosForecast());
			}

			if (!productosForecast.isEmpty()) {
				if (companiaSeleccionada == null) {
					companias = moduloGeneralEJB.consultarCompanias();
				} else {
					companias.add(companiaSeleccionada);
				}

				forecast.setCompania(companiaSeleccionadaIde);

				Forecast forecast1 = moduloGeneralEJB.consultarForecastByCompania(marcasGrupo, mesSeleccionado,
						companias, forecastCerrado);
				if (forecast1 != null) {
					estadoForecast = forecast1.getEstadoFk().getCodigo();
				}
			}

			if (!productosForecast.isEmpty()) {
				if (estadoForecast != null && estadoForecast.equals(ConstantesDemand.EST_FORECAST_CNFD)) {
					if (moduloGeneralEJB.verificarSupply(Integer.parseInt(mesSeleccionado.getId().toString()),
							companiaSeleccionada, marcasGrupo)) {
						moduloGeneralEJB.llenarUnidadesSupply(forecast, nombreMeses, companiaSeleccionada,
								Integer.parseInt(mesSeleccionado.getId().toString()));
						moduloGeneralEJB.calcularTotalAMarca(forecast, nombreMeses, nombreMarcas);
						estadoSupply = true;
					}
				}
			}

			if (!productosForecast.isEmpty()) {
				if (estadoForecast != null && estadoForecast.equals(ConstantesDemand.EST_FORECAST_CNFD)) {
					estadoForecast = ConstantesDemand.EST_FORECAST_CERR;
				} else {
					estadoForecast = ConstantesDemand.EST_FORECAST_INI;
				}
			} else
				estadoForecast = null;

			mostrarBotnConfirmar = (companiaSeleccionada == null && !productosForecast.isEmpty());
			mostrarBotn = (!productosForecast.isEmpty());
			try {
				HttpSession session = Util.getSession();

				session.setAttribute("modulo_bitacora", ConstantesBitacora.FORECAST_GENERAL);
				session.setAttribute("mes_bitacora", mesesMap.get(mesSeleccionadoIde));

			} catch (Exception e) {
				log.error("Error en ForecastGeneralMB.consultar () ", e);
			}

		} catch (ExcepcionSOP ex) {
			log.error("Error en ForecastGeneralMB.consultar () ", ex);
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			log.error("Error en ForecastGeneralMB.consultar () ", ex);
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}
	}

	// Autor : Manuel Cortes Granados
	// Fecha : 17 Abril 2017 1:40 PM

	public void exportarAExcel() throws IOException {
		String compania = companiaSeleccionadaIde;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			GestorExcelForecast exportarExcel = new GestorExcelForecast();
			if (forecast != null) {
				exportarExcel.generarArchivoExcelForecast(workbook, forecast, GestorExcelForecast.FORECAST_GENERAL,
						estadoSupply, null, null);
			} else {
				throw new ExcepcionSOP("Debe primero generar la consulta antes de solicitar el archivo de excel.");
			}
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			externalContext.setResponseContentType("application/vnd.ms-excel");
			externalContext.setResponseHeader("Content-Disposition",
					"attachment; filename=\"ReporteForecastGeneral.xls\"");
			workbook.write(externalContext.getResponseOutputStream());
			facesContext.responseComplete();
		} catch (ExcepcionSOP ex) {
			log.error("[exportarAExcel] Error en exportarAExcel () ", ex);
			Mensajes.mostrarMensajeError(ex, this.getClass());
		}
	}

	public void guardar() {

		try {
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);
			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);
			if (companiaSeleccionada != null) {
				throw new ExcepcionSOP(
						"No es posible confirmar el Forecast de solo una Compañía, debe ser de todas las Compañías");
			}
			if (selectedMarca.isEmpty()) {
				throw new ExcepcionSOP("Debe seleccionar GrupoMarca");
			}
			if (mesSeleccionado == null) {
				throw new ExcepcionSOP("Debe seleccionar Mes");
			}
			if (productosForecast == null || productosForecast.isEmpty()) {
				throw new ExcepcionSOP("No hay productos para guardar");
			}

			String marcasGrupo = moduloGeneralEJB.getMarcasByIDGrupoMarca(selectedMarca);
			List<String> listMarcas = moduloGeneralEJB.getMarcasByGrupo(selectedMarca);
			forecastEJB.guardarForecastGeneral(null, marcasGrupo, mesSeleccionado, usuario, estadoForecast, listMarcas,
					mesesForecastParametro, grupoMarcaSeleccionadaIde);
			forecastEJB.notificarConfirmacionForecastGeneral(mesSeleccionado, grupoMarcaSeleccionadaIde, forecast);

			HttpSession session = Util.getSession();
			String modulo = (String) session.getAttribute("modulo_bitacora");
			MesSop mes = (MesSop) session.getAttribute("mes_bitacora");
			if (!observacion.equals("")) {
				bitacora = new Bitacora();
				bitacora.setModulo(modulo);
				bitacora.setMesFk(mes);
				bitacora.setObservacion(observacion);
				bitacora.setUsuarioCrea(usuario.getUserName());
				bitacoraEJB.guardarRegistroBitacora(bitacora);
			}
			limpiarCampos();
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje("msj_proc_exitoso"));
		} catch (ExcepcionSOP ex) {
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

	}

	public void devolver() {
		try {
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);
			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);
			if (companiaSeleccionada == null) {
				throw new ExcepcionSOP("Debe seleccionar Compañía");
			}
			if (selectedMarca.isEmpty()) {
				throw new ExcepcionSOP("Debe seleccionar GrupoMarca");
			}
			if (mesSeleccionado == null) {
				throw new ExcepcionSOP("Debe seleccionar Mes");
			}
			if (productosForecast == null || productosForecast.isEmpty()) {
				throw new ExcepcionSOP("No hay productos para guardar");
			}

			String marcasGrupo = moduloGeneralEJB.getMarcasByIDGrupoMarca(selectedMarca);
			List<String> l_marcas = moduloGeneralEJB.getMarcasByGrupo(selectedMarca);
			forecastEJB.devolverForecastGeneral(companiaSeleccionada, marcasGrupo, mesSeleccionado, usuario, l_marcas,
					mesesForecastParametro, grupoMarcaSeleccionadaIde, forecast);

			limpiarCampos();
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje("msj_proc_exitoso"));

		} catch (ExcepcionSOP ex) {
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}
	}

	public void limpiarCampos() {
		marcaSeleccionadaIde = null;
		productosForecast.clear();
		observacion = null;
		forecast = null;
		mostrarBotn = false;
		mostrarBotnConfirmar = false;
		estadoForecast = null;
		lazyModelCag = new ForecastLazyModel(new ArrayList<ProductosForecastMesVO>());
	}

	public ModuloGeneralEJB getModuloGeneralEJB() {
		return moduloGeneralEJB;
	}

	public void setModuloGeneralEJB(ModuloGeneralEJB moduloGeneralEJB) {
		this.moduloGeneralEJB = moduloGeneralEJB;
	}

	public ForecastEJB getForecastEJB() {
		return forecastEJB;
	}

	public void setForecastEJB(ForecastEJB forecastEJB) {
		this.forecastEJB = forecastEJB;
	}

	public List<Compania> getCompanias() {
		return companias;
	}

	public void setCompanias(List<Compania> companias) {
		this.companias = companias;
	}

	public Compania getCompaniaSeleccionada() {
		return companiaSeleccionada;
	}

	public void setCompaniaSeleccionada(Compania companiaSeleccionada) {
		this.companiaSeleccionada = companiaSeleccionada;
	}

	public Map<String, Compania> getCompaniasMap() {
		return companiasMap;
	}

	public void setCompaniasMap(Map<String, Compania> companiasMap) {
		this.companiasMap = companiasMap;
	}

	public String getCompaniaSeleccionadaIde() {
		return companiaSeleccionadaIde;
	}

	public void setCompaniaSeleccionadaIde(String companiaSeleccionadaIde) {
		this.companiaSeleccionadaIde = companiaSeleccionadaIde;
	}

	public List<MarcaCat2> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<MarcaCat2> marcas) {
		this.marcas = marcas;
	}

	public MarcaCat2 getMarcaSeleccionada() {
		return marcaSeleccionada;
	}

	public void setMarcaSeleccionada(MarcaCat2 marcaSeleccionada) {
		this.marcaSeleccionada = marcaSeleccionada;
	}

	public Map<String, MarcaCat2> getMarcasMap() {
		return marcasMap;
	}

	public void setMarcasMap(Map<String, MarcaCat2> marcasMap) {
		this.marcasMap = marcasMap;
	}

	public String getMarcaSeleccionadaIde() {
		return marcaSeleccionadaIde;
	}

	public void setMarcaSeleccionadaIde(String marcaSeleccionadaIde) {
		this.marcaSeleccionadaIde = marcaSeleccionadaIde;
	}

	public List<MesSop> getMeses() {
		return meses;
	}

	public void setMeses(List<MesSop> meses) {
		this.meses = meses;
	}

	public MesSop getMesSeleccionado() {
		return mesSeleccionado;
	}

	public void setMesSeleccionado(MesSop mesSeleccionado) {
		this.mesSeleccionado = mesSeleccionado;
	}

	public Long getMesSeleccionadoIde() {
		return mesSeleccionadoIde;
	}

	public void setMesSeleccionadoIde(Long mesSeleccionadoIde) {
		this.mesSeleccionadoIde = mesSeleccionadoIde;
	}

	public Map<Long, MesSop> getMesesMap() {
		return mesesMap;
	}

	public void setMesesMap(Map<Long, MesSop> mesesMap) {
		this.mesesMap = mesesMap;
	}

	public List<ItemForecastRetailerVO> getProductosForecast() {
		return productosForecast;
	}

	public void setProductosForecast(List<ItemForecastRetailerVO> productosForecast) {
		this.productosForecast = productosForecast;
	}

	public boolean isConsultaRetailer() {
		return consultaRetailer;
	}

	public void setConsultaRetailer(boolean consultaRetailer) {
		this.consultaRetailer = consultaRetailer;
	}

	public String getEstadoForecast() {
		return estadoForecast;
	}

	public void setEstadoForecast(String estadoForecast) {
		this.estadoForecast = estadoForecast;
	}

	public boolean isMostrarBotnConfirmar() {
		return mostrarBotnConfirmar;
	}

	public void setMostrarBotnConfirmar(boolean mostrarBotnConfirmar) {
		this.mostrarBotnConfirmar = mostrarBotnConfirmar;
	}

	public boolean isMostrarBotn() {
		return mostrarBotn;
	}

	public void setMostrarBotn(boolean mostrarBotn) {
		this.mostrarBotn = mostrarBotn;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public int getMesesForecastParametro() {
		return mesesForecastParametro;
	}

	public void setMesesForecastParametro(int mesesForecastParametro) {
		this.mesesForecastParametro = mesesForecastParametro;
	}

	public List<String> getNombreMeses() {
		return nombreMeses;
	}

	public void setNombreMeses(List<String> nombreMeses) {
		this.nombreMeses = nombreMeses;
	}

	public ForecastXMesVO getForecast() {
		return forecast;
	}

	public void setForecast(ForecastXMesVO forecast) {
		this.forecast = forecast;
	}

	public boolean isEstadoSupply() {
		return estadoSupply;
	}

	public void setEstadoSupply(boolean estadoSupply) {
		this.estadoSupply = estadoSupply;
	}

	public List<GrupoMarca> getGrupoMarcas() {
		return grupoMarcas;
	}

	public void setGrupoMarcas(List<GrupoMarca> grupoMarcas) {
		this.grupoMarcas = grupoMarcas;
	}

	public GrupoMarca getGrupoMarcaSeleccionada() {
		return grupoMarcaSeleccionada;
	}

	public void setGrupoMarcaSeleccionada(GrupoMarca grupoMarcaSeleccionada) {
		this.grupoMarcaSeleccionada = grupoMarcaSeleccionada;
	}

	public LinkedHashMap<String, GrupoMarca> getGrupoMarcasMap() {
		return grupoMarcasMap;
	}

	public void setGrupoMarcasMap(LinkedHashMap<String, GrupoMarca> grupoMarcasMap) {
		this.grupoMarcasMap = grupoMarcasMap;
	}

	public String getGrupoMarcaSeleccionadaIde() {
		return grupoMarcaSeleccionadaIde;
	}

	public void setGrupoMarcaSeleccionadaIde(String grupoMarcaSeleccionadaIde) {
		this.grupoMarcaSeleccionadaIde = grupoMarcaSeleccionadaIde;
	}

	public boolean isUnicaCompania() {
		return unicaCompania;
	}

	public void setUnicaCompania(boolean unicaCompania) {
		this.unicaCompania = unicaCompania;
	}

	public LazyDataModel<ProductosForecastMesVO> getLazyModelCag() {
		return lazyModelCag;
	}

	public void setLazyModelCag(LazyDataModel<ProductosForecastMesVO> lazyModelCag) {
		this.lazyModelCag = lazyModelCag;
	}

	public List<String> getSelectedMarca() {
		return selectedMarca;
	}

	public void setSelectedMarca(List<String> selectedMarca) {
		this.selectedMarca = selectedMarca;
	}
}
