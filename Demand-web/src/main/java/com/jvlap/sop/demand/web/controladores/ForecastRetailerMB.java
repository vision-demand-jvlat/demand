/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.controladores;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.model.LazyDataModel;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlap.sop.demand.constantes.ConstantesDemand;
import com.jvlap.sop.demand.logica.ForecastEJB;
import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.vos.ForecastXMesVO;
import com.jvlap.sop.demand.logica.vos.ItemForecastRetailerVO;
import com.jvlap.sop.demand.logica.vos.ProductosForecastMesVO;
import com.jvlap.sop.demand.web.excel.GestorExcelForecast;
import com.jvlap.sop.demand.web.lazymodel.ForecastLazyModel;
import com.jvlap.sop.demand.web.utilitarios.Mensajes;
import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlat.sop.bitacora.constantes.ConstantesBitacora;
import com.jvlat.sop.bitacora.logica.BitacoraEJB;
import com.jvlat.sop.entidadessop.entidades.Bitacora;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.entidadessop.entidades.Usuario;
import com.jvlat.sop.entidadessop.entidades.UsuariosXRol;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author iviasusx
 */
@SuppressWarnings("serial")
@Named(value = "forecastRetailerMB")
@ViewScoped
public class ForecastRetailerMB implements Serializable {

	private static Logger log = LogManager.getLogger(ForecastRetailerMB.class.getName());

	@EJB
	ModuloGeneralEJB moduloGeneralEJB;

	@EJB
	private ForecastEJB forecastEJB;

	@EJB
	private BitacoraEJB bitacoraEJB;

	private UsuarioVO usuario = null;
	private List<Compania> companias = new ArrayList<>();
	private Compania companiaSeleccionada;
	private Map<String, Compania> companiasMap = new LinkedHashMap<>();
	private String companiaSeleccionadaIde;
	private boolean unicaCompania = false;

	// ---------------------------------------------------------------------------------------------------------------------------
	// Controles relacionados con GrupoMarca
	// ---------------------------------------------------------------------------------------------------------------------------
	private List<GrupoMarca> grupoMarcas;
	private GrupoMarca grupoMarcaSeleccionada;
	private LinkedHashMap<String, GrupoMarca> grupoMarcasMap = new LinkedHashMap<>();
	private String grupoMarcaSeleccionadaIde;
	private List<String> selectedMarca = new ArrayList<>();
	// ---------------------------------------------------------------------------------------------------------------------------

	private List<MarcaCat2> marcas;
	private MarcaCat2 marcaSeleccionada;
	private Map<String, MarcaCat2> marcasMap = new LinkedHashMap<>();
	private String marcaSeleccionadaIde;

	private List<Retailer> retailers;
	private Retailer retailerSeleccionado;
	private Map<String, Retailer> retailersMap = new LinkedHashMap<>();
	private String retailerSeleccionadoIde;
	private List<String> selectedRetailer = new ArrayList<>();

	private List<MesSop> meses;
	private MesSop mesSeleccionado;
	private Long mesSeleccionadoIde;
	private Map<Long, MesSop> mesesMap = new LinkedHashMap<>();

	private List<ItemForecastRetailerVO> productosForecast;

	private ForecastXMesVO forecast;

	private String estadoForecast = null;

	// Control para inactivar boton de edicion
	private boolean inactivarEdit;
	private Bitacora bitacora;
	private String observacion = "";
	private int mesesForecastParametro;
	private List<String> nombreMeses;

	private LazyDataModel<ProductosForecastMesVO> lazyModelCag;
	private LazyDataModel<ProductosForecastMesVO> lazyProductos;
	private Map<String, ForecastXMesVO> productoCategoria;
	private String nombreCategoria;

	private List<String> selectItems;
	private boolean upc;
	private boolean modelo;
	private boolean descripcion;

	private boolean mostrarBotones = false;

	public ForecastRetailerMB() {
		//
	}

	@PostConstruct
	public void init() {
		log.info("[init] Inicio  init ForecastRetailerMB ");
		selectItems = Arrays.asList("Producto");
		upc = false;
		modelo = false;
		descripcion = true;
		boolean esRolForecast;
		usuario = getUsuario();
		productoCategoria = new LinkedHashMap<>();
		// Valida rol si es de tipo Forecast Retailer, sólo puede ver su propia compañía
		log.info("[init] Consulta ROL ForecastRetailerMB ");
		esRolForecast = validaRol(usuario);
		if (esRolForecast) {
			companias = Arrays.asList(usuario.getCompaniaId());
		} else {
			companias = moduloGeneralEJB.consultarCompaniasUsuario(usuario);
		}
		log.info("[init]  Consulta Marcas ForecastRetailerMB ");
		marcas = moduloGeneralEJB.consultarMarcas();
		log.info("[init]  Consulta Meses ForecastRetailerMB ");
		meses = moduloGeneralEJB.consultarMesesSOP(false);
		log.info("[init]  Consulta Grupos ForecastRetailerMB ");
		grupoMarcas = moduloGeneralEJB.consultarGrupoMarcas();

		for (Compania item : companias) {
			companiasMap.put(item.getId(), item);
		}

		for (GrupoMarca item : grupoMarcas) {
			grupoMarcasMap.put(String.valueOf(item.getId()), item);
		}

		for (MarcaCat2 item : marcas) {
			marcasMap.put(item.getId(), item);
		}

		for (MesSop item : meses) {
			mesesMap.put(item.getId(), item);
		}

		if (companias.size() == 1) {
			companiaSeleccionadaIde = companias.get(0).getId();
			cargaListaRetailers();
			unicaCompania = true;
		}

		bitacora = new Bitacora();

	}

	/**
	 * 
	 * @return
	 */
	public UsuarioVO getUsuario() {
		if (usuario == null) {
			usuario = new UsuarioVO();
			HttpSession session = Util.getSession();
			usuario.setId((Long) session.getAttribute("usrLogueadoId"));
			usuario.setNombre((String) session.getAttribute("usrLogueadoNom"));
			usuario.setUserName((String) session.getAttribute("usrLogueadoUserNom"));
			return usuario;
		} else {
			return usuario;
		}
	}

	/**
	 * 
	 * @param usuario
	 * @return
	 */
	private boolean validaRol(UsuarioVO usuario) {
		Usuario usuarioTM = new Usuario();
		usuarioTM.setId(usuario.getId().intValue());
		List<UsuariosXRol> usuarioXRolTM;
		log.info("[validaRol] Consulta ROL ForecastRetailerMB ");
		try {
			usuarioTM = moduloGeneralEJB.buscaUsuarioPorId(usuarioTM);
		} catch (Exception ex) {
			log.error("[validaRol] Error  validaRol", ex);
		}

		usuarioXRolTM = usuarioTM.getUsuariosXRolList();
		boolean result = false;
		if (usuarioXRolTM != null && usuarioXRolTM.size() == 1) {
			for (UsuariosXRol uxr : usuarioXRolTM) {
				if (uxr.getIdRol().getNombre().equals(ConstantesDemand.ROL_FORECAST_RETAILER)) {
					result = true;
					break;
				}
			}
		}
		usuario.setCompaniaId(usuarioTM.getCompaniaId());
		return result;
	}

	/**
	 * 
	 */
	public void consultar() {
		estadoForecast = null;
		productosForecast = null;
		forecast = null;
		productoCategoria = new LinkedHashMap<>();

		lazyModelCag = new ForecastLazyModel(new ArrayList<ProductosForecastMesVO>());

		boolean evaluaEstado = false;
		observacion = "";
		Long ini = System.currentTimeMillis();
		try {
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);
			companiaSeleccionada = companiasMap.get(companiaSeleccionadaIde);

			this.validaciones();

			String marcasGrupo = moduloGeneralEJB.getMarcasByIDGrupoMarca(selectedMarca);
			evaluaEstado = moduloGeneralEJB.consultarForecast(selectedRetailer, marcasGrupo, mesSeleccionado,
					"('CNFD','CNFG', 'ELSE', 'DEVL', 'CERR')");

			mesesForecastParametro = moduloGeneralEJB.consultarMeses(evaluaEstado, 0L);
			nombreMeses = moduloGeneralEJB.consultarNombresMeses(mesesForecastParametro, mesSeleccionado.getId());
			List<String> listMarcas = moduloGeneralEJB.getMarcasByGrupo(selectedMarca);
			productosForecast = forecastEJB.consultarForecastRetailer(selectedRetailer, mesSeleccionado, marcasGrupo,
					evaluaEstado, companiaSeleccionada, mesesForecastParametro, true, false, null, false);

			if (productosForecast.isEmpty()) {
				FacesMessage msg = new FacesMessage(
						"No se encontraron resultados para la consulta, por favor verifique que se haya realizado el proceso de Semanas Requeridas para esos filtros.");
				msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {
				forecast = moduloGeneralEJB.armarListaPantallaNMeses(productosForecast, nombreMeses);
				List<ProductosForecastMesVO> objTemp = new ArrayList<>();
				for (ProductosForecastMesVO oCategoria : forecast.getProductosForecast()) {
					if (!oCategoria.isEsTotalMarca()) {
						List<String> oIDRetailer = new ArrayList<>();
						oIDRetailer.add(oCategoria.getIdRetailer());
						List<ItemForecastRetailerVO> lstItemForecastRetailerVO = forecastEJB.consultarForecastRetailer(
								oIDRetailer, mesSeleccionado, marcasGrupo, evaluaEstado, companiaSeleccionada,
								mesesForecastParametro, false, true, oCategoria.getIdCategoria(), false);
						if (!lstItemForecastRetailerVO.isEmpty()) {
							ForecastXMesVO prod = moduloGeneralEJB.armarListaPantallaNMeses(lstItemForecastRetailerVO,
									nombreMeses);
							forecastEJB.actualizarCategoriaForecast(oCategoria, prod);
							productoCategoria.put(oCategoria.getIdProducto(), prod);
						} else {
							objTemp.add(oCategoria);
						}
					}
				}
				forecast.getProductosForecast().removeAll(objTemp);
				forecast = moduloGeneralEJB.calcularTotalAMarca(forecast, nombreMeses, listMarcas, selectedRetailer);

				lazyModelCag = new ForecastLazyModel(forecast.getProductosForecast());
				mostrarBotones = !productosForecast.isEmpty();
			}

			// Por un bug, la consulta trae más meses que la consulta del parámetro. debe
			// ser igual cuándo eso se corrija, de momento se preogunta si es menor o igual.
			boolean retailerSinMesesCompletos = false;

			if (forecast != null && forecast.getProductosForecast() != null
					&& !forecast.getProductosForecast().isEmpty()) {

				if (forecast.getProductosForecast() != null) {
					for (ProductosForecastMesVO productosForecastMesVO : forecast.getProductosForecast()) {

						if (productosForecastMesVO.getInformacionForecastMes().keySet().size() < nombreMeses.size()) {
							retailerSinMesesCompletos = true;
							break;
						}
					}
					log.info("[consultar] retailerSinMesesCompletos {}", retailerSinMesesCompletos);
				}

				if (retailerSinMesesCompletos) {

					FacesMessage msg = new FacesMessage(
							"Los meses marcados en rojo no tienen información de Sellout sugerido. "
									+ "Falta procesar y enviar en la pantalla de variación Sellout y Semanas Requeridas para los filtros escogidos. Por favor procese nuevamente");
					msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_FATAL);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}
			}

			if (!productosForecast.isEmpty()) {
				if (!evaluaEstado) {
					log.info("[consultar] Verifica devueltos KAM");
					boolean isDevueltoskam = moduloGeneralEJB.consultaRetailerDEVL_KAM(selectedRetailer, listMarcas,
							mesSeleccionado);
					if (isDevueltoskam) {
						estadoForecast = ConstantesDemand.EST_FORECAST_DEVL_KAM;
					} else {
						estadoForecast = ConstantesDemand.EST_FORECAST_INI;
					}
					inactivarEdit = false;
				} else {
					estadoForecast = ConstantesDemand.EST_FORECAST_CERR;
					inactivarEdit = true;
				}
			}

			HttpSession session = Util.getSession();
			session.setAttribute("modulo_bitacora", ConstantesBitacora.DEMAND_FORECAST_RETAILER);
			session.setAttribute("mes_bitacora", mesSeleccionado);

		} catch (ExcepcionSOP ex) {
			log.error("[consultar] Error en consultando la informacion", ex);
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			log.error("[consultar] Error en consultando la informacion", ex);
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}
		Long fin = System.currentTimeMillis();
		log.info("[consultar]  ===================== Tiempo de ejecucion Consultar Forecast Retailer {}", (fin - ini));
	}

	/**
	 * Guarda forecast, en caso de existir lo modifica si el estado es INIC
	 */
	public void guardarParcial() {

		try {
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);

			this.validaciones();

			if (productosForecast == null || productosForecast.isEmpty()) {
				throw new ExcepcionSOP("No hay productos para guardar");
			}
			forecastEJB.guardarForecastMeses(forecast, ConstantesDemand.EST_FORECAST_INI, usuario, nombreMeses,
					productoCategoria);
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje("msj_proc_exitoso"));
			limpiarCampos();
		} catch (ExcepcionSOP ex) {
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

	}

	/**
	 * Guarda el forecast y lo deja en estado Cerrado
	 */
	public void guardarEnviar() {
		try {
			mesSeleccionado = mesesMap.get(mesSeleccionadoIde);

			this.validaciones();

			if (productosForecast == null || productosForecast.isEmpty()) {
				throw new ExcepcionSOP("No hay productos para guardar");
			}

			moduloGeneralEJB.guardarEstadoForecast(forecast, ConstantesDemand.EST_FORECAST_ELSE, usuario,
					productoCategoria);
			forecastEJB.generarPreciosReatilerOtro(productoCategoria, companiaSeleccionada, mesSeleccionadoIde);

			forecastEJB.notificarCreacionForecastRetailer(mesSeleccionado, forecast, companiaSeleccionada);

			HttpSession session = Util.getSession();
			String modulo = (String) session.getAttribute("modulo_bitacora");
			MesSop mes = (MesSop) session.getAttribute("mes_bitacora");
			if (!observacion.equals("")) {
				bitacora.setModulo(modulo);
				bitacora.setMesFk(mes);
				bitacora.setObservacion(observacion);
				bitacora.setUsuarioCrea(usuario.getUserName());
				bitacoraEJB.guardarRegistroBitacora(bitacora);
				bitacora = new Bitacora();
			}
			limpiarCampos();
			Mensajes.mostrarMensajeInfo(Mensajes.getMensaje("msj_proc_exitoso"));

		} catch (ExcepcionSOP ex) {
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

	}

	/**
	 * 
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public void exportarAExcel() throws IOException {
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			GestorExcelForecast exportarExcel = new GestorExcelForecast();
			if (forecast != null) {
				exportarExcel.generarArchivoExcelForecastRetailerMod(workbook, forecast, productoCategoria,
						mesSeleccionado, marcasMap);
			} else {
				throw new ExcepcionSOP("Debe primero generar la consulta antes de solicitar el archivo de excel.");
			}
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			externalContext.setResponseContentType("application/vnd.ms-excel");
			externalContext.setResponseHeader("Content-Disposition",
					"attachment; filename=\"ReporteForecastRetailer.xls\"");
			workbook.write(externalContext.getResponseOutputStream());
			facesContext.responseComplete();
		} catch (ExcepcionSOP ex) {
			Mensajes.mostrarMensajeError(ex, this.getClass());
		}
	}

	/**
	 * 
	 */
	public void limpiarCampos() {
		estadoForecast = null;
		productosForecast.clear();
		forecast = null;
		observacion = "";
		mostrarBotones = false;
		grupoMarcaSeleccionadaIde = null;
		productoCategoria.clear();
		nombreCategoria = null;
		lazyModelCag = new ForecastLazyModel(new ArrayList<ProductosForecastMesVO>());
	}

	/**
	 * Carga la lista de retailers a partir de la compania seleccionada
	 */
	public void cargaListaRetailers() {

		try {
			retailersMap = new HashMap<>();

			if (companiaSeleccionadaIde != null) {
				retailers = moduloGeneralEJB.consultarRetailers(companiaSeleccionadaIde);
				for (Retailer item : retailers) {
					retailersMap.put(item.getId(), item);
				}
				retailersMap = Util.sortByComparator(retailersMap, true);
			}
		} catch (Exception e) {
			Mensajes.mostrarMensajeErrorNoManejado(e, this.getClass());
		}
	}

	/**
	 * 
	 * @param event
	 */
	public void onCellEdit(CellEditEvent event) {
		ProductosForecastMesVO item = (ProductosForecastMesVO) ((DataTable) event.getComponent()).getRowData();
		String msg = "Item selec " + item.getIdProducto() + " + " + item.getIndiceTabla();
		log.info("[onCellEdit] {}", msg);
		moduloGeneralEJB.actualizarInfoTabla(item, forecast, nombreMeses, false,
				productoCategoria.get(item.getIdCategoria() + "-" + item.getIdRetailer()), true);
	}

	@SuppressWarnings("deprecation")
	public void cargarDialogo(ProductosForecastMesVO item) {
		RequestContext context2 = RequestContext.getCurrentInstance();
		nombreCategoria = item.getNombreProducto().replaceFirst("0_", "");
		lazyProductos = new ForecastLazyModel(productoCategoria.get(item.getIdProducto()).getProductosForecast());
		context2.execute("PF('carDialog').show()");
		verificarDimensionDatoMaestro();
	}

	/**
	 * 
	 */
	public void selecion() {
		upc = selectItems.contains("UPC");
		descripcion = selectItems.contains("Producto");
		modelo = selectItems.contains("Modelo");
		verificarDimensionDatoMaestro();
	}

	@SuppressWarnings("deprecation")
	/**
	 * 
	 */
	public void verificarDimensionDatoMaestro() {

		if (!upc && !descripcion && !modelo) {
			String script = "$('.dgProductos .ui-datatable-frozenlayout-left').css(\"width\", 0 + \"px\");\n"
					+ "$('.dgProductos .ui-datatable-frozen-container').css(\"width\", 0 + \"px\");\n";
			RequestContext.getCurrentInstance().execute(script);
		}
	}

	/**
	 * 
	 * @param producto
	 * @return
	 */
	public String color(String producto) {
		String color = "color: lightslategray";
		String esta = moduloGeneralEJB.buscaEstadoProductoPorNombre(producto);
		if (esta == null || esta.equals("C")) {
			esta = moduloGeneralEJB.buscarEstadoDescontinuadoProducto(producto, companiaSeleccionadaIde, esta);
		}
		if (esta != null) {
			switch (esta) {
			case "L":
				color = "color: lime";
				break;
			case "D":
				color = "color: red";
				break;
			default:
				color = "color: lightslategray";
				break;
			}
		}
		return color;
	}

	/**
	 * 
	 * @param mes
	 * @return
	 */
	public boolean tieneDatosMes(String mes) {
		if (forecast != null && forecast.getProductosForecast() != null) {
			boolean tieneInfoMesRetailer = true;
			for (ProductosForecastMesVO productosForecastMesVO : forecast.getProductosForecast()) {
				if (!productosForecastMesVO.getInformacionForecastMes().containsKey(mes)) {
					tieneInfoMesRetailer = false;
					break;
				}
			}
			return tieneInfoMesRetailer;
		} else {
			return true;
		}
	}

	/**
	 * 
	 * @throws ExcepcionSOP
	 */
	private void validaciones() throws ExcepcionSOP {
		if (selectedMarca.isEmpty()) {
			throw new ExcepcionSOP("Debe Seleccionar Grupo Marca");
		}
		if (selectedRetailer.isEmpty()) {
			throw new ExcepcionSOP("Debe seleccionar Retailer");
		}
		if (mesSeleccionado == null) {
			throw new ExcepcionSOP("Debe seleccionar Mes");
		}
	}

	/********************************************************************************************************************
	 * SET y GET
	 *******************************************************************************************************************/

	public boolean isInactivarEdit() {
		return inactivarEdit;
	}

	public void setInactivarEdit(boolean inactivarEdit) {
		this.inactivarEdit = inactivarEdit;
	}

	public ModuloGeneralEJB getModuloGeneralEJB() {
		return moduloGeneralEJB;
	}

	public void setModuloGeneralEJB(ModuloGeneralEJB moduloGeneralEJB) {
		this.moduloGeneralEJB = moduloGeneralEJB;
	}

	public void setUsuario(UsuarioVO usuario) {
		this.usuario = usuario;
	}

	public List<Compania> getCompanias() {
		return companias;
	}

	public void setCompanias(List<Compania> companias) {
		this.companias = companias;
	}

	public Compania getCompaniaSeleccionada() {
		return companiaSeleccionada;
	}

	public void setCompaniaSeleccionada(Compania companiaSeleccionada) {
		this.companiaSeleccionada = companiaSeleccionada;
	}

	public Map<String, Compania> getCompaniasMap() {
		return companiasMap;
	}

	public void setCompaniasMap(Map<String, Compania> companiasMap) {
		this.companiasMap = companiasMap;
	}

	public String getCompaniaSeleccionadaIde() {
		return companiaSeleccionadaIde;
	}

	public void setCompaniaSeleccionadaIde(String companiaSeleccionadaIde) {
		this.companiaSeleccionadaIde = companiaSeleccionadaIde;
	}

	public List<MarcaCat2> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<MarcaCat2> marcas) {
		this.marcas = marcas;
	}

	public MarcaCat2 getMarcaSeleccionada() {
		return marcaSeleccionada;
	}

	public void setMarcaSeleccionada(MarcaCat2 marcaSeleccionada) {
		this.marcaSeleccionada = marcaSeleccionada;
	}

	public Map<String, MarcaCat2> getMarcasMap() {
		return marcasMap;
	}

	public void setMarcasMap(Map<String, MarcaCat2> marcasMap) {
		this.marcasMap = marcasMap;
	}

	public String getMarcaSeleccionadaIde() {
		return marcaSeleccionadaIde;
	}

	public void setMarcaSeleccionadaIde(String marcaSeleccionadaIde) {
		this.marcaSeleccionadaIde = marcaSeleccionadaIde;
	}

	public List<Retailer> getRetailers() {
		return retailers;
	}

	public void setRetailers(List<Retailer> retailers) {
		this.retailers = retailers;
	}

	public Retailer getRetailerSeleccionado() {
		return retailerSeleccionado;
	}

	public void setRetailerSeleccionado(Retailer retailerSeleccionado) {
		this.retailerSeleccionado = retailerSeleccionado;
	}

	public Map<String, Retailer> getRetailersMap() {
		return retailersMap;
	}

	public void setRetailersMap(Map<String, Retailer> retailersMap) {
		this.retailersMap = retailersMap;
	}

	public ForecastEJB getForecastEJB() {
		return forecastEJB;
	}

	public void setForecastEJB(ForecastEJB forecastEJB) {
		this.forecastEJB = forecastEJB;
	}

	public MesSop getMesSeleccionado() {
		return mesSeleccionado;
	}

	public void setMesSeleccionado(MesSop mesSeleccionado) {
		this.mesSeleccionado = mesSeleccionado;
	}

	public Long getMesSeleccionadoIde() {
		return mesSeleccionadoIde;
	}

	public void setMesSeleccionadoIde(Long mesSeleccionadoIde) {
		this.mesSeleccionadoIde = mesSeleccionadoIde;
	}

	public String getRetailerSeleccionadoIde() {
		return retailerSeleccionadoIde;
	}

	public void setRetailerSeleccionadoIde(String retailerSeleccionadoIde) {
		this.retailerSeleccionadoIde = retailerSeleccionadoIde;
	}

	public List<MesSop> getMeses() {
		return meses;
	}

	public void setMeses(List<MesSop> meses) {
		this.meses = meses;
	}

	public Map<Long, MesSop> getMesesMap() {
		return mesesMap;
	}

	public void setMesesMap(Map<Long, MesSop> mesesMap) {
		this.mesesMap = mesesMap;
	}

	public List<ItemForecastRetailerVO> getProductosForecast() {
		return productosForecast;
	}

	public void setProductosForecast(List<ItemForecastRetailerVO> productosForecast) {
		this.productosForecast = productosForecast;
	}

	public String getEstadoForecast() {
		return estadoForecast;
	}

	public void setEstadoForecast(String estadoForecast) {
		this.estadoForecast = estadoForecast;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public ForecastXMesVO getForecast() {
		return forecast;
	}

	public void setForecast(ForecastXMesVO forecast) {
		this.forecast = forecast;
	}

	public int getMesesForecastParametro() {
		return mesesForecastParametro;
	}

	public void setMesesForecastParametro(int mesesForecastParametro) {
		this.mesesForecastParametro = mesesForecastParametro;
	}

	public List<String> getNombreMeses() {
		return nombreMeses;
	}

	public void setNombreMeses(List<String> nombreMeses) {
		this.nombreMeses = nombreMeses;
	}

	/**
	 * 
	 * @param mes
	 * @return
	 */
	public boolean isMesActual(String mes) {
		if (mesSeleccionado != null) {
			return mes.equals(mesSeleccionado.getNombre());
		} else {
			return false;
		}
	}

	public List<GrupoMarca> getGrupoMarcas() {
		return grupoMarcas;
	}

	public void setGrupoMarcas(List<GrupoMarca> grupoMarcas) {
		this.grupoMarcas = grupoMarcas;
	}

	public GrupoMarca getGrupoMarcaSeleccionada() {
		return grupoMarcaSeleccionada;
	}

	public void setGrupoMarcaSeleccionada(GrupoMarca grupoMarcaSeleccionada) {
		this.grupoMarcaSeleccionada = grupoMarcaSeleccionada;
	}

	public Map<String, GrupoMarca> getGrupoMarcasMap() {
		return grupoMarcasMap;
	}

	public void setGrupoMarcasMap(Map<String, GrupoMarca> grupoMarcasMap) {
		this.grupoMarcasMap = (LinkedHashMap<String, GrupoMarca>) grupoMarcasMap;
	}

	public String getGrupoMarcaSeleccionadaIde() {
		return grupoMarcaSeleccionadaIde;
	}

	public void setGrupoMarcaSeleccionadaIde(String grupoMarcaSeleccionadaIde) {
		this.grupoMarcaSeleccionadaIde = grupoMarcaSeleccionadaIde;
	}

	public boolean isMostrarBotones() {
		return mostrarBotones;
	}

	public void setMostrarBotones(boolean mostrarBotones) {
		this.mostrarBotones = mostrarBotones;
	}

	public boolean isUnicaCompania() {
		return unicaCompania;
	}

	public void setUnicaCompania(boolean unicaCompania) {
		this.unicaCompania = unicaCompania;
	}

	public LazyDataModel<ProductosForecastMesVO> getLazyModelCag() {
		return lazyModelCag;
	}

	public void setLazyModelCag(LazyDataModel<ProductosForecastMesVO> lazyModelCag) {
		this.lazyModelCag = lazyModelCag;
	}

	public LazyDataModel<ProductosForecastMesVO> getLazyProductos() {
		return lazyProductos;
	}

	public void setLazyProductos(LazyDataModel<ProductosForecastMesVO> lazyProductos) {
		this.lazyProductos = lazyProductos;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public List<String> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<String> selectItems) {
		this.selectItems = selectItems;
	}

	public boolean isUpc() {
		return upc;
	}

	public void setUpc(boolean upc) {
		this.upc = upc;
	}

	public boolean isModelo() {
		return modelo;
	}

	public void setModelo(boolean modelo) {
		this.modelo = modelo;
	}

	public boolean isDescripcion() {
		return descripcion;
	}

	public void setDescripcion(boolean descripcion) {
		this.descripcion = descripcion;
	}

	public List<String> getSelectedRetailer() {
		return selectedRetailer;
	}

	public void setSelectedRetailer(List<String> selectedRetailer) {
		this.selectedRetailer = selectedRetailer;
	}

	public List<String> getSelectedMarca() {
		return selectedMarca;
	}

	public void setSelectedMarca(List<String> selectedMarca) {
		this.selectedMarca = selectedMarca;
	}
}
