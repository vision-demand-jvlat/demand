/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.controladores;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.jvlap.sop.demand.web.model.Dominio;
import com.jvlap.sop.demand.web.model.Funcionalidad;
import com.jvlap.sop.demand.web.model.SesionDTO;
import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlap.sop.web.client.AuthenticateClient;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author Roberto Osorio
 */
@ManagedBean(name = "loginMB")
@SessionScoped
public class LoginMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String user;
	private String clave;
	private MenuModel model;
	private UsuarioVO usuarioVO = null;
	private String urlNueva;
	private String etiqueta;
	private Long idDominioSelec;
	private List<Dominio> listDominio;

	private AuthenticateClient authenticateClient;

	private static final Logger log = LogManager.getLogger(LoginMB.class.getName());

	public LoginMB() {
		super();
		authenticateClient = new AuthenticateClient();
		listDominio = authenticateClient.getDominio();
	}

	@PostConstruct
	public void init() {
		if (usuarioVO == null) {
			usuarioVO = new UsuarioVO();
		}
		if (getValueFromSession("model") != null) {
			this.model = (MenuModel) getValueFromSession("model");
		}
		if (getValueFromSession("username") != null) {
			this.user = (String) getValueFromSession("username");
		}
		this.etiqueta = "SE";

	}

	// Operations
	public String cargaPaginas(String pagina) {
		return "/" + pagina;

	}

	public String validarUsuario() {
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
		@SuppressWarnings("unused")

		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance()
				.getExternalContext().getResponse();

		String ipAddress = httpServletRequest.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = httpServletRequest.getRemoteAddr();
		}

		SesionDTO sesion = authenticateClient.autenticar(this.user, this.clave, ipAddress, this.idDominioSelec);

		List<Funcionalidad> funcionalidadesXUsuario = sesion.getFuncionalidades();
		if (funcionalidadesXUsuario != null && !funcionalidadesXUsuario.isEmpty()) {
			usuarioVO.setId(sesion.getUsuario().getId().longValue());
			usuarioVO.setLoginUsuario(sesion.getUsuario().getUserName());
			usuarioVO.setNombre(sesion.getUsuario().getNombre());
			usuarioVO.setUserName(sesion.getUsuario().getUserName());
			String url = httpServletRequest.getRequestURL().toString();
			String uri = httpServletRequest.getRequestURI();
			urlNueva = url.replaceAll(uri, "");
			configuraMenu(funcionalidadesXUsuario);

			if (getValueFromSession(usuarioVO.getLoginUsuario()) != null) {
				removeValueFromSession(usuarioVO.getLoginUsuario());
			}

			HttpSession session = Util.getSession();
			session.setAttribute("username", this.user);
			session.setAttribute("model", this.model);
			session.setAttribute("urllogin", this.urlNueva);
			session.setAttribute("usrLogueadoId", usuarioVO.getId());
			session.setAttribute("usrLogueadoNom", usuarioVO.getNombre());
			session.setAttribute("usrLogueadoUserNom", usuarioVO.getUserName());

			for (Funcionalidad f : funcionalidadesXUsuario) {
				if (f.getPermisos() != null && !f.getPermisos().isEmpty()) {
					session.setAttribute(f.getNombre(), f.getPermisos());
				}
			}
			return "/menu";
		} else {
			return "/error";
		}
	}

	public String cerrarSesion() {

		HttpSession session = Util.getSession();
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		authenticateClient.cerrarSesion(this.user);

		session.invalidate();
		usuarioVO = null;
		try {
			ec.redirect(ec.getRequestContextPath() + "/faces/login.xhtml");
		} catch (Exception e) {
			log.error("[cerrarSesion] Hubo un error cerrando la sesion", e);
		}
		return null;
	}

	/**
	 * 
	 * @param funcionalidad
	 */
	private void configuraMenu(List<Funcionalidad> funcionalidad) {
		model = new DefaultMenuModel();

		DefaultSubMenu submenu;
		List<Funcionalidad> funcionalidadTM = funcionalidad;
		for (Funcionalidad funcio : funcionalidad) {
			if (funcio.getPadre() == null) {
				submenu = new DefaultSubMenu(funcio.getNombre(), funcio.getIcono());
				buscaOpcionesSubMenu(funcionalidadTM, submenu, funcio);
			}
		}
	}

	/**
	 * 
	 * @param funcionalidad
	 * @param submenu
	 * @param funcional
	 */
	private void buscaOpcionesSubMenu(List<Funcionalidad> funcionalidad, DefaultSubMenu submenu,
			Funcionalidad funcional) {
		DefaultMenuItem item;

		for (Funcionalidad funcio : funcionalidad) {
			if (funcio.getPadre() != null && funcio.getPadre().getId().compareTo(funcional.getId()) == 0) {
				item = new DefaultMenuItem(funcio.getNombre());
				item.setUrl(urlNueva + "/" + funcio.getURL());
				submenu.addElement(item);
			}
		}
		submenu.setStyle("height:auto");
		model.addElement(submenu);
	}

	/**
	 * 
	 * @param llave
	 * @return
	 */
	private Object getValueFromSession(String llave) {
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
		return httpServletRequest.getSession().getAttribute(llave);
	}

	private void removeValueFromSession(String llave) {
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
		httpServletRequest.getSession().removeAttribute(llave);
	}

	// Setter and Getter
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public MenuModel getModel() {
		return model;
	}

	public void setModel(MenuModel model) {
		this.model = model;
	}

	public List<Dominio> getListDominio() {
		return listDominio;
	}

	public void setListDominio(List<Dominio> listDominio) {
		this.listDominio = listDominio;
	}

	public Long getIdDominioSelec() {
		return idDominioSelec;
	}

	public void setIdDominioSelec(Long idDominioSelec) {
		this.idDominioSelec = idDominioSelec;
	}

}
