/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.controladores;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.vos.CategoriasVO;
import com.jvlap.sop.demand.logica.vos.GraficaSellOutVO;
import com.jvlap.sop.demand.logica.vos.InvRetailerPuntoVentaVO;
import com.jvlap.sop.demand.logica.vos.ParametrosDTO;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.GrupoCat4;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.PuntoDeVenta;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.reportes.logica.ReportesEJB;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author Roberto Osorio
 */
@SuppressWarnings("serial")
@ManagedBean(name = "reportesMB")
@SessionScoped
public class ReportesMB implements Serializable {

	@EJB
	ReportesEJB reportesEJB;

	private String idCompania;
	private String retailerId;
	private List<Retailer> listaRetailer;
	private List<Compania> listaCompania;
	private Long mesSeleccionado;
	private List<Long> meses = new ArrayList<>();
	private List<MesSop> listMeseAnio;
	private Map<String, MarcaCat2> marcasMap = new LinkedHashMap<>();
	private List<MarcaCat2> marcas;
	private List<GrupoCat4> listGrupoCat4;
	private List<PuntoDeVenta> listPuntoVenta;
	private String codigoPuntoVentaSeleccionado;
	private List<CategoriasVO> listaCategorias;

	private String[] puntoVentasSeleccionados;

	private String categoriaId;

	private String marcaSeleccionadaIde;

	JasperPrint jasperPrint;
	HttpServletResponse httpServletResponse;
	ServletOutputStream servletOutputStream;

	@EJB
	ModuloGeneralEJB moduloGeneralEJB;

	/**
	 * Creates a new instance of ReportesMB
	 */
	public ReportesMB() {
		//
	}

	@PostConstruct
	public void init() {

		this.getListaCompania();
		this.getListaRetailer();
		this.getListMeseAnio();
		this.getListPuntoVenta();

		marcas = moduloGeneralEJB.consultarMarcas();

		for (MarcaCat2 item : marcas) {
			marcasMap.put(item.getId(), item);
		}

	}

	/**
	 * Rutina que permite buscar los retailers de una compañía
	 */
	public void buscaRetailersPorCompania() {

		if (idCompania.equals("")) {
			this.listaRetailer = new ArrayList<>();
		}
		listaRetailer = reportesEJB.buscarListaRetailersPorCompania(idCompania);
	}

	/**
	 * Rutina que permite buscar los puntos deventa de un retailer
	 */
	public void buscaPuntoVentaPorRetailers() {

		if (retailerId.equals("")) {
			this.listPuntoVenta = new ArrayList<>();
		}
		listPuntoVenta = reportesEJB.buscaPuntoVentaPorRetailer(retailerId);
	}

	/**
	 * Obtiene nombre mes a partir del numero
	 *
	 * @param month
	 * @return
	 */
	public String getMesTexto(Long month) {
		return new DateFormatSymbols().getMonths()[month.intValue() - 1];
	}

	// List
	public List<Compania> getListaCompania() {
		if (listaCompania == null) {
			listaCompania = reportesEJB.listaCompanias();
		}
		return listaCompania;
	}

	public void setListaCompania(List<Compania> listaCompania) {
		this.listaCompania = listaCompania;
	}

	public String getIdCompania() {
		return idCompania;
	}

	public void setIdCompania(String idCompania) {
		this.idCompania = idCompania;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	public List<Retailer> getListaRetailer() {
		return listaRetailer;
	}

	public void setListaRetailer(List<Retailer> listaRetailer) {
		this.listaRetailer = listaRetailer;
	}

	public Long getMesSeleccionado() {
		return mesSeleccionado;
	}

	public void setMesSeleccionado(Long mesSeleccionado) {
		this.mesSeleccionado = mesSeleccionado;
	}

	public List<Long> getMeses() {
		return meses;
	}

	public void setMeses(List<Long> meses) {
		this.meses = meses;
	}

	public List<MesSop> getListMeseAnio() {
		if (listMeseAnio == null) {
			listMeseAnio = new ArrayList<>();
			listMeseAnio = reportesEJB.listaMesSop();
		}
		return listMeseAnio;
	}

	public void setListMeseAnio(List<MesSop> listMeseAnio) {
		this.listMeseAnio = listMeseAnio;
	}

	public Map<String, MarcaCat2> getMarcasMap() {
		return marcasMap;
	}

	public void setMarcasMap(Map<String, MarcaCat2> marcasMap) {
		this.marcasMap = marcasMap;
	}

	public List<MarcaCat2> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<MarcaCat2> marcas) {
		this.marcas = marcas;
	}

	public List<GrupoCat4> getListGrupoCat4() {
		if (listGrupoCat4 == null) {
			listGrupoCat4 = new ArrayList<>();
			listGrupoCat4 = reportesEJB.listaGrupoCat();
		}
		return listGrupoCat4;
	}

	public void setListGrupoCat4(List<GrupoCat4> listGrupoCat4) {
		this.listGrupoCat4 = listGrupoCat4;
	}

	public List<PuntoDeVenta> getListPuntoVenta() {
		return listPuntoVenta;
	}

	public void setListPuntoVenta(List<PuntoDeVenta> listPuntoVenta) {
		this.listPuntoVenta = listPuntoVenta;
	}

	public String getCodigoPuntoVentaSeleccionado() {
		return codigoPuntoVentaSeleccionado;
	}

	public void setCodigoPuntoVentaSeleccionado(String codigoPuntoVentaSeleccionado) {
		this.codigoPuntoVentaSeleccionado = codigoPuntoVentaSeleccionado;
	}

	public String[] getPuntoVentasSeleccionados() {
		return puntoVentasSeleccionados;
	}

	public void setPuntoVentasSeleccionados(String[] puntoVentasSeleccionados) {
		this.puntoVentasSeleccionados = puntoVentasSeleccionados;
	}

	public String getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(String categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getMarcaSeleccionadaIde() {
		return marcaSeleccionadaIde;
	}

	public void setMarcaSeleccionadaIde(String marcaSeleccionadaIde) {
		this.marcaSeleccionadaIde = marcaSeleccionadaIde;
	}

	public List<CategoriasVO> getListaCategorias() {
		if (listaCategorias == null) {
			listaCategorias = new ArrayList<>();
			listaCategorias = reportesEJB.listaCategorias();
		}
		return listaCategorias;
	}

	public void setListaCategorias(List<CategoriasVO> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void generaReporteVariacionCategoria() throws JRException, IOException {
		InputStream reportStream = ReportesMB.class
				.getResourceAsStream("/com/jvlat/sop/demand/web/reportes/RPT_VariacionXCategoria.jasper");
		if (reportStream == null) {
			FacesContext.getCurrentInstance().addMessage("Busca",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Archivo reporte no se encontró ", "Error"));
		} else {
			List<ParametrosDTO> parametrosDTOList = reportesEJB.datosReporteVariacionCategoria(idCompania, retailerId,
					mesSeleccionado);
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(parametrosDTOList);
			jasperPrint = JasperFillManager.fillReport(reportStream, new HashMap(), beanCollectionDataSource);
			HttpServletResponse httpServletResponse2 = (HttpServletResponse) FacesContext.getCurrentInstance()
					.getExternalContext().getResponse();
			OutputStream out = httpServletResponse2.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, out);
			FacesContext.getCurrentInstance().responseComplete();

			idCompania = "";
			retailerId = "";
			mesSeleccionado = null;

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void generaReporteGraficarVariacion() throws JRException, IOException {
		InputStream reportStream = ReportesMB.class
				.getResourceAsStream("/com/jvlat/sop/demand/web/reportes/RPT_GraficarVariacionSellout.jasper");
		if (reportStream == null) {
			FacesContext.getCurrentInstance().addMessage("Busca",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Archivo reporte no se encontró ", "Error"));
		} else {
			List<GraficaSellOutVO> graficaSellOutVOList = reportesEJB.datosReporteGraficarVariacion(idCompania,
					marcaSeleccionadaIde, categoriaId);
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(graficaSellOutVOList);
			jasperPrint = JasperFillManager.fillReport(reportStream, new HashMap(), beanCollectionDataSource);
			HttpServletResponse httpServletResponse2 = (HttpServletResponse) FacesContext.getCurrentInstance()
					.getExternalContext().getResponse();
			OutputStream out = httpServletResponse2.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, out);
			FacesContext.getCurrentInstance().responseComplete();
			marcaSeleccionadaIde = "";
			idCompania = "";
			categoriaId = "";
		}
	}

	/**
	 * 
	 * @throws JRException
	 * @throws IOException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void generaReporteInvRetailerPuntoVenta() throws JRException, IOException {
		InputStream reportStream = ReportesMB.class
				.getResourceAsStream("/com/jvlat/sop/demand/web/reportes/RPT_InvRetailerXPuntoVenta.jasper");
		if (reportStream == null) {
			FacesContext.getCurrentInstance().addMessage("Busca",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Archivo reporte no se encontró ", "Error"));
		} else {
			List<InvRetailerPuntoVentaVO> listInvRetailerPuntoVentaVO = reportesEJB.datosReporteInvRetailerPuntoVenta(
					idCompania, retailerId, mesSeleccionado, puntoVentasSeleccionados);
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
					listInvRetailerPuntoVentaVO);
			jasperPrint = JasperFillManager.fillReport(reportStream, new HashMap(), beanCollectionDataSource);
			HttpServletResponse httpServletResponse2 = (HttpServletResponse) FacesContext.getCurrentInstance()
					.getExternalContext().getResponse();
			OutputStream out = httpServletResponse2.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, out);
			FacesContext.getCurrentInstance().responseComplete();

			idCompania = "";
			retailerId = "";
			mesSeleccionado = null;
			puntoVentasSeleccionados = null;
		}

	}

}
