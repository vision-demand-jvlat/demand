package com.jvlap.sop.demand.web.controladores;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.util.Log;

import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.ScheduledProcessesEJB;
import com.jvlap.sop.demand.logica.vos.ScheduleVO;
import com.jvlap.sop.demand.web.model.ScheduleDTO;
import com.jvlap.sop.demand.web.utilitarios.Mensajes;
import com.jvlap.sop.demand.web.utilitarios.ScheduledProcessesUtil;
import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.login.vos.UsuarioVO;

@SuppressWarnings("serial")
@Named(value = "scheduledProcessesMB")
@ViewScoped
public class ScheduledProcessesMB implements Serializable {

	private UsuarioVO usuario = null;

	private Logger logger = LogManager.getLogger(ScheduledProcessesMB.class.getName());

	@EJB
	ModuloGeneralEJB moduloGeneralEJB;

	@EJB
	ScheduledProcessesEJB scheduledProcessesEJB;

	// Lista de filtro
	private List<String> selectMarcas;
	private String selectCompanias;
	private List<String> selectUsuario;
	private Date selectFechaCreacion;

	// Lista de formulario
	private List<String> listUsuario = new ArrayList<>();
	private Map<String, GrupoMarca> grupoMarcasMap = new LinkedHashMap<>();
	private Map<String, Compania> companiasMap = new LinkedHashMap<>();
	private Map<String, Retailer> retailersMap = new LinkedHashMap<>();
	private Map<Long, MesSop> mesesMap = new LinkedHashMap<>();
	private List<ScheduleDTO> listProcesos;

	private ScheduleDTO procesoActual;

	@PostConstruct
	public void init() {

		selectMarcas = new ArrayList<>();
		selectUsuario = new ArrayList<>();
		selectFechaCreacion = new Date();

		procesoActual = new ScheduleDTO();

		logger.info("[init] Inicio ScheduledProcessesMB");
		usuario = getUsuario();

		for (GrupoMarca item : moduloGeneralEJB.consultarGrupoMarcas()) {
			grupoMarcasMap.put(String.valueOf(item.getId()), item);
		}

		for (Compania item : moduloGeneralEJB.consultarCompaniasUsuario(usuario)) {
			companiasMap.put(item.getId(), item);
		}

		boolean isMesesFuturas = false;

		try {
			Map<String, String> permisos = Util.obtenerPermisosXFuncionalidad("Automatización de los Procesos");

			String txtMesFuturo = "";
			String txtFechaActivacion = "";

			if (!permisos.isEmpty()) {
				txtMesFuturo = permisos.get("MES_FUTUROS");
				txtFechaActivacion = permisos.get("FECHA_ACTIVACION");
			}

			if (txtMesFuturo != null) {
				isMesesFuturas = Boolean.parseBoolean(txtMesFuturo);
			}

			if (txtFechaActivacion != null) {
				LocalDate getActualDate = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				LocalDate getActivacionDate = (new SimpleDateFormat("dd/MM/yyyy").parse(txtFechaActivacion)).toInstant()
						.atZone(ZoneId.systemDefault()).toLocalDate();
				isMesesFuturas = isMesesFuturas && getActivacionDate.getDayOfMonth() <= getActualDate.getDayOfMonth();

			}

		} catch (Exception e) {
			Log.error("[init] Hubo un error consultando los posibles permisos", e);
		}
		for (MesSop item : moduloGeneralEJB.consultarMesesSOP(isMesesFuturas)) {
			mesesMap.put(item.getId(), item);
		}

	}

	/**
	 * 
	 */
	public void consultar() {
		try {
			this.procesoActual = new ScheduleDTO();
			listProcesos = obtenerProcesosProgramados(this.selectUsuario, this.selectMarcas, this.selectCompanias,
					this.selectFechaCreacion);
		} catch (Exception e) {
			logger.error("[consultar] Error consultando los procesos programados ", e);
		}
	}

	/**
	 * 
	 * @param inListUsuario
	 * @param inMarcas
	 * @param inCompania
	 * @param inFecha
	 * @return
	 */

	@SuppressWarnings("rawtypes")
	private List<ScheduleDTO> obtenerProcesosProgramados(List<String> inListUsuario, List<String> inMarcas,
			String inCompania, Date inFecha) {
		Map<String, List> inParametros = new HashMap<>();
		if (inListUsuario != null && !inListUsuario.isEmpty()) {
			inParametros.put("USER_ID", inListUsuario);
		}

		if (inMarcas != null && !inMarcas.isEmpty()) {
			inParametros.put("MARCAS", Arrays.asList(String.join(",", inMarcas)));
		}

		if (inCompania != null && !inCompania.isEmpty()) {
			inParametros.put("COMPANIAS", Arrays.asList(inCompania));
		}

		if (inFecha != null) {
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			inParametros.put("FECHA_EJECUCION", Arrays.asList(df.format(inFecha)));
		}

		List<ScheduleVO> inListScheduleVO = scheduledProcessesEJB.consultar(inParametros);

		List<ScheduleDTO> out = ScheduledProcessesUtil.scheduleVoToDTO(inListScheduleVO);
		Collections.sort(out);
		Collections.reverse(out);

		return out;
	}

	/**
	 * Metodo para consulta el usuario
	 * 
	 * @return
	 */
	private UsuarioVO getUsuario() {
		if (usuario == null) {
			usuario = new UsuarioVO();
			HttpSession session = Util.getSession();
			usuario.setId((Long) session.getAttribute("usrLogueadoId"));
			usuario.setNombre((String) session.getAttribute("usrLogueadoNom"));
			usuario.setUserName((String) session.getAttribute("usrLogueadoUserNom"));
			return usuario;
		} else {
			return usuario;
		}
	}

	/***
	 * 
	 * @param inCompania
	 */
	public void cargaListaRetailers(String inCompania) {
		try {
			retailersMap = new HashMap<>();

			if (inCompania != null) {
				List<Retailer> retailers = moduloGeneralEJB.consultarRetailers(inCompania);
				for (Retailer item : retailers) {
					retailersMap.put(item.getId(), item);
				}
				retailersMap = Util.sortByComparator(retailersMap, true);
			}
		} catch (Exception e) {
			Mensajes.mostrarMensajeErrorNoManejado(e, this.getClass());
		}
	}

	/**
	 * 
	 */
	public void actualizarProceso() {

		boolean informacionCompleta = true;

		if (procesoActual.getListMarcas().isEmpty()) {
			informacionCompleta = informacionCompleta && false;
			FacesMessage msg = new FacesMessage("Es requerido defini un grupo de marcas");
			msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		if (procesoActual.getCompanias() == null || procesoActual.getCompanias().equals("")) {
			informacionCompleta = informacionCompleta && false;
			FacesMessage msg = new FacesMessage("Es requerido selecciona una compañia");
			msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		if (procesoActual.getListRetailer().isEmpty()) {
			informacionCompleta = informacionCompleta && false;
			FacesMessage msg = new FacesMessage("Es requerido defini un grupo de retailer");
			msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		if (!procesoActual.isProcessSemanasRequeridas() && !procesoActual.isProcessVariacionSellOut()) {
			informacionCompleta = informacionCompleta && false;
			FacesMessage msg = new FacesMessage("Es requerido selecciona procesos para programar");
			msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		if (procesoActual.getFechaEjecucion() == null || !procesoActual.getFechaEjecucion().after(new Date())) {
			informacionCompleta = informacionCompleta && false;
			FacesMessage msg = new FacesMessage("Es requerido selecciona una fecha futura para programar el proceso");
			msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		if (informacionCompleta) {
			try {

				List<ScheduleVO> procesosVO = ScheduledProcessesUtil.scheduleDTOtoVO(usuario.getUserName(),
						procesoActual);
				if (procesosVO.isEmpty()) {
					FacesMessage msg = new FacesMessage("No existe proceso para Programar o Actualizar");
					msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				} else {
					if (scheduledProcessesEJB.merge(procesosVO)) {
						Mensajes.mostrarMensajeInfo(
								Mensajes.getMensaje("msj_proc_programado") + procesoActual.getCode());
						listProcesos = obtenerProcesosProgramados(this.selectUsuario, this.selectMarcas,
								this.selectCompanias, this.selectFechaCreacion);
					} else {
						FacesMessage msg = new FacesMessage("Hubo un error programando los procesos");
						msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_FATAL);
						FacesContext.getCurrentInstance().addMessage(null, msg);
					}
				}

			} catch (Exception e) {
				logger.error("[init] Error actualizando o programando los procesos programados ", e);

				FacesMessage msg = new FacesMessage("Hubo un error programando los procesos");
				msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_FATAL);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
		}

		consultar();
	}

	/**
	 * 
	 * @param inMes
	 * @return
	 */
	public String getMes(Long inMes) {
		MesSop mes = this.mesesMap.get(inMes);
		if (mes != null) {
			return mes.getAnio() + "-" + mes.getNombre();
		} else {
			return "";
		}
	}

	/**
	 * 
	 * @param inIdCompania
	 * @return
	 */
	public String getCompania(String inIdCompania) {
		Compania compania = this.companiasMap.get(inIdCompania);
		if (compania != null) {
			return compania.getId() + "-" + compania.getNombre();
		} else {
			return "";
		}
	}

	/**
	 * 
	 * @param inMarcas
	 * @return
	 */
	public String getMarcas(List<String> inMarcas) {
		List<String> out = new ArrayList<>();
		for (String m : inMarcas) {
			GrupoMarca temp = grupoMarcasMap.get(m);
			if (temp != null) {
				out.add(temp.getId() + "-" + temp.getNombre());
			}

		}

		return String.join(",", out);
	}

	/**
	 * 
	 */
	public void nuevoProceso() {
		this.procesoActual = new ScheduleDTO();

		if (companiasMap.size() == 1) {
			Optional<String> firstKey = companiasMap.keySet().stream().findFirst();
			if (firstKey.isPresent()) {
				String key = firstKey.get();
				procesoActual.setCompanias(companiasMap.get(key).getId());
				cargaListaRetailers(procesoActual.getCompanias());
			}

		}
	}

	/**
	 * Metodo para cancelar el proceso actual
	 */
	public void cancelarProceso() {
		if (this.procesoActual != null) {
			try {
				List<ScheduleVO> procesosVO = ScheduledProcessesUtil.scheduleDTOtoVOCancelado(procesoActual);
				if (procesosVO.isEmpty()) {
					FacesMessage msg = new FacesMessage("No se permite cancelar el proceso " + procesoActual.getCode());
					msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				} else {
					if (scheduledProcessesEJB.merge(procesosVO)) {
						Mensajes.mostrarMensajeInfo(
								Mensajes.getMensaje("msj_proc_cancelado") + procesoActual.getCode());

						listProcesos = obtenerProcesosProgramados(this.selectUsuario, this.selectMarcas,
								this.selectCompanias, this.selectFechaCreacion);
					} else {
						FacesMessage msg = new FacesMessage(
								"Hubo un error cancelando el proceso " + procesoActual.getCode());
						msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_FATAL);
						FacesContext.getCurrentInstance().addMessage(null, msg);
					}
				}

			} catch (Exception e) {
				FacesMessage msg = new FacesMessage("Hubo un error cancelando el proceso " + procesoActual.getCode());
				msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_FATAL);
				FacesContext.getCurrentInstance().addMessage(null, msg);
				logger.error("[cancelarProceso] Error cancelando el proceso programado ", e);
			}
		}

	}

	/**
	 * 
	 */
	public void cargaListaUsuario() {
		listUsuario = moduloGeneralEJB.consultarUsuarioCompania(this.selectCompanias);
	}

	/**
	 * 
	 */

	public ModuloGeneralEJB getModuloGeneralEJB() {
		return moduloGeneralEJB;
	}

	public void setModuloGeneralEJB(ModuloGeneralEJB moduloGeneralEJB) {
		this.moduloGeneralEJB = moduloGeneralEJB;
	}

	public Map<String, GrupoMarca> getGrupoMarcasMap() {
		return grupoMarcasMap;
	}

	public void setGrupoMarcasMap(LinkedHashMap<String, GrupoMarca> grupoMarcasMap) {
		this.grupoMarcasMap = grupoMarcasMap;
	}

	public Map<String, Compania> getCompaniasMap() {
		return companiasMap;
	}

	public void setCompaniasMap(Map<String, Compania> companiasMap) {
		this.companiasMap = companiasMap;
	}

	public Map<String, Retailer> getRetailersMap() {
		return retailersMap;
	}

	public void setRetailersMap(Map<String, Retailer> retailersMap) {
		this.retailersMap = retailersMap;
	}

	public Map<Long, MesSop> getMesesMap() {
		return mesesMap;
	}

	public void setMesesMap(Map<Long, MesSop> mesesMap) {
		this.mesesMap = mesesMap;
	}

	public void setUsuario(UsuarioVO usuario) {
		this.usuario = usuario;
	}

	public List<ScheduleDTO> getListProcesos() {
		return listProcesos;
	}

	public void setListProcesos(List<ScheduleDTO> listProcesos) {
		this.listProcesos = listProcesos;
	}

	public ScheduleDTO getProcesoActual() {
		return procesoActual;
	}

	public void setProcesoActual(ScheduleDTO procesoActual) {
		if (procesoActual != null && procesoActual.getCompanias() != null) {
			this.cargaListaRetailers(procesoActual.getCompanias());
		}
		this.procesoActual = procesoActual;
	}

	public List<String> getSelectMarcas() {
		return selectMarcas;
	}

	public void setSelectMarcas(List<String> selectMarcas) {
		this.selectMarcas = selectMarcas;
	}

	public String getSelectCompanias() {
		return selectCompanias;
	}

	public void setSelectCompanias(String selectCompanias) {
		this.selectCompanias = selectCompanias;
	}

	public List<String> getSelectUsuario() {
		return selectUsuario;
	}

	public void setSelectUsuario(List<String> selectUsuario) {
		this.selectUsuario = selectUsuario;
	}

	public Date getSelectFechaCreacion() {
		return selectFechaCreacion;
	}

	public void setSelectFechaCreacion(Date selectFechaCreacion) {
		this.selectFechaCreacion = selectFechaCreacion;
	}

	public List<String> getListUsuario() {
		return listUsuario;
	}

	public void setListUsuario(List<String> listUsuario) {
		this.listUsuario = listUsuario;
	}

	public void setGrupoMarcasMap(Map<String, GrupoMarca> grupoMarcasMap) {
		this.grupoMarcasMap = grupoMarcasMap;
	}

}
