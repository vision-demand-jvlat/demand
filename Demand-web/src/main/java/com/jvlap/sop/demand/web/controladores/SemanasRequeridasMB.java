/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.controladores;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.util.Log;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.model.LazyDataModel;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.SemanasRequeridasEJB;
import com.jvlap.sop.demand.logica.vos.ItemSemanasRequeridasVO;
import com.jvlap.sop.demand.logica.vos.SemanasRequeridasVO;
import com.jvlap.sop.demand.web.lazymodel.SemanasLazyModel;
import com.jvlap.sop.demand.web.utilitarios.Mensajes;
import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlat.sop.bitacora.constantes.ConstantesBitacora;
import com.jvlat.sop.bitacora.logica.BitacoraEJB;
import com.jvlat.sop.entidadessop.entidades.Bitacora;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.entidadessop.entidades.SellInSugeridoCategoriaTMP;
import com.jvlat.sop.entidadessop.entidades.SellInSugeridoProductoTMP;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author Seidor
 */
@SuppressWarnings("serial")
@Named(value = "semanasRequeridasMB")
@ViewScoped
public class SemanasRequeridasMB implements Serializable {

	private static Logger log = LogManager.getLogger(SemanasRequeridasMB.class.getName());

	@EJB
	private ModuloGeneralEJB moduloGeneralEJB;
	@EJB
	private SemanasRequeridasEJB semanasEJB;
	@EJB
	private BitacoraEJB bitacoraEJB;

	private List<Compania> companias = new ArrayList<>();
	private Compania companiaSeleccionada;
	private LinkedHashMap<String, Compania> companiasMap = new LinkedHashMap<>();
	private String companiaSeleccionadaIde;
	private boolean unicaCompania = false;

	// ---------------------------------------------------------------------------------------------------------------------------
	// MCG Controles relacionados con GrupoMarca
	// ---------------------------------------------------------------------------------------------------------------------------
	private List<GrupoMarca> grupoMarcas;
	private GrupoMarca grupoMarcaSeleccionada;
	private LinkedHashMap<String, GrupoMarca> grupoMarcasMap = new LinkedHashMap<>();
	private List<String> grupoMarcaSeleccionadaIde;
	// ---------------------------------------------------------------------------------------------------------------------------

	private List<MarcaCat2> marcas;
	private MarcaCat2 marcaSeleccionada;
	private LinkedHashMap<String, MarcaCat2> marcasMap = new LinkedHashMap<>();
	private String marcaSeleccionadaIde;

	private List<Retailer> retailers;
	private Retailer retailerSeleccionado;
	private LinkedHashMap<String, Retailer> retailersMap = new LinkedHashMap<>();
	private List<String> retailerSeleccionadoIde;

	private List<MesSop> meses;
	private Long mesSeleccionado;
	private Map<Long, MesSop> mesesMap = new LinkedHashMap<>();

	private List<SemanasRequeridasVO> productos;
	private ArrayList<String> mesesTabla;
	private UsuarioVO usuarioLogueado;

	private boolean editaSemana = false;
	private String estiloEditaSemana;
	private SemanasRequeridasVO itemTemp;
	private Bitacora bitacora;
	private String observacion = "";

	private boolean mostrarBotnConfirmar = false;

	private List<String> nombreMeses;
	private boolean mostrarBotn = false;

	// Controladores LazyModel para la vista.
	private LazyDataModel<SemanasRequeridasVO> lazyCategoria;
	private LazyDataModel<SemanasRequeridasVO> lazyProducto;
	private Map<String, List<SemanasRequeridasVO>> prodCategoria;
	private String nomCategoria;
	private List<String> selectItems;
	private boolean upc;
	private boolean modelo;
	private boolean descripcion;
	private boolean isEditCategoria;

	/**
	 * Metodo que inicializa variables para el backing
	 */
	@PostConstruct
	public void init() {

		selectItems = new ArrayList<>();
		selectItems.add("Producto");
		editaSemana = false;
		estiloEditaSemana = "font-weight:bold";
		usuarioLogueado = new UsuarioVO();
		HttpSession session = Util.getSession();
		usuarioLogueado.setId((Long) session.getAttribute("usrLogueadoId"));
		usuarioLogueado.setNombre((String) session.getAttribute("usrLogueadoNom"));
		usuarioLogueado.setUserName((String) session.getAttribute("usrLogueadoUserNom"));
		bitacora = new Bitacora();
		prodCategoria = new LinkedHashMap<>();
		upc = false;
		modelo = false;
		descripcion = true;
		isEditCategoria = false;
		try {

			boolean isMesesFuturas = false;

			try {
				Map<String, String> permisos = Util.obtenerPermisosXFuncionalidad("Semanas Requeridas");

				String txtMesFuturo = "";
				String txtFechaActivacion = "";

				if (!permisos.isEmpty()) {
					txtMesFuturo = permisos.get("MES_FUTUROS");
					txtFechaActivacion = permisos.get("FECHA_ACTIVACION");
				}

				if (txtMesFuturo != null) {
					isMesesFuturas = Boolean.parseBoolean(txtMesFuturo);
				}

				if (txtFechaActivacion != null) {
					LocalDate getActualDate = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					LocalDate getActivacionDate = (new SimpleDateFormat("dd/MM/yyyy").parse(txtFechaActivacion))
							.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					isMesesFuturas = isMesesFuturas
							&& getActivacionDate.getDayOfMonth() <= getActualDate.getDayOfMonth();

					log.info(isMesesFuturas + " && " + getActivacionDate.getDayOfMonth() + " <= "
							+ getActualDate.getDayOfMonth());

				}

			} catch (Exception e) {
				Log.error("[init] Hubo un error consultando los posibles permisos", e);
			}

			companias = moduloGeneralEJB.consultarCompaniasUsuario(usuarioLogueado);
			marcas = moduloGeneralEJB.consultarMarcas();
			grupoMarcas = moduloGeneralEJB.consultarGrupoMarcas();
			meses = moduloGeneralEJB.consultarMesesSOP(isMesesFuturas);

			for (Compania item : companias) {
				companiasMap.put(item.getId(), item);
			}
			for (GrupoMarca item : grupoMarcas) {
				grupoMarcasMap.put(String.valueOf(item.getId()), item);
			}
			for (MarcaCat2 item : marcas) {
				marcasMap.put(item.getId(), item);
			}
			for (MesSop item : meses) {
				mesesMap.put(item.getId(), item);
			}

			if (companias.size() == 1) {
				companiaSeleccionadaIde = companias.get(0).getId();
				cargaListaRetailers();
				unicaCompania = true;
			}

		} catch (Exception e) {
			log.error("[init()] Error inicializando el controlador", e);
		}
	}

	/**
	 * Consulta las semanas requeridas para el calculo de SellIn sugerido En este
	 * metodo se consulta categorias y productos, solo teniendo en cuenta que se
	 * visualizara en pantalla unicamente categorias.
	 *
	 * @return
	 */
	public String consultarSemanasRequeridas() {

		try {
			// Inicializacion meses
			mesesTabla = new ArrayList<>();
			prodCategoria = new LinkedHashMap<>();
			nombreMeses = moduloGeneralEJB.consultarNombresMeses(4, mesSeleccionado);
			// Actualiza mes y anio para el siguiente query
			for (String mes : nombreMeses) {
				mesesTabla.add(mes != null ? mes : "Mes");
			}
			List<SemanasRequeridasVO> objTemp = new ArrayList<>();

			List<SemanasRequeridasVO> listProducto = new ArrayList<>();

			for (String r : retailerSeleccionadoIde) {
				for (String g : grupoMarcaSeleccionadaIde) {
					String marcasGrupo = moduloGeneralEJB.getMarcasByIDGrupoMarca(g);
					List<SemanasRequeridasVO> aux = semanasEJB.consultarSemanas(mesSeleccionado, r, marcasGrupo,
							companiaSeleccionadaIde, false, null);
					listProducto.addAll(aux);
				}
			}

			for (SemanasRequeridasVO oCategoria : listProducto) {
				List<SemanasRequeridasVO> prdProductos = semanasEJB.consultarSemanas(mesSeleccionado,
						oCategoria.getRetailer().split("-")[0],
						moduloGeneralEJB.getMarcasByIDGrupoMarca(grupoMarcaSeleccionadaIde), companiaSeleccionadaIde,
						true, oCategoria);

				if (!prdProductos.isEmpty()) {
					String id = oCategoria.getRetailer().concat("-").concat(oCategoria.getCategoria());
					prodCategoria.put(id, prdProductos);
				} else {
					objTemp.add(oCategoria);
				}
			}

			listProducto.removeAll(objTemp);
			String re = "";

			productos = new ArrayList<>();
			for (SemanasRequeridasVO p : listProducto) {
				if (re.equals("") || !re.equals(p.getRetailer())) {
					re = p.getRetailer();
					productos.add(new SemanasRequeridasVO(re, "", re, re, "", "", false, "", "", ""));
				}
				productos.add(p);
			}

			lazyCategoria = new SemanasLazyModel(productos);

			if (productos.isEmpty()) {
				FacesMessage msg = new FacesMessage(
						"No se encontraron resultados para la consulta, por favor verifique que se haya realizado el proceso de Variation Sellout para esos filtros.  En el caso de que el proceso de Variación Sellout se haya llevado a cabo de la manera correcta, haga click en el botón Procesar.");
				msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_FATAL);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

			observacion = "";
			mostrarBotnConfirmar = true;
			mostrarBotn = !productos.isEmpty();
		} catch (ExcepcionSOP ex) {
			Mensajes.mostrarMensajeError(ex, this.getClass());
		} catch (Exception ex) {
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}
		try {
			HttpSession session = Util.getSession();
			session.setAttribute("modulo_bitacora", ConstantesBitacora.DEMAND_SEMANAS);
			session.setAttribute("mes_bitacora", this.mesesMap.get(mesSeleccionado));
		} catch (Exception e) {
			log.error("[consultarSemanasRequeridas] Error en VariacionMB.consultar () ", e);
		}

		return null;
	}

	/**
	 * Recalcula las semanas requeridas para el calculo de SellIn sugerido
	 *
	 * @return
	 */
	public String recalcularSemanasRequeridas() {

		try {

			mesesTabla = new ArrayList<>(); // Inicializacion meses
			prodCategoria = new LinkedHashMap<>();
			nombreMeses = moduloGeneralEJB.consultarNombresMeses(4, mesSeleccionado); // Actualiza mes y anio para el
																						// siguiente query
			for (String mes : nombreMeses) { // Actualiza mes y anio para el siguiente query
				mesesTabla.add(mes != null ? mes : "Mes");
			}

			String marcasGrupo = moduloGeneralEJB.getMarcasByIDGrupoMarca(grupoMarcaSeleccionadaIde);
			List<String> listMarcas = moduloGeneralEJB.getMarcaGrupoPorIdGrupoMarca(grupoMarcaSeleccionadaIde);

			productos = new ArrayList<>();
			List<SemanasRequeridasVO> listProducto = new ArrayList<>();
			for (String retailer : retailerSeleccionadoIde) {
				List<SemanasRequeridasVO> aux = semanasEJB.procesarSemanasRequeridas(mesSeleccionado, retailer,
						marcasGrupo, companiaSeleccionadaIde, listMarcas, prodCategoria, productos);
				listProducto.addAll(aux);
			}

			consultarSemanasRequeridas();

			if (!productos.isEmpty()) {
				lazyCategoria = new SemanasLazyModel(productos);
				mostrarBotn = !productos.isEmpty();
			} else {
				lazyCategoria = new SemanasLazyModel(new ArrayList<SemanasRequeridasVO>());
			}

		} catch (Exception ex) {
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

		return null;
	}

	/**
	 * Almacena en el sistema las semanas ingresadas por el usuario
	 *
	 * @return
	 */
	public String guardarSemanasRequeridas() {

		try {
			Compania compaTM = moduloGeneralEJB.buscaCompaniaPorId(companiaSeleccionadaIde);
			semanasEJB.guardarSemanasRequeridas(compaTM, mesSeleccionado, retailerSeleccionadoIde, usuarioLogueado,
					grupoMarcaSeleccionadaIde);
			HttpSession session = Util.getSession();
			String modulo = (String) session.getAttribute("modulo_bitacora");
			MesSop mes = (MesSop) session.getAttribute("mes_bitacora");
			if (!observacion.equals("")) {
				bitacora.setModulo(modulo);
				bitacora.setMesFk(mes);
				bitacora.setObservacion(observacion);
				bitacora.setUsuarioCrea(usuarioLogueado.getUserName());
				bitacoraEJB.guardarRegistroBitacora(bitacora);
				observacion = "";
				bitacora = new Bitacora();
			}
			FacesMessage msg = new FacesMessage("Semanas requeridas almacenadas");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			limpiarCampos();
		} catch (Exception ex) {
			log.error("[guardarSemanasRequeridas] Hubo un error guardando las semanas requeridas", ex);
			Mensajes.mostrarMensajeErrorNoManejado(ex, this.getClass());
		}

		return null;
	}

	/**
	 * Encargado de recibir los valores de las filas de la tabla Semanas requeridas
	 * cuando hay cambios a nivel de categoria.
	 *
	 * @param event
	 */
	public void onCellEditCategoria(CellEditEvent event) {

		SemanasRequeridasVO semanasReqVO = (SemanasRequeridasVO) ((DataTable) event.getComponent()).getRowData();
		SellInSugeridoCategoriaTMP sellInSugeCate = new SellInSugeridoCategoriaTMP();
		isEditCategoria = true;

		ItemSemanasRequeridasVO valueMes = semanasReqVO.getInformacionSemanasMes().get(mesesTabla.get(0));
		sellInSugeCate.setId(valueMes.getVarCateg().longValue());
		sellInSugeCate.setSemanasRequeridas(valueMes.getSemanasRequeridas());
		sellInSugeCate.setSellin(valueMes.getSellIn());
		semanasEJB.actualizarSellInCategoria(sellInSugeCate);

		semanasEJB.calcularSemanasRequeridas(mesSeleccionado, semanasReqVO.getRetailer(), semanasReqVO.getMarca(), 1);

		FacesMessage msg = new FacesMessage("Categoria " + semanasReqVO.getCategoriaProducto() + " actualizada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	/**
	 * Encargado de recibir los valores de las filas de la tabla Semanas requeridas
	 * cuando hay cambios a nivel de producto.
	 *
	 * @param event
	 */
	public void onCellEditProducto(CellEditEvent event) {

		SemanasRequeridasVO semanasReqVO = (SemanasRequeridasVO) ((DataTable) event.getComponent()).getRowData();
		ItemSemanasRequeridasVO valueMes = semanasReqVO.getInformacionSemanasMes().get(mesesTabla.get(0));
		SellInSugeridoProductoTMP sellInSugeProdu = new SellInSugeridoProductoTMP();
		isEditCategoria = false;

		sellInSugeProdu.setId(valueMes.getVarProd().longValue());
		sellInSugeProdu.setSemanasRequeridas(valueMes.getSemanasRequeridas());
		sellInSugeProdu.setSellin(valueMes.getSellIn());
		semanasEJB.actualizarSellInProducto(sellInSugeProdu);
		SemanasRequeridasVO oCategoria = semanasEJB.buscarCategoria(semanasReqVO.getCategoria(), productos);
		if (oCategoria != null) {
			String id = semanasReqVO.getRetailer().concat("-").concat(semanasReqVO.getCategoria());
			semanasEJB.actualizarCategoriasSemanas(prodCategoria.get(id), oCategoria);
		}
		FacesMessage msg = new FacesMessage("Producto " + semanasReqVO.getCategoriaProducto() + " actualizado");
		FacesContext.getCurrentInstance().addMessage(null, msg);

	}

	/**
	 * Actualiza la grilla a partir de los cambios realizados a nivel de categoria o
	 * producto.
	 *
	 * @throws java.lang.Exception
	 */
	public void actualizarGrid() throws Exception {

		if (isEditCategoria) {
			String marcasGrupo = moduloGeneralEJB.getMarcasByIDGrupoMarca(grupoMarcaSeleccionadaIde);

			productos = new ArrayList<>();
			for (String r : retailerSeleccionadoIde) {
				List<SemanasRequeridasVO> aux = semanasEJB.consultarSemanas(mesSeleccionado, r, marcasGrupo,
						companiaSeleccionadaIde, false, null);
				productos.addAll(aux);
			}
			prodCategoria = new LinkedHashMap<>();
			for (SemanasRequeridasVO oCategoria : productos) {
				String id = oCategoria.getRetailer().concat("-").concat(oCategoria.getCategoria());
				prodCategoria.put(id, semanasEJB.consultarSemanas(mesSeleccionado, oCategoria.getRetailer(),
						marcasGrupo, companiaSeleccionadaIde, true, oCategoria));
			}
			lazyCategoria = new SemanasLazyModel(productos);
		}
	}

	/**
	 * Carga el dialogo y el LazyDataModel con el listado de productos, segun la
	 * categoria seleccionada.
	 *
	 * @param item
	 */
	@SuppressWarnings("deprecation")
	public void cargarDialogo(SemanasRequeridasVO item) {

		RequestContext context2 = RequestContext.getCurrentInstance();
		nomCategoria = item.getCategoriaProducto().replaceFirst("0_", "");
		String id = item.getRetailer().concat("-").concat(item.getCategoria());
		lazyProducto = new SemanasLazyModel(prodCategoria.get(id));
		context2.execute("PF('carDialog').show()");
		verificarDimensionDatoMaestro();
	}

	public void selecion() {

		upc = selectItems.contains("UPC");
		descripcion = selectItems.contains("Producto");
		modelo = selectItems.contains("Modelo");
		verificarDimensionDatoMaestro();
	}

	@SuppressWarnings("deprecation")
	public void verificarDimensionDatoMaestro() {

		if (!upc && !descripcion && !modelo) {
			String script = "$('.dgProductos .ui-datatable-frozenlayout-left').css(\"width\", 0 + \"px\");\n"
					+ "$('.dgProductos .ui-datatable-frozen-container').css(\"width\", 0 + \"px\");\n";
			RequestContext.getCurrentInstance().execute(script);
		}
	}

	/**
	 * Carga la lista de retailers a partir de la compania seleccionada
	 */
	public void cargaListaRetailers() {

		retailersMap = new LinkedHashMap<>();
		try {
			if (companiaSeleccionadaIde != null) {
				retailers = moduloGeneralEJB.consultarRetailers(companiaSeleccionadaIde);
				for (Retailer item : retailers) {
					retailersMap.put(item.getId(), item);
				}
			}
		} catch (Exception e) {
			log.error("[cargaListaRetailers] Error en VariacionMB.cargaListaRetailers() ", e);
		}
	}

	/////////////////// Objetos Static
	/**
	 * Objeto utilizado para presentar los datos de inventario, sellout, sellin, etc
	 * de manera dinamica en la pantalla
	 */
	static public class DatosSemanaColumn implements Serializable {

		private String header;
		private String property;

		public DatosSemanaColumn(String header, String property) {
			this.header = header;
			this.property = property;
		}

		public String getHeader() {
			return header;
		}

		public String getProperty() {
			return property;
		}
	}

	/////////////////// Getters and Setters
	public List<Compania> getCompanias() {
		return companias;
	}

	public Compania getCompaniaSeleccionada() {
		return companiaSeleccionada;
	}

	public void setCompanias(List<Compania> companias) {
		this.companias = companias;
	}

	public void setCompaniaSeleccionada(Compania companiaSeleccionada) {
		this.companiaSeleccionada = companiaSeleccionada;
	}

	public LinkedHashMap<String, Compania> getCompaniasMap() {
		return companiasMap;
	}

	public void setCompaniasMap(LinkedHashMap<String, Compania> companiasMap) {
		this.companiasMap = companiasMap;
	}

	public String getCompaniaSeleccionadaIde() {
		return companiaSeleccionadaIde;
	}

	public void setCompaniaSeleccionadaIde(String companiaSeleccionadaIde) {
		this.companiaSeleccionadaIde = companiaSeleccionadaIde;
	}

	public List<MarcaCat2> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<MarcaCat2> marcas) {
		this.marcas = marcas;
	}

	public MarcaCat2 getMarcaSeleccionada() {
		return marcaSeleccionada;
	}

	public void setMarcaSeleccionada(MarcaCat2 marcaSeleccionada) {
		this.marcaSeleccionada = marcaSeleccionada;
	}

	public LinkedHashMap<String, MarcaCat2> getMarcasMap() {
		return marcasMap;
	}

	public void setMarcasMap(LinkedHashMap<String, MarcaCat2> marcasMap) {
		this.marcasMap = marcasMap;
	}

	public String getMarcaSeleccionadaIde() {
		return marcaSeleccionadaIde;
	}

	public void setMarcaSeleccionadaIde(String marcaSeleccionadaIde) {
		this.marcaSeleccionadaIde = marcaSeleccionadaIde;
	}

	public List<Retailer> getRetailers() {
		return retailers;
	}

	public void setRetailers(List<Retailer> retailers) {
		this.retailers = retailers;
	}

	public Retailer getRetailerSeleccionado() {
		return retailerSeleccionado;
	}

	public void setRetailerSeleccionado(Retailer retailerSeleccionado) {
		this.retailerSeleccionado = retailerSeleccionado;
	}

	public LinkedHashMap<String, Retailer> getRetailersMap() {
		return retailersMap;
	}

	public void setRetailersMap(LinkedHashMap<String, Retailer> retailersMap) {
		this.retailersMap = retailersMap;
	}

	public List<String> getRetailerSeleccionadoIde() {
		return retailerSeleccionadoIde;
	}

	public void setRetailerSeleccionadoIde(List<String> retailerSeleccionadoIde) {
		this.retailerSeleccionadoIde = retailerSeleccionadoIde;
	}

	public List<MesSop> getMeses() {
		return meses;
	}

	public void setMeses(List<MesSop> meses) {
		this.meses = meses;
	}

	public Long getMesSeleccionado() {
		return mesSeleccionado;
	}

	public void setMesSeleccionado(Long mesSeleccionado) {
		this.mesSeleccionado = mesSeleccionado;
	}

	public Map<Long, MesSop> getMesesMap() {
		return mesesMap;
	}

	public void setMesesMap(Map<Long, MesSop> mesesMap) {
		this.mesesMap = mesesMap;
	}

	public List<SemanasRequeridasVO> getProductos() {
		return productos;
	}

	public void setProductos(List<SemanasRequeridasVO> productos) {
		this.productos = productos;
	}

	public ArrayList<String> getMesesTabla() {
		return mesesTabla;
	}

	public void setMesesTabla(ArrayList<String> mesesTabla) {
		this.mesesTabla = mesesTabla;
	}

	public boolean isEditaSemana() {
		return editaSemana;
	}

	public void setEditaSemana(boolean editaSemana) {
		this.editaSemana = editaSemana;
	}

	public String getEstiloEditaSemana() {
		return estiloEditaSemana;
	}

	public void setEstiloEditaSemana(String estiloEditaSemana) {
		this.estiloEditaSemana = estiloEditaSemana;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String verificaFlag(int flag) {
		if (flag == 1) {
			estiloEditaSemana = "font-weight:bold";
		} else {
			estiloEditaSemana = "font-weight:normal";
		}
		return estiloEditaSemana;
	}

	public boolean isMostrarBotnConfirmar() {
		return mostrarBotnConfirmar;
	}

	public void setMostrarBotnConfirmar(boolean mostrarBotnConfirmar) {
		this.mostrarBotnConfirmar = mostrarBotnConfirmar;
	}

	public boolean isMostrarBotn() {
		return mostrarBotn;
	}

	public void setMostrarBotn(boolean mostrarBotn) {
		this.mostrarBotn = mostrarBotn;
	}

	public String color(String producto) {
		String esta = moduloGeneralEJB.buscarEstadoDescontinuadoProducto(producto, companiaSeleccionadaIde, null);
		if (esta == null) {
			return "color: lightslategray";
		} else if (esta.equals("D")) {
			return "color: red";
		} else if (esta.equals("L")) {
			return "color: lime";
		} else if (esta.equals("C")) {
			return "color: lightslategray";
		} else {
			return "color: lightslategray";
		}
	}

	public void limpiarCampos() {
		companiaSeleccionadaIde = null;
		marcaSeleccionadaIde = null;
		retailerSeleccionadoIde = null;
		mesSeleccionado = null;
		mostrarBotnConfirmar = false;
		mostrarBotn = false;
		grupoMarcaSeleccionadaIde = null;
		this.productos.clear();
	}

	public List<GrupoMarca> getGrupoMarcas() {
		return grupoMarcas;
	}

	public void setGrupoMarcas(List<GrupoMarca> grupoMarcas) {
		this.grupoMarcas = grupoMarcas;
	}

	public GrupoMarca getGrupoMarcaSeleccionada() {
		return grupoMarcaSeleccionada;
	}

	public void setGrupoMarcaSeleccionada(GrupoMarca grupoMarcaSeleccionada) {
		this.grupoMarcaSeleccionada = grupoMarcaSeleccionada;
	}

	public LinkedHashMap<String, GrupoMarca> getGrupoMarcasMap() {
		return grupoMarcasMap;
	}

	public void setGrupoMarcasMap(LinkedHashMap<String, GrupoMarca> grupoMarcasMap) {
		this.grupoMarcasMap = grupoMarcasMap;
	}

	public List<String> getGrupoMarcaSeleccionadaIde() {
		return grupoMarcaSeleccionadaIde;
	}

	public void setGrupoMarcaSeleccionadaIde(List<String> grupoMarcaSeleccionadaIde) {
		this.grupoMarcaSeleccionadaIde = grupoMarcaSeleccionadaIde;
	}

	public boolean isUnicaCompania() {
		return unicaCompania;
	}

	public void setUnicaCompania(boolean unicaCompania) {
		this.unicaCompania = unicaCompania;
	}

	public LazyDataModel<SemanasRequeridasVO> getLazyCategoria() {
		return lazyCategoria;
	}

	public void setLazyCategoria(LazyDataModel<SemanasRequeridasVO> lazyCategoria) {
		this.lazyCategoria = lazyCategoria;
	}

	public LazyDataModel<SemanasRequeridasVO> getLazyProducto() {
		return lazyProducto;
	}

	public void setLazyProducto(LazyDataModel<SemanasRequeridasVO> lazyProducto) {
		this.lazyProducto = lazyProducto;
	}

	public String getNomCategoria() {
		return nomCategoria;
	}

	public void setNomCategoria(String nomCategoria) {
		this.nomCategoria = nomCategoria;
	}

	public boolean isUpc() {
		return upc;
	}

	public void setUpc(boolean upc) {
		this.upc = upc;
	}

	public boolean isModelo() {
		return modelo;
	}

	public void setModelo(boolean modelo) {
		this.modelo = modelo;
	}

	public boolean isDescripcion() {
		return descripcion;
	}

	public void setDesripcion(boolean descripcion) {
		this.descripcion = descripcion;
	}

	public List<String> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<String> selectItems) {
		this.selectItems = selectItems;
	}
}
