/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.controladores;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jfree.util.Log;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.Visibility;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;
import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.VariacionEJB;
import com.jvlap.sop.demand.logica.vos.VariacionSellOutVO;
import com.jvlap.sop.demand.web.excel.GestorExcelForecast;
import com.jvlap.sop.demand.web.lazymodel.VariacionLazyModel;
import com.jvlap.sop.demand.web.utilitarios.Mensajes;
import com.jvlap.sop.demand.web.utilitarios.Util;
import com.jvlat.sop.bitacora.constantes.ConstantesBitacora;
import com.jvlat.sop.bitacora.logica.BitacoraEJB;
import com.jvlat.sop.entidadessop.entidades.Compania;
import com.jvlat.sop.entidadessop.entidades.GrupoMarca;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.entidadessop.entidades.Retailer;
import com.jvlat.sop.login.vos.UsuarioVO;

/**
 *
 * @author Seidor
 */
@SuppressWarnings("serial")
@Named(value = "variacionMB")
@ViewScoped
public class VariacionMB implements Serializable {

	private static final Logger LOGGER = LogManager.getLogger(VariacionMB.class.getName());

	@EJB
	private ModuloGeneralEJB moduloGeneralEJB;

	@EJB
	private VariacionEJB variacionEJB;

	@EJB
	private BitacoraEJB bitacoraEJB;

	private List<Compania> companias = new ArrayList<>();
	private Compania companiaSeleccionada;
	private LinkedHashMap<String, Compania> companiasMap = new LinkedHashMap<>();
	private String companiaSeleccionadaIde;

	// ----------------------------------------------------------------------------------------------------------------
	// MCG Controles relacionados con GrupoMarca
	// ----------------------------------------------------------------------------------------------------------------
	private List<GrupoMarca> grupoMarcas;
	private GrupoMarca grupoMarcaSeleccionada;
	private LinkedHashMap<String, GrupoMarca> grupoMarcasMap = new LinkedHashMap<>();
	private List<String> grupoMarcaSeleccionadaIde;
	// ----------------------------------------------------------------------------------------------------------------

	private List<MarcaCat2> marcas;
	private MarcaCat2 marcaSeleccionada;
	private LinkedHashMap<String, MarcaCat2> marcasMap = new LinkedHashMap<>();
	private String marcaSeleccionadaIde;

	private List<Retailer> retailers;
	private Retailer retailerSeleccionado;
	private LinkedHashMap<String, Retailer> retailersMap = new LinkedHashMap<>();
	private List<String> retailerSeleccionadoIde;

	private List<MesSop> meses;
	private Long mesSeleccionado;
	private Map<Long, MesSop> mesesMap = new LinkedHashMap<>();

	private List<VariacionSellOutVO> productos;

	private boolean mostrarBotones;
	private boolean mostrarBotn;
	private boolean unicaCompania;
	private UsuarioVO oUsuario;

	private LazyDataModel<VariacionSellOutVO> lazyModel;
	private LazyDataModel<VariacionSellOutVO> lazyProducto;
	private Map<String, List<VariacionSellOutVO>> prodCategoria;
	private String nomCategoria;
	private List<Boolean> columnList;

	private List<String> selectItems;
	private boolean upc;
	private boolean modelo;
	private boolean descripcion;

	/**
	 * Costructor por defecto
	 */
	public VariacionMB() {
		//
	}

	/**
	 * Metodo que inicializa variables para el backing
	 */
	@PostConstruct
	public void init() {

		selectItems = new ArrayList<>();
		selectItems.add("Producto");
		upc = false;
		modelo = false;
		descripcion = true;

		boolean isMesesFuturas = false;

		try {
			Map<String, String> permisos = Util.obtenerPermisosXFuncionalidad("Variación SellOut");

			String txtMesFuturo = "";
			String txtFechaActivacion = "";

			if (!permisos.isEmpty()) {
				txtMesFuturo = permisos.get("MES_FUTUROS");
				txtFechaActivacion = permisos.get("FECHA_ACTIVACION");
			}

			if (txtMesFuturo != null) {
				isMesesFuturas = Boolean.parseBoolean(txtMesFuturo);
			}

			if (txtFechaActivacion != null) {
				LocalDate getActualDate = (new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				LocalDate getActivacionDate = (new SimpleDateFormat("dd/MM/yyyy").parse(txtFechaActivacion)).toInstant()
						.atZone(ZoneId.systemDefault()).toLocalDate();
				isMesesFuturas = isMesesFuturas && getActivacionDate.getDayOfMonth() <= getActualDate.getDayOfMonth();
				LOGGER.info(isMesesFuturas + " && " + getActivacionDate.getDayOfMonth() + " <= "
						+ getActualDate.getDayOfMonth());

			}

		} catch (Exception e) {
			Log.error("[init] Hubo un error consultando los posibles permisos", e);
		}

		try {
			oUsuario = getUsuario();
			companias = moduloGeneralEJB.consultarCompaniasUsuario(oUsuario);
			marcas = moduloGeneralEJB.consultarMarcas();
			grupoMarcas = moduloGeneralEJB.consultarGrupoMarcas();
			prodCategoria = new LinkedHashMap<>();
			meses = moduloGeneralEJB.consultarMesesSOP(isMesesFuturas);

			for (Compania item : companias) {
				companiasMap.put(item.getId(), item);
			}
			for (GrupoMarca item : grupoMarcas) {
				grupoMarcasMap.put(String.valueOf(item.getId()), item);
			}
			for (MarcaCat2 item : marcas) {
				marcasMap.put(item.getId(), item);
			}
			for (MesSop item : meses) {
				mesesMap.put(item.getId(), item);
			}

			if (companias.size() == 1) {
				companiaSeleccionadaIde = companias.get(0).getId();
				cargaListaRetailers();
				unicaCompania = true;
			}

		} catch (Exception e) {
			LOGGER.error("Error en VariacionMB.init", e);
		}

	}

	public List<String> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<String> selectItems) {
		this.selectItems = selectItems;
	}

	public boolean isUpc() {
		return upc;
	}

	public void setUpc(boolean upc) {
		this.upc = upc;
	}

	public boolean isModelo() {
		return modelo;
	}

	public void setModelo(boolean modelo) {
		this.modelo = modelo;
	}

	public boolean isDescripcion() {
		return descripcion;
	}

	public void setDescripcion(boolean descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Consulta la variacion de sellout para los parametros seleccionados
	 *
	 * @return
	 */
	/**
	 * Consulta la variacion de sellout para los parametros seleccionados
	 *
	 * @return
	 */
	public String consultar() {
		Long ini = System.currentTimeMillis();
		try {

			String marcasGrupo = variacionEJB.getMarcasByIDGrupoMarca(grupoMarcaSeleccionadaIde);

			List<VariacionSellOutVO> productosAux = variacionEJB.consultarVariacion(this.mesSeleccionado,
					retailerSeleccionadoIde, marcasGrupo, false, null);

			productos = new ArrayList<>();
			String re = "";

			for (VariacionSellOutVO p : productosAux) {
				if (re.equals("") || !re.equals(p.getRetailer())) {
					re = p.getRetailer();
					VariacionSellOutVO aux = new VariacionSellOutVO(re, re, re, null, null, null, null, null,
							new BigDecimal(1), null, "", "", "");
					productos.add(aux);
				}
				productos.add(p);
			}

			lazyModel = new VariacionLazyModel(productos, false);

			for (VariacionSellOutVO oCategoria : productos) {
				if (oCategoria.getCategoria() != null && oCategoria.getRetailer() != null) {
					String id = oCategoria.getRetailer().concat("-").concat(oCategoria.getCategoria());

					prodCategoria.put(id,
							variacionEJB.consultarVariacion(this.mesSeleccionado,
									Arrays.asList(oCategoria.getRetailer().split("-")[0]), marcasGrupo, true,
									oCategoria.getCategoria()));
				}
			}
			if (productos.isEmpty()) {
				FacesMessage msg = new FacesMessage(
						"No se encontraron resultados para la consulta, por favor haga clic en Procesar para generar datos");
				msg.setSeverity(FacesMessage.SEVERITY_WARN);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

			mostrarBotones = true;
			mostrarBotn = !productos.isEmpty();

		} catch (

		Exception e) {
			LOGGER.error("[consultar] Error realizando la consulta de la informacion", e);
		}
		try {
			HttpSession session = Util.getSession();
			session.setAttribute("modulo_bitacora", ConstantesBitacora.DEMAND_VARIACION);
			session.setAttribute("mes_bitacora", this.mesesMap.get(mesSeleccionado));
		} catch (Exception e) {
			LOGGER.error("[consultar] Error actualizando la informacion de la session ", e);
		}

		LOGGER.info("[consultar] Tiempo de ejecucion Consultar Variacion SellOut : {}",
				(System.currentTimeMillis() - ini));

		return "Exitoso";
	}

	/**
	 * Recalcula la variacion de sellout para los parametros seleccionados
	 *
	 * @return
	 */
	public String recalcular() {
		try {
			Long ini = System.currentTimeMillis();

			List<String> listMarcas = new ArrayList<>();
			for (String grpMarca : grupoMarcaSeleccionadaIde) {
				List<String> lstMarcas = variacionEJB.getMarcaGrupoPorIdGrupoMarca(grpMarca);
				listMarcas.addAll(lstMarcas);
			}

			String marcasGrupo = variacionEJB.getMarcasByIDGrupoMarca(grupoMarcaSeleccionadaIde);

			variacionEJB.procesarVariacion(marcasGrupo, mesSeleccionado, retailerSeleccionadoIde,
					companiaSeleccionadaIde, grupoMarcaSeleccionadaIde, prodCategoria);

			consultar();
			mostrarBotn = !productos.isEmpty();

			LOGGER.info("[recalcular] Tiempo de ejecucion recalcular : {}", (System.currentTimeMillis() - ini));

		} catch (Exception e) {
			FacesMessage msg = new FacesMessage("Error: Procesado Variacion SellOut: " + e.getMessage());
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			LOGGER.error("[recalcular] Error en VariacionMB.recalcular () ", e);
		}
		return "Exitoso";
	}

	/**
	 * Envia a guardar los cambios realizados por el usuario
	 *
	 * @return
	 */
	public String guardarVariacion() {
		try {
			variacionEJB.guardarModificacionVariacion(productos, prodCategoria);
			FacesMessage msg = new FacesMessage("Variacion actualizada");
			msg.setSeverity(FacesMessage.SEVERITY_INFO);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			limpiarCampos();
		} catch (Exception e) {
			LOGGER.error("[guardarVariacion] Error en VariacionMB.guardarVariacion ", e);
		}

		return "";
	}

	/**
	 * Carga la lista de retailers a partir de la compania seleccionada
	 */
	public void cargaListaRetailers() {

		retailersMap = new LinkedHashMap<>();
		try {
			if (companiaSeleccionadaIde != null) {
				retailers = moduloGeneralEJB.consultarRetailers(companiaSeleccionadaIde);
				for (Retailer item : retailers) {
					retailersMap.put(item.getId(), item);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error en VariacionMB.cargaListaRetailers() ", e);
		}
	}

	/**
	 * Encargado de recibir los valores de las filas de la tabla Variacion cuando
	 * hay cambios
	 *
	 * @param event
	 */
	public void onRowEdit(RowEditEvent event) {
		VariacionSellOutVO variacionVO = (VariacionSellOutVO) event.getObject();

		// ACTUALIZAR PORCENTAJE DE LOS PRODUCTOS
		// Obtiene el porcentaje de la variacion modificada
		BigDecimal porcentajeModificado = variacionVO.getPorcentajeModificado();
		BigDecimal porcentajeBase = variacionVO.getPorcentajeBase();
		BigDecimal cien = new BigDecimal(100);
		BigDecimal sumaUnidadesCat = new BigDecimal(0);

		// Busca los porcentajes de los productos asociados a la categoria seleccionada
		String id = variacionVO.getRetailer().concat("-").concat(variacionVO.getCategoria());
		List<VariacionSellOutVO> prodCat = prodCategoria.get(id);
		for (VariacionSellOutVO productoVO : prodCat) {

			if (productoVO.getPorcentajeBase() != null) {
				// Se valida si el numero es positivo para ver que cálculo se aplica
				if (productoVO.getPorcentajeBase().signum() > 0) {
					productoVO.setPorcentajeModificado((productoVO.getPorcentajeBase().multiply(porcentajeModificado))
							.divide(porcentajeBase, RoundingMode.HALF_UP));
				} else {
					productoVO.setPorcentajeModificado((productoVO.getPorcentajeBase().multiply(porcentajeBase))
							.divide(porcentajeModificado, RoundingMode.HALF_UP));
				}
				// Realiza calculo de porcentaje y unidades con la modificacion de categoria
				if (productoVO.getSellOutAnt() != null) {
					BigDecimal calculoPorcentaje = productoVO.getSellOutAnt()
							.multiply(productoVO.getPorcentajeModificado().divide(cien))
							.setScale(2, RoundingMode.CEILING);
					productoVO.setSellOut(
							calculoPorcentaje.add(productoVO.getSellOutAnt()).setScale(2, RoundingMode.CEILING));
				}

				// Suma las unidades calculadas de los prodctos afectados de la categoria
			}
			// Suma las unidades calculadas de los prodctos afectados de la categoria Se
			// cambia a esta rutina
			// porque no estaba totalizando bien
			sumaUnidadesCat = sumaUnidadesCat.add(productoVO.getSellOut());

		}
		// Actualiza la suma de unidades de la cateria modificada
		LOGGER.info("sumatoria sellout{}", sumaUnidadesCat);
		variacionVO.setSellOut(sumaUnidadesCat);

		variacionEJB.guardarModificacionVariacion(variacionVO, prodCat);
		FacesMessage msg = new FacesMessage("Categoria" + variacionVO.getCategoriaProducto() + " actualizada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	/**
	 * Encargado de cancelar los cambios hechos a las filas de la tabla Variacion
	 *
	 * @param event
	 */
	public void onRowCancel(RowEditEvent event) {
		//
	}

	public void cargarDialogo(VariacionSellOutVO item) {
		RequestContext context2 = RequestContext.getCurrentInstance();
		nomCategoria = item.getCategoriaProducto().replaceFirst("0_", "");
		String id = item.getRetailer().concat("-").concat(item.getCategoria());
		lazyProducto = new VariacionLazyModel(prodCategoria.get(id), true);
		context2.execute("PF('carDialog').show()");
	}

	/**
	 * 
	 */
	public void selecion() {
		upc = selectItems.contains("UPC");
		descripcion = selectItems.contains("Producto");
		modelo = selectItems.contains("Modelo");
	}

	/**
	 * 
	 * @param e
	 */
	public void onToggle(ToggleEvent e) {
		columnList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);

	}

	/**
	 * 
	 * @throws IOException
	 */
	public void exportarAExcel() throws IOException {
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			GestorExcelForecast exportarExcel = new GestorExcelForecast();
			if (!productos.isEmpty() && !prodCategoria.isEmpty()) {
				exportarExcel.generarArchivoExcelVariacion(workbook, productos, prodCategoria);
			} else {
				throw new ExcepcionSOP("Debe primero generar la consulta antes de solicitar el archivo de excel.");
			}
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			externalContext.setResponseContentType("application/vnd.ms-excel");
			externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"Variacion.xls\"");
			workbook.write(externalContext.getResponseOutputStream());
			facesContext.responseComplete();
		} catch (ExcepcionSOP ex) {
			LOGGER.error("[exportarAExcel] Error exportando el archivo Excel", ex);
			Mensajes.mostrarMensajeError(ex, this.getClass());
		}
	}

	/**
	 * 
	 * @return
	 */
	public UsuarioVO getUsuario() {
		if (oUsuario == null) {
			oUsuario = new UsuarioVO();
			HttpSession session = Util.getSession();
			oUsuario.setId((Long) session.getAttribute("usrLogueadoId"));
			oUsuario.setNombre((String) session.getAttribute("usrLogueadoNom"));
			oUsuario.setUserName((String) session.getAttribute("usrLogueadoUserNom"));
			return oUsuario;
		} else {
			return oUsuario;
		}
	}

	public List<Compania> getCompanias() {
		return companias;
	}

	public Compania getCompaniaSeleccionada() {
		return companiaSeleccionada;
	}

	public void setCompanias(List<Compania> companias) {
		this.companias = companias;
	}

	public void setCompaniaSeleccionada(Compania companiaSeleccionada) {
		this.companiaSeleccionada = companiaSeleccionada;
	}

	public LinkedHashMap<String, Compania> getCompaniasMap() {
		return companiasMap;
	}

	public void setCompaniasMap(LinkedHashMap<String, Compania> companiasMap) {
		this.companiasMap = companiasMap;
	}

	public String getCompaniaSeleccionadaIde() {
		return companiaSeleccionadaIde;
	}

	public void setCompaniaSeleccionadaIde(String companiaSeleccionadaIde) {
		this.companiaSeleccionadaIde = companiaSeleccionadaIde;
	}

	public List<VariacionSellOutVO> getProductos() {
		return productos;
	}

	public void setProductos(List<VariacionSellOutVO> productos) {
		this.productos = productos;
	}

	public List<MarcaCat2> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<MarcaCat2> marcas) {
		this.marcas = marcas;
	}

	public MarcaCat2 getMarcaSeleccionada() {
		return marcaSeleccionada;
	}

	public void setMarcaSeleccionada(MarcaCat2 marcaSeleccionada) {
		this.marcaSeleccionada = marcaSeleccionada;
	}

	public LinkedHashMap<String, MarcaCat2> getMarcasMap() {
		return marcasMap;
	}

	public void setMarcasMap(LinkedHashMap<String, MarcaCat2> marcasMap) {
		this.marcasMap = marcasMap;
	}

	public String getMarcaSeleccionadaIde() {
		return marcaSeleccionadaIde;
	}

	public void setMarcaSeleccionadaIde(String marcaSeleccionadaIde) {
		this.marcaSeleccionadaIde = marcaSeleccionadaIde;
	}

	public List<Retailer> getRetailers() {
		return retailers;
	}

	public void setRetailers(List<Retailer> retailers) {
		this.retailers = retailers;
	}

	public Retailer getRetailerSeleccionado() {
		return retailerSeleccionado;
	}

	public void setRetailerSeleccionado(Retailer retailerSeleccionado) {
		this.retailerSeleccionado = retailerSeleccionado;
	}

	public LinkedHashMap<String, Retailer> getRetailersMap() {
		return retailersMap;
	}

	public void setRetailersMap(LinkedHashMap<String, Retailer> retailersMap) {
		this.retailersMap = retailersMap;
	}

	public List<String> getRetailerSeleccionadoIde() {
		return retailerSeleccionadoIde;
	}

	public void setRetailerSeleccionadoIde(List<String> retailerSeleccionadoIde) {
		this.retailerSeleccionadoIde = retailerSeleccionadoIde;
	}

	public List<MesSop> getMeses() {
		return meses;
	}

	public void setMeses(List<MesSop> meses) {
		this.meses = meses;
	}

	public Long getMesSeleccionado() {
		return mesSeleccionado;
	}

	public void setMesSeleccionado(Long mesSeleccionado) {
		this.mesSeleccionado = mesSeleccionado;
	}

	public Map<Long, MesSop> getMesesMap() {
		return mesesMap;
	}

	public void setMesesMap(Map<Long, MesSop> mesesMap) {
		this.mesesMap = mesesMap;
	}

	public boolean isMostrarBotones() {
		return mostrarBotones;
	}

	public void setMostrarBotones(boolean mostrarBotones) {
		this.mostrarBotones = mostrarBotones;
	}

	public boolean isMostrarBotn() {
		return mostrarBotn;
	}

	public void setMostrarBotn(boolean mostrarBotn) {
		this.mostrarBotn = mostrarBotn;
	}

	public String color(String producto) {
		String esta = moduloGeneralEJB.buscarEstadoDescontinuadoProducto(producto, companiaSeleccionadaIde, null);

		if (esta == null) {
			return "color: lightslategray";
		} else if (esta.equals("D")) {
			return "color: red";
		} else if (esta.equals("L")) {
			return "color: lime";
		} else if (esta.equals("C")) {
			return "color: lightslategray";
		} else {
			return "color: lightslategray";
		}
	}

	/**
	 * Se incorpora este método para efectivamente limpiar los campos, en vista de
	 * que se incorporo el valor true a la propiedad ajax de los commandButtons, y
	 * es necesario incorporar el elemento en la propiedad update
	 *
	 * @author Manuel Cortes Granados
	 * @since 10 Mayo 2017 10:51 AM
	 */
	public void limpiarCampos() {
		marcaSeleccionadaIde = null;
		retailerSeleccionadoIde = null;
		grupoMarcaSeleccionadaIde = null;
		mesSeleccionado = null;
		mostrarBotones = false;
		nomCategoria = null;
		this.productos.clear();
		prodCategoria.clear();
	}

	public List<GrupoMarca> getGrupoMarcas() {
		return grupoMarcas;
	}

	public void setGrupoMarcas(List<GrupoMarca> grupoMarcas) {
		this.grupoMarcas = grupoMarcas;
	}

	public GrupoMarca getGrupoMarcaSeleccionada() {
		return grupoMarcaSeleccionada;
	}

	public void setGrupoMarcaSeleccionada(GrupoMarca grupoMarcaSeleccionada) {
		this.grupoMarcaSeleccionada = grupoMarcaSeleccionada;
	}

	public LinkedHashMap<String, GrupoMarca> getGrupoMarcasMap() {
		return grupoMarcasMap;
	}

	public void setGrupoMarcasMap(LinkedHashMap<String, GrupoMarca> grupoMarcasMap) {
		this.grupoMarcasMap = grupoMarcasMap;
	}

	public List<String> getGrupoMarcaSeleccionadaIde() {
		return grupoMarcaSeleccionadaIde;
	}

	public void setGrupoMarcaSeleccionadaIde(List<String> grupoMarcaSeleccionadaIde) {
		this.grupoMarcaSeleccionadaIde = grupoMarcaSeleccionadaIde;
	}

	public boolean isUnicaCompania() {
		return unicaCompania;
	}

	public void setUnicaCompania(boolean unicaCompania) {
		this.unicaCompania = unicaCompania;
	}

	public LazyDataModel<VariacionSellOutVO> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<VariacionSellOutVO> lazyModel) {
		this.lazyModel = lazyModel;
	}

	public LazyDataModel<VariacionSellOutVO> getLazyProducto() {
		return lazyProducto;
	}

	public void setLazyProducto(LazyDataModel<VariacionSellOutVO> lazyProducto) {
		this.lazyProducto = lazyProducto;
	}

	public String getNomCategoria() {
		return nomCategoria;
	}

	public void setNomCategoria(String nomCategoria) {
		this.nomCategoria = nomCategoria;
	}

	public List<Boolean> getListColumn() {
		return columnList;
	}
}
