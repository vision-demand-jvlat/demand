/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.excel;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.jvlap.sop.demand.constantes.ConstantesDemand;
import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.vos.ForecastVO;
import com.jvlap.sop.demand.logica.vos.ForecastXMesVO;
import com.jvlap.sop.demand.logica.vos.InformacionForecastXProductoVO;
import com.jvlap.sop.demand.logica.vos.ProductosForecastMesVO;
import com.jvlap.sop.demand.logica.vos.VariacionSellOutVO;
import com.jvlap.sop.demand.web.utilitarios.ExportadorAMSExcel;
import com.jvlap.sop.demand.web.utilitarios.LetraInicialFinalVO;
import com.jvlat.sop.entidadessop.entidades.MarcaCat2;
import com.jvlat.sop.entidadessop.entidades.MesSop;
import com.jvlat.sop.reportes.logica.ReportesEJB;

/**
 *
 * @author Fabian Gonzalez
 */
@SuppressWarnings("serial")
public class GestorExcelForecast implements Serializable {

	public static final int TEXTO = 1;
	public static final int NUMERICO = 2;

	public static final int FORECAST_RETAILER = 1000;
	public static final int FORECAST_COMPANIA = 1001;
	public static final int FORECAST_COMPANIA_CON_RETAILER = 1010;
	public static final int FORECAST_COMPANIA_SIN_RETAILER = 1012;
	public static final int FORECAST_GENERAL = 1002;
	public static final int ELEVACION_SELLIN = 1003;

	private static final String ETI_PROD_CAT = "Producto/Categoria";
	public static final int ESTADO_FORECAST_RETAILER = 1004;

	@EJB
	ModuloGeneralEJB generalEJB;

	@EJB
	ReportesEJB reportesEJB;

	private static Logger log = LogManager.getLogger(GestorExcelForecast.class.getName());

	/**
	 * Este metodo tiene por objeto generar los encabezados o titulos de las
	 * columnas solamente
	 *
	 * @param filaActual => Se usa arreglo por eso y aquello del mantener un
	 *                   parametro por referencia
	 * @param workbook
	 * @param sheet
	 * @param forecast   Se agregan mas casos al case para el soporte de mas de 6,
	 *                   hasta 12 meses de acuerdo al valor del parametro
	 *                   MESES_FORECAST de la tabla PARAMETRO
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	private void generarTitulosEncabezadosColumnasForecastRetailer(int[] filaActual, HSSFWorkbook workbook,
			HSSFSheet sheet, ForecastXMesVO forecast) {

		int columna = 1;
		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		String etiqueta = "";

		HSSFRow row1 = sheet.createRow(filaActual[0]);

		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 0, "Año",
				"A" + (filaActual[0] + 1) + ":A" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 1, "Mes",
				"B" + (filaActual[0] + 1) + ":B" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 2, "Retailer",
				"C" + (filaActual[0] + 1) + ":C" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 3, "Descripcion Retailer",
				"D" + (filaActual[0] + 1) + ":D" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 4, "Marca",
				"E" + (filaActual[0] + 1) + ":E" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 5, "Budget Category",
				"F" + (filaActual[0] + 1) + ":F" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 6, "Codigo Corto",
				"G" + (filaActual[0] + 1) + ":G" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 7, "SKU(Modelo)",
				"H" + (filaActual[0] + 1) + ":H" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 8, "UPC",
				"I" + (filaActual[0] + 1) + ":I" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 9, "Descripcion",
				"J" + (filaActual[0] + 1) + ":J" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 10, "Inventario WH",
				"K" + (filaActual[0] + 1) + ":K" + (filaActual[0] + 3), 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 11, "Transito",
				"L" + (filaActual[0] + 1) + ":L" + (filaActual[0] + 3), 10);

		List<ProductosForecastMesVO> listProductos = forecast.getProductosForecast();
		Iterator iterador = listProductos.iterator();
		if (iterador.hasNext()) {
			log.debug("Inicio de Iterator");
		}
		ProductosForecastMesVO productos = (ProductosForecastMesVO) iterador.next();
		Map<String, InformacionForecastXProductoVO> mapa = productos.getInformacionForecastMes();
		int rangoCampos = mapa.size();

		columna = 12;
		List<String> rangos = parametrizarRangoLetrasInicialFinal(columna, rangoCampos);
		String rangoExcel = rangos.get(0) + (filaActual[0] + 1) + ":" + rangos.get(1) + (filaActual[0] + 1);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, "Forecast Kam - Units",
				rangoExcel, 18);

		columna += rangoCampos;
		rangos = parametrizarRangoLetrasInicialFinal(columna, rangoCampos);
		rangoExcel = rangos.get(0) + String.valueOf(filaActual[0] + 1) + ":" + rangos.get(1) + (filaActual[0] + 1);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna,
				"Forecast Kam Ciclo Anterior - Units", rangoExcel, 18);

		columna += rangoCampos;
		rangos = parametrizarRangoLetrasInicialFinal(columna, rangoCampos);
		rangoExcel = rangos.get(0) + (filaActual[0] + 1) + ":" + rangos.get(1) + (filaActual[0] + 1);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna,
				"Forecast Propuesto - Units", rangoExcel, 18);

		columna += rangoCampos;
		rangos = parametrizarRangoLetrasInicialFinal(columna, rangoCampos);
		rangoExcel = rangos.get(0) + (filaActual[0] + 1) + ":" + rangos.get(1) + (filaActual[0] + 1);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna,
				"Forecast Kam Gross Sales", rangoExcel, 18);

		filaActual[0]++;
		row1 = sheet.createRow(filaActual[0]);
		columna = 12;
		if (!mapa.isEmpty()) {
			for (int i = 1; i <= 4; i++) {
				for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
					etiqueta = entry.getKey();
					excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
							null, 10);
					columna++;
				}
			}
			filaActual[0]++;
			row1 = sheet.createRow(filaActual[0]);
			columna = 12;
			for (int i = 1; i <= 4; i++) {
				for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
					if (i == 3) {
						excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna,
								"Forecast Propuesto", null, 10);
					} else {
						excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna,
								"Forecast Kam", null, 10);
					}
					columna++;
				}
			}

		} else {
			columna = rangoCampos;
		}
		this.autoRedimensionarColumnas(sheet);
		filaActual[0]++;
	}

	/**
	 * 
	 * @param columna
	 * @param rango
	 * @return
	 */
	private List<String> parametrizarRangoLetrasInicialFinal(int columna, int rango) {

		List<String> rangos = new ArrayList<>();
		rangos.add(ConstantesDemand.COLUMNAS.get(columna));
		rangos.add(ConstantesDemand.COLUMNAS.get(rango + columna - 1));

		return rangos;
	}

	/**
	 * @param filaActual => Se usa arreglo por eso y aquello del mantener un
	 *                   parametro por referencia
	 * @param workbook
	 * @param sheet
	 * @param forecast
	 */
	@SuppressWarnings("rawtypes")
	private void generarTitulosEncabezadosColumnasForecastCompania(int[] filaActual, HSSFWorkbook workbook,
			HSSFSheet sheet, ForecastXMesVO forecast, boolean retailer, boolean estado) {
		int offset = 6;

		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		String etiqueta = ETI_PROD_CAT;
		HSSFRow row1 = sheet.createRow(filaActual[0]);
		// Este metodo tiene por objeto crear la celda combinada con respecto al titulo
		// principal en donde va el nombre del mes.
		// Aqui hay cambios en caso de que aumenten el numero de columnas a mostrar
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 0, etiqueta,
				"A" + (filaActual[0] + 1) + ":A" + (filaActual[0] + 2), 10);
		List<ProductosForecastMesVO> listProducto = forecast.getProductosForecast();
		Iterator iterador = listProducto.iterator();
		if (iterador.hasNext()) {
			log.debug("- Inicio de Iterator");
		}
		ProductosForecastMesVO productos = (ProductosForecastMesVO) iterador.next();
		Map<String, InformacionForecastXProductoVO> mapa = productos.getInformacionForecastMes();
		int contador = 1;
		LetraInicialFinalVO letraVO = new LetraInicialFinalVO();
		int columna = 1;
		// Iteramos sobre cada uno de los meses, los cuales cada uno a su vez le
		// corresponden varios campos
		for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
			if (entry.getKey() != null) {
				etiqueta = entry.getKey();
				// Cada caso del swith es directamente proporcional al numero de meses de
				// acuerdo con el valor del parametro
				// MESES_FORECAST en la tabla PARAMETRO
				if (!retailer && estado) {
					parametrizarRangoLetrasInicialFinalForecastCompaniaSinRetailer(letraVO, contador);
				} else {
					parametrizarRangoLetrasInicialFinalForecastCompaniaConRetailer(letraVO, contador);
				}

				contador++;
				String rangoExcel = letraVO.getLetraInicial() + (filaActual[0] + 1) + ":" + letraVO.getLetraFinal()
						+ (filaActual[0] + 1);
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
						rangoExcel, 20);
				columna = columna + offset;
			}
		}
		filaActual[0]++;
		row1 = sheet.createRow(filaActual[0]);
		columna = 1;
		for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
			if (entry.getKey() != null) {
				etiqueta = "SellIn Propuesto";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
				etiqueta = "Total Propuesto";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
				etiqueta = "Forecast Kam";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
				etiqueta = "Total Forecast Kam";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
				etiqueta = "Forecast Gerente";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
				etiqueta = "Total Forecast Gerente";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
			}
		}
		this.autoRedimensionarColumnas(sheet);
		filaActual[0]++;
	}

	/**
	 * @param filaActual
	 * @param workbook
	 * @param sheet
	 * @param forecast
	 */
	@SuppressWarnings("rawtypes")
	private void generarTitulosEncabezadosColumnasForecastGeneral(int[] filaActual, HSSFWorkbook workbook,
			HSSFSheet sheet, ForecastXMesVO forecast, boolean estado) {

		int offset = 4;

		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		String etiqueta = ETI_PROD_CAT;
		HSSFRow row1 = sheet.createRow(filaActual[0]);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 0, etiqueta,
				"A" + (filaActual[0] + 1) + ":A" + (filaActual[0] + 2), 10);
		List<ProductosForecastMesVO> listProductos = forecast.getProductosForecast();
		Iterator iterador = listProductos.iterator();
		if (iterador.hasNext()) {
			log.debug("[generarTitulosEncabezadosColumnasForecastGeneral] --Inicio de Iterator");
		}
		ProductosForecastMesVO productos = (ProductosForecastMesVO) iterador.next();
		Map<String, InformacionForecastXProductoVO> mapa = productos.getInformacionForecastMes();
		int contador = 1;
		LetraInicialFinalVO letraVO = new LetraInicialFinalVO();
		int columna = 1;
		// Iteramos sobre cada uno de los meses, los cuales cada uno a su vez le
		// corresponden varios campos
		for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
			etiqueta = entry.getKey();
			if (etiqueta != null) {
				// Cada caso del swith es directamente proporcional al numero de meses de
				// acuerdo con el valor del parametro
				// MESES_FORECAST en la tabla PARAMETRO
				if (estado) {
					parametrizarRangoLetrasInicialFinalForecastGeneral(letraVO, contador);
				} else {
					parametrizarRangoLetrasInicialFinalForecastGeneralSinUnidades(letraVO, contador);
				}

				contador++;
				String rangoExcel = letraVO.getLetraInicial() + (filaActual[0] + 1) + ":" + letraVO.getLetraFinal()
						+ (filaActual[0] + 1);
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
						rangoExcel, 20);
				columna = columna + offset;
			}
		}
		filaActual[0]++;
		row1 = sheet.createRow(filaActual[0]);
		columna = 1;

		for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {

			if (entry.getKey() != null) {
				if (estado) {
					etiqueta = "Forecast Actual";
					excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
							null, 10);
					columna++;
					etiqueta = "Forecast Anterior";
					excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
							null, 10);
					columna++;
					etiqueta = "Forecast Actual (USD) $";
					excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
							null, 10);
					columna++;
					etiqueta = "Forecast Anterior (USD) $";
					excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
							null, 10);
					columna++;

				} else {
					etiqueta = "Forecast Actual";
					excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
							null, 10);
					columna++;
					etiqueta = "Forecast Anterior";
					excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
							null, 10);
					columna++;
					etiqueta = "Forecast Actual (USD) $";
					excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
							null, 10);
					columna++;
					etiqueta = "Forecast Anterior (USD) $";
					excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
							null, 10);
					columna++;
				}
			}
		}
		this.autoRedimensionarColumnas(sheet);
		filaActual[0]++;
	}

	/**
	 * Este metodo tiene por objeto generar los encabezados o titulos de las
	 * columnas solamente
	 *
	 * @param filaActual => Se usa arreglo por eso y aquello del mantener un
	 *                   parametro por referencia
	 * @param workbook
	 * @param sheet
	 * @param forecast   Se agregan mas casos al case para el soporte de mas de 6,
	 *                   hasta 12 meses de acuerdo al valor del parametro
	 *                   MESES_FORECAST de la tabla PARAMETRO
	 */
	@SuppressWarnings("rawtypes")
	private void generarTitulosEncabezadosColumnasElevacionSellin(int[] filaActual, HSSFWorkbook workbook,
			HSSFSheet sheet, ForecastXMesVO forecast) {
		int offset = 4;
		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		String etiqueta = ETI_PROD_CAT;
		HSSFRow row1 = sheet.createRow(filaActual[0]);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 0, etiqueta,
				"A" + (filaActual[0] + 1) + ":A" + (filaActual[0] + 2), 10);
		List<ProductosForecastMesVO> listProductos = forecast.getProductosForecast();
		Iterator iterador = listProductos.iterator();
		if (iterador.hasNext()) {
			log.debug("[generarTitulosEncabezadosColumnasElevacionSellin]--- Inicio de Iterator");
		}
		ProductosForecastMesVO productos = (ProductosForecastMesVO) iterador.next();
		Map<String, InformacionForecastXProductoVO> mapa = productos.getInformacionForecastMes();
		int contador = 1;
		int columna = 1;
		LetraInicialFinalVO letraVO = new LetraInicialFinalVO();
		// Iteramos sobre cada uno de los meses, los cuales cada uno a su vez le
		// corresponden varios campos
		for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
			etiqueta = entry.getKey();
			if (etiqueta != null) {
				// Cada caso del swith es directamente proporcional al numero de meses de
				// acuerdo con el valor del parametro
				// MESES_FORECAST en la tabla PARAMETRO
				parametrizarRangoLetrasInicialFinalElevacionSellin(letraVO, contador);
				contador++;
				String rangoExcel = letraVO.getLetraInicial() + (filaActual[0] + 1) + ":" + letraVO.getLetraFinal()
						+ (filaActual[0] + 1);
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta,
						rangoExcel, 20);
				columna = columna + offset;

			}
		}
		filaActual[0]++;
		row1 = sheet.createRow(filaActual[0]);
		columna = 1;
		for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
			if (entry.getKey() != null) {
				etiqueta = "SellIn Propuesto";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
				etiqueta = "Forecast Kam";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
				etiqueta = "Total Propuesto";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
				etiqueta = "Total Forecast Kam";
				excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], columna, etiqueta, null,
						10);
				columna++;
			}
		}
		this.autoRedimensionarColumnas(sheet);
		filaActual[0]++;
	}

	/**
	 * @param filaActual
	 * @param workbook
	 * @param sheet
	 * @param forecast
	 */
	private void exportarDatosForecastRetailer(int[] filaActual, HSSFWorkbook workbook, HSSFSheet sheet,
			ForecastXMesVO forecast, Map<String, ForecastXMesVO> oProductos, MesSop mes, Map<String, MarcaCat2> marca) {
		int columna = 0;
		int camposCeldas = 4;
		boolean negrita = false;
		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		List<ProductosForecastMesVO> oCategoria = forecast.getProductosForecast();

		for (ProductosForecastMesVO objCat : oCategoria) {
			if (!objCat.isEsTotalMarca()) {
				ForecastXMesVO oProd = oProductos.get(objCat.getIdCategoria() + "-" + objCat.getIdRetailer());
				columna = 0;
				List<ProductosForecastMesVO> oPrduct = oProd.getProductosForecast();
				for (ProductosForecastMesVO objProd : oPrduct) {
					HSSFRow row1 = sheet.createRow(filaActual[0]);
					Map<String, InformacionForecastXProductoVO> mapa = objProd.getInformacionForecastMes();
					filaActual[0]++;

					negrita = false;
					if (objProd.isEsProdcuto()) {

						int contador = 1;
						for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
							InformacionForecastXProductoVO infoVO = entry.getValue();
							if (contador == 1) {
								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
										mes.getAnio().toString(), null, false, TEXTO, negrita);
								columna++;
								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, mes.getNombre(),
										null, false, TEXTO, negrita);
								columna++;
								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
										oProd.getRetailer(), null, false, TEXTO, negrita);
								columna++;
								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
										oProd.getDescripcionRetailer(), null, false, TEXTO, negrita);
								columna++;

								MarcaCat2 marcaNombre = marca.get(oProd.getMarca());
								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
										marcaNombre.getNombre(), null, false, TEXTO, negrita);
								columna++;

								String bg = "";
								if (objCat.getBudgetCategory() != null) {
									String[] categoriaSplit = objCat.getBudgetCategory().split("/");
									bg = categoriaSplit[0];
								}

								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, bg, null, false,
										TEXTO, negrita);
								columna++;
								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
										objProd.getIdProducto(), null, false, TEXTO, negrita);
								columna++;
								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
										objProd.getModelo(), null, false, TEXTO, negrita);
								columna++;
								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, objProd.getUpc(),
										null, false, TEXTO, negrita);
								columna++;
								excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
										objProd.getNombreProducto(), null, false, TEXTO, negrita);
								columna++;

								if (columna == 10) {
									excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
											String.valueOf(infoVO.getInventarioWH()), null, true, NUMERICO, negrita);
									columna++;
									excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
											String.valueOf(infoVO.getTransito()), null, true, NUMERICO, negrita);
									columna++;
								}
							}
							contador++;
						}
						columna = 12;
						for (int i = 1; i <= camposCeldas; i++) {

							for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
								InformacionForecastXProductoVO infoVO = entry.getValue();
								if (entry.getKey() != null) {

									switch (i) {
									case 1:
										excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
												String.valueOf(infoVO.getForecast()), null, true, NUMERICO, negrita);
										columna++;
										break;
									case 2:
										excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
												String.valueOf(infoVO.getTotalForecastGerenteAnterior()), null, true,
												NUMERICO, negrita);
										columna++;
										break;
									case 3:
										excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
												String.valueOf(infoVO.getSellInPropuesto()), null, true, NUMERICO,
												negrita);
										columna++;
										break;
									case 4:
										excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
												String.valueOf(infoVO.getTotalForecastKam()), null, true, TEXTO,
												negrita);
										columna++;
										break;
									default:
										break;
									}
								}
							}
						}
						columna = 0;
					}
				}
			}
			columna = 0;
		}
	}

	/**
	 * @param filaActual
	 * @param workbook
	 * @param sheet
	 * @param forecast
	 */
	private void exportarDatosForecastCompania(int[] filaActual, HSSFWorkbook workbook, HSSFSheet sheet,
			ForecastXMesVO forecast, boolean retailer, boolean estado, Map<String, ForecastXMesVO> oProductos) {
		int columna = 1;
		boolean negrita = false;
		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		List<ProductosForecastMesVO> listProductos = forecast.getProductosForecast();
		for (ProductosForecastMesVO productos : listProductos) {
			String valor = productos.getNombreProducto();
			HSSFRow row1 = sheet.createRow(filaActual[0]);
			Map<String, InformacionForecastXProductoVO> mapa = productos.getInformacionForecastMes();
			filaActual[0]++;
			negrita = !productos.isEsProdcuto();
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 0, valor, null, false, TEXTO, negrita);
			for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
				InformacionForecastXProductoVO infoVO = entry.getValue();

				if (entry.getKey() != null) {
					excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
							String.valueOf(infoVO.getSellInPropuesto()), null, true, NUMERICO, negrita);
					columna++;
					excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
							String.valueOf(infoVO.getTotalPropuesto()), null, true, NUMERICO, negrita);
					columna++;
					excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
							String.valueOf(infoVO.getForecast()), null, true, NUMERICO, negrita);
					columna++;
					excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
							String.valueOf(infoVO.getTotalForecastKam()), null, true, NUMERICO, negrita);
					columna++;
					excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
							String.valueOf(infoVO.getForecastGerente()), null, true, NUMERICO, negrita);
					columna++;
					excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
							String.valueOf(infoVO.getTotalForecastGerente()), null, true, NUMERICO, negrita);
					columna++;
				}
			}
			if (!productos.isEsTotalMarca()) {
				columna = 1;
				ForecastXMesVO oProd = oProductos.get(productos.getIdCategoria() + "-" + productos.getIdRetailer());
				List<ProductosForecastMesVO> oPrduct = oProd.getProductosForecast();
				for (ProductosForecastMesVO objProd : oPrduct) {
					valor = objProd.getNombreProducto();
					row1 = sheet.createRow(filaActual[0]);
					mapa = objProd.getInformacionForecastMes();
					filaActual[0]++;
					negrita = !objProd.isEsProdcuto();
					excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 0, valor, null, false, TEXTO, negrita);
					for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
						InformacionForecastXProductoVO infoVO = entry.getValue();
						if (entry.getKey() != null) {
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getSellInPropuesto()), null, true, NUMERICO, negrita);
							columna++;
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getTotalPropuesto()), null, true, NUMERICO, negrita);
							columna++;
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getForecast()), null, true, NUMERICO, negrita);
							columna++;
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getTotalForecastKam()), null, true, NUMERICO, negrita);
							columna++;
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getForecastGerente()), null, true, NUMERICO, negrita);
							columna++;
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getTotalForecastGerente()), null, true, NUMERICO, negrita);
							columna++;
						}
					}
					columna = 1;
				}
			}
			columna = 1;
		}
	}

	/**
	 * Este metodo tiene por objeto como tal la exportacion de la data.
	 *
	 * @param filaActual
	 * @param workbook
	 * @param sheet
	 * @param forecast
	 */
	private void exportarDatosForecastGeneral(int[] filaActual, HSSFWorkbook workbook, HSSFSheet sheet,
			ForecastXMesVO forecast, boolean estado) {
		int columna = 1;
		boolean negrita = false;
		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		List<ProductosForecastMesVO> listProductos = forecast.getProductosForecast();
		for (ProductosForecastMesVO productos : listProductos) {
			String valor = productos.getNombreProducto();
			HSSFRow row1 = sheet.createRow(filaActual[0]);
			Map<String, InformacionForecastXProductoVO> mapa = productos.getInformacionForecastMes();
			filaActual[0]++;
			negrita = false;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 0, valor, null, false, TEXTO, negrita);
			for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {

				if (entry.getKey() != null) {
					InformacionForecastXProductoVO infoVO = entry.getValue();
					if (estado) {// Consulta con retailer, muestra las columnas por defecto
						excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
								String.valueOf(infoVO.getForecastGerente()), null, true, NUMERICO, negrita);
						columna++;
						excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
								String.valueOf(infoVO.getForecastGerenteAnterior()), null, true, NUMERICO, negrita);
						columna++;
						excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
								String.valueOf(infoVO.getTotalForecastGerente()), null, true, NUMERICO, negrita);
						columna++;
						excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
								String.valueOf(infoVO.getTotalForecastGerenteAnterior()), null, true, NUMERICO,
								negrita);
						columna++;
					} else {
						excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
								String.valueOf(infoVO.getForecastGerente()), null, true, NUMERICO, negrita);
						columna++;
						excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
								String.valueOf(infoVO.getForecastGerenteAnterior()), null, true, NUMERICO, negrita);
						columna++;
						excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
								String.valueOf(infoVO.getTotalForecastGerente()), null, true, NUMERICO, negrita);
						columna++;
						excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
								String.valueOf(infoVO.getTotalForecastGerenteAnterior()), null, true, NUMERICO,
								negrita);
						columna++;
					}
				}
			}
			columna = 1;
		}
	}

	/**
	 * 
	 * @param filaActual
	 * @param workbook
	 * @param sheet
	 * @param forecast
	 * @param oProductos
	 */
	private void exportarDatosElevacionSellin(int[] filaActual, HSSFWorkbook workbook, HSSFSheet sheet,
			ForecastXMesVO forecast, Map<String, ForecastXMesVO> oProductos) {
		int columna = 1;
		boolean negrita = false;
		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		List<ProductosForecastMesVO> listProductos = forecast.getProductosForecast();
		for (ProductosForecastMesVO productos : listProductos) {
			String valor = productos.getNombreProducto();
			HSSFRow row1 = sheet.createRow(filaActual[0]);
			Map<String, InformacionForecastXProductoVO> mapa = productos.getInformacionForecastMes();
			filaActual[0]++;
			negrita = !productos.isEsProdcuto();
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 0, valor, null, false, TEXTO, negrita);
			for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
				InformacionForecastXProductoVO infoVO = entry.getValue();
				excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
						String.valueOf(infoVO.getSellInPropuesto()), null, true, NUMERICO, negrita);
				columna++;
				excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
						String.valueOf(infoVO.getForecast()), null, true, NUMERICO, negrita);
				columna++;
				excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
						String.valueOf(infoVO.getTotalPropuesto()), null, true, NUMERICO, negrita);
				columna++;
				excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
						String.valueOf(infoVO.getTotalForecastKam()), null, true, NUMERICO, negrita);
				columna++;
			}

			if (!productos.isEsTotalMarca()) {
				columna = 1;
				ForecastXMesVO oProd = oProductos.get(productos.getIdCategoria());
				log.info("[exportarDatosElevacionSellin] Categoria  prod cellIn {}", productos.getIdCategoria());
				log.info("[exportarDatosElevacionSellin] Productos forecast {}", oProd.getProductosForecast().size());
				List<ProductosForecastMesVO> oPrduct = oProd.getProductosForecast();
				log.info("[exportarDatosElevacionSellin] Tamaño list {} ", oPrduct.size());
				for (ProductosForecastMesVO objProd : oPrduct) {
					valor = objProd.getNombreProducto();
					row1 = sheet.createRow(filaActual[0]);
					mapa = objProd.getInformacionForecastMes();
					filaActual[0]++;
					negrita = !objProd.isEsProdcuto();
					excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 0, valor, null, false, TEXTO, negrita);
					for (Map.Entry<String, InformacionForecastXProductoVO> entry : mapa.entrySet()) {
						InformacionForecastXProductoVO infoVO = entry.getValue();
						if (entry.getKey() != null) {
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getSellInPropuesto()), null, true, NUMERICO, negrita);
							columna++;
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getForecast()), null, true, NUMERICO, negrita);
							columna++;
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getTotalPropuesto()), null, true, NUMERICO, negrita);
							columna++;
							excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
									String.valueOf(infoVO.getTotalForecastKam()), null, true, NUMERICO, negrita);
							columna++;
						}
					}
					columna = 1;
				}
				columna = 1;
			}
		}
	}

	/**
	 *
	 * @param workbook
	 * @param forecast
	 * @param reporte
	 * @throws IOException
	 */
	public void generarArchivoExcelForecast(HSSFWorkbook workbook, ForecastXMesVO forecast, int reporte, boolean estado,
			Map<String, ForecastXMesVO> productos, List<ForecastVO> listforecast) {
		HSSFSheet sheet = workbook.createSheet();
		int[] filaActual = new int[1];
		filaActual[0] = 1;
		switch (reporte) {
		case GestorExcelForecast.FORECAST_RETAILER:
			this.generarTitulosEncabezadosColumnasForecastRetailer(filaActual, workbook, sheet, forecast);
			this.exportarDatosForecastRetailer(filaActual, workbook, sheet, forecast, productos, null, null);
			break;
		case GestorExcelForecast.FORECAST_COMPANIA_CON_RETAILER:
			this.generarTitulosEncabezadosColumnasForecastCompania(filaActual, workbook, sheet, forecast, true, false);
			this.exportarDatosForecastCompania(filaActual, workbook, sheet, forecast, true, false, productos);
			break;
		case GestorExcelForecast.FORECAST_COMPANIA_SIN_RETAILER:
			this.generarTitulosEncabezadosColumnasForecastCompania(filaActual, workbook, sheet, forecast, false,
					estado);
			this.exportarDatosForecastCompania(filaActual, workbook, sheet, forecast, false, estado, productos);
			break;
		case GestorExcelForecast.FORECAST_GENERAL:
			this.generarTitulosEncabezadosColumnasForecastGeneral(filaActual, workbook, sheet, forecast, estado);
			this.exportarDatosForecastGeneral(filaActual, workbook, sheet, forecast, estado);
			break;
		case GestorExcelForecast.ELEVACION_SELLIN:
			this.generarTitulosEncabezadosColumnasElevacionSellin(filaActual, workbook, sheet, forecast);
			this.exportarDatosElevacionSellin(filaActual, workbook, sheet, forecast, productos);
			break;
		case GestorExcelForecast.ESTADO_FORECAST_RETAILER:
			filaActual[0] = 0;
			this.generarTitulosEncabezadosColumnasEstadoForecast(filaActual, workbook, sheet);
			this.exportarEstadoForescat(filaActual, workbook, sheet, listforecast);
			break;
		default:
			break;
		}

	}

	/**
	 * 
	 * @param filaActual
	 * @param workbook
	 * @param sheet
	 * @param listForecast
	 */
	private void exportarEstadoForescat(int[] filaActual, HSSFWorkbook workbook, HSSFSheet sheet,
			List<ForecastVO> listForecast) {
		int columna = 0;
		boolean negrita = false;
		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		for (ForecastVO forecast : listForecast) {

			HSSFRow row1 = sheet.createRow(filaActual[0]);

			negrita = false;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, forecast.getMarca(), null, false,
					TEXTO, negrita);
			columna++;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, forecast.getCompania(), null, true,
					TEXTO, negrita);
			columna++;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, forecast.getRetailer(), null, true,
					TEXTO, negrita);
			columna++;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, forecast.getDescripcionRetailer(),
					null, true, TEXTO, negrita);
			columna++;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, forecast.getUsuario(), null, true,
					TEXTO, negrita);
			columna++;

			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna,
					dateFormat.format(forecast.getFechaCreacion()), null, true, TEXTO, negrita);
			columna++;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, forecast.getAnhoMes(), null, true,
					TEXTO, negrita);
			columna++;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, forecast.getEstado(), null, true,
					TEXTO, negrita);
			columna++;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], columna, forecast.getTraduccion(), null, true,
					TEXTO, negrita);

			columna = 0;
			filaActual[0]++;
		}
	}

	/**
	 * 
	 * @param filaActual
	 * @param workbook
	 * @param sheet
	 * @param forecast
	 */
	private void generarTitulosEncabezadosColumnasEstadoForecast(int[] filaActual, HSSFWorkbook workbook,
			HSSFSheet sheet) {

		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);

		HSSFRow row1 = sheet.createRow(filaActual[0]);

		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 0, "Marca", null, 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 1, "Compañía", null, 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 2, "Retailer", null, 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 3, "Descripcion Retailer", null,
				10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 4, "Usuario", null, 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 5, "Fecha de creación", null,
				10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 6, "Año -Mes", null, 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 7, "Estados", null, 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 8, "TRADUCCIÓN", null, 10);

		this.autoRedimensionarColumnas(sheet);
		filaActual[0]++;
	}

	/**
	 *
	 * @param workbook
	 * @param forecast
	 * @throws IOException
	 */
	public void generarArchivoExcelForecastRetailerMod(HSSFWorkbook workbook, ForecastXMesVO forecast,
			Map<String, ForecastXMesVO> productos, MesSop mes, Map<String, MarcaCat2> marca) {
		HSSFSheet sheet = workbook.createSheet();
		int[] filaActual = new int[1];
		filaActual[0] = 1;
		this.generarTitulosEncabezadosColumnasForecastRetailer(filaActual, workbook, sheet, forecast);
		this.exportarDatosForecastRetailer(filaActual, workbook, sheet, forecast, productos, mes, marca);
	}

	/**
	 * @param letraInicial
	 * @param letraFinal
	 * @param contador
	 */
	private void parametrizarRangoLetrasInicialFinalElevacionSellin(LetraInicialFinalVO letraVO, int contador) {
		switch (contador) {
		case 1: // B-F
			letraVO.setLetraInicial("B");
			letraVO.setLetraFinal("E");
			break;
		case 2: // G-K
			letraVO.setLetraInicial("F");
			letraVO.setLetraFinal("I");
			break;
		case 3: // L-P
			letraVO.setLetraInicial("J");
			letraVO.setLetraFinal("M");
			break;
		case 4: // Q-U
			letraVO.setLetraInicial("N");
			letraVO.setLetraFinal("Q");
			break;
		case 5: // V-Z
			letraVO.setLetraInicial("R");
			letraVO.setLetraFinal("U");
			break;
		case 6: // AA-AE
			letraVO.setLetraInicial("V");
			letraVO.setLetraFinal("Y");
			break;
		case 7: // AF-AJ
			letraVO.setLetraInicial("Z");
			letraVO.setLetraFinal("AC");
			break;
		case 8: // AK-AO
			letraVO.setLetraInicial("AB");
			letraVO.setLetraFinal("AF");
			break;
		case 9: // AP-AT
			letraVO.setLetraInicial("AG");
			letraVO.setLetraFinal("AJ");
			break;
		case 10: // AU-AY
			letraVO.setLetraInicial("AK");
			letraVO.setLetraFinal("AN");
			break;
		case 11: // AZ-BD
			letraVO.setLetraInicial("AO");
			letraVO.setLetraFinal("AR");
			break;
		case 12: // BE-BI
			letraVO.setLetraInicial("AS");
			letraVO.setLetraFinal("AV");
			break;
		default:
			break;
		}
	}

	/**
	 * @param letraInicial
	 * @param letraFinal
	 * @param contador
	 */
	private void parametrizarRangoLetrasInicialFinalForecastGeneral(LetraInicialFinalVO letraVO, int contador) {
		switch (contador) {
		case 1: // B-J
			letraVO.setLetraInicial("B");
			letraVO.setLetraFinal("E");
			break;
		case 2: // K-S
			letraVO.setLetraInicial("F");
			letraVO.setLetraFinal("I");
			break;
		case 3: // T-AB
			letraVO.setLetraInicial("J");
			letraVO.setLetraFinal("M");
			break;
		case 4: // AC-AK
			letraVO.setLetraInicial("N");
			letraVO.setLetraFinal("Q");
			break;
		case 5: // AL-AT
			letraVO.setLetraInicial("R");
			letraVO.setLetraFinal("U");
			break;
		case 6: // AU-BC
			letraVO.setLetraInicial("V");
			letraVO.setLetraFinal("Z");
			break;
		case 7: // BD-BL
			letraVO.setLetraInicial("AA");
			letraVO.setLetraFinal("AD");
			break;
		case 8: // BM-BU
			letraVO.setLetraInicial("AH");
			letraVO.setLetraFinal("AL");
			break;
		case 9: // BV-CD
			letraVO.setLetraInicial("AM");
			letraVO.setLetraFinal("AP");
			break;
		case 10: // CE-CM
			letraVO.setLetraInicial("AQ");
			letraVO.setLetraFinal("AT");
			break;
		case 11: // CN-CV
			letraVO.setLetraInicial("AU");
			letraVO.setLetraFinal("AX");
			break;
		case 12: // CW-DE
			letraVO.setLetraInicial("AY");
			letraVO.setLetraFinal("BB");
			break;
		default:
			break;
		}
	}

	/**
	 *
	 * Por configuracion de cambios, este metodo solo aplica en el caso de que en
	 * Forecast Compañia se consulte con retailer.
	 *
	 * @param letraInicial
	 * @param letraFinal
	 * @param contador
	 */
	private void parametrizarRangoLetrasInicialFinalForecastCompaniaConRetailer(LetraInicialFinalVO letraVO,
			int contador) {
		switch (contador) {
		case 1: // B-H
			letraVO.setLetraInicial("B");
			letraVO.setLetraFinal("G");
			break;
		case 2: // I-O
			letraVO.setLetraInicial("H");
			letraVO.setLetraFinal("M");
			break;
		case 3: // P-V
			letraVO.setLetraInicial("N");
			letraVO.setLetraFinal("S");
			break;
		case 4: // W-AC
			letraVO.setLetraInicial("T");
			letraVO.setLetraFinal("Y");
			break;
		case 5: // AD-AJ
			letraVO.setLetraInicial("Z");
			letraVO.setLetraFinal("AE");
			break;
		case 6: // AK-AQ
			letraVO.setLetraInicial("AF");
			letraVO.setLetraFinal("AK");
			break;
		case 7: // AR-AX
			letraVO.setLetraInicial("AL");
			letraVO.setLetraFinal("AQ");
			break;
		case 8: // AY-BE
			letraVO.setLetraInicial("AR");
			letraVO.setLetraFinal("AW");
			break;
		case 9: // BF-BL
			letraVO.setLetraInicial("AX");
			letraVO.setLetraFinal("BD");
			break;
		case 10: // BM-BS
			letraVO.setLetraInicial("BE");
			letraVO.setLetraFinal("BJ");
			break;
		case 11: // BT-BZ
			letraVO.setLetraInicial("BK");
			letraVO.setLetraFinal("BP");
			break;
		case 12: // CA-CG
			letraVO.setLetraInicial("BQ");
			letraVO.setLetraFinal("BV");
			break;
		default:
			break;
		}
	}

	/**
	 * @param letraVO
	 * @param contador
	 */
	private void parametrizarRangoLetrasInicialFinalForecastCompaniaSinRetailer(LetraInicialFinalVO letraVO,
			int contador) {
		switch (contador) {
		case 1: // B-L
			letraVO.setLetraInicial("B");
			letraVO.setLetraFinal("L");
			break;
		case 2: // M-W
			letraVO.setLetraInicial("M");
			letraVO.setLetraFinal("W");
			break;
		case 3: // X-AH
			letraVO.setLetraInicial("X");
			letraVO.setLetraFinal("AH");
			break;
		case 4: // AI-AS
			letraVO.setLetraInicial("AI");
			letraVO.setLetraFinal("AS");
			break;
		case 5: // AT-BD
			letraVO.setLetraInicial("AT");
			letraVO.setLetraFinal("BD");
			break;
		case 6: // BE-BO
			letraVO.setLetraInicial("BE");
			letraVO.setLetraFinal("BO");
			break;
		case 7: // BP-BZ
			letraVO.setLetraInicial("BP");
			letraVO.setLetraFinal("BZ");
			break;
		case 8: // CA-CK
			letraVO.setLetraInicial("CA");
			letraVO.setLetraFinal("CK");
			break;
		case 9: // CL-CV
			letraVO.setLetraInicial("CL");
			letraVO.setLetraFinal("CV");
			break;
		case 10: // CW-DG
			letraVO.setLetraInicial("CW");
			letraVO.setLetraFinal("DG");
			break;
		case 11: // DH-DR
			letraVO.setLetraInicial("DH");
			letraVO.setLetraFinal("DR");
			break;
		case 12: // DS-EC
			letraVO.setLetraInicial("DS");
			letraVO.setLetraFinal("EC");
			break;
		default:
			break;
		}

	}

	/**
	 * @author Fabian Gonzalez
	 * @param sheet
	 */
	private void autoRedimensionarColumnas(HSSFSheet sheet) {
		for (int i = 0; i < 90; i++) {
			sheet.autoSizeColumn(i);
		}
	}

	/**
	 * 
	 * @param letraVO
	 * @param contador
	 */
	private void parametrizarRangoLetrasInicialFinalForecastGeneralSinUnidades(LetraInicialFinalVO letraVO,
			int contador) {

		switch (contador) {
		case 1: // B-J
			letraVO.setLetraInicial("B");
			letraVO.setLetraFinal("E");
			break;
		case 2: // K-S
			letraVO.setLetraInicial("F");
			letraVO.setLetraFinal("I");
			break;
		case 3: // T-AB
			letraVO.setLetraInicial("J");
			letraVO.setLetraFinal("M");
			break;
		case 4: // AC-AK
			letraVO.setLetraInicial("N");
			letraVO.setLetraFinal("Q");
			break;
		case 5: // AL-AT
			letraVO.setLetraInicial("R");
			letraVO.setLetraFinal("U");
			break;
		case 6: // AU-BC
			letraVO.setLetraInicial("V");
			letraVO.setLetraFinal("Y");
			break;
		case 7: // BD-BL
			letraVO.setLetraInicial("Z");
			letraVO.setLetraFinal("AC");
			break;
		case 8: // BM-BU
			letraVO.setLetraInicial("AD");
			letraVO.setLetraFinal("AG");
			break;
		case 9: // BV-CD
			letraVO.setLetraInicial("AH");
			letraVO.setLetraFinal("AK");
			break;
		case 10: // CE-CM
			letraVO.setLetraInicial("AL");
			letraVO.setLetraFinal("AO");
			break;
		case 11: // CN-CV
			letraVO.setLetraInicial("AP");
			letraVO.setLetraFinal("AS");
			break;
		case 12: // CW-DE
			letraVO.setLetraInicial("AT");
			letraVO.setLetraFinal("AW");
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * @param workbook
	 * @param lCategoria
	 * @param lProducto
	 * @throws IOException
	 */
	public void generarArchivoExcelVariacion(HSSFWorkbook workbook, List<VariacionSellOutVO> lCategoria,
			Map<String, List<VariacionSellOutVO>> lProducto) {

		HSSFSheet sheet = workbook.createSheet();
		int[] filaActual = new int[1];
		filaActual[0] = 1;
		this.generarTitulosEncabezadosColumnasVariacion(filaActual, workbook, sheet);
		this.exportarDatosVariacion(filaActual, workbook, sheet, lCategoria, lProducto);
	}

	/**
	 * 
	 * @param filaActual
	 * @param workbook
	 * @param sheet
	 */
	private void generarTitulosEncabezadosColumnasVariacion(int[] filaActual, HSSFWorkbook workbook, HSSFSheet sheet) {
		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		HSSFRow row1 = sheet.createRow(filaActual[0]);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 0, "Categoria/Producto", null,
				10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 1, "Porcentaje Variacion", null,
				10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 2, "Unidades de Venta", null,
				10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 3,
				"Porcentaje Variacion Modificada", null, 10);
		excel.crearCeldaTitulosoEtiquetasColumna(workbook, sheet, row1, filaActual[0], 4,
				"Unidades de Venta Modificada", null, 10);
		this.autoRedimensionarColumnas(sheet);
		filaActual[0]++;
	}

	/**
	 * 
	 * @param filaActual
	 * @param workbook
	 * @param sheet
	 * @param lCategoria
	 * @param lProductos
	 */
	private void exportarDatosVariacion(int[] filaActual, HSSFWorkbook workbook, HSSFSheet sheet,
			List<VariacionSellOutVO> lCategoria, Map<String, List<VariacionSellOutVO>> lProductos) {
		ExportadorAMSExcel excel = new ExportadorAMSExcel(workbook);
		for (VariacionSellOutVO categoria : lCategoria) {
			String valor = categoria.getCategoriaProducto();
			HSSFRow row1 = sheet.createRow(filaActual[0]);
			filaActual[0]++;
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 0, valor.replaceFirst("0_", ""), null, false,
					TEXTO, true);
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 1,
					categoria.getPorcentajeBase() != null ? String.valueOf(categoria.getPorcentajeBase())
							: String.valueOf(0),
					null, false, NUMERICO, true);
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 2,
					categoria.getSellOutBase() != null ? String.valueOf(categoria.getSellOutBase()) : String.valueOf(0),
					null, false, NUMERICO, true);
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 3,
					categoria.getPorcentajeModificado() != null ? String.valueOf(categoria.getPorcentajeModificado())
							: String.valueOf(0),
					null, false, NUMERICO, true);
			excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 4,
					categoria.getSellOut() != null ? String.valueOf(categoria.getSellOut()) : String.valueOf(0), null,
					false, NUMERICO, true);
			for (VariacionSellOutVO producto : lProductos.get(categoria.getCategoria())) {
				row1 = sheet.createRow(filaActual[0]);
				filaActual[0]++;
				excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 0, producto.getCategoriaProducto(), null,
						false, TEXTO, false);
				excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 1,
						producto.getPorcentajeBase() != null ? String.valueOf(producto.getPorcentajeBase())
								: String.valueOf(0),
						null, false, NUMERICO, false);
				excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 2,
						producto.getSellOutBase() != null ? String.valueOf(producto.getSellOutBase())
								: String.valueOf(0),
						null, false, NUMERICO, false);
				excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 3,
						producto.getPorcentajeModificado() != null ? String.valueOf(producto.getPorcentajeModificado())
								: String.valueOf(0),
						null, false, NUMERICO, false);
				excel.crearCeldaDatos(workbook, sheet, row1, filaActual[0], 4,
						producto.getSellOut() != null ? String.valueOf(producto.getSellOut()) : String.valueOf(0), null,
						false, NUMERICO, false);
			}
		}
	}
}
