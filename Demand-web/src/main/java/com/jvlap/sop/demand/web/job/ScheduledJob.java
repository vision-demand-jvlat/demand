package com.jvlap.sop.demand.web.job;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.demand.logica.ModuloGeneralEJB;
import com.jvlap.sop.demand.logica.ScheduledProcessesEJB;
import com.jvlap.sop.demand.logica.SemanasRequeridasEJB;
import com.jvlap.sop.demand.logica.VariacionEJB;
import com.jvlap.sop.demand.logica.utilitarios.FuncionesUtilitariasDemand;
import com.jvlap.sop.demand.logica.vos.SemanasRequeridasVO;
import com.jvlap.sop.demand.logica.vos.VariacionSellOutVO;
import com.jvlap.sop.demand.web.constantes.ScheduledProcessConst;
import com.jvlap.sop.demand.web.model.ProcessDTO;
import com.jvlap.sop.demand.web.model.ScheduleDTO;
import com.jvlat.sop.entidadessop.entidades.Usuario;

@Stateless
public class ScheduledJob {

	private Logger logger = LogManager.getLogger(ScheduledJob.class.getName());

	@EJB
	private ModuloGeneralEJB moduloGeneralEJB;

	@EJB
	private VariacionEJB variacionEJB;

	@EJB
	private SemanasRequeridasEJB semanasEJB;

	@EJB
	private ScheduledProcessesEJB scheduledProcessesEJB;

	/**
	 * Metodo para la ejecucion del job programado
	 * 
	 * @param proceso
	 */
	@Asynchronous
	public void ejecutarSchedule(ScheduleDTO scheduleDTO) {
		logger.info("[ejecutarSchedule] Inicio proceso: {}", scheduleDTO.getCode());

		// Se actualizar el estado a todos los procesos
		actualizarEstadoProcesos(scheduleDTO.getCode(), ScheduledProcessConst.ESTADO_EN_PROCESO);
		boolean isCanceled = false;
		List<String> resultados = new ArrayList<>();
		for (ProcessDTO proceso : scheduleDTO.getProceso()) {
			try {
				logger.info("[ejecutarSchedule] ejecutar proceso: {}/{} ", scheduleDTO.getCode(), proceso);

				List<String> inGrupoMarca = scheduleDTO.getListMarcas();
				List<String> inListRetailer = scheduleDTO.getListRetailer();
				String resultado = ejecutarProceso(proceso, inGrupoMarca, scheduleDTO.getCompanias(), inListRetailer,
						scheduleDTO.getMes(), isCanceled);

				proceso.setEstado(resultado);

				if (resultado.equals(ScheduledProcessConst.SIN_DATOS)) {
					resultados.add(proceso.toString().concat(" -> ").concat(
							"No se encontraron resultados para la consulta, por favor verifique que se haya realizado el proceso de Variation Sellout para esos filtros."));
				} else {
					resultados.add(proceso.toString());
				}
				scheduledProcessesEJB.actualizarEstadoProcesoXId(proceso.getId(), resultado);

			} catch (Exception e) {
				String msgError = "Hubo un error realizando el proceso S:" + scheduleDTO.getCode() + "/P:" + proceso;
				logger.error(msgError, e);
				isCanceled = true;
			}
		}
		//notificar(scheduleDTO, resultados);

		logger.info("[ejecutarSchedule] Fin proceso: {}", scheduleDTO.getCode());
	}

	/**
	 * 
	 * @param inProceso
	 * @param inGrupoMarca
	 * @param inCompania
	 * @param inListRetailer
	 * @param inMes
	 * @param isCanceled
	 * @param inUsuario
	 * @return
	 * @throws Exception
	 */
	private String ejecutarProceso(ProcessDTO inProceso, List<String> inGrupoMarca, String inCompania,
			List<String> inListRetailer, Long inMes, boolean isCanceled) {
		try {
			if (isCanceled) {
				return ScheduledProcessConst.ESTADO_CANCELADO;
			} else {
				scheduledProcessesEJB.actualizarEstadoProcesoXId(inProceso.getId(),
						ScheduledProcessConst.ESTADO_EJECUTANDO);

				String out = "";
				switch (inProceso.getName()) {
				case ScheduledProcessConst.PROCESO_1:
					out = ejecutarProceso1(inGrupoMarca, inCompania, inListRetailer, inMes);
					break;
				case ScheduledProcessConst.PROCESO_2:
					out = ejecutarProceso2(inGrupoMarca, inCompania, inListRetailer, inMes);
					break;
				default:
					out = ScheduledProcessConst.ESTADO_INDEFINIDO;
				}
				return out;
			}
		} catch (Exception e) {
			String msgError = "[ejecutarSchedule] Hubo un error ejecutando el proceso :" + inProceso.getId();
			logger.error(msgError, e);
			return ScheduledProcessConst.ESTADO_ERROR;
		}
	}

	/**
	 * 
	 * @param procesoId
	 * @param inGrupoMarca
	 * @param inCompania
	 * @param inListRetailer
	 * @param inMes
	 */
	private String ejecutarProceso1(List<String> inGrupoMarca, String inCompania, List<String> inListRetailer,
			Long inMes) {

		Map<String, List<VariacionSellOutVO>> prodCategoria = new HashMap<>();
		variacionEJB.procesarVariacion(variacionEJB.getMarcasByIDGrupoMarca(inGrupoMarca), inMes, inListRetailer,
				inCompania, inGrupoMarca, prodCategoria);

		return ScheduledProcessConst.ESTADO_TERMINADO;
	}

	/**
	 * 
	 * @param procesoId
	 * @param inGrupoMarca
	 * @param inCompania
	 * @param inListRetailer
	 * @param inMes
	 * @param inUsuario
	 * @throws Exception
	 */
	private String ejecutarProceso2(List<String> inGrupoMarca, String inCompania, List<String> inListRetailer,
			Long inMes) {

		if (validarDatosVariationSellout(inGrupoMarca, inListRetailer, inMes)) {

			String marcasGrupo = moduloGeneralEJB.getMarcasByIDGrupoMarca(inGrupoMarca);
			List<String> listMarcas = moduloGeneralEJB.getMarcaGrupoPorIdGrupoMarca(inGrupoMarca);

			Map<String, List<SemanasRequeridasVO>> prodCategoria = new LinkedHashMap<>();
			for (String retailer : inListRetailer) {
				List<SemanasRequeridasVO> productos = new ArrayList<>();
				semanasEJB.procesarSemanasRequeridas(inMes, retailer, marcasGrupo, inCompania, listMarcas,
						prodCategoria, productos);
			}

			semanasEJB.guardarSemanasRequeridas(moduloGeneralEJB.buscaCompaniaPorId(inCompania), inMes, inListRetailer,
					null, inGrupoMarca);

			return ScheduledProcessConst.ESTADO_TERMINADO;
		} else {
			return ScheduledProcessConst.SIN_DATOS;
		}
	}

	/**
	 * Metodo para actualizar todos los procesos asociado al codigo
	 * 
	 * @param inCodigo
	 * @param inEstado
	 */
	private void actualizarEstadoProcesos(String inCodigo, String inEstado) {
		try {
			scheduledProcessesEJB.actualizarEstadoProcesoXCode(inCodigo, inEstado);
		} catch (Exception e) {
			String msgError = "Hubo un error actualizando Codigo:" + inCodigo + " Estado:" + inEstado;
			logger.error(msgError, e);
		}
	}

	/**
	 * 
	 * @param inGrupoMarca
	 * @param inListRetailer
	 * @param inMes
	 * @return
	 */
	private boolean validarDatosVariationSellout(List<String> inGrupoMarca, List<String> inListRetailer, Long inMes) {
		String marcasGrupo = variacionEJB.getMarcasByIDGrupoMarca(inGrupoMarca);
		List<VariacionSellOutVO> productos = variacionEJB.consultarVariacion(inMes, inListRetailer, marcasGrupo, false,
				null);

		return !productos.isEmpty();
	}

	/**
	 * 
	 * @param inScheduleDTO
	 * @param resultados
	 */
	private void notificar(ScheduleDTO inScheduleDTO, List<String> resultados) {
		try {

			Usuario usuario = moduloGeneralEJB.buscaUsuarioPorUserName(inScheduleDTO.getUsuario());
			if (usuario == null) {
				logger.info("[execute] No se existe una forma para notificar al usuario del proceso  {}",
						inScheduleDTO.getUsuario());
			}
			List<String> datosServidor = new ArrayList<>();
			datosServidor = moduloGeneralEJB.leeDatosServidorCorreo(datosServidor);

			String asunto = ("Proceso Terminado [").concat(inScheduleDTO.getCode()).concat("]");
			String msg = ("Se ha completado el proceso automatizado ").concat(inScheduleDTO.getCode()).concat(" :");
			for (String r : resultados) {
				msg = msg.concat("<br>").concat(r);
			}
			if (usuario != null) {
				FuncionesUtilitariasDemand funcionesUtilitariasDemand = new FuncionesUtilitariasDemand();
				funcionesUtilitariasDemand.enviarCorreo(Arrays.asList(usuario), datosServidor, asunto, msg);
			}

		} catch (Exception e) {
			String msgError = "[ejecutarSchedule] Hubo un error enviando el correo del proceso: "
					+ inScheduleDTO.getCode();
			logger.error(msgError, e);
		}
	}

}