package com.jvlap.sop.demand.web.job;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.demand.logica.ScheduledProcessesEJB;
import com.jvlap.sop.demand.web.constantes.ScheduledProcessConst;
import com.jvlap.sop.demand.web.model.ScheduleDTO;
import com.jvlap.sop.demand.web.utilitarios.ScheduledProcessesUtil;

@Startup
@Singleton
public class ScheduledProcessesJob {

	private Logger logger = LogManager.getLogger(ScheduledProcessesJob.class.getName());

	@EJB
	ScheduledProcessesEJB scheduledProcessesEJB;

	@EJB
	private ScheduledJob asyncEjb;

	@Lock(LockType.READ)
	@Schedule(hour = "*", minute = "*", second = "*/50", info = "Se ejecuta cada 50 segundo")
	public void execute() {
		try {
			// Se consulta los procesos programados
			List<ScheduleDTO> listSchedule = obtenerProcesosProgramados();

			logger.info("[execute] Procesos a ejecutar : {} ", listSchedule.size());
			if (!listSchedule.isEmpty()) {

				for (ScheduleDTO scheduleDTO : listSchedule) {
					asyncEjb.ejecutarSchedule(scheduleDTO);
				}
			}
		} catch (Exception e) {
			logger.error("[execute] Hubo un error ejecutando los procesos", e);
		}

	}

	/**
	 * Metodo para consulta los procesos en estado programados
	 * 
	 * @return
	 */
	private List<ScheduleDTO> obtenerProcesosProgramados() {
		Map<String, List> inParametros = new HashMap<>();
		inParametros.put("ESTADO", Arrays.asList(ScheduledProcessConst.ESTADO_PROGRAMADO));
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		inParametros.put("FECHA_EJECUCION-P", Arrays.asList(df.format(new Date())));
		return ScheduledProcessesUtil.scheduleVoToDTO(scheduledProcessesEJB.consultar(inParametros));
	}

}