
package com.jvlap.sop.demand.web.lazymodel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.jvlap.sop.demand.logica.vos.ProductosForecastMesVO;



@SuppressWarnings("serial")
public class ForecastLazyModel extends LazyDataModel<ProductosForecastMesVO> {
    
    private final List<ProductosForecastMesVO> datasource;

    public ForecastLazyModel (List<ProductosForecastMesVO> datasource ){
        this.datasource = datasource ;

    }        

    @Override
    public ProductosForecastMesVO getRowData(String rowKey) {
        
      for (ProductosForecastMesVO car : datasource) {
          if(car.getIdProducto().equals(rowKey))
            return car;
        }
         return null; 
    }    
   
    @Override
    public Object getRowKey(ProductosForecastMesVO car) {
        
        return car.getIdProducto();
    }
    
    @Override
    public List<ProductosForecastMesVO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
    
        List<ProductosForecastMesVO> data = new ArrayList<>();
        
        //filter
        for(ProductosForecastMesVO car : datasource) {
            boolean match = true;
 
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        
                        Field miembroPrivado =  car.getClass().getDeclaredField(filterProperty);
                        miembroPrivado.setAccessible(true);
                        String fieldValue = String.valueOf(miembroPrivado.get(car));
                        
                        //String fieldValue = String.valueOf(car.getClass().getField(filterProperty).get(car));
                        System.out.println("FilterProperty: "+ filterProperty + " FilterValue "+ filterValue + " fieldValue "+ fieldValue);
                                                                        
                        if(filterValue == null || fieldValue.toUpperCase().startsWith(filterValue.toString().toUpperCase())) {
                            System.out.println("Match True!  "+ fieldValue);
                            match = true;
                        }
                        else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        System.out.println("Error al filtrar Lazy Model "+ e.getMessage());
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(car);
            }
        }
        
        int dataSize =data.size();
        this.setRowCount(dataSize);
    
        if(dataSize > pageSize && first < dataSize){
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
                 
        } else {
            return data;
        }    
    }  
}
