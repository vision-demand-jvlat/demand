
package com.jvlap.sop.demand.web.lazymodel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.jvlap.sop.demand.web.model.ScheduleDTO;

@SuppressWarnings("serial")
public class ScheduledLazyModel extends LazyDataModel<ScheduleDTO> {

	private final List<ScheduleDTO> datasource;

	public ScheduledLazyModel(List<ScheduleDTO> datasource) {
		this.datasource = datasource;
	}

	@Override
	public ScheduleDTO getRowData(String rowKey) {
		for (ScheduleDTO car : datasource) {
			if (car.getCode().equals(rowKey))
				return car;
		}
		return null;
	}

	@Override
	public Object getRowKey(ScheduleDTO car) {
		return car.getCode();
	}

	@Override
	public List<ScheduleDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {

		List<ScheduleDTO> data = new ArrayList<>();

		// filter
		for (ScheduleDTO car : datasource) {
			boolean match = true;

			if (filters != null) {
				for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {

					try {
						String filterProperty = it.next();
						Object filterValue = filters.get(filterProperty);

						Field miembroPrivado = car.getClass().getDeclaredField(filterProperty);
						miembroPrivado.setAccessible(true);
						String fieldValue = String.valueOf(miembroPrivado.get(car));

						System.out.println("FilterProperty: " + filterProperty + " FilterValue " + filterValue
								+ " fieldValue " + fieldValue);

						if (filterValue == null
								|| fieldValue.toUpperCase().startsWith(filterValue.toString().toUpperCase())) {
							System.out.println("Match True!  " + fieldValue);
							match = true;
						} else {
							match = false;
							break;
						}
					} catch (Exception e) {
						System.out.println("Error al filtrar Lazy Model " + e.getMessage());
						match = false;
					}
				}
			}

			if (match) {
				data.add(car);
			}
		}

		int dataSize = data.size();
		this.setRowCount(dataSize);

		if (dataSize > pageSize && first < dataSize) {
			try {
				return data.subList(first, first + pageSize);
			} catch (IndexOutOfBoundsException e) {
				return data.subList(first, first + (dataSize % pageSize));
			}
		} else {
			return data;
		}
	}
}
