package com.jvlap.sop.demand.web.lazymodel;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.jvlap.sop.demand.logica.vos.VariacionSellOutVO;

@SuppressWarnings("serial")
public class VariacionLazyModel extends LazyDataModel<VariacionSellOutVO> {

	private final List<VariacionSellOutVO> datasource;
	private boolean isProducto;

	public VariacionLazyModel(List<VariacionSellOutVO> datasource, boolean isProducto) {
		this.datasource = datasource;
		this.isProducto = isProducto;
	}

	@Override
	public VariacionSellOutVO getRowData(String rowKey) {
		try {
			for (VariacionSellOutVO car : datasource) {
				if (isProducto) {
					if (car.getVarProd().toString().equals(rowKey))
						return car;
				} else {
					if (car.getVarCateg().toString().equals(rowKey))
						return car;
				}
			}
			return null;
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public Object getRowKey(VariacionSellOutVO car) {
		try {
			if (isProducto)
				return car.getVarProd().toString();
			else
				return car.getVarCateg().toString();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<VariacionSellOutVO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {

		List<VariacionSellOutVO> data = datasource;
		if (data != null) {
			int dataSize = data.size();
			this.setRowCount(dataSize);

			if (dataSize > pageSize && first < dataSize) {
				try {
					return data.subList(first, first + pageSize);
				} catch (IndexOutOfBoundsException e) {
					return data.subList(first, first + (dataSize % pageSize));
				}
			} else {
				return data;
			}
		} else {
			return data;
		}
	}
}
