package com.jvlap.sop.demand.web.model;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

public class CheckIP {
	 static private final Logger LOGGER = Logger.getLogger("mx.com.hash.checkip.CheckIP");

	   public String obtenerIP() throws UnknownHostException {
	        InetAddress ip = InetAddress.getLoopbackAddress();
	        return ip.getHostAddress();
	    }
}
