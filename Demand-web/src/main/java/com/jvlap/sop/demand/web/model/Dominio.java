package com.jvlap.sop.demand.web.model;

public class Dominio {
	private Long id;
	private String ldapDomain;

	/**
	 * 
	 */
	public Dominio() {
		super();
	}

	/**
	 * 
	 * @param id
	 * @param ldapDomain
	 */
	public Dominio(Long id, String ldapDomain) {
		super();
		this.id = id;
		this.ldapDomain = ldapDomain;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getLdapDomain() {
		return ldapDomain;
	}

	/**
	 * 
	 * @param ldapDomain
	 */
	public void setLdapDomain(String ldapDomain) {
		this.ldapDomain = ldapDomain;
	}

}
