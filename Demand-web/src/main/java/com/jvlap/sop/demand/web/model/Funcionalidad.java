package com.jvlap.sop.demand.web.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Funcionalidad implements Serializable, Comparable<Funcionalidad> {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String nombre;

	private String descripcion;

	private String etiqueta;

	private Integer orden;

	private String icono;

	private String url;

	private Funcionalidad padre;

	private List<Permiso> permisos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getURL() {
		return url;
	}

	public void setURL(String uRL) {
		url = uRL;
	}

	public Funcionalidad getPadre() {
		return padre;
	}

	public void setPadre(Funcionalidad padre) {
		this.padre = padre;
	}

	public List<Permiso> getPermisos() {
		return permisos;
	}

	public void setPermisos(List<Permiso> permisos) {
		this.permisos = permisos;
	}

	public void addPermiso(Permiso inPermiso) {
		if (this.permisos == null) {
			this.permisos = new ArrayList<>();
		}
		this.permisos.add(inPermiso);

	}

	@Override
	public int compareTo(Funcionalidad o) {
		if (this.orden < o.getOrden()) {
			return -1;
		} else if (this.orden > o.getOrden()) {
			return 1;
		}
		return 0;
	}
}
