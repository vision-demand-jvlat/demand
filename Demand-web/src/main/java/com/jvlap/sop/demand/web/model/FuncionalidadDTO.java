package com.jvlap.sop.demand.web.model;

public class FuncionalidadDTO {
	
	private Integer id;
	private FuncionalidadDTO padre;
    private String nombre;
    private String icono;
    private String url;
    
    
    
    
    
	
    public FuncionalidadDTO(Integer id, FuncionalidadDTO padre, String nombre, String icono, String url) {
    	this.id = id;
		this.padre = padre;
		this.nombre = nombre;
		this.icono = icono;
		this.url = url;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public FuncionalidadDTO getPadre() {
		return padre;
	}
	public void setPadre(FuncionalidadDTO padre) {
		this.padre = padre;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getIcono() {
		return icono;
	}
	public void setIcono(String icono) {
		this.icono = icono;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
