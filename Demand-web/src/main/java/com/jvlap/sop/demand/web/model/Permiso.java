package com.jvlap.sop.demand.web.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Permiso implements Serializable {
	
	private String nombre;
	private String valor;

	public Permiso(String nombre, String valor) {
		super();
		this.nombre = nombre;
		this.valor = valor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "Permiso [nombre=" + nombre + ", valor=" + valor + "]";
	}
}
