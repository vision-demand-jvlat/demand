package com.jvlap.sop.demand.web.model;

import com.jvlap.sop.demand.web.constantes.ScheduledProcessConst;

public class ProcessDTO {

	private Long id;
	private String name;
	private String estado;
	private int orden;

	// ---------------------- Constructores

	public ProcessDTO(Long id, String name, String estado, int orden) {
		super();
		this.id = id;
		this.name = name;
		this.estado = estado;
		this.orden = orden;
	}

	public ProcessDTO() {
		super();
	}

	// ------------------SET y GET

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		String descripcion = "";
		if (name.trim().equals(ScheduledProcessConst.PROCESO_1)) {
			descripcion = "Variacion Sell Out";
		}
		if (name.trim().equals(ScheduledProcessConst.PROCESO_2)) {
			descripcion = "Semanas Requeridas";
		}
		return orden + "-" + descripcion + " ( " + estado + " ) ";
	}

}
