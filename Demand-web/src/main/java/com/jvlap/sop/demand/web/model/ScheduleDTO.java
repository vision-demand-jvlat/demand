package com.jvlap.sop.demand.web.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jvlap.sop.demand.web.constantes.ScheduledProcessConst;

public class ScheduleDTO implements Comparable<ScheduleDTO> {
	private String code;
	private List<String> listMarcas;
	private String companias;
	private List<String> listRetailer;
	private Long mes;
	private Date fechaEjecucion;
	private String usuario;
	private Date fechaCreacion;
	private List<ProcessDTO> proceso;

	// Valores Selecionados
	private boolean processVariacionSellOut = false;
	private boolean processSemanasRequeridas = false;

	// Constructores
	public ScheduleDTO() {
		super();
		listMarcas = new ArrayList<>();
		listRetailer = new ArrayList<>();
		fechaCreacion = new Date((new Date()).getTime() + (1000 * 60 * 60 * 24));
		processVariacionSellOut = false;
		processSemanasRequeridas = false;
		proceso = new ArrayList<>();
	}

	// SET y GET

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<String> getListMarcas() {
		return listMarcas;
	}

	public void setListMarcas(List<String> listMarcas) {
		this.listMarcas = listMarcas;
	}

	public String getCompanias() {
		return companias;
	}

	public void setCompanias(String companias) {
		this.companias = companias;
	}

	public List<String> getListRetailer() {
		return listRetailer;
	}

	public void setListRetailer(List<String> listRetailer) {
		this.listRetailer = listRetailer;
	}

	public Long getMes() {
		return mes;
	}

	public void setMes(Long mes) {
		this.mes = mes;
	}

	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public boolean isProcessVariacionSellOut() {
		return processVariacionSellOut;
	}

	public void setProcessVariacionSellOut(boolean processVariacionSellOut) {
		this.processVariacionSellOut = processVariacionSellOut;
	}

	public boolean isProcessSemanasRequeridas() {
		return processSemanasRequeridas;
	}

	public void setProcessSemanasRequeridas(boolean processSemanasRequeridas) {
		this.processSemanasRequeridas = processSemanasRequeridas;
	}

	/**
	 * 
	 * @return
	 */
	public List<ProcessDTO> getProceso() {
		return proceso;
	}

	/**
	 * 
	 * @return
	 */
	public List<ProcessDTO> getProcesoTemp() {
		List<ProcessDTO> procesoTemp = new ArrayList<>();

		if (processVariacionSellOut) {
			ProcessDTO p = obtenerProceso(ScheduledProcessConst.PROCESO_1);
			procesoTemp.add(new ProcessDTO(p == null ? null : p.getId(), ScheduledProcessConst.PROCESO_1,
					ScheduledProcessConst.ESTADO_PROGRAMADO, 0));
		}

		if (processSemanasRequeridas) {
			ProcessDTO p = obtenerProceso(ScheduledProcessConst.PROCESO_2);
			procesoTemp.add(new ProcessDTO(p == null ? null : p.getId(), ScheduledProcessConst.PROCESO_2,
					ScheduledProcessConst.ESTADO_PROGRAMADO, 1));
		}

		return procesoTemp;
	}

	/**
	 * 
	 * @param inNombreProces
	 * @return
	 */
	private ProcessDTO obtenerProceso(String inNombreProces) {
		ProcessDTO out = null;
		if (proceso != null) {
			for (ProcessDTO p : proceso) {
				if (p.getName().equals(inNombreProces)) {
					out = p;
				}
			}
		}
		return out;
	}

	public void setProceso(List<ProcessDTO> proceso) {
		this.proceso = proceso;
	}

	@Override
	public String toString() {
		return "ScheduleDTO [code=" + code + ", listMarcas=" + String.join(",", listMarcas) + ", companias=" + companias
				+ ", listRetailer=" + String.join(",", listRetailer) + ", mes=" + mes + ", fechaEjecucion="
				+ fechaEjecucion + ", usuario=" + usuario + ", fechaCreacion=" + fechaCreacion
				+ ", processVariacionSellOut=" + processVariacionSellOut + ", processSemanasRequeridas="
				+ processSemanasRequeridas + "]";
	}

	@Override
	public int compareTo(ScheduleDTO o) {
		if (getCode() == null || o.getCode() == null) {
			return 0;
		}
		return getCode().compareTo(o.getCode());
	}

	/**
	 * 
	 * @param inProcessDTO
	 */
	public void adicionarProceso(ProcessDTO inProcessDTO) {
		if (inProcessDTO.getName().equals(ScheduledProcessConst.PROCESO_1)) {
			this.processVariacionSellOut = true;
		}

		if (inProcessDTO.getName().equals(ScheduledProcessConst.PROCESO_2)) {
			this.processSemanasRequeridas = true;
		}
		this.proceso.add(inProcessDTO);

	}
}
