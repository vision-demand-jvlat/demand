package com.jvlap.sop.demand.web.model;

public class Sesion {
	private Integer id;
	private String fecha_ult;
	private int idEstado;
	private int idusuario;
	private String token;
	private String ip;

	public Sesion(Integer id, String fecha_ult, int idEstado, int idusuario, String token, String ip) {
		this.id = id;
		this.fecha_ult = fecha_ult;
		this.idEstado = idEstado;
		this.idusuario = idusuario;
		this.token = token;
		this.ip = ip;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFecha_ult() {
		return fecha_ult;
	}

	public void setFecha_ult(String fecha_ult) {
		this.fecha_ult = fecha_ult;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

	public int getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(int idusuario) {
		this.idusuario = idusuario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
