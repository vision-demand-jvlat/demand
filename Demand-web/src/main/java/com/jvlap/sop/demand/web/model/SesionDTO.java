package com.jvlap.sop.demand.web.model;

import java.util.List;

import com.jvlat.sop.entidadessop.entidades.Usuario;

public class SesionDTO {

	private Usuario usuario;
	private Sesion sesion;
	private List<Funcionalidad> funcionalidades;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Sesion getSesion() {
		return sesion;
	}

	public void setSesion(Sesion sesion) {
		this.sesion = sesion;
	}

	public List<Funcionalidad> getFuncionalidades() {
		return funcionalidades;
	}

	public void setFuncionalidades(List<Funcionalidad> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

}