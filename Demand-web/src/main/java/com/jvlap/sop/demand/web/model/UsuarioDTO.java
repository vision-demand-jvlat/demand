package com.jvlap.sop.demand.web.model;

public class UsuarioDTO {
	private Integer id;
    private String nombre;
    private String user_Name;
    
    
	public UsuarioDTO(Integer id, String nombre, String user_Name) {
		this.id = id;
		this.nombre = nombre;
		this.user_Name = user_Name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUser_Name() {
		return user_Name;
	}
	public void setUser_Name(String user_Name) {
		this.user_Name = user_Name;
	}
    
    
}
