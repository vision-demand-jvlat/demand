/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.utilitarios;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 *
 * @author Manuel Cortes Granados
 * @since 
 */
public class ExportadorAMSExcel {

    CellStyle estilo_etiquetas = null;
    CellStyle estilo_valores = null;
    HSSFFont txtFont = null;
    
    public ExportadorAMSExcel(HSSFWorkbook workbook) {
        estilo_etiquetas = workbook.createCellStyle();        
        estilo_valores = workbook.createCellStyle();        
        txtFont = (HSSFFont) workbook.createFont();
    }
    
    /**
     * @author Manuel Cortes Granados
     * @param row
     * @since Abril 18 2017 10:05
     * @param workbook
     * @param sheet
     * @param fila
     * @param columna
     * @param texto
     * @param rangoCeldasCombinadas 
     */
    public void crearCeldaTitulosoEtiquetasColumna(HSSFWorkbook workbook,
                                                                    HSSFSheet sheet,
                                                                    HSSFRow row,
                                                                    int fila,
                                                                    int columna,
                                                                    String texto,
                                                                    String rangoCeldasCombinadas,
                                                                    int tamanoFuente){
        if (row==null)
            row = sheet.createRow(fila);
        
        if (rangoCeldasCombinadas!=null){
            sheet.addMergedRegion(CellRangeAddress.valueOf(rangoCeldasCombinadas));
        }
        
        HSSFCell cell = row.createCell(columna);
        estilo_etiquetas = workbook.createCellStyle();
        estilo_etiquetas.setAlignment(HorizontalAlignment.CENTER);
        txtFont = (HSSFFont) workbook.createFont();
        txtFont.setFontName("Arial");
        txtFont.setFontHeightInPoints((short) tamanoFuente);
        txtFont.setBold(true);
        estilo_etiquetas.setFont(txtFont);
        cell.setCellStyle(estilo_etiquetas);
        cell.setCellValue(texto);
      
    }
    
    /**
     * @author Manuel Cortes Granados
     * @param tipoDatoCelda
     * @param negrita
     * @param centrado
     * @since 18 Abril 2017 10:51
     * @param workbook
     * @param sheet
     * @param row
     * @param fila
     * @param columna
     * @param texto
     * @param rangoCeldasCombinadas 
     */
    
    public void crearCeldaDatos(HSSFWorkbook workbook,
                                                                    HSSFSheet sheet,
                                                                    HSSFRow row,
                                                                    int fila,
                                                                    int columna,
                                                                    String texto,
                                                                    String rangoCeldasCombinadas,
                                                                    boolean centrado,
                                                                    int tipoDatoCelda,
                                                                    boolean negrita){
        if (row==null)
            row = sheet.createRow(fila);
        
        HSSFCell cell = row.createCell(columna);
        if (centrado)
            estilo_valores.setAlignment(HorizontalAlignment.CENTER);
        else
            estilo_valores.setAlignment(HorizontalAlignment.LEFT);
        
        txtFont.setFontName("Arial");
        txtFont.setFontHeightInPoints((short) 10);
        txtFont.setBold(negrita);
        estilo_valores.setFont(txtFont);
        estilo_valores.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
        cell.setCellStyle(estilo_valores);
                
        if (tipoDatoCelda==2){
            cell.setCellValue(Double.parseDouble(texto));
            cell.setCellType(CellType.NUMERIC);
        }
        else if (tipoDatoCelda==1){
            cell.setCellValue(texto);
            cell.setCellType(CellType.STRING);
        }
       
        if (rangoCeldasCombinadas!=null){
            sheet.addMergedRegion(CellRangeAddress.valueOf(rangoCeldasCombinadas));
        }
    }
    
}
