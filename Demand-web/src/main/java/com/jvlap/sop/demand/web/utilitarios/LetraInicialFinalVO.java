/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.utilitarios;

/**
 *
 * @author Manuel Cortes Granados
 * @since 5 Mayo 2017 3:17 PM
 */
public class LetraInicialFinalVO {
    private String letraInicial;
    private String letraFinal;

    public LetraInicialFinalVO() {
    }
    
    

    public LetraInicialFinalVO(String letraInicial, String letraFinal) {
        this.letraInicial = letraInicial;
        this.letraFinal = letraFinal;
    }

    public String getLetraInicial() {
        return letraInicial;
    }

    public void setLetraInicial(String letraInicial) {
        this.letraInicial = letraInicial;
    }

    public String getLetraFinal() {
        return letraFinal;
    }

    public void setLetraFinal(String letraFinal) {
        this.letraFinal = letraFinal;
    }
    
    
    
    
}
