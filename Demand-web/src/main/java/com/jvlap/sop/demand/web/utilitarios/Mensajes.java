/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.utilitarios;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;

import com.jvlap.sop.commonsop.excepciones.ExcepcionSOP;

/**
 *
 * @author iviasus
 */
public class Mensajes {

	public static String getMensaje(String resourceBundleKey) {

		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "msg");
			return bundle.getString(resourceBundleKey);
		} catch (Exception e) {

		}
		return "N/E";
	}

	public static void mostrarMensajeWarny(String mensaje) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, mensaje, "");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	/**
	 * Presenta en pantalla el mensaje
	 *
	 * @param mensaje
	 */
	public static void mostrarMensajeInfo(String mensaje) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, mensaje, "");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public static void mostrarMensajeError(String mensaje) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, "");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	/**
	 * Presenta en pantalla el mensaje funcional y en el logger el error tecnico
	 *
	 * @param e
	 * @param clase
	 */
	public static void mostrarMensajeError(ExcepcionSOP e, Class clase) {
		// TODO En produccion unicamente se debe presentar el error funcional y el
		// tecnico al logger
		FacesMessage msg = null;
		if (e.getTipo() == 0) {
			// Requiere escribir en log, cuando esté en producción el msg unicamente debe
			// presentar el error funcional
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getErrorTecnico(), "");
			LogManager.getLogger(clase.getName()).error(e.getErrorTecnico(), e);

		} else {
			msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getErrorFuncional(), "");
		}

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	/**
	 * Muestra mensaje de excepciones no manejadas, escribe en el logger parte del
	 * stack trace del error
	 *
	 * @param clase
	 * @autor Ivan Viasus
	 * @param e Excepcion generada
	 */
	public static void mostrarMensajeErrorNoManejado(Throwable e, Class clase) {
		String mensaje = "Error no manejado, consulte con el administrador : " + e.getMessage() + ". ";
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, mensaje, "");
		FacesContext.getCurrentInstance().addMessage(null, msg);

		LogManager.getLogger(clase.getName()).error(mensaje + "  ****   " + ExcepcionSOP.getStackTrace(e), e);
	}

}
