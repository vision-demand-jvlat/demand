package com.jvlap.sop.demand.web.utilitarios;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jvlap.sop.demand.logica.vos.ScheduleVO;
import com.jvlap.sop.demand.web.constantes.ScheduledProcessConst;
import com.jvlap.sop.demand.web.model.ProcessDTO;
import com.jvlap.sop.demand.web.model.ScheduleDTO;

public class ScheduledProcessesUtil {

	private static Logger logger = LogManager.getLogger(ScheduledProcessesUtil.class.getName());

	private ScheduledProcessesUtil() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * 
	 * @param inListScheduleVO
	 * @return
	 */
	public static List<ScheduleDTO> scheduleVoToDTO(List<ScheduleVO> inListScheduleVO) {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Map<String, ScheduleDTO> temp = new HashMap<>();
		for (ScheduleVO t : inListScheduleVO) {

			ScheduleDTO aux = temp.get(t.getCode());
			if (aux == null) {
				ScheduleDTO scheduleDTO = new ScheduleDTO();

				try {
					scheduleDTO.setFechaCreacion(dateFormat.parse(t.getFechaCreacion()));
					scheduleDTO.setFechaEjecucion(dateFormat.parse(t.getFechaEjecucion()));
				} catch (ParseException e) {
					logger.error("[ScheduleVoToDTO] Error obteniendo la fecha");
				}

				scheduleDTO.setCode(t.getCode());
				scheduleDTO.setCompanias(t.getCompanias());

				if (t.getMarcas() != null) {
					scheduleDTO.setListMarcas(Arrays.asList(t.getMarcas().split(",")));
				}

				if (t.getRetailers() != null) {
					scheduleDTO.setListRetailer(Arrays.asList(t.getRetailers().split(",")));
				}

				scheduleDTO.setMes(t.getMes());
				scheduleDTO.setUsuario(t.getUsuario());

				temp.put(t.getCode(), scheduleDTO);
			}
			temp.get(t.getCode())
					.adicionarProceso(new ProcessDTO(t.getId(), t.getProceso(), t.getEstado(), t.getOrden()));

		}
		return new ArrayList<>(temp.values());

	}

	/**
	 * 
	 * @param inScheduleDTO
	 * @return
	 */
	public static List<ScheduleVO> scheduleDTOtoVOCancelado(ScheduleDTO inScheduleDTO) {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		List<ScheduleVO> out = new ArrayList<>();

		boolean isPermitido = true;

		if (inScheduleDTO != null) {
			for (ProcessDTO p : inScheduleDTO.getProceso()) {
				isPermitido = isPermitido && !(p.getEstado().equals(ScheduledProcessConst.ESTADO_EJECUTANDO));
				if (p.getEstado().equals(ScheduledProcessConst.ESTADO_EN_PROCESO)
						|| p.getEstado().equals(ScheduledProcessConst.ESTADO_PROGRAMADO)) {
					ScheduleVO vo = new ScheduleVO(p.getId(), inScheduleDTO.getCode(),
							String.join(",", inScheduleDTO.getListMarcas()), inScheduleDTO.getCompanias(),
							String.join(",", inScheduleDTO.getListRetailer()), inScheduleDTO.getMes(),
							dateFormat.format(inScheduleDTO.getFechaEjecucion()),
							ScheduledProcessConst.ESTADO_CANCELADO, inScheduleDTO.getUsuario(), p.getName(),
							p.getOrden());
					out.add(vo);
				}
			}
		}
		return isPermitido ? out : new ArrayList<ScheduleVO>();
	}

	/**
	 * 
	 * @param inUsuario
	 * @param inScheduleDTO
	 * @return
	 */
	public static List<ScheduleVO> scheduleDTOtoVO(String inUsuario, ScheduleDTO inScheduleDTO) {
		if (inScheduleDTO != null) {
			if (inScheduleDTO.getCode() == null || inScheduleDTO.getCode().equals("")) {
				return scheduleDTOtoVOInicial(inUsuario, inScheduleDTO);
			} else {
				boolean isPermitido = true;

				boolean isClonar = false;
				boolean isEditar = false;

				for (ProcessDTO p : inScheduleDTO.getProceso()) {

					isPermitido = isPermitido && !(p.getEstado().equals(ScheduledProcessConst.ESTADO_EJECUTANDO)
							|| p.getEstado().equals(ScheduledProcessConst.ESTADO_EN_PROCESO));

					isClonar = isClonar || (p.getEstado().equals(ScheduledProcessConst.ESTADO_CANCELADO)
							|| p.getEstado().equals(ScheduledProcessConst.ESTADO_TERMINADO)
							|| p.getEstado().equals(ScheduledProcessConst.ESTADO_ERROR));

					isEditar = isEditar || (p.getEstado().equals(ScheduledProcessConst.ESTADO_PROGRAMADO)
							|| p.getEstado().equals(ScheduledProcessConst.ESTADO_INDEFINIDO)
							|| p.getEstado().equals(ScheduledProcessConst.SIN_DATOS));
				}

				if (isPermitido) {
					if (isEditar) {
						DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
						List<ScheduleVO> out = new ArrayList<>();

						for (ProcessDTO p : inScheduleDTO.getProcesoTemp()) {
							out.add(new ScheduleVO(p.getId(), inScheduleDTO.getCode(),
									String.join(",", inScheduleDTO.getListMarcas()), inScheduleDTO.getCompanias(),
									String.join(",", inScheduleDTO.getListRetailer()), inScheduleDTO.getMes(),
									dateFormat.format(inScheduleDTO.getFechaEjecucion()),
									ScheduledProcessConst.ESTADO_PROGRAMADO, inUsuario, p.getName(), p.getOrden()));
						}
						return out;

					} else {
						if (isClonar) {
							return scheduleDTOtoVOInicial(inUsuario, inScheduleDTO);
						}
					}
				} else {
					return new ArrayList<>();
				}
			}
		}
		return new ArrayList<>();
	}

	/**
	 * 
	 * @param inUsuario
	 * @param inScheduleDTO
	 * @return
	 */
	private static List<ScheduleVO> scheduleDTOtoVOInicial(String inUsuario, ScheduleDTO inScheduleDTO) {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		List<ScheduleVO> out = new ArrayList<>();

		inScheduleDTO.setCode(String.valueOf(System.currentTimeMillis()));

		if (inScheduleDTO.isProcessVariacionSellOut()) {
			ScheduleVO vo = new ScheduleVO(null, inScheduleDTO.getCode(),
					String.join(",", inScheduleDTO.getListMarcas()), inScheduleDTO.getCompanias(),
					String.join(",", inScheduleDTO.getListRetailer()), inScheduleDTO.getMes(),
					dateFormat.format(inScheduleDTO.getFechaEjecucion()), ScheduledProcessConst.ESTADO_PROGRAMADO,
					inUsuario, ScheduledProcessConst.PROCESO_1, 0);
			out.add(vo);
		}

		if (inScheduleDTO.isProcessSemanasRequeridas()) {
			ScheduleVO vo = new ScheduleVO(null, inScheduleDTO.getCode(),
					String.join(",", inScheduleDTO.getListMarcas()), inScheduleDTO.getCompanias(),
					String.join(",", inScheduleDTO.getListRetailer()), inScheduleDTO.getMes(),
					dateFormat.format(inScheduleDTO.getFechaEjecucion()), ScheduledProcessConst.ESTADO_PROGRAMADO,
					inUsuario, ScheduledProcessConst.PROCESO_2, 1);
			out.add(vo);
		}

		return out;
	}

}
