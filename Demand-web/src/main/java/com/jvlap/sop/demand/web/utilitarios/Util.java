/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.demand.web.utilitarios;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;

import com.jvlap.sop.demand.web.model.Funcionalidad;
import com.jvlap.sop.demand.web.model.Permiso;
import com.jvlat.sop.entidadessop.entidades.Retailer;

/**
 *
 * @author Roberto Osorio
 */
public class Util {
	private static final Logger log = LogManager.getLogger(Util.class.getName());

	public static HttpSession getSession() {
		return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	public static void leerAtributos(HttpSession sesion) {

	}

	public static DefaultMenuModel configuraMenu(String urlNueva, List<Funcionalidad> funcionalidad) {

		DefaultMenuModel model = new DefaultMenuModel();
		try {
			DefaultSubMenu submenu;
			List<Funcionalidad> funcionalidadTM = funcionalidad;
			for (Funcionalidad funcio : funcionalidad) {
				if (funcio.getPadre() == null) {
					submenu = new DefaultSubMenu(funcio.getNombre(), funcio.getIcono());
					DefaultMenuItem item;
					for (Funcionalidad funcion : funcionalidadTM) {
						if (funcion.getPadre() != null && funcion.getPadre().getId().compareTo(funcio.getId()) == 0) {
							item = new DefaultMenuItem(funcion.getNombre());
							item.setUrl(urlNueva + "/" + funcion.getURL());
							submenu.addElement(item);
						}
					}
					submenu.setStyle("height:auto");
					model.addElement(submenu);
				}
			}
		} catch (Exception e) {
			log.error("[configuraMenu] Hubo un error construyendo el menu:", e);
		}
		return model;
	}

	public static String getUserName() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		return session.getAttribute("username").toString();
	}

	public static String getUserId() {
		HttpSession session = getSession();
		if (session != null) {
			return (String) session.getAttribute("userid");
		} else {
			return null;
		}
	}

	public static Map<String, Retailer> sortByComparator(Map<String, Retailer> unsortMap, final boolean order) {

		List<Map.Entry<String, Retailer>> list = new LinkedList<Map.Entry<String, Retailer>>(unsortMap.entrySet());

		// Sorting the list based on values
		Collections.sort(list, new Comparator<Map.Entry<String, Retailer>>() {
			public int compare(Map.Entry<String, Retailer> o1, Map.Entry<String, Retailer> o2) {
				if (order) {
					return o1.getValue().getNombreComercial().compareTo(o2.getValue().getNombreComercial());
				} else {
					return o2.getValue().getNombreComercial().compareTo(o1.getValue().getNombreComercial());

				}
			}
		});

		// Maintaining insertion order with the help of LinkedList
		Map<String, Retailer> sortedMap = new LinkedHashMap<String, Retailer>();
		for (Map.Entry<String, Retailer> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	/**
	 * 
	 * @param funcionalidad
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, String> obtenerPermisosXFuncionalidad(String funcionalidad) {
		Map<String, String> permisos = new HashMap<>();
		List<Permiso> listPermiso = (List<Permiso>) getSession().getAttribute(funcionalidad);

		if (listPermiso != null && !listPermiso.isEmpty()) {
			for (Permiso p : listPermiso) {
				permisos.put(p.getNombre(), p.getValor());
			}
		}
		return permisos;
	}

}
