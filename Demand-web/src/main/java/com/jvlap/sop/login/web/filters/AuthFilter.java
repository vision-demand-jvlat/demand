/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jvlap.sop.login.web.filters;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.jvlap.sop.demand.web.model.Funcionalidad;
import com.jvlap.sop.demand.web.model.SesionDTO;
import com.jvlap.sop.demand.web.utilitarios.Util;

/**
 *
 * @author DITECH
 */
@WebFilter(filterName = "AuthFilter", urlPatterns = { "*.xhtml" })
public class AuthFilter implements Filter {

	private static final Logger log = LogManager.getLogger(AuthFilter.class.getName());

	public AuthFilter() {
		super();
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		log.info("[init] Inicio");

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;
			res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			res.setDateHeader("Expires", 0); // Proxies.

			String reqURI = req.getRequestURI();
			HttpSession ses = obtenerSesion(req);

			if (reqURI.contains("/login.xhtml") || true || reqURI.contains("/public/")
					|| reqURI.contains("javax.faces.resource")) {
				chain.doFilter(request, response);
			} else {
				res.sendRedirect(req.getContextPath() + "/faces/login.xhtml"); // Anonymous user. Redirect to login page
			}

		} catch (Exception e) {
			log.error("[doFilter] Hubo un error realizando el filtro", e);
		}
	}

	@Override
	public void destroy() {
		log.info("[destroy] Eliminacion");

	}

	/**
	 * 
	 * @param req
	 * @return
	 */
	private HttpSession obtenerSesion(HttpServletRequest req) {
		HttpSession ses = req.getSession(true);
		ses.setAttribute("username", null);
		ses.setAttribute("urllogin", null);
		ses.setAttribute("usrLogueadoId", null);
		ses.setAttribute("usrLogueadoNom", null);
		ses.setAttribute("usrLogueadoUserNom", null);
		Util.leerAtributos(ses);
		String ipAddress = req.getHeader("X-FORWARDED-FOR");

		if (ipAddress == null) {
			ipAddress = req.getRemoteAddr();
		}
		Gson gson = new Gson();
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			String urlServicio = ("http://localhost:8080/autorizacion-0.0.1-SNAPSHOT/auth/id?ip=").concat(ipAddress);
			HttpResponse response = httpclient.execute(new HttpGet(urlServicio));
			if (response.getStatusLine().getStatusCode() != 200) {
				log.error("[obtenerSesion] Failed : HTTP code : {}", response.getStatusLine().getStatusCode());

			} else {
				String result = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);

				SesionDTO session = gson.fromJson(result.replace("user_Name", "userName"), SesionDTO.class);
				if (session != null && session.getUsuario() != null && session.getUsuario().getId() != null) {

					List<Funcionalidad> funcionalidadesXUsuario = session.getFuncionalidades();

					ses.setAttribute("username", null);
					ses.setAttribute("urllogin", null);
					ses.setAttribute("usrLogueadoId", null);
					ses.setAttribute("usrLogueadoNom", null);
					ses.setAttribute("usrLogueadoUserNom", null);
					ses.setAttribute("username", session.getUsuario().getUserName());

					log.info(ses.getAttribute("username"));

					String url = req.getRequestURL().toString();
					String uri = req.getRequestURI();

					ses.setAttribute("model",
							Util.configuraMenu(url.replaceAll(uri, ""), session.getFuncionalidades()));
					ses.setAttribute("urllogin", url.replaceAll(req.getRequestURI(), ""));
					ses.setAttribute("usrLogueadoId", Long.parseLong(String.valueOf(session.getUsuario().getId())));
					ses.setAttribute("usrLogueadoNom", String.valueOf(session.getUsuario().getNombre()));
					ses.setAttribute("usrLogueadoUserNom", String.valueOf(session.getUsuario().getUserName()));

					for (Funcionalidad f : funcionalidadesXUsuario) {
						if (f.getPermisos() != null && !f.getPermisos().isEmpty()) {
							ses.setAttribute(f.getNombre(), f.getPermisos());
						}
					}

				}

			}
		} catch (Exception e) {
			log.error("[obtenerSesion] Error inicializando la sesion", e);
		}
		Util.leerAtributos(ses);
		return ses;
	}
}
