package com.jvlap.sop.web.client;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.jvlap.sop.demand.web.controladores.HttpDeleteWithBody;
import com.jvlap.sop.demand.web.model.Dominio;
import com.jvlap.sop.demand.web.model.SesionDTO;

public class AuthenticateClient {

	private static final Logger log = LogManager.getLogger(AuthenticateClient.class.getName());

	public SesionDTO autenticar(String nombreUsuario, String clave, String ip, Long inDominioId) {
		SesionDTO out = new SesionDTO();
		try {
			JSONObject json = new JSONObject();

			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost getRequest = new HttpPost("http://localhost:8080/authenticate/api/authentication/v1.0/login");

			// Request parameters and other properties.
			json.put("nombreUsuario", nombreUsuario);
			json.put("clave", clave);
			json.put("ip", ip);
			JSONObject jsonD = new JSONObject();
			jsonD.put("id", inDominioId);
			json.put("dominio", jsonD);
			StringEntity se = new StringEntity(json.toString(), "UTF-8");
			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

			getRequest.setEntity(se);

			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.error("[autenticar] Failed : HTTP error code : {}", response.getStatusLine().getStatusCode());
				return out;
			}

			String result = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
			Gson gson = new Gson();

			return gson.fromJson(result, SesionDTO.class);

		} catch (Exception e) {
			log.error("[autenticar] Hubo un error", e);
		}
		return out;
	}

	public boolean cerrarSesion(String nombreUsuario) {
		try {
			JSONObject json = new JSONObject();

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpDeleteWithBody getRequest = new HttpDeleteWithBody(
					"http://localhost:8080/authenticate/api/authentication/v1.0/login");

			json.put("nombreUsuario", nombreUsuario);
			json.put("clave", nombreUsuario);
			json.put("ip", nombreUsuario);

			StringEntity se = new StringEntity(json.toString(), "UTF-8");
			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

			getRequest.setEntity(se);

			HttpResponse response = httpclient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.error("[cerrarSesion] Failed : HTTP error code : {}", response.getStatusLine().getStatusCode());
				return false;
			} else {
				return true;
			}

		} catch (Exception e) {
			log.error("[cerrarSesion] Hubo un error", e);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Dominio> getDominio() {
		List<Dominio> out = new ArrayList<>();
		try {

			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet("http://localhost:8080/authenticate/api/resources/1.0/domains");

			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				log.error("[getDominio] Failed : HTTP error code : {}", response.getStatusLine().getStatusCode());
				return out;
			}

			String result = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
			Gson gson = new Gson();

			List<Dominio> fromJson = gson.fromJson(result, List.class);
			return fromJson;

		} catch (Exception e) {
			log.error("[getDominio] Hubo un error", e);
		}
		return out;
	}
}
