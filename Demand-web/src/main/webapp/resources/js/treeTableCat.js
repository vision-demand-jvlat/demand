
var treetable_rowstate = new Array();
var treetable_callbacks = new Array();
var flag_ToogleForecastRetailer = 1;

function treetable_hideRow(rowId) {
  el = document.getElementsByClassName(rowId)[0];
  el.style.display = "none";
}

function treetable_showRow(rowId) {
  el = document.getElementsByClassName(rowId)[0];
  el.style.display = "";
}

function treetable_hasChildren(rowId) {
  res = document.getElementsByClassName(rowId + '_0')[0];
  return (res != null);
}

function treetable_getRowChildren(rowId) {
  
  var arr = new Array();
  i = 0;
  while (true) {
    childRowId = rowId + '_' + i;
    childEl = document.getElementsByClassName(childRowId)[0];
    if (childEl) {
      arr[i] = childRowId;
    } else {
      break;
    }
    i++;
  }
  return (arr);
}

function treetable_toggleRow(rowId, state, force, aux) {
    var rowChildren;
    var i;
    var aux_2;

    if (aux == null) {
        aux_2 = 0;
        state = flag_ToogleForecastRetailer;
    }

    // open or close all children rows depend on current state
    force = (force == null) ? 1 : force;
    if (state == null) {
        row_state = ((treetable_rowstate[rowId]) ? (treetable_rowstate[rowId]) : 1) * -1;
    } else {
        row_state = state;
    }
    rowChildren = treetable_getRowChildren(rowId);
    if (rowChildren.length == 0) {
        return (false);
    }
    for (i = 0; i < rowChildren.length; i++) {
        if (row_state == -1) {
            treetable_hideRow(rowChildren[i]);
            treetable_toggleRow(rowChildren[i], row_state, -1, aux_2);
        } else {
            if (force == 1 || treetable_rowstate[rowId] != -1) {
                treetable_showRow(rowChildren[i]);
                treetable_toggleRow(rowChildren[i], row_state, -1, aux_2);
            }
        }
    }
    if (force == 1) {
        treetable_rowstate[rowId] = row_state;
        treetable_fireEventRowStateChanged(rowId, row_state);
    }
    if (aux == null) {
        if (flag_ToogleForecastRetailer == 1) {
            flag_ToogleForecastRetailer = -1;
        } else if (flag_ToogleForecastRetailer = -1)
            flag_ToogleForecastRetailer = 1;
    }
    return (true);
}

function treetable_toggleRow2(rowId, state, force) {
  var rowChildren;
  var i;
  
  // open or close all children rows depend on current state
  force = (force == null) ? 1 : force; 
  if (state == null) {
    row_state = ((treetable_rowstate[rowId]) ? (treetable_rowstate[rowId]) : 1) * -1;
  } else {
        row_state = state;
  }
  rowChildren = treetable_getRowChildren(rowId);
  if (rowChildren.length == 0){
        alert('4');
        return (false);
  }
  for (i = 0; i < rowChildren.length; i++) {
    if (row_state == -1) {
      treetable_hideRow(rowChildren[i]);
      treetable_toggleRow(rowChildren[i], row_state, -1);
    } else {
      if (force == 1 || treetable_rowstate[rowId] != -1) {
        treetable_showRow(rowChildren[i]);
        treetable_toggleRow(rowChildren[i], row_state, -1);
      }
    }
  }
  if (force == 1) {
    treetable_rowstate[rowId] = row_state;
    treetable_fireEventRowStateChanged(rowId, row_state);
  }
  return (true);
}

function treetable_fireEventRowStateChanged(rowId, state) {
  if (treetable_callbacks['eventRowStateChanged']) {
    callback = treetable_callbacks['eventRowStateChanged'] + "('" + rowId + "', " + state + ");";
    eval(callback);
  }
}

function treetable_collapseAll(tableId) {
  table = document.getElementsByClassName(tableId)[0];
  rowChildren = table.getElementsByTagName('tr');  
  for (i = 0; i < rowChildren.length; i++) {
    if (index = rowChildren[i].className.indexOf('_')) {        
      // do not hide root elements
      if(index != rowChildren[i].className.lastIndexOf('_')) {          
        rowChildren[i].style.display = 'none';
      }
      if (treetable_hasChildren(rowChildren[i].id)) {          
        treetable_rowstate[rowChildren[i].id] = -1;
        treetable_fireEventRowStateChanged(rowChildren[i].id, -1);
      }
    }
  }  
  return (true);
}    

function treetable_expandAll(tableId) {
  table = document.getElementsByClassName(tableId)[0];
  rowChildren = table.getElementsByTagName('tr');
  for (i = 0; i < rowChildren.length; i++) {
    if (index = rowChildren[i].id.indexOf('_')) {
      rowChildren[i].style.display = '';
      if (treetable_hasChildren(rowChildren[i].id)) {
        treetable_rowstate[rowChildren[i].id] = 1;
        treetable_fireEventRowStateChanged(rowChildren[i].id, 1);
      }
    }
  }
  return (true);
}


